import {DarkTheme, DefaultTheme} from '@react-navigation/native';

export const CustomDarkTheme = {
  ...DarkTheme,
  colors: {
    ...DarkTheme.colors,
    card: '#1C1C1E',
    background: '#0F0F0F',
  },
};

export const APP_THEMES = {
  light: DefaultTheme,
  dark: CustomDarkTheme,
};
