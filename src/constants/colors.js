// --------- General ---------
export const PASTEL_RED = '#ED4545';
export const DARK_RED = 'rgb(217, 35, 35)';

export const GRAY_1 = 'rgb(224, 224, 232)';
export const GRAY_2 = '#c7c7cc';
export const GRAY_3 = 'rgb(184, 190, 206)';
export const GRAY_4 = '#8E8E93';
export const GRAY_5 = '#5C5959';
export const GRAY_6 = 'rgb(74, 74, 74)';

export const NAVY = '#3C4560';

// --------- Campus ---------
export const YELLOW = '#FEC938'; // College Ave
export const PURPLE = '#8D3092'; // Busch
export const BLUE = '#006FBA'; // Livingston
export const GREEN = '#65BE6B'; // Cook/Douglass

// --------- Dining ---------
export const LIGHT_GREEN = 'rgb(41, 225, 139)';
