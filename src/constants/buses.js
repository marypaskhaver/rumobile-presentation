export const CAMPUS_IDs_TO_GEOGRAPHIC_AREAS_MAP = {
  // New Brunswick
  1: '40.382690%2C-74.595626%7C40.625639%2C-74.280317',

  // Newark
  2: '40.841884%2C-74.011088%7C40.660668%2C-74.277053',
};

export const CAMPUS_IDs_TO_AGENCY_IDs_MAP = {
  // Rutgers
  1: 1323,

  // Newark
  2: 1323,

  // Camden
  3: 1233,
};

export const RECEIVE_BUS_ROUTES = 'RECEIVE_BUS_ROUTES';
export const RECEIVE_BUS_STOPS = 'RECEIVE_BUS_STOPS';
export const RECEIVE_BUS_PREDICTIONS = 'RECEIVE_BUS_PREDICTIONS';
export const RECEIVE_VEHICLES = 'RECEIVE_VEHICLES';

export const RECEIVE_USER_LOCATION = 'RECEIVE_USER_LOCATION';
export const SORT_FAVORITE_BUSES = 'SORT_FAVORITE_BUSES';

export const ADD_FAVORITE_BUS_STOP_ID_WITH_ROUTE_ID =
  'ADD_FAVORITE_BUS_STOP_ID_WITH_ROUTE_ID';
export const REMOVE_FAVORITE_BUS_STOP_ID_WITH_ROUTE_ID =
  'REMOVE_FAVORITE_BUS_STOP_ID_WITH_ROUTE_ID';

export const busTagToDisplayNameMap = {
  W1: 'Weekend 1',
  'Route Weekend 1': 'Weekend 1',
  W2: 'Weekend 2',
  'Route Weekend 2': 'Weekend 2',
  'W1-Night': 'ON Weekend 1',
  'W2-Night': 'ON Weekend 2',
  ALL: 'All Campuses',
  S1: 'NB Shuttle 1',
  S2: 'NB Shuttle 2',
  SUMM1: 'Summer 1',
  SUMM2: 'Summer 2',
  'Penn St': 'Penn Station',
  Connect: 'Campus Connect',
  'Penn St E': 'Penn Station E',
  'Connect E': 'Campus Connect E',
  CR: 'Camden Rising',
  CL: 'Courthouse Loop',
  RCS: 'Camden Shuttle',
};
