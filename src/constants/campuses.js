export const CAMPUS_IDs_TO_NAMES = {
  1: {id: 1, name: 'New Brunswick'},
  2: {id: 2, name: 'Newark'},
  3: {id: 3, name: 'Camden'},
};

export const ALL_CAMPUSES = {
  NB: 'New Brunswick',
  NK: 'Newark',
  CM: 'Camden',
  B: 'Burlington County Community College - Mt Laurel',
  CC: 'Camden County College - Blackwood Campus',
  H: 'County College of Morris',
  CU: 'Cumberland County College',
  MC: 'Denville - RU-Morris',
  WM: 'Freehold WMHEC - RU-BCC',
  L: 'Lincroft - RU-BCC',
  AC: 'Mays Landing - RU-ACCC',
  J: 'McGuire-Dix-Lakehurst RU-JBMDL',
  D: 'Mercer County Community College',
  RV: 'North Branch - RU-RVCC',
};
