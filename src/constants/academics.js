export const RECEIVE_CLASS_CATEGORIES = 'RECEIVE_CLASS_CATEGORIES';

export const ADD_CAMPUS = 'ADD_CAMPUS';
export const REMOVE_CAMPUS = 'REMOVE_CAMPUS';
export const SET_CAMPUSES = 'SET_CAMPUSES';

export const ADD_LEVEL = 'ADD_LEVEL';
export const REMOVE_LEVEL = 'REMOVE_LEVEL';
export const SET_LEVELS = 'SET_LEVELS';

export const UPDATE_SEMESTER = 'UPDATE_SEMESTER';

export const ADD_FAVORITE_SECTION = 'ADD_FAVORITE_SECTION';
export const REMOVE_FAVORITE_SECTION = 'REMOVE_FAVORITE_SECTION';
export const EDIT_SECTION = 'EDIT_SECTION';

export const START_CLASS_CATEGORIES_FETCH = 'START_CLASS_CATEGORIES_FETCH';
export const COMPLETE_CLASS_CATEGORIES_FETCH =
  'COMPLETE_CLASS_CATEGORIES_FETCH';
export const FAIL_CLASS_CATEGORIES_FETCH = 'FAIL_CLASS_CATEGORIES_FETCH';

export const abbreviationToWeekdayMap = {
  M: 'Monday',
  T: 'Tuesday',
  W: 'Wednesday',
  TH: 'Thursday',
  F: 'Friday',
  S: 'Saturday',
  U: 'Sunday',
  null: 'Independent Study',
};
