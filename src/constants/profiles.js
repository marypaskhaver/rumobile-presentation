import {PASTEL_RED} from './colors';

export default profiles = {
  TARO: {
    image: require('../assets/images/message_of_the_day/taro_kuma.jpeg'),
    backgroundImage: require('../assets/images/profile-screens/gray_rectangle.png'),
    name: 'Taro Kumakabe',
    role: 'Mastermind',
    quote: {
      quotationMarksColor: PASTEL_RED,
      text: "I'm a bear and I love to eat, build apps, and sleep ʕ•ᴥ•ʔ",
    },
    sections: [
      {
        type: 'Badges',
        contents: [
          {
            image: require('../assets/images/profile-screens/computer.png'),
            title: 'Coding',
            subtitle: 'Advanced',
            level: 5,
          },
          {
            image: require('../assets/images/profile-screens/paletteAndBrush.png'),
            title: 'Artist',
            subtitle: 'Intermediate',
            level: 3,
          },
        ],
      },
      {
        type: 'Contact Info',
        contents: [
          {
            image: require('../assets/images/profile-screens/mail.png'),
            contactInfo: 'rutgersmobileapp@gmail.com',
            url: 'mailto:rutgersmobileapp@gmail.com',
          },
          {
            image: require('../assets/images/profile-screens/facebook.png'),
            contactInfo: 'RUMobile Bear',
            url: 'https://www.facebook.com/people/RUMobile-Bear/100015577364990/',
          },
        ],
      },
    ],
  },
  ALEX: {
    image: require('../assets/images/profile-screens/watson.png'),
    backgroundImage: require('../assets/images/profile-screens/autumn.jpg'),
    name: 'Alex',
    role: 'Developer',
    quote: {
      quotationMarksColor: 'rgb(215, 86, 62)',
      text: "Oh, I don't know, you know, don't you know?",
    },
    sections: [
      {
        type: 'Badges',
        title: 'Major',
        contents: [
          {
            image: require('../assets/images/profile-screens/computer.png'),
            title: 'Computer Science',
            subtitle: 'Sophomore',
            level: 2,
          },
        ],
      },
      {
        type: 'Hobbies',
        contents: [
          {
            hobbyInfo: 'Reading biographies',
          },
          {
            hobbyInfo: 'Comedy',
          },
        ],
      },
    ],
  },
  JC: {
    image: require('../assets/images/profile-screens/jc.png'),
    backgroundImage: require('../assets/images/profile-screens/blue_ny.png'),
    name: 'Juan Carlos',
    role: 'Time Traveler',
    quote: {
      quotationMarksColor: 'rgb(64, 143, 247)',
      text: 'Dreams do come true, you just have to work for them ʕ•ᴥ•ʔ',
    },
    sections: [
      {
        type: 'Badges',
        title: 'Major',
        contents: [
          {
            image: require('../assets/images/profile-screens/computer.png'),
            title: 'Computer Engineer',
            subtitle: 'Alumni',
            level: 5,
          },
        ],
      },
      {
        type: 'Badges',
        contents: [
          {
            image: require('../assets/images/profile-screens/color_wheel.png'),
            title: 'Designer',
            subtitle: 'God Mode',
            level: 6,
            isColored: true,
          },
          {
            image: require('../assets/images/profile-screens/code.png'),
            title: 'Coding',
            subtitle: 'Intermediate',
            level: 2,
          },
          {
            image: require('../assets/images/profile-screens/piano.png'),
            title: 'Piano',
            subtitle: 'Intermediate',
            level: 2,
          },
        ],
      },
    ],
  },
  MARY: {
    image: require('../assets/images/profile-screens/mary.png'),
    backgroundImage: require('../assets/images/profile-screens/purple_capitol.png'),
    name: 'Mary',
    role: 'Developer',
    quote: {
      quotationMarksColor: 'rgb(119, 58, 175)',
      text: 'If you put your mind to it, you can accomplish anything!',
    },
    sections: [
      {
        type: 'Badges',
        title: 'Major',
        contents: [
          {
            image: require('../assets/images/profile-screens/computer.png'),
            title: 'Computer Science',
            subtitle: 'Sophomore',
            level: 2,
          },
        ],
      },
      {
        type: 'Hobbies',
        contents: [
          {
            hobbyInfo: 'Writing',
          },
          {
            hobbyInfo: 'History',
          },
          {
            image: require('../assets/images/profile-screens/computer.png'),
            title: 'Computer Science',
            subtitle: 'Sophomore',
            level: 2,
          },
        ],
      },
    ],
  },
  XU: {
    image: require('../assets/images/profile-screens/bunny.jpg'),
    backgroundImage: require('../assets/images/profile-screens/green_background.jpg'),
    name: 'Xu',
    role: 'Developer',
    quote: {
      quotationMarksColor: 'rgb(170, 183, 122)',
      text: 'All children, except one, grow up.',
    },
    sections: [
      {
        type: 'Badges',
        contents: [
          {
            image: require('../assets/images/profile-screens/computer.png'),
            title: 'Coding',
            subtitle: 'Advanced',
            level: 5,
          },
          {
            image: require('../assets/images/profile-screens/paletteAndBrush.png'),
            title: 'Gaming',
            subtitle: 'Intermediate',
            level: 3,
          },
        ],
      },
      {
        type: 'Contact Info',
        contents: [
          {
            image: require('../assets/images/profile-screens/mail.png'),
            contactInfo: 'xuthebunno@gmail.com',
            url: 'mailto:xuthebunno@gmail.com',
          },
          {
            image: require('../assets/images/profile-screens/twitch.png'),
            contactInfo: 'xuthebunno',
            url: 'https://www.twitch.tv/xuthebunno',
          },
          {
            image: require('../assets/images/profile-screens/instagram.png'),
            contactInfo: 'ellas_cook_book',
            url: 'https://www.instagram.com/ellas_cook_book/',
          },
        ],
      },
      {
        type: 'Hobbies',
        contents: [
          {
            hobbyInfo: 'Coding',
          },
          {
            hobbyInfo: 'Gaming',
          },
          {
            hobbyInfo: 'Cooking',
          },
          {
            hobbyInfo: 'Daydreaming',
          },
        ],
      },
    ],
  },
};
