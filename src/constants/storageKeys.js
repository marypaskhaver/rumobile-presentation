// Keys to store items with in Async Storage

export const FAVORITE_SECTIONS_STORAGE_KEY = '@favorite_sections_storage_key';
export const FAVORITE_BUS_STOPS_STORAGE_KEY = '@favorite_bus_stops_storage_key';

// If user wants favorite buses to be sorted by distance
export const SORT_FAVORITE_BUSES_STORAGE_KEY =
  '@sort_favorite_buses_storage_key';

// If user disabled automatic dark mode: App changes themes based on user's selected theme in the app
export const OVERRIDE_THEME_CHOICE_STORAGE_KEY =
  '@override_theme_choice_storage_key';

// User's chosen Rutgers campus (New Brunswick, Newark, or Camden)
export const CAMPUS_STORAGE_KEY = '@campus_storage_key';

export const CLASSES_CATEGORIES_FILTER_CAMPUSES_STORAGE_KEY =
  '@classes_categories_filter_campuses_storage_key';
export const CLASSES_CATEGORIES_FILTER_SEMESTER_STORAGE_KEY =
  '@classes_categories_filter_semester_storage_key';
export const CLASSES_CATEGORIES_FILTER_EDUCATION_LEVEL_STORAGE_KEY =
  '@classes_categories_filter_education_level_storage_key';

// If user passed through welcome screens
export const PASSED_WELCOME_SCREENS_STORAGE_KEY =
  '@passed_welcome_screens_storage_key';
