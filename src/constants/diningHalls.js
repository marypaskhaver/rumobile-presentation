export const START_MENU_ITEMS_FETCH = 'START_MENU_ITEMS_FETCH';
export const COMPLETE_MENU_ITEMS_FETCH = 'COMPLETE_MENU_ITEMS_FETCH';
export const FAIL_MENU_ITEMS_FETCH = 'FAIL_MENU_ITEMS_FETCH';
export const RECEIVE_MENU_ITEMS = 'RECEIVE_MENU_ITEMS';

export const START_NUTRITION_ITEMS_FETCH = 'START_NUTRITION_ITEMS_FETCH';
export const COMPLETE_NUTRITION_ITEMS_FETCH = 'COMPLETE_NUTRITION_ITEMS_FETCH';
export const FAIL_NUTRITION_ITEMS_FETCH = 'FAIL_NUTRITION_ITEMS_FETCH';
export const RECEIVE_NUTRITION_ITEMS = 'RECEIVE_NUTRITION_ITEMS';

export const diningHallNamesToDisplayNames = {
  browerCommons: 'Brower',
  buschDiningHall: 'Busch',
  livingstonDiningCommons: 'Livingston',
  neilsonDiningHall: 'Neilson',
};

export const diningHallNamesToFormattedNames = {
  browerCommons: 'Brower Commons',
  buschDiningHall: 'Busch Dining Hall',
  livingstonDiningCommons: 'Livingston Dining Commons',
  neilsonDiningHall: 'Neilson Dining Hall',
};

export const diningHallNameToCampusName = {
  browerCommons: 'College Ave',
  buschDiningHall: 'Busch',
  livingstonDiningCommons: 'Livingston',
  neilsonDiningHall: 'Cook/Douglass',
};
