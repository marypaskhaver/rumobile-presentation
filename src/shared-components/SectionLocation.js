import React from 'react';
import {View, Text} from 'react-native';
import CampusIcon from './CampusIcon';
import {GRAY_4} from '../constants/colors';
import {SFProTextRegular} from '../constants/fonts';

export default SectionLocation = ({
  meetingTime,
  useEditedLocationIfExists = false,
}) => {
  const {buildingCode, roomNumber, editedLocation} = meetingTime;

  let buildingCodeAndRoomNumber =
    (buildingCode ?? '') +
    ' ' +
    (roomNumber ? `Room ${roomNumber}` : 'No room info');

  if (useEditedLocationIfExists && editedLocation !== undefined) {
    if (editedLocation.trim().length > 0)
      buildingCodeAndRoomNumber = editedLocation.trim();
    else buildingCodeAndRoomNumber = 'No room provided';
  }

  return (
    <View style={{flexDirection: 'row', alignItems: 'center'}}>
      <CampusIcon sectionMeetingTime={meetingTime} />
      <Text
        style={{
          fontFamily: SFProTextRegular,
          fontSize: 13,
          color: GRAY_4,
          marginLeft: 4,
        }}
      >
        {buildingCodeAndRoomNumber}
      </Text>
    </View>
  );
};
