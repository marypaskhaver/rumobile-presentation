import React, {useState} from 'react';
import {TouchableOpacity, Image} from 'react-native';

export default HeartButton = ({
  style,
  selected = false,
  onSelectAction,
  onDeselectAction,
}) => {
  const unfilledHeart = require('../assets/images/other/like.png');
  const filledHeart = require('../assets/images/other/likeFilled.png');

  const [isSelected, setSelected] = useState(selected);

  const handleOnPress = () => {
    if (isSelected) onDeselectAction();
    else onSelectAction();

    setSelected(!isSelected);
  };

  return (
    <TouchableOpacity onPress={handleOnPress}>
      <Image source={isSelected ? filledHeart : unfilledHeart} style={style} />
    </TouchableOpacity>
  );
};
