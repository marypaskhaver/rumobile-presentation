import React, {useEffect, useState} from 'react';
import {Text} from 'react-native';
import {DateFormatter, refreshAt} from '../utils/';

const DateText = ({style}) => {
  const [dateFormatter, setDateFormatter] = useState(new DateFormatter());
  const formattedDate = dateFormatter.getFormattedDate();

  useEffect(() => {
    refreshAt(0, 0, 0, () => {
      setDateFormatter(new DateFormatter());
    });
  }, []);

  return <Text style={style}>{formattedDate}</Text>;
};

export default DateText;
