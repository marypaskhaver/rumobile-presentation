import React from 'react';
import {TouchableOpacity, Image} from 'react-native';
import {useNavigation} from '@react-navigation/native';

export const BackVArrow = ({color, paddingTop = 0, onPressAction}) => {
  const navigation = useNavigation();

  const goBack = () => navigation.goBack();

  return (
    <TouchableOpacity
      style={{
        marginLeft: 'auto',
        width: 24,
        height: 24,
        paddingTop,
        paddingLeft: 12,
        paddingBottom: 28,
        paddingRight: 28,
      }}
      onPress={onPressAction ?? goBack}
    >
      <Image
        style={{
          tintColor: `${color ?? 'white'}`,
          height: 24,
          resizeMode: 'contain',
          transform: [{rotate: '90deg'}],
        }}
        source={require('../assets/icons/other/forward.png')}
      />
    </TouchableOpacity>
  );
};
