import {useTheme} from '@react-navigation/native';
import React from 'react';
import {ActivityIndicator, Text, View} from 'react-native';
import {SFProTextRegular} from '../constants/fonts';

export const LoadingSign = ({loadingText}) => {
  const {colors} = useTheme();

  return (
    <View style={{height: 120, alignItems: 'center', justifyContent: 'center'}}>
      <ActivityIndicator color="#b8b8b8" size="large" />
      {loadingText && (
        <Text
          style={{
            fontSize: 20,
            fontFamily: SFProTextRegular,
            color: colors.text,
            paddingTop: 20,
          }}
        >
          {loadingText}
        </Text>
      )}
    </View>
  );
};
