import React from 'react';
import {View, Text} from 'react-native';
import {busTagToDisplayNameMap} from '../constants/buses';

export default RouteTag = ({routeColorInfo}) => {
  return (
    <View
      key={routeColorInfo.rtag}
      style={{
        backgroundColor: routeColorInfo.rcolor,
        paddingHorizontal: 6,
        paddingVertical: 4,
        borderRadius: 6,
        marginRight: 8,
      }}
    >
      <Text
        style={{
          color: 'white',
          fontSize: 16,
        }}
      >
        {busTagToDisplayNameMap[routeColorInfo.rtag]
          ? busTagToDisplayNameMap[routeColorInfo.rtag]
          : routeColorInfo.rtag}
      </Text>
    </View>
  );
};
