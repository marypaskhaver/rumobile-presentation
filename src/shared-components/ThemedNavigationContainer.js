import React, {useEffect, useRef} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {updateCurrentPhoneTheme} from '../redux/actions/themeActions';
import {NavigationContainer} from '@react-navigation/native';
import {OVERRIDE_THEME_CHOICE_STORAGE_KEY} from '../constants/storageKeys';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  selectCurrentAppTheme,
  selectChosenOverrideThemeID,
} from '../redux/reducers/themesReducer';
import useColorScheme from 'react-native/Libraries/Utilities/useColorScheme';
import {getThemeFromScheme} from '../utils/';
import {AppState} from 'react-native';
import {
  requestBusPredictions,
  requestBusRoutes,
  requestBusStops,
  requestVehicles,
} from '../redux/actions/busesActions';
import {selectChosenCampusID} from '../redux/reducers/campusesReducer';
import {selectBusRoutes} from '../redux/reducers/busesReducer';
const {
  Storyboard,
  StoryboardTouchMode,
} = require('@glassbox/react-native-storyboard');

export default ThemedNavigationContainer = ({children}) => {
  const currentAppTheme = useSelector(selectCurrentAppTheme);
  const currentChosenOverrideThemeID = useSelector(selectChosenOverrideThemeID);

  useEffect(() => {
    // Save theme choices
    try {
      AsyncStorage.setItem(
        OVERRIDE_THEME_CHOICE_STORAGE_KEY,
        JSON.stringify(currentChosenOverrideThemeID),
      );
    } catch (error) {
      console.log(`Error saving data. ${error}`);
    }
  }, [currentAppTheme, currentChosenOverrideThemeID]);

  const scheme = useColorScheme();
  const currentPhoneTheme = getThemeFromScheme(scheme);

  // If auto dark mode is enabled, get theme from user's phone and update store
  const chosenOverrideThemeID = useSelector(selectChosenOverrideThemeID);
  updateCurrentPhoneTheme(currentPhoneTheme);

  const navigationRef = useRef();

  const appState = useRef(AppState.currentState);
  const dispatch = useDispatch();
  const chosenCampusID = useSelector(selectChosenCampusID);
  const busRoutes = useSelector(selectBusRoutes);

  useEffect(() => {
    const subscription = AppState.addEventListener('change', nextAppState => {
      if (
        appState.current.match(/inactive|background/) &&
        nextAppState === 'active'
      ) {
        dispatch(requestBusRoutes(chosenCampusID));
        dispatch(requestVehicles(chosenCampusID));
        dispatch(requestBusStops(chosenCampusID));

        dispatch(
          requestBusPredictions(busRoutes.map(busRoute => busRoute.route_id)),
        );
      }

      appState.current = nextAppState;
    });

    return () => {
      subscription.remove();
    };
  }, []);

  return (
    <NavigationContainer
      theme={chosenOverrideThemeID ? currentAppTheme : currentPhoneTheme}
      ref={navigationRef}
      onReady={() => Storyboard.trackNavigation(navigationRef)}
    >
      {children}
    </NavigationContainer>
  );
};
