import React, {useEffect, useState} from 'react';
import NetInfo from '@react-native-community/netinfo';
import LowToast from './LowToast';

const InternetToast = ({distanceFromBottom}) => {
  const [isConnected, setIsConnected] = useState(false);

  // Subscribe
  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener(state => {
      const {isConnected} = state;
      setIsConnected(isConnected);
    });

    // Unsubscribe
    unsubscribe();
  }, []);

  NetInfo.fetch().then(state => {
    const {isConnected} = state;
    setIsConnected(isConnected);
  });

  return (
    !isConnected && (
      <LowToast
        message={'No internet.'}
        persist={true}
        distanceFromBottom={distanceFromBottom}
      />
    )
  );
};

export default InternetToast;
