import React from 'react';
import {useSelector} from 'react-redux';
import {Text, View} from 'react-native';
import {useTheme} from '@react-navigation/native';

import {PASTEL_RED, GRAY_4} from '../constants/colors';

import {getMinutesBetweenDates} from '../utils';
import {
  SFProTextRegular,
  SFProDisplayBold,
  SFProTextSemibold,
} from '../constants/fonts';
import {Row} from './Row';
import {selectBusPredictions} from '../redux/reducers/busesReducer';

const StopPrediction = ({stop_id, route_id}) => {
  const {colors} = useTheme();

  const allBusPredictions = useSelector(selectBusPredictions);

  const predictionForThisRoute = allBusPredictions.find(
    prediction =>
      prediction.stop_id === stop_id &&
      prediction.arrivals.filter(arrival => arrival.route_id === route_id)
        .length > 0,
  );

  if (!predictionForThisRoute)
    return (
      <View style={{flexDirection: 'column', marginLeft: 'auto'}}>
        <Text
          style={{
            fontFamily: SFProDisplayBold,
            fontSize: 24,
            textAlign: 'right',
            color: colors.text,
          }}>
          --
        </Text>
        <Text
          style={{
            fontFamily: SFProTextRegular,
            color: GRAY_4,
            fontSize: 12,
            textAlign: 'right',
          }}>
          No predictions
        </Text>
      </View>
    );

  const filteredStopPrediction = {
    ...predictionForThisRoute,
    arrivals: predictionForThisRoute?.arrivals
      ? predictionForThisRoute?.arrivals.filter(
          arrival => arrival.route_id === route_id,
        )
      : [],
  };

  const busArrivalTimes = filteredStopPrediction.arrivals
    .map(arrival =>
      getMinutesBetweenDates(new Date(arrival.arrival_at), new Date()),
    )
    .filter(time => time > 0)
    .sort((time1, time2) => time1 - time2);

  return busArrivalTimes.length === 0 ? (
    <View style={{flexDirection: 'column', marginLeft: 'auto'}}>
      <Text
        style={{
          fontFamily: SFProDisplayBold,
          fontSize: 24,
          textAlign: 'right',
          color: colors.text,
        }}>
        --
      </Text>
      <Text
        style={{
          fontFamily: SFProTextRegular,
          color: GRAY_4,
          fontSize: 12,
          textAlign: 'right',
        }}>
        Invalid predictions
      </Text>
    </View>
  ) : (
    <View
      style={{
        flexDirection: 'column',
        marginLeft: 'auto',
      }}>
      <Text
        style={{
          fontFamily: SFProDisplayBold,
          fontSize: 24,
          textAlign: 'right',
          color: busArrivalTimes[0] < 1 ? PASTEL_RED : colors.text,
        }}>
        {busArrivalTimes[0] < 1 ? 'Now' : Math.round(busArrivalTimes[0])}
      </Text>

      <Row>
        {busArrivalTimes.slice(1).length > 0 && (
          <Text
            style={{
              fontFamily: SFProTextRegular,
              color: GRAY_4,
              fontSize: 12,
              textAlign: 'right',
            }}>
            Also in
          </Text>
        )}
        <Text
          style={{
            fontFamily: SFProTextSemibold,
            color: colors.text,
            fontSize: 12,
            textAlign: 'right',
          }}>
          {' '}
          {busArrivalTimes.length < 3
            ? busArrivalTimes
                .slice(1)
                .map(time => Math.round(time))
                .join(', ')
            : busArrivalTimes
                .slice(1, 3)
                .map(time => Math.round(time))
                .join(', ')}{' '}
        </Text>
        <Text
          style={{
            fontFamily: SFProTextRegular,
            color: GRAY_4,
            fontSize: 12,
            textAlign: 'right',
          }}>
          {busArrivalTimes.slice(1).length === 0 &&
          Math.round(busArrivalTimes[0]) === 1
            ? ' min'
            : busArrivalTimes[0] < 1 && busArrivalTimes.slice(1).length === 0
            ? ''
            : ' mins'}
        </Text>
      </Row>
    </View>
  );
};

export default StopPrediction;
