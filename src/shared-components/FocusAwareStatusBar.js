import React from 'react';
import {StatusBar} from 'react-native';
import {useIsFocused, useTheme} from '@react-navigation/native';

export default FocusAwareStatusBar = props => {
  const {dark} = useTheme();

  const isFocused = useIsFocused();

  return isFocused ? (
    Object.entries(props).length > 0 ? (
      <StatusBar {...props} />
    ) : (
      <StatusBar
        barStyle={dark ? 'light-content' : 'dark-content'}
        hidden={false}
        translucent={true}
        backgroundColor="transparent"
      />
    )
  ) : null;
};
