import React from 'react';
import {useTheme} from '@react-navigation/native';
import {View} from 'react-native';
import {GRAY_2} from '../constants/colors';

export const Line = ({style}) => {
  const {dark} = useTheme();

  return (
    <View
      style={[
        {
          borderBottomColor: dark ? '#353434' : GRAY_2,
          borderBottomWidth: 1,
          height: 1,
          width: '100%',
          opacity: 0.2,
        },
        style,
      ]}
    />
  );
};
