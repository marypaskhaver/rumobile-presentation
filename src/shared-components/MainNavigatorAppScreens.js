import React from 'react';
import ThemedNavigationContainer from '../shared-components/ThemedNavigationContainer';

import {createStackNavigator} from '@react-navigation/stack';

import MessageOfTheDayPopupScreen from '../screens/today/message-of-the-day/MessageOfTheDayPopupScreen';
import FeedbackScreen from '../screens/more/opinion-screens/FeedbackScreen';
import BugReportScreen from '../screens/more/opinion-screens/BugReportScreen';
import IAmDisappointedRUMobileScreen from '../screens/more/opinion-screens/IAmDisappointedRUMobileScreen';
import MyScheduleScreen from '../screens/today/favorite-sections/MyScheduleScreen';
import DoYouLikeRUMobileScreen from '../screens/more/opinion-screens/DoYouLikeRUMobileScreen';
import ClassCategoriesFiltersScreen from '../screens/more/classes-screens/ClassCategoriesFiltersScreen';
import Tabs from '../shared-components/Tabs';
import EditSectionScreen from '../screens/today/favorite-sections/edit-section/EditSectionScreen';
import SectionDaysOptionsScreen from '../screens/today/favorite-sections/edit-section/SectionDaysOptionsScreen';
import SectionCampusOptionsScreen from '../screens/today/favorite-sections/edit-section/SectionCampusOptionsScreen';

import WelcomeScreen1 from '../screens/welcome/WelcomeScreen1';
import WelcomeScreen2 from '../screens/welcome/WelcomeScreen2';
import WelcomeScreen3 from '../screens/welcome/WelcomeScreen3';
import {
  selectLoaded,
  selectPassed,
} from '../redux/reducers/welcomeScreensReducer';
import {useSelector} from 'react-redux';
import LoadingScreen from '../screens/welcome/LoadingScreen';
import MyBusStopsScreen from '../screens/today/favorite-bus-stops/MyBusStopsScreen';
import InternetToast from './InternetToast';
import {Dimensions, Platform} from 'react-native';
import RateUsScreen from '../screens/more/opinion-screens/RateUsScreen';

export const MainNavigatorAppScreens = () => {
  const Stack = createStackNavigator();

  const loadedWelcomeScreen = useSelector(selectLoaded);
  const passedWelcomeScreen = useSelector(selectPassed);

  if (!loadedWelcomeScreen) {
    return <LoadingScreen />;
  }

  const isLandscape = () => {
    const dimensions = Dimensions.get('screen');
    return dimensions.width >= dimensions.height;
  };

  const tabBarHeight = () => {
    const majorVersion = parseInt(Platform.Version, 10);
    const isIos = Platform.OS === 'ios';
    const isIOS11 = majorVersion >= 11 && isIos;
    if (Platform.isPad) return 49;
    if (isIOS11 && !isLandscape()) return 49;
    return 29;
  };

  return (
    <ThemedNavigationContainer>
      <InternetToast distanceFromBottom={34 + tabBarHeight()} />
      <Stack.Navigator
        initialRouteName={passedWelcomeScreen ? 'Tabs' : 'WelcomeScreen1'}
        screenOptions={{
          headerShown: false,
          presentation: 'modal',
        }}
      >
        <Stack.Screen
          name="Tabs"
          component={Tabs}
          options={{
            presentation: 'card',
            gestureEnabled: false,
          }}
        />
        <Stack.Screen
          name="MessageOfTheDayPopupScreen"
          component={MessageOfTheDayPopupScreen}
          options={{
            presentation: 'card',
          }}
        />
        <Stack.Screen
          name="FeedbackScreen"
          children={() => <FeedbackScreen />}
        />
        <Stack.Screen
          name="BugReportScreen"
          children={() => <BugReportScreen />}
        />
        <Stack.Screen
          name="DoYouLikeRUMobileScreen"
          children={() => <DoYouLikeRUMobileScreen />}
        />
        <Stack.Screen
          name="IAmDisappointedRUMobileScreen"
          children={() => <IAmDisappointedRUMobileScreen />}
        />
        <Stack.Screen name="RateUsScreen" children={() => <RateUsScreen />} />
        <Stack.Screen
          name="ClassCategoriesFiltersScreen"
          component={ClassCategoriesFiltersScreen}
          options={{
            presentation: 'modal',
          }}
        />
        <Stack.Screen
          name="MyBusStopsScreen"
          children={() => <MyBusStopsScreen />}
        />
        <Stack.Screen
          name="MyScheduleScreen"
          children={() => <MyScheduleScreen />}
        />
        <Stack.Screen name="EditSectionScreen" component={EditSectionScreen} />
        <Stack.Screen
          name="SectionDaysOptionsScreen"
          component={SectionDaysOptionsScreen}
        />
        <Stack.Screen
          name="SectionCampusOptionsScreen"
          component={SectionCampusOptionsScreen}
        />
        <Stack.Screen
          name="WelcomeScreen1"
          options={{
            presentation: 'card',
          }}
          children={() => <WelcomeScreen1 />}
        />
        <Stack.Screen
          name="WelcomeScreen2"
          options={{
            presentation: 'card',
          }}
          children={() => <WelcomeScreen2 />}
        />
        <Stack.Screen
          name="WelcomeScreen3"
          options={{
            presentation: 'card',
          }}
          children={() => <WelcomeScreen3 />}
        />
      </Stack.Navigator>
    </ThemedNavigationContainer>
  );
};
