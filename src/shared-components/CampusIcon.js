import React from 'react';
import {
  getFormattedCampusNameFromSectionMeetingTime,
  getCampusColorFromCampusName,
} from '../utils';
import {View, Text} from 'react-native';
import {SFProTextRegular} from '../constants/fonts';

export default CampusIcon = ({sectionMeetingTime}) => {
  const campusName =
    getFormattedCampusNameFromSectionMeetingTime(sectionMeetingTime);
  const campusColor = getCampusColorFromCampusName(
    sectionMeetingTime.campusName,
  );

  return (
    <View
      style={{
        backgroundColor: campusColor,
        paddingVertical: 4,
        paddingHorizontal: 6,
        borderRadius: 5,
      }}
    >
      <Text
        style={{
          fontFamily: SFProTextRegular,
          fontSize: 12,
          color: 'white',
          fontWeight: '400',
        }}
      >
        {campusName}
      </Text>
    </View>
  );
};
