import React from 'react';
import {Image} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import LinksScreen from '../screens/links/LinksScreen';
import TodayScreen from '../screens/today/TodayScreen';
import FoodScreen from '../screens/food/FoodScreen';
import MoreScreen from '../screens/more/MoreScreen';
import BusScreen from '../screens/buses/BusScreen';
import {PASTEL_RED} from '../constants/colors';
import {selectTabBarBadges} from '../redux/reducers/tabBarBadgeReducer';
import {useSelector} from 'react-redux';

export default Tabs = () => {
  const Tab = createBottomTabNavigator();
  let badgeCount = useSelector(selectTabBarBadges);

  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused}) => {
          let imageSource;

          switch (route.name) {
            case 'Today':
              imageSource = focused
                ? require('../assets/icons/tab_bar/todayiconselected.png')
                : require('../assets/icons/tab_bar/todayicongray.png');
              break;
            case 'Bus':
              imageSource = focused
                ? require('../assets/icons/tab_bar/busiconselected.png')
                : require('../assets/icons/tab_bar/busicon.png');
              break;
            case 'Food':
              imageSource = focused
                ? require('../assets/icons/tab_bar/foodiconselected.png')
                : require('../assets/icons/tab_bar/foodicon.png');
              break;
            case 'Links':
              imageSource = focused
                ? require('../assets/icons/tab_bar/linksiconselected.png')
                : require('../assets/icons/tab_bar/linksicon.png');
              break;
            case 'More':
              imageSource = focused
                ? require('../assets/icons/tab_bar/moreiconselected.png')
                : require('../assets/icons/tab_bar/moreicon.png');
              break;
            default:
              imageSource = focused
                ? require('../assets/icons/tab_bar/moreiconselected.png')
                : require('../assets/icons/tab_bar/moreicon.png');
          }

          return (
            <Image
              source={imageSource}
              style={{
                flex: 1,
                width: 22,
                height: 22,
                tintColor: focused ? PASTEL_RED : '#666666',
                resizeMode: 'contain',
              }}
            />
          );
        },
        tabBarActiveTintColor: PASTEL_RED,
        tabBarInactiveTintColor: '#666666',
        headerShown: false,
      })}
    >
      <Tab.Screen
        name="Today"
        options={{tabBarBadge: badgeCount > 0 ? badgeCount : null}}
        children={() => <TodayScreen />}
      />
      <Tab.Screen name="Bus" children={() => <BusScreen />} />
      <Tab.Screen name="Food" children={() => <FoodScreen title={'Food'} />} />
      <Tab.Screen
        name="Links"
        children={() => <LinksScreen title={'Links'} />}
      />
      <Tab.Screen name="More" children={() => <MoreScreen title={'More'} />} />
    </Tab.Navigator>
  );
};
