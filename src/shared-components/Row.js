import styled from 'styled-components/native';

export const Row = styled.View`
  flex-direction: row;
  height: auto;
  align-items: center;
`;
