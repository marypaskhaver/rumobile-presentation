import React from 'react';
import {View, Text} from 'react-native';
import {SFProTextRegular, SFProTextSemibold} from '../constants/fonts';
import {GRAY_4} from '../constants/colors';
import {TimeFormatter} from '../utils';
import {useTheme} from '@react-navigation/native';

export default SectionTime = ({meetingTime}) => {
  const {colors} = useTheme();

  const {startTime, endTime, pmCode} = meetingTime;
  const startTimeFormatter = new TimeFormatter(startTime)
    .addColon()
    .addTimeOfDay(pmCode);
  const endTimeFormatter = new TimeFormatter(endTime)
    .addColon()
    .addTimeOfDay(pmCode);

  const finalEndTime = !startTimeFormatter.isSpanValidAsAClassLength(
    endTimeFormatter,
  )
    ? endTimeFormatter.getOppositeTime()
    : endTimeFormatter.time;

  const specialSpan = meetingTime.specialSpan;
  const specialStartTime = specialSpan ? specialSpan.split('-')[0] : '';
  const specialEndTime = specialSpan ? specialSpan.split('-')[1] : '';

  return (
    <View>
      <Text
        style={{
          fontFamily: SFProTextSemibold,
          textAlign: 'right',
          fontSize: 17,
          color: colors.text,
        }}
      >
        {specialStartTime
          ? specialStartTime
          : startTimeFormatter.time
          ? startTimeFormatter.time
          : '--'}
      </Text>
      <Text
        style={{
          fontFamily: SFProTextRegular,
          textAlign: 'right',
          fontSize: 12,
          color: GRAY_4,
        }}
      >
        {specialEndTime
          ? specialEndTime
          : finalEndTime
          ? finalEndTime
          : 'No time defined'}
      </Text>
    </View>
  );
};
