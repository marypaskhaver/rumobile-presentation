import React from 'react';
import {Text, Image} from 'react-native';
import styled from 'styled-components/native';
import {useTheme} from '@react-navigation/native';
import {Row} from './Row';
import {PASTEL_RED} from '../constants/colors';
import checkmarkImage from '../assets/images/other/checkmark.png';

const BulletPoint = styled.View`
  width: 8px;
  height: 8px;
  border-radius: 4px;
  background-color: ${props => props.color};
  margin-right: 12px;
  margin-left: 16px;
`;
const Checkmark = () => {
  return (
    <Image
      style={{
        marginLeft: 'auto',
        marginRight: 4,
        width: 16,
        height: 16,
        tintColor: PASTEL_RED,
      }}
      source={checkmarkImage}
    />
  );
};

export const RowWithCheckmark = ({
  textToDisplay,
  isChecked,
  style,
  customBulletPoint,
}) => {
  const {colors} = useTheme();

  return (
    <Row style={style}>
      {customBulletPoint ? (
        customBulletPoint
      ) : (
        <BulletPoint color={colors.text} />
      )}
      <Text style={{fontSize: 17, color: colors.text, paddingRight: 60}}>
        {textToDisplay}
      </Text>

      {/* Will only show Checkmark when showCheckmark is true */}
      {isChecked && <Checkmark />}
    </Row>
  );
};
