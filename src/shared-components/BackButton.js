import React from 'react';
import {View, TouchableOpacity, Image, Text} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {PASTEL_RED} from '../constants/colors';
import {SFProTextRegular} from '../constants/fonts';

export const BackButton = ({text, isWhite, style, screenToGoTo}) => {
  const navigation = useNavigation();
  const color = isWhite ? 'white' : PASTEL_RED;
  return (
    <TouchableOpacity
      onPress={() =>
        screenToGoTo ? navigation.navigate(screenToGoTo) : navigation.goBack()
      }
      style={[
        style,
        {
          alignSelf: 'flex-start',
          position: 'absolute',
          top: 32,
          left: 8,
          zIndex: 50,
        },
      ]}
    >
      <View
        style={{flexDirection: 'row', paddingTop: 16, alignItems: 'center'}}
      >
        <Image
          style={{
            tintColor: color,
            height: 16,
            resizeMode: 'contain',
            transform: [{scaleX: -1}],
          }}
          source={require('../assets/icons/other/forward.png')}
        />
        <Text
          style={{
            fontSize: 16,
            fontFamily: SFProTextRegular,
            color: color,
          }}
        >
          {text}
        </Text>
      </View>
    </TouchableOpacity>
  );
};
