import React, {useEffect, useRef} from 'react';
import {Animated, Image, Text} from 'react-native';
import {SFProTextRegular} from '../constants/fonts';
import {Row} from '.';
import {PanGestureHandler} from 'react-native-gesture-handler';
import {GRAY_6} from '../constants/colors';

export default LowToast = ({
  message,
  persist = false,
  distanceFromBottom = 12,
}) => {
  const startingPosition = useRef(new Animated.Value(100)).current;
  const errorImage = require('../assets/icons/other/error.png');

  const slideIn = () => {
    Animated.timing(startingPosition, {
      toValue: -distanceFromBottom,
      duration: 500,
      useNativeDriver: true,
    }).start();
  };

  slideIn();

  const slideOut = () => {
    Animated.timing(startingPosition, {
      toValue: 100,
      duration: 500,
      useNativeDriver: true,
    }).start();
  };

  // const onPanGestureEvent = Animated.event(
  //   [
  //     {
  //       nativeEvent: {
  //         translationY: startingPosition,
  //       },
  //     },
  //   ],
  //   {useNativeDriver: true},
  // );

  useEffect(() => {
    if (!persist) {
      const timeId = setTimeout(() => {
        slideOut();
      }, 5000);

      return () => {
        clearTimeout(timeId);
      };
    }
  }, []);

  return (
    <PanGestureHandler onGestureEvent={slideOut}>
      <Animated.View
        style={{
          position: 'absolute',
          transform: [
            {
              translateY: startingPosition,
            },
          ],
          bottom: 0,
          alignSelf: 'center',
          backgroundColor: GRAY_6,
          borderRadius: 4,
          width: '90%',
          zIndex: 50,
        }}
      >
        <Row style={{padding: 12}}>
          <Image
            source={errorImage}
            style={{width: 20, height: 20, tintColor: 'white'}}
          />
          <Text
            style={{
              fontFamily: SFProTextRegular,
              color: 'white',
              paddingLeft: 8,
              fontSize: 16,
            }}
          >
            {message}
          </Text>
        </Row>
      </Animated.View>
    </PanGestureHandler>
  );
};
