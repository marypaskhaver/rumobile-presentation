// styled-components library, used for styling components
import styled from 'styled-components/native';

import {SFProDisplayBold} from '../constants/fonts';

export const BigText = styled.Text`
  font-family: ${`${SFProDisplayBold}`};
  width: 320px;
  font-size: 34px;
  letter-spacing: 0.37px;
`;
