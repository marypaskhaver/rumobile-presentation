import {useEffect} from 'react';
import store from '../redux/store';
import {PermissionsAndroid, Platform} from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import {Storyboard} from '@glassbox/react-native-storyboard';

import {
  receiveUserLocation,
  requestBusPredictions,
  requestBusRoutes,
  requestBusStops,
  requestVehicles,
} from '../redux/actions/busesActions';
import {useDispatch, useSelector} from 'react-redux';
import {selectChosenCampusID} from '../redux/reducers/campusesReducer';
import {selectBusRoutes} from '../redux/reducers/busesReducer';

export const getLocation = async () => {
  const hasPermission = await hasLocationPermission();

  if (!hasPermission) {
    store.dispatch(receiveUserLocation(null));
    return;
  }

  Geolocation.getCurrentPosition(
    position => {
      store.dispatch(receiveUserLocation(position));
    },
    error => {
      store.dispatch(receiveUserLocation(null));
      Storyboard.reportEvent('code2', {
        message: JSON.stringify(error),
      });
      console.log(error);
    },
    {
      accuracy: {
        android: 'high',
        ios: 'best',
      },
      enableHighAccuracy: true,
      timeout: 15000,
      maximumAge: 10000,
      distanceFilter: 0,
      forceRequestLocation: true,
      forceLocationManager: true,
      showLocationDialog: true,
    },
  );
};

const hasLocationPermission = async () => {
  if (Platform.OS === 'ios') {
    const hasPermission = await hasPermissionIOS();
    return hasPermission;
  }

  if (Platform.OS === 'android' && Platform.Version < 23) {
    return true;
  }

  const hasPermission = await PermissionsAndroid.check(
    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  );

  if (hasPermission) {
    return true;
  }

  const status = await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  );

  if (status === PermissionsAndroid.RESULTS.GRANTED) {
    return true;
  }

  store.dispatch(receiveUserLocation(null));
  return false;
};

const hasPermissionIOS = async () => {
  const status = await Geolocation.requestAuthorization('whenInUse');

  if (status === 'granted') {
    return true;
  }

  store.dispatch(receiveUserLocation(null));
  return false;
};

// Refreshes predictions only
const BusPredictions = () => {
  const chosenCampusID = useSelector(selectChosenCampusID);

  const REFRESH_RATE = 30000; // 30 seconds
  const dispatch = useDispatch();

  const busRoutes = useSelector(selectBusRoutes);

  useEffect(() => {
    const interval = setInterval(() => {
      getLocation();

      dispatch(
        requestBusPredictions(busRoutes.map(busRoute => busRoute.route_id)),
      );
    }, REFRESH_RATE);

    return () => clearInterval(interval);
  }, [chosenCampusID]);

  return null;
};

const AllBusData = () => {
  const chosenCampusID = useSelector(selectChosenCampusID);

  const REFRESH_RATE = 30000; // 30 seconds
  const dispatch = useDispatch();

  const busRoutes = useSelector(selectBusRoutes);

  useEffect(() => {
    const interval = setInterval(() => {
      getLocation();

      dispatch(requestBusRoutes(chosenCampusID));
      dispatch(requestVehicles(chosenCampusID));
      dispatch(requestBusStops(chosenCampusID));

      dispatch(
        requestBusPredictions(busRoutes.map(busRoute => busRoute.route_id)),
      );
    }, REFRESH_RATE);

    return () => clearInterval(interval);
  }, [chosenCampusID]);

  return null;
};

export const LocationGetter = {
  AllBusData,
  BusPredictions,
};
