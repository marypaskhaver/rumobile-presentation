import React from 'react';
import {Image} from 'react-native';
import {GRAY_4} from '../constants/colors';

export default DropdownArrow = ({selected = true}) => {
  return (
    <Image
      style={{
        tintColor: GRAY_4,
        height: 16,
        width: 16,
        resizeMode: 'contain',
        transform: [{rotate: selected ? '-90deg' : '90deg'}],
      }}
      source={require('../assets/icons/other/forward.png')}
    />
  );
};
