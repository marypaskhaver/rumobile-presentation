import React from 'react';
import styled from 'styled-components/native';
import {useTheme} from '@react-navigation/native';

// > symbol at the end of a cell
const StyledChevron = styled.Image`
  width: 8px;
  height: 13px;
  margin-right: 16px;
  margin-left: auto;
  opacity: 0.4;
  tint-color: ${props => props.tintColor};
`;

export const Chevron = () => {
  const {colors} = useTheme();

  return (
    <StyledChevron
      source={require('../assets/icons/other/forward.png')}
      tintColor={colors.text}
    />
  );
};
