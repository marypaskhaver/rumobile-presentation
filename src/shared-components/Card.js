import styled from 'styled-components/native';
import {GRAY_4} from '../constants/colors';

export const Card = styled.View`
  padding: 16px;
  border-radius: 8px;
  border-color: ${`${GRAY_4}`};
  box-shadow: 0 5px 5px rgba(0, 0, 0, 0.12);
  background-color: white;
`;
