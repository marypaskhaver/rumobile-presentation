import {building} from '../../building.json';

export default getSectionLocationFromMeetingTime = meetingTimes => {
  if (!meetingTimes.meetingDay) return 'Independent Study';

  if (meetingTimes.buildingCode) {
    const sectionMeetingLocation = building.find(building =>
      building.tag.includes(meetingTimes.buildingCode.toLowerCase()),
    );

    if (sectionMeetingLocation) return sectionMeetingLocation.bName;
    return meetingTimes.buildingCode;
  } else {
    return 'No building data';
  }
};
