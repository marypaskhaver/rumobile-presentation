import formatInstructorNames from '../formatInstructorNames';

describe('formatInstructorNames', () => {
  it('given a list of instructors, returns their names in one line with each name separated by a colon', () => {
    const namesList = [
      {name: 'McGONAGALL, MINERVA'},
      {name: 'SPROUT, POMONA'},
      {name: 'BINNS'},
    ];
    const formattedNames = formatInstructorNames(namesList);
    expect(formattedNames).toBe('McGONAGALL, MINERVA; SPROUT, POMONA; BINNS');
  });

  it('shows NO INSTRUCTOR LISTED when not given a list of instructors', () => {
    const formattedNames = formatInstructorNames();
    expect(formattedNames).toBe('NO INSTRUCTOR LISTED');
  });

  it('shows NO INSTRUCTOR LISTED when given an empty list of instructors', () => {
    const formattedNames = formatInstructorNames([]);
    expect(formattedNames).toBe('NO INSTRUCTOR LISTED');
  });
});
