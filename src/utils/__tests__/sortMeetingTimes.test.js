import sortMeetingTimes from '../sortMeetingTimes';

describe('sortMeetingTimes', () => {
  it('returns a list of meeting times sorted by meeting day in week order, i.e. Sun., Mon., and by startTime - Test 1', () => {
    const meetingTimes = [
      {
        meetingDay: null,
        startTime: null,
        endTime: null,
      },
      {
        meetingDay: 'S',
        startTime: null,
        endTime: null,
      },
      {
        meetingDay: 'M',
        startTime: '1000',
        pmCode: 'P',
        endTime: '1130',
      },
      {
        meetingDay: 'M',
        startTime: '1200',
        endTime: '0130',
        pmCode: 'P',
      },
      {
        meetingDay: 'M',
        startTime: '1200',
        endTime: '0130',
        pmCode: 'A',
      },
    ];

    const sortedMeetingTimes = sortMeetingTimes(meetingTimes);
    expect(sortedMeetingTimes).toEqual([
      {
        meetingDay: null,
        startTime: null,
        endTime: null,
      },
      {
        meetingDay: 'M',
        startTime: '1200',
        pmCode: 'A',
        endTime: '0130',
      },
      {
        meetingDay: 'M',
        startTime: '1200',
        pmCode: 'P',
        endTime: '0130',
      },
      {
        meetingDay: 'M',
        startTime: '1000',
        pmCode: 'P',
        endTime: '1130',
      },
      {
        meetingDay: 'S',
        startTime: null,
        endTime: null,
      },
    ]);
  });

  it('returns a list of meeting times sorted by meeting day in week order, i.e. Sun., Mon., and by startTime - Test 2', () => {
    const meetingTimes = [
      {
        endTime: '0130',
        pmCode: 'A',
        startTime: '0900',
      },
      {
        endTime: '0130',
        pmCode: 'P',
        startTime: '1200',
      },
      {
        endTime: '0130',
        pmCode: 'A',
        startTime: '1200',
      },
    ];

    const sortedMeetingTimes = sortMeetingTimes(meetingTimes);
    expect(sortedMeetingTimes).toEqual([
      {
        endTime: '0130',
        pmCode: 'A',
        startTime: '1200',
      },
      {
        endTime: '0130',
        pmCode: 'A',
        startTime: '0900',
      },
      {
        endTime: '0130',
        pmCode: 'P',
        startTime: '1200',
      },
    ]);
  });

  it('returns a list of meeting times sorted by meeting day in week order, i.e. Sun., Mon., and by startTime - Test 3', () => {
    const meetingTimes = [
      {
        endTime: '0130',
        pmCode: 'A',
        startTime: '0700',
      },
      {
        endTime: '0130',
        pmCode: 'A',
        startTime: '0800',
      },
    ];

    const sortedMeetingTimes = sortMeetingTimes(meetingTimes);
    expect(sortedMeetingTimes).toEqual([
      {
        endTime: '0130',
        pmCode: 'A',
        startTime: '0700',
      },
      {
        endTime: '0130',
        pmCode: 'A',
        startTime: '0800',
      },
    ]);
  });

  it('returns a list of meeting times sorted by meeting day in week order, i.e. Sun., Mon., and by startTime - Test 4', () => {
    const meetingTimes = [
      {
        endTime: '1230',
        pmCode: 'A',
        startTime: '1200',
      },
      {
        endTime: '0130',
        pmCode: 'A',
        startTime: '0100',
      },
    ];

    const sortedMeetingTimes = sortMeetingTimes(meetingTimes);
    expect(sortedMeetingTimes).toEqual([
      {
        endTime: '1230',
        pmCode: 'A',
        startTime: '1200',
      },
      {
        endTime: '0130',
        pmCode: 'A',
        startTime: '0100',
      },
    ]);
  });

  it('returns a list of meeting times sorted by meeting day in week order, i.e. Sun., Mon., and by startTime - Test 5', () => {
    const meetingTimes = [
      {
        endTime: '0850',
        pmCode: 'P',
        startTime: '0730',
      },
      {
        endTime: '0130',
        pmCode: 'P',
        startTime: '1210',
      },
    ];

    const sortedMeetingTimes = sortMeetingTimes(meetingTimes);
    expect(sortedMeetingTimes).toEqual([
      {
        endTime: '0130',
        pmCode: 'P',
        startTime: '1210',
      },
      {
        endTime: '0850',
        pmCode: 'P',
        startTime: '0730',
      },
    ]);
  });

  it('returns a list of meeting times sorted by meeting day in week order, i.e. Sun., Mon., and by startTime - Test 6', () => {
    const meetingTimes = [
      {
        endTime: '0950',
        pmCode: 'A',
        startTime: '0830',
      },
      {
        endTime: '0840',
        pmCode: 'P',
        startTime: '0745',
      },
      {
        endTime: '0120',
        pmCode: 'P',
        startTime: '1225',
      },
    ];

    const sortedMeetingTimes = sortMeetingTimes(meetingTimes);
    expect(sortedMeetingTimes).toEqual([
      {
        endTime: '0950',
        pmCode: 'A',
        startTime: '0830',
      },
      {
        endTime: '0120',
        pmCode: 'P',
        startTime: '1225',
      },
      {
        endTime: '0840',
        pmCode: 'P',
        startTime: '0745',
      },
    ]);
  });
});
