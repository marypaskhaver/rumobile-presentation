import {getColorFromBusRouteName} from '../getColorFromBusRouteName';

describe('getColorFromBusRouteName', () => {
  it('given the name of a New Brunswick bus, returns the color associated with it', () => {
    const routeName = 'Route A';
    const color = getColorFromBusRouteName(routeName);
    expect(color).toBe('rgb(124, 31, 206)');
  });

  it('works for New Brunswick exam buses', () => {
    const routeName = 'Route A - Exam';
    const color = getColorFromBusRouteName(routeName);
    expect(color).toBe('rgb(124, 31, 206)');
  });

  it('given the name of a Newark bus, returns the color associated with it', () => {
    const routeName = 'Newark Penn Station';
    const color = getColorFromBusRouteName(routeName);
    expect(color).toBe('rgb(255, 102, 199)');
  });

  it('given the name of a Camden bus, returns the color associated with it', () => {
    const routeName = 'Rutgers Camden Shuttle';
    const color = getColorFromBusRouteName(routeName);
    expect(color).toBe('rgb(245, 166, 35)');
  });

  it('handles non-existent route names', () => {
    const routeName = 'Cat Bus';
    const color = getColorFromBusRouteName(routeName);
    expect(color).toBe('#8E8E93'); // GRAY_4
  });
});
