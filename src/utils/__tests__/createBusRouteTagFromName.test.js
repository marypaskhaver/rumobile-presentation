import createBusRouteTagFromName from '../createBusRouteTagFromName';

describe('createBusRouteTagFromName', () => {
  it('creates tag from the first letter of each word in the route name except for the last, which it includes entirely', () => {
    const routeName = 'Campus Connect PM';
    const result = createBusRouteTagFromName(routeName);
    expect(result).toBe('CC PM');
  });
});
