import getCampusColorFromCampusName from '../getCampusColorFromCampusName';

describe('getCampusColorFromCampusName', () => {
  it("returns the color associated with a campus given the campus's name", () => {
    const campusName = 'COLLEGE AVENUE';
    const collegeAvenueCampusColor = '#FEC938';

    const calculatedCampusColor = getCampusColorFromCampusName(campusName);

    expect(calculatedCampusColor).toBe(collegeAvenueCampusColor);
  });

  it("handles campuses not in Rutgers' database)", () => {
    const invalidCampusName = 'Hogwarts';
    const invalidCampusColorGray = '#5C5959'; // GRAY_5

    const calculatedCampusColor =
      getCampusColorFromCampusName(invalidCampusName);

    expect(calculatedCampusColor).toBe(invalidCampusColorGray);
  });

  it('handles invalid inputs', () => {
    const invalidCampusName = null;
    const invalidCampusColorRed = 'rgb(217, 35, 35)'; // DARK_RED

    const calculatedCampusColor =
      getCampusColorFromCampusName(invalidCampusName);

    expect(calculatedCampusColor).toBe(invalidCampusColorRed);
  });
});
