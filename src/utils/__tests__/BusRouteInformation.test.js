import BusRouteInformation from '../BusRouteInformation';

describe('BusRouteInformation', () => {
  describe('getActiveBusRoutes', () => {
    it('given a list of bus routes, returns a list of only the active ones', () => {
      const busRoutes = [
        {
          is_active: true,
          long_name: 'Route Weekend 1',
          route_id: '123',
        },
        {
          is_active: false,
          long_name: 'Route Weekend 2',
          route_id: '456',
        },
      ];

      const activeVehicles = [
        {
          route_id: '123',
          arrival_estimates: [
            {
              route_id: '123',
              arrival_at: '2022-05-30T10:05:31-04:00',
            },
          ],
        },
      ];

      const busRouteInformation = new BusRouteInformation(
        busRoutes,
        activeVehicles,
      );

      const activeBusRoutes = busRouteInformation.getActiveRoutes();

      expect(activeBusRoutes).toEqual([
        {
          is_active: true,
          long_name: 'Route Weekend 1',
          route_id: '123',
        },
      ]);
    });

    it('handles invalid input', () => {
      const busRouteInformation = new BusRouteInformation([], []);
      const activeBusRoutes = busRouteInformation.getActiveRoutes();
      expect(activeBusRoutes).toEqual([]);
    });
  });

  describe('getInactiveBusRoutes', () => {
    it('given a list of bus routes, returns a list of only the inactive ones', () => {
      const busRoutes = [
        {
          is_active: true,
          long_name: 'Route Weekend 1',
          route_id: '123',
        },
        {
          is_active: true,
          long_name: 'Route Weekend 2',
          route_id: '456',
        },
        {
          is_active: false,
          long_name: 'Route A',
          route_id: '789',
        },
      ];

      const activeVehicles = [
        {
          route_id: '123',
          arrival_estimates: [
            {
              route_id: '123',
              arrival_at: '2022-05-30T10:05:31-04:00',
            },
          ],
        },
      ];

      const busRouteInformation = new BusRouteInformation(
        busRoutes,
        activeVehicles,
      );
      const inactiveBusRoutes = busRouteInformation.getInactiveRoutes();

      expect(inactiveBusRoutes).toEqual([
        {
          is_active: true,
          long_name: 'Route Weekend 2',
          route_id: '456',
        },
        {
          is_active: false,
          long_name: 'Route A',
          route_id: '789',
        },
      ]);
    });

    it('handles invalid input', () => {
      const busRouteInformation = new BusRouteInformation();
      const inactiveBusRoutes = busRouteInformation.getInactiveRoutes();
      expect(inactiveBusRoutes).toEqual([]);
    });
  });
});
