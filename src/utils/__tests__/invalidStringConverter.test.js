import invalidStringConverter from '../invalidStringConverter';

describe('converts an invalid input to text representation', () => {
  it('handles valid input', () => {
    const original = 'By Jove, Jeeves, that was practically poetry.';
    const result = invalidStringConverter(original);
    expect(result).toBe(original);
  });

  it('handles invalid input', () => {
    const result = invalidStringConverter(null);
    expect(result).toBe('');
  });
});
