import TimeFormatter from '../TimeFormatter';

describe('TimeFormatter', () => {
  it('separates given hours and minutes with a colon', () => {
    const timeWithoutColon = '1210';
    const timeFormatter = new TimeFormatter(timeWithoutColon);
    timeFormatter.addColon();

    expect(timeFormatter.time).toBe('12:10');
  });

  describe('when time has leading 0', () => {
    it('separates given hours without a leading 0 and minutes with a colon', () => {
      const timeWithoutColon = '0130';
      const timeFormatter = new TimeFormatter(timeWithoutColon);
      timeFormatter.addColon();

      expect(timeFormatter.time).toBe('1:30');
    });

    it('designates time of day', () => {
      const timeWithoutColon = '0130';
      const timeFormatter = new TimeFormatter(timeWithoutColon);
      timeFormatter.addTimeOfDay('P').addColon();

      expect(timeFormatter.time).toBe('1:30PM');
    });

    it('designates time of day when given extra space', () => {
      const timeWithoutColon = '0945';
      const timeFormatter = new TimeFormatter(timeWithoutColon);
      timeFormatter.addTimeOfDay('    P   ').addColon();

      expect(timeFormatter.time).toBe('9:45PM');
    });

    it('handles invalid arguments', () => {
      const timeFormatter = new TimeFormatter(null);
      timeFormatter.addColon().addTimeOfDay('A');
      expect(timeFormatter.time).toBe('');
    });
  });

  describe('isSpanValidAsAClassLength', () => {
    it("deems a class's span (start time to end time) invalid if the class lasts > 12 hours - AM", () => {
      const timeFormatter1 = new TimeFormatter('1100')
        .addColon()
        .addTimeOfDay('A');
      const timeFormatter2 = new TimeFormatter('1220')
        .addColon()
        .addTimeOfDay('A');

      const isValid = timeFormatter1.isSpanValidAsAClassLength(timeFormatter2);
      expect(isValid).toEqual(false);
    });

    it("deems a class's span (start time to end time) invalid if the class lasts > 12 hours - PM", () => {
      const timeFormatter1 = new TimeFormatter('0900')
        .addColon()
        .addTimeOfDay('P');
      const timeFormatter2 = new TimeFormatter('1200')
        .addColon()
        .addTimeOfDay('P');

      const isValid = timeFormatter1.isSpanValidAsAClassLength(timeFormatter2);
      expect(isValid).toEqual(false);
    });

    it("deems a class's span (start time to end time) valid if the class starts at 12 AM and ends after 12 AM", () => {
      const timeFormatter1 = new TimeFormatter('1200')
        .addColon()
        .addTimeOfDay('A');
      const timeFormatter2 = new TimeFormatter('0130')
        .addColon()
        .addTimeOfDay('A');

      const isValid = timeFormatter1.isSpanValidAsAClassLength(timeFormatter2);
      expect(isValid).toEqual(true);
    });

    it('handles if other time formatter to make the span with is not provided', () => {
      const timeFormatter1 = new TimeFormatter(null)
        .addColon()
        .addTimeOfDay('P');

      const result = timeFormatter1.isSpanValidAsAClassLength();
      expect(result).toBe('');
    });

    it('handles if other time formatter to make the span does not have a time', () => {
      const timeFormatter1 = new TimeFormatter(null)
        .addColon()
        .addTimeOfDay('P');
      const timeFormatter2 = new TimeFormatter(null);

      const result = timeFormatter1.isSpanValidAsAClassLength(timeFormatter2);
      expect(result).toBe('');
    });
  });

  describe('getMinutes', () => {
    it('given a time in hh:mm format, gets minutes in that time', () => {
      const timeFormatter1 = new TimeFormatter('1145')
        .addColon()
        .addTimeOfDay('A');
      expect(timeFormatter1.getMinutes()).toBe(45);
    });

    it('returns the minutes of a given time', () => {
      const timeFormatter2 = new TimeFormatter('0932');
      expect(timeFormatter2.getMinutes('8:59')).toBe(59);
    });
  });

  describe('replace0thHourWith12thIfNeeded', () => {
    it('given a time in hh:mm format where the hours are 0, returns time with hours as 12 if needed', () => {
      const timeFormatter1 = new TimeFormatter('0022')
        .addColon()
        .addTimeOfDay('A');
      expect(
        timeFormatter1.replace0thHourWith12thIfNeeded(timeFormatter1.time),
      ).toBe('12:22AM');
    });

    it('given a time in hh:mm format where the hours are not 0, returns the time', () => {
      const timeFormatter1 = new TimeFormatter('0633')
        .addColon()
        .addTimeOfDay('A');
      expect(
        timeFormatter1.replace0thHourWith12thIfNeeded(timeFormatter1.time),
      ).toBe('6:33AM');
    });
  });

  describe('getOppositeTime', () => {
    it('returns a given formatted time ending in PM if current formatted time ends in AM', () => {
      const timeFormatter1 = new TimeFormatter('1100')
        .addColon()
        .addTimeOfDay('A');
      expect(timeFormatter1.time).toBe('11:00AM');

      const toggledTime = timeFormatter1.getOppositeTime();
      expect(toggledTime).toBe('11:00PM');
    });

    it('returns a given formatted time ending in AM if current formatted time ends in PM', () => {
      const timeFormatter1 = new TimeFormatter('1200')
        .addColon()
        .addTimeOfDay('P');
      expect(timeFormatter1.time).toBe('12:00PM');

      const toggledTime = timeFormatter1.getOppositeTime();
      expect(toggledTime).toBe('12:00AM');
    });

    it('handles invalid (non-existent) times', () => {
      const timeFormatter = new TimeFormatter(null);
      const toggledTime = timeFormatter.getOppositeTime();
      expect(toggledTime).toBe('');
    });

    it('handles times without colons or times of day', () => {
      const timeFormatter = new TimeFormatter('1200');
      const toggledTime = timeFormatter.getOppositeTime();
      expect(toggledTime).toBe('');
    });

    it('handles times with colons but without times of day', () => {
      const timeFormatter = new TimeFormatter('1200').addColon();
      const toggledTime = timeFormatter.getOppositeTime();
      expect(toggledTime).toBe('');
    });
  });

  describe('createSpan', () => {
    it('merges start time and end time together', () => {
      const timeFormatter1 = new TimeFormatter('0100')
        .addColon()
        .addTimeOfDay('P');
      const timeFormatter2 = new TimeFormatter('0200')
        .addColon()
        .addTimeOfDay('P');

      const result = timeFormatter1.createSpan(timeFormatter2);
      expect(result).toBe('1:00PM-2:00PM');
    });

    it('handles classes with double-digit hours that start in the morning and end in the morning', () => {
      const timeFormatter1 = new TimeFormatter('0830')
        .addColon()
        .addTimeOfDay('A');
      const timeFormatter2 = new TimeFormatter('0950')
        .addColon()
        .addTimeOfDay('A');

      const result = timeFormatter1.createSpan(timeFormatter2);
      expect(result).toBe('8:30AM-9:50AM');
    });

    it('handles classes that are less than 1 hour long', () => {
      const timeFormatter1 = new TimeFormatter('0800')
        .addColon()
        .addTimeOfDay('A');
      const timeFormatter2 = new TimeFormatter('0850')
        .addColon()
        .addTimeOfDay('A');

      const result = timeFormatter1.createSpan(timeFormatter2);
      expect(result).toBe('8:00AM-8:50AM');
    });

    it('handles classes that are 1 hour long', () => {
      const timeFormatter1 = new TimeFormatter('0800')
        .addColon()
        .addTimeOfDay('A');
      const timeFormatter2 = new TimeFormatter('0900')
        .addColon()
        .addTimeOfDay('A');

      const result = timeFormatter1.createSpan(timeFormatter2);
      expect(result).toBe('8:00AM-9:00AM');
    });

    it('handles classes with double-digit hours that start in the morning and end in the afternoon', () => {
      // In the JSON data, the pmCode is the same for a section's startTime and
      // endTime, even if the startTime is in the morning (AM) endTime is in the
      // afternoon (PM).

      const timeFormatter1 = new TimeFormatter('1100')
        .addColon()
        .addTimeOfDay('A');
      const timeFormatter2 = new TimeFormatter('1220')
        .addColon()
        .addTimeOfDay('A');

      const result = timeFormatter1.createSpan(timeFormatter2);
      expect(result).toBe('11:00AM-12:20PM');
    });

    it('handles classes with single-digit hours that start in the morning and end in the afternoon', () => {
      const timeFormatter1 = new TimeFormatter('0900')
        .addColon()
        .addTimeOfDay('A');
      const timeFormatter2 = new TimeFormatter('0400')
        .addColon()
        .addTimeOfDay('A');

      const result = timeFormatter1.createSpan(timeFormatter2);
      expect(result).toBe('9:00AM-4:00PM');
    });

    it('handles classes with single and double-digit hours that start in the morning and end in the afternoon', () => {
      const timeFormatter1 = new TimeFormatter('1000')
        .addColon()
        .addTimeOfDay('A');
      const timeFormatter2 = new TimeFormatter('0400')
        .addColon()
        .addTimeOfDay('A');

      const result = timeFormatter1.createSpan(timeFormatter2);
      expect(result).toBe('10:00AM-4:00PM');
    });
  });

  it('handles if other time formatter to make the span with is not provided', () => {
    const timeFormatter1 = new TimeFormatter(null).addColon().addTimeOfDay('P');

    const result = timeFormatter1.createSpan();
    expect(result).toBe('');
  });

  it('handles invalid times in either TimeFormatter', () => {
    const timeFormatter1 = new TimeFormatter(null).addColon().addTimeOfDay('P');
    const timeFormatter2 = new TimeFormatter(null).addColon().addTimeOfDay('P');

    const result = timeFormatter1.createSpan(timeFormatter2);
    expect(result).toBe('');
  });

  describe('isEndTimeAfterStartTime', () => {
    // 12:00 AM - 12:00 PM → true
    // 12:00 PM - 12:00 AM → false
    describe("returns a boolean representing whether the class's end time is after its start time", () => {
      it('PM → PM: returns true if the starting time is less than the ending time', () => {
        const timeFormatter1 = new TimeFormatter('0100')
          .addColon()
          .addTimeOfDay('P');
        const timeFormatter2 = new TimeFormatter('0300')
          .addColon()
          .addTimeOfDay('P');

        const result = timeFormatter1.createSpan(timeFormatter2);

        const endTimeIsAfterStartTime =
          timeFormatter1.isEndTimeAfterStartTime(result);
        expect(endTimeIsAfterStartTime).toBe(true);
      });

      it('PM → PM: returns false if the starting time is greater than the ending time', () => {
        const timeFormatter1 = new TimeFormatter('0300')
          .addColon()
          .addTimeOfDay('P');
        const timeFormatter2 = new TimeFormatter('0100')
          .addColon()
          .addTimeOfDay('P');

        const result = timeFormatter1.createSpan(timeFormatter2);

        const endTimeIsAfterStartTime =
          timeFormatter1.isEndTimeAfterStartTime(result);
        expect(endTimeIsAfterStartTime).toBe(false);
      });

      it('AM → PM: returns true if the starting time is greater than the ending time', () => {
        const timeFormatter1 = new TimeFormatter('0100')
          .addColon()
          .addTimeOfDay('A');
        const timeFormatter2 = new TimeFormatter('0300')
          .addColon()
          .addTimeOfDay('P');

        const result = timeFormatter1.createSpan(timeFormatter2);

        const endTimeIsAfterStartTime =
          timeFormatter1.isEndTimeAfterStartTime(result);
        expect(endTimeIsAfterStartTime).toBe(true);
      });

      it('AM → PM: returns true if the starting time is AM and the ending time is PM', () => {
        const timeFormatter1 = new TimeFormatter('0300')
          .addColon()
          .addTimeOfDay('A');
        const timeFormatter2 = new TimeFormatter('0100')
          .addColon()
          .addTimeOfDay('P');

        const result = timeFormatter1.createSpan(timeFormatter2);

        const endTimeIsAfterStartTime =
          timeFormatter1.isEndTimeAfterStartTime(result);
        expect(endTimeIsAfterStartTime).toBe(true);
      });

      it('AM → PM: returns false if the starting time is the same as the ending time', () => {
        const timeFormatter1 = new TimeFormatter('0300')
          .addColon()
          .addTimeOfDay('A');
        const timeFormatter2 = new TimeFormatter('0300')
          .addColon()
          .addTimeOfDay('A');

        const result = timeFormatter1.createSpan(timeFormatter2);

        const endTimeIsAfterStartTime =
          timeFormatter1.isEndTimeAfterStartTime(result);
        expect(endTimeIsAfterStartTime).toBe(false);
      });

      it('AM → PM: returns true if the starting time is AM and the ending time is PM, 12 AM to 12 PM', () => {
        const timeFormatter1 = new TimeFormatter('1200')
          .addColon()
          .addTimeOfDay('A');
        const timeFormatter2 = new TimeFormatter('1200')
          .addColon()
          .addTimeOfDay('P');

        const result = timeFormatter1.createSpan(timeFormatter2);

        const endTimeIsAfterStartTime =
          timeFormatter1.isEndTimeAfterStartTime(result);
        expect(endTimeIsAfterStartTime).toBe(true);
      });

      it('PM → AM: returns false if the starting time is AM and the ending time is PM, 12 PM to 12 AM', () => {
        const timeFormatter1 = new TimeFormatter('1200')
          .addColon()
          .addTimeOfDay('P');
        const timeFormatter2 = new TimeFormatter('1200')
          .addColon()
          .addTimeOfDay('A');

        const result = timeFormatter1.createSpan(timeFormatter2);

        const endTimeIsAfterStartTime =
          timeFormatter1.isEndTimeAfterStartTime(result);
        expect(endTimeIsAfterStartTime).toBe(false);
      });

      it("AM → AM: returns false if the starting and ending time's hours are the same, but the starting minutes < ending minutes", () => {
        const timeFormatter1 = new TimeFormatter('1145')
          .addColon()
          .addTimeOfDay('P');
        const timeFormatter2 = new TimeFormatter('1125')
          .addColon()
          .addTimeOfDay('A');

        const result = timeFormatter1.createSpan(timeFormatter2);

        const endTimeIsAfterStartTime =
          timeFormatter1.isEndTimeAfterStartTime(result);
        expect(endTimeIsAfterStartTime).toBe(false);
      });

      it('PM → PM: returns true if the starting hour is 12 and the ending hour is later', () => {
        const timeFormatter1 = new TimeFormatter('1200')
          .addColon()
          .addTimeOfDay('P');
        const timeFormatter2 = new TimeFormatter('0130')
          .addColon()
          .addTimeOfDay('P');

        const result = timeFormatter1.createSpan(timeFormatter2);

        const endTimeIsAfterStartTime =
          timeFormatter1.isEndTimeAfterStartTime(result);

        expect(endTimeIsAfterStartTime).toBe(true);
      });

      it('PM → PM: returns true if the starting time is earlier than the ending time', () => {
        const timeFormatter1 = new TimeFormatter('0120')
          .addColon()
          .addTimeOfDay('P');
        const timeFormatter2 = new TimeFormatter('0130')
          .addColon()
          .addTimeOfDay('P');

        const result = timeFormatter1.createSpan(timeFormatter2);

        const endTimeIsAfterStartTime =
          timeFormatter1.isEndTimeAfterStartTime(result);

        expect(endTimeIsAfterStartTime).toBe(true);
      });

      it('PM → PM: returns true if the starting time is earlier than the ending time, 12PM to something', () => {
        const timeFormatter1 = new TimeFormatter('1200')
          .addColon()
          .addTimeOfDay('P');
        const timeFormatter2 = new TimeFormatter('1230')
          .addColon()
          .addTimeOfDay('P');

        const result = timeFormatter1.createSpan(timeFormatter2);

        const endTimeIsAfterStartTime =
          timeFormatter1.isEndTimeAfterStartTime(result);

        expect(endTimeIsAfterStartTime).toBe(true);
      });

      it('PM → AM: returns true if the starting time is earlier than the ending time, 12PM to something', () => {
        const timeFormatter1 = new TimeFormatter('1200')
          .addColon()
          .addTimeOfDay('P');
        const timeFormatter2 = new TimeFormatter('1230')
          .addColon()
          .addTimeOfDay('A');

        const result = timeFormatter1.createSpan(timeFormatter2);

        const endTimeIsAfterStartTime =
          timeFormatter1.isEndTimeAfterStartTime(result);

        expect(endTimeIsAfterStartTime).toBe(false);
      });

      it('AM → AM: returns true if the starting time is earlier than the ending time, 12AM to something', () => {
        const timeFormatter1 = new TimeFormatter('1200')
          .addColon()
          .addTimeOfDay('A');

        const endTimeIsAfterStartTime =
          timeFormatter1.isEndTimeAfterStartTime('12:00AM-1:30AM');

        expect(endTimeIsAfterStartTime).toBe(true);
      });

      it('AM → AM: returns true if the starting time is earlier than the ending time, 12AM to 12PM+', () => {
        const timeFormatter1 = new TimeFormatter('1200')
          .addColon()
          .addTimeOfDay('A');

        const endTimeIsAfterStartTime =
          timeFormatter1.isEndTimeAfterStartTime('12:04AM-12:30PM');

        expect(endTimeIsAfterStartTime).toBe(true);
      });

      it('AM → AM: returns false if the starting time is earlier than the ending time, 11AM to 12PM+', () => {
        const timeFormatter1 = new TimeFormatter().addColon().addTimeOfDay('A');

        const endTimeIsAfterStartTime =
          timeFormatter1.isEndTimeAfterStartTime('11:00AM-12:30AM');

        expect(endTimeIsAfterStartTime).toBe(false);
      });

      it('AM → AM: returns true if the starting time is earlier than the ending time, 9AM to 11AM', () => {
        const timeFormatter1 = new TimeFormatter().addColon().addTimeOfDay('A');

        const endTimeIsAfterStartTime =
          timeFormatter1.isEndTimeAfterStartTime('9:00AM-11:00AM');

        expect(endTimeIsAfterStartTime).toBe(true);
      });
    });
  });

  describe('createUnadjustedSpan', () => {
    it("merges start time and end time together without adjusting the end time's pmCode", () => {
      const timeFormatter1 = new TimeFormatter('1000')
        .addColon()
        .addTimeOfDay('A');
      const timeFormatter2 = new TimeFormatter('0400')
        .addColon()
        .addTimeOfDay('A');

      const result = timeFormatter1.createUnadjustedSpan(timeFormatter2);
      expect(result).toBe('10:00AM-4:00AM');
    });

    it('handles invalid input', () => {
      const timeFormatter1 = new TimeFormatter('1000')
        .addColon()
        .addTimeOfDay('A');

      const result = timeFormatter1.createUnadjustedSpan();
      expect(result).toBe('');
    });
  });

  describe('getOriginalTime', () => {
    it('removes colon and pmCode from time', () => {
      const timeFormatter = new TimeFormatter('0130')
        .addColon()
        .addTimeOfDay('P');

      expect(timeFormatter.getOriginalTime()).toBe('0130');
    });

    it('works when the time has > 10 hours', () => {
      const timeFormatter = new TimeFormatter('1205')
        .addColon()
        .addTimeOfDay('A');

      expect(timeFormatter.getOriginalTime()).toBe('1205');
    });
  });

  describe('getPmCodeFromFormattedTime', () => {
    describe('returns pm code given a time with a colon and time of day', () => {
      it('works for AM times', () => {
        const timeFormatter = new TimeFormatter('1205')
          .addColon()
          .addTimeOfDay('P');

        expect(timeFormatter.getPmCodeFromFormattedTime('11:00AM')).toBe('A');
      });

      it('works for PM times', () => {
        const timeFormatter = new TimeFormatter('0142')
          .addColon()
          .addTimeOfDay('A');

        expect(timeFormatter.getPmCodeFromFormattedTime('3:00PM')).toBe('P');
      });
    });
  });
});
