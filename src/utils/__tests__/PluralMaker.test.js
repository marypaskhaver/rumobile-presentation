import PluralMaker from '../PluralMaker';

describe('sentenceWithQuantityAndSubject', () => {
  describe('makePlural', () => {
    describe('when given a quantity and a singular item, it returns one line', () => {
      it('with a quantity and singular item if the quantity is 1', () => {
        const pluralMaker = new PluralMaker('credit');
        expect(pluralMaker.sentenceWithQuantityAndSubject(1)).toBe('1 credit');
      });

      it('with a quantity and item in plural form if the quantity is > 1', () => {
        const pluralMaker = new PluralMaker('Credit');
        expect(pluralMaker.sentenceWithQuantityAndSubject(2)).toBe('2 Credits');
      });

      it('with a quantity and item in plural form if the quantity is 0', () => {
        const pluralMaker = new PluralMaker('CREDIT');
        expect(pluralMaker.sentenceWithQuantityAndSubject(0)).toBe('0 CREDITs');
      });
    });
  });

  describe('itemWithLetterSAdded', () => {
    it('returns the singular item with an s appended at its end', () => {
      const pluralMaker = new PluralMaker('DOG');
      expect(pluralMaker.itemWithLetterSAdded()).toBe('DOGs');
    });
  });
});
