import getMinutesBetweenDates from '../getMinutesBetweenDates';

describe('getMinutesBetweenDates', () => {
  it('returns the minutes between two times', () => {
    const currentDate = new Date('2022-06-01T22:08:15-04:00');
    const futureDate = new Date('2022-06-01T22:11:18-04:00');

    const minutesDifference = getMinutesBetweenDates(futureDate, currentDate);

    expect(minutesDifference).toBe(3.05);
  });
});
