import getCurrentFoodTimeOfDay from '../getCurrentFoodTimeOfDay';

describe('getCurrentFoodTimeOfDay', () => {
  beforeAll(() => {
    jest.useFakeTimers('modern');
  });

  describe("if it's after 7 AM", () => {
    it("says it's breakfast-time", () => {
      const date = new Date('Fri Sep 30 2022 07:25:23');
      jest.setSystemTime(date);
      expect(getCurrentFoodTimeOfDay()).toBe('breakfast');
    });
  });

  describe("if it's after 11 AM", () => {
    it("says it's lunch-time", () => {
      const date = new Date('Fri Sep 30 2022 11:25:23');
      jest.setSystemTime(date);
      expect(getCurrentFoodTimeOfDay()).toBe('lunch');
    });
  });

  describe("if it's after 5 PM", () => {
    it("says it's dinner-time", () => {
      const date = new Date('Fri Sep 30 2022 17:25:23');
      jest.setSystemTime(date);
      expect(getCurrentFoodTimeOfDay()).toBe('dinner');
    });
  });

  describe("if it's between 9 - 11 PM", () => {
    describe("if it's Monday - Thursday", () => {
      it("says it's takeout-time", () => {
        const date = new Date('Thu Sep 29 2022 22:25:23');
        jest.setSystemTime(date);
        expect(getCurrentFoodTimeOfDay()).toBe('takeout');
      });
    });

    describe("if it's Friday", () => {
      it("says it's dinner-time", () => {
        const date = new Date('Fri Sep 30 2022 22:25:23');
        jest.setSystemTime(date);
        expect(getCurrentFoodTimeOfDay()).toBe('dinner');
      });
    });

    describe("if it's Saturday", () => {
      it("says it's dinner-time", () => {
        const date = new Date('Sat Oct 1 2022 22:25:23');
        jest.setSystemTime(date);
        expect(getCurrentFoodTimeOfDay()).toBe('dinner');
      });
    });

    describe("if it's Sunday", () => {
      it("says it's dinner-time", () => {
        const date = new Date('Sun Oct 2 2022 22:25:23');
        jest.setSystemTime(date);
        expect(getCurrentFoodTimeOfDay()).toBe('dinner');
      });
    });
  });

  describe("if it's after 11 PM", () => {
    it("says it's breakfast-time", () => {
      const date = new Date('Fri Sep 29 2022 0:00:00');
      jest.setSystemTime(date);
      expect(getCurrentFoodTimeOfDay()).toBe('breakfast');
    });
  });

  describe("if it's after 11 PM", () => {
    describe("if it's Monday - Thursday", () => {
      it("says it's breakfast-time", () => {
        const date = new Date('Thu Sep 29 2022 23:25:23');
        jest.setSystemTime(date);
        expect(getCurrentFoodTimeOfDay()).toBe('breakfast');
      });
    });

    describe("if it's not Monday - Thursday", () => {
      it("says it's breakfast-time", () => {
        const date = new Date('Fri Sep 29 2022 23:25:23');
        jest.setSystemTime(date);
        expect(getCurrentFoodTimeOfDay()).toBe('breakfast');
      });
    });
  });
});
