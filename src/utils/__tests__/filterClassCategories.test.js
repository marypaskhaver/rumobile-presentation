import filterClassCategories from '../filterClassCategories';

describe('filterClassCategories', () => {
  it('given a search query and a complete list of class categories, returns class categories whose data contains the query', () => {
    const classCategories = [
      {
        title: 'A',
        data: [
          {displayTitle: 'Astronomy', code: 'ASTR-101'},
          {displayTitle: 'Arithmancy', code: 'ARIT-101'},
          {displayTitle: 'Advanced Arithmancy Studies', code: 'ARIT-102'},
        ],
      },
      {
        title: 'C',
        data: [
          {displayTitle: 'Care of Magical Creatures', code: 'COMC-101'},
          {displayTitle: 'Charms', code: 'CHRM-101'},
        ],
      },
      {title: 'F', data: [{displayTitle: 'Flying', code: 'FLY-101'}]},
    ];

    const searchQuery = 'Arithmancy';
    const expectedFilteredClassCategories = [
      {
        title: 'A',
        data: [
          {displayTitle: 'Arithmancy', code: 'ARIT-101'},
          {displayTitle: 'Advanced Arithmancy Studies', code: 'ARIT-102'},
        ],
      },
    ];

    const actualFilteredClassCategories = filterClassCategories(
      classCategories,
      searchQuery,
    );
    expect(actualFilteredClassCategories).toEqual(
      expectedFilteredClassCategories,
    );
  });
});
