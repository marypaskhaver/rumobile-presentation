import DateFormatter from '../DateFormatter';

describe('DateFormatter', () => {
  describe('constructor', () => {
    it('it uses the current date if not provided with one', () => {
      const dateFormatter1 = new DateFormatter();

      const todaysDate = new Date();
      const dateFormatter2 = new DateFormatter(todaysDate);

      expect(dateFormatter1.date.getFullYear()).toBe(
        dateFormatter2.date.getFullYear(),
      );
      expect(dateFormatter1.date.getMonth()).toBe(
        dateFormatter2.date.getMonth(),
      );
      expect(dateFormatter1.date.getDay()).toBe(dateFormatter2.date.getDay());
      expect(dateFormatter1.date.getDate()).toBe(dateFormatter2.date.getDate());
    });
  });

  describe('getMonth', () => {
    it('generates the month given a date of the first day of the month', () => {
      const march1 = new Date(2022, 2);
      const dateFormatter = new DateFormatter(march1);

      expect(dateFormatter.getMonth()).toEqual('MARCH');
    });

    it('generates the month given the date of the last day of the month', () => {
      const jan31 = new Date(2022, 0, 31);
      const dateFormatter = new DateFormatter(jan31);

      expect(dateFormatter.getMonth()).toEqual('JANUARY');
    });

    it('generates the month given a date of a leap year day', () => {
      const feb29 = new Date(2020, 1, 29);
      const dateFormatter = new DateFormatter(feb29);

      expect(dateFormatter.getMonth()).toEqual('FEBRUARY');
    });

    it('generates the month of APRIL given a date in April', () => {
      const apr15 = new Date(2022, 3, 15);
      const dateFormatter = new DateFormatter(apr15);

      expect(dateFormatter.getMonth()).toEqual('APRIL');
    });

    it('generates the month of MAY given a date in May', () => {
      const may15 = new Date(2022, 4, 15);
      const dateFormatter = new DateFormatter(may15);

      expect(dateFormatter.getMonth()).toEqual('MAY');
    });

    it('generates the month of JUNE given a date in June', () => {
      const jun15 = new Date(2022, 5, 15);
      const dateFormatter = new DateFormatter(jun15);

      expect(dateFormatter.getMonth()).toEqual('JUNE');
    });

    it('generates the month of JULY given a date in July', () => {
      const jul15 = new Date(2022, 6, 15);
      const dateFormatter = new DateFormatter(jul15);

      expect(dateFormatter.getMonth()).toEqual('JULY');
    });

    it('generates the month of AUGUST given a date in August', () => {
      const aug14 = new Date(2022, 7, 14);
      const dateFormatter = new DateFormatter(aug14);

      expect(dateFormatter.getMonth()).toEqual('AUGUST');
    });

    it('generates the month of SEPTEMBER given a date in September', () => {
      const sep17 = new Date(2022, 8, 17);
      const dateFormatter = new DateFormatter(sep17);

      expect(dateFormatter.getMonth()).toEqual('SEPTEMBER');
    });

    it('generates the month of OCTOBER given a date in October', () => {
      const oct3 = new Date(2022, 9, 3);
      const dateFormatter = new DateFormatter(oct3);

      expect(dateFormatter.getMonth()).toEqual('OCTOBER');
    });

    it('generates the month of NOVEMBER given a date in November', () => {
      const nov18 = new Date(2022, 10, 18);
      const dateFormatter = new DateFormatter(nov18);

      expect(dateFormatter.getMonth()).toEqual('NOVEMBER');
    });

    it('generates the month of DECEMBER given a date in December', () => {
      const dec25 = new Date(2022, 11, 25);
      const dateFormatter = new DateFormatter(dec25);

      expect(dateFormatter.getMonth()).toEqual('DECEMBER');
    });

    it('generates null given an invalid date', () => {
      const badDate = new Date(NaN);
      const dateFormatter = new DateFormatter(badDate);

      expect(dateFormatter.getMonth()).toEqual(null);
    });
  });

  describe('getWeekday', () => {
    it('generates SUNDAY given a date of some Sunday', () => {
      const mar29 = new Date(2022, 2, 20);
      const dateFormatter = new DateFormatter(mar29);

      expect(dateFormatter.getWeekday()).toEqual('SUNDAY');
    });

    it('generates MONDAY given a date of some Monday', () => {
      const sep15 = new Date(2003, 8, 15);
      const dateFormatter = new DateFormatter(sep15);

      expect(dateFormatter.getWeekday()).toEqual('MONDAY');
    });

    it('generates TUESDAY given a date of some Tuesday', () => {
      const jan1 = new Date(2019, 0, 1);
      const dateFormatter = new DateFormatter(jan1);

      expect(dateFormatter.getWeekday()).toEqual('TUESDAY');
    });

    it('generates WEDNESDAY given a date of some Wednesday', () => {
      const mar23 = new Date(2022, 2, 23);
      const dateFormatter = new DateFormatter(mar23);

      expect(dateFormatter.getWeekday()).toEqual('WEDNESDAY');
    });

    it('generates THURSDAY given a date of some Thursday', () => {
      const sep16 = new Date(2021, 8, 16);
      const dateFormatter = new DateFormatter(sep16);

      expect(dateFormatter.getWeekday()).toEqual('THURSDAY');
    });

    it('generates FRIDAY given a date of some Friday', () => {
      const dec10 = new Date(2021, 11, 10);
      const dateFormatter = new DateFormatter(dec10);

      expect(dateFormatter.getWeekday()).toEqual('FRIDAY');
    });

    it('generates SATURDAY given a date of some Saturday', () => {
      const apr14 = new Date(2012, 3, 14);
      const dateFormatter = new DateFormatter(apr14);

      expect(dateFormatter.getWeekday()).toEqual('SATURDAY');
    });

    it('generates null given an invalid date', () => {
      const badDate = new Date(NaN);
      const dateFormatter = new DateFormatter(badDate);

      expect(dateFormatter.getWeekday()).toEqual(null);
    });
  });

  describe('getDay', () => {
    it('returns the day of the week for the specified date according to local time, where 0 represents Sunday, 1 represents Monday, etc', () => {
      const sep17 = new Date(2022, 9, 17);
      const dateFormatter = new DateFormatter(sep17);

      expect(dateFormatter.getDay()).toEqual(1);
    });
  });

  describe('getYear', () => {
    it('returns the year of the specified date', () => {
      const sep17 = new Date(2022, 9, 17);
      const dateFormatter = new DateFormatter(sep17);

      expect(dateFormatter.getYear()).toEqual(2022);
    });
  });

  describe('getFormattedDate', () => {
    it('generates a date in the form WEEKDAY, MONTH NUMBER_OF_DAY', () => {
      const mar29 = new Date(2022, 2, 20);
      const dateFormatter = new DateFormatter(mar29);

      expect(dateFormatter.getFormattedDate()).toEqual('SUNDAY, MARCH 20');
    });

    it('generates a leap year day in the form WEEKDAY, MONTH NUMBER_OF_DAY', () => {
      const feb29 = new Date(2020, 1, 29);
      const dateFormatter = new DateFormatter(feb29);

      expect(dateFormatter.getFormattedDate()).toEqual('SATURDAY, FEBRUARY 29');
    });
  });
});
