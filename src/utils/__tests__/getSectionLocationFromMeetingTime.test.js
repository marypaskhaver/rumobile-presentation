import getSectionLocationFromMeetingTime from '../getSectionLocationFromMeetingTime';

describe('getSectionLocationFromMeetingTime', () => {
  it('provides the name of the building the section is supposed to meet in given its meeting times', () => {
    const sectionMeetingTimes = {
      meetingDay: 'F',
      buildingCode: 'BRR',
    };

    const buildingName = getSectionLocationFromMeetingTime(sectionMeetingTimes);

    expect(buildingName).toBe('Business Rockafeller Road');
  });

  it('provides the name of the building the section is supposed to meet in if the building can be referred to by multiple abbreviations', () => {
    const sectionMeetingTimes1 = {
      meetingDay: 'S',
      buildingCode: 'EE',
    };
    const buildingName1 =
      getSectionLocationFromMeetingTime(sectionMeetingTimes1);
    expect(buildingName1).toBe('Electrical Engineering Building');

    const sectionMeetingTimes2 = {
      meetingDay: 'S',
      buildingCode: 'ENGN',
    };
    const buildingName2 =
      getSectionLocationFromMeetingTime(sectionMeetingTimes2);
    expect(buildingName2).toBe('Electrical Engineering Building');
  });

  it('handles invalid given building codes', () => {
    const sectionMeetingTimes = {
      meetingDay: 'F',
      buildingCode: 'MARKET_SNODSBURY',
    };

    const buildingName = getSectionLocationFromMeetingTime(sectionMeetingTimes);

    expect(buildingName).toBe('MARKET_SNODSBURY');
  });

  it('handles invalid non-existent building codes', () => {
    const sectionMeetingTimes = {
      meetingDay: 'S',
      buildingCode: null,
    };

    const buildingName = getSectionLocationFromMeetingTime(sectionMeetingTimes);

    expect(buildingName).toBe('No building data');
  });

  it('handles independent study sections', () => {
    const sectionMeetingTimes = {
      meetingDay: null,
      buildingCode: null,
    };

    const buildingName = getSectionLocationFromMeetingTime(sectionMeetingTimes);

    expect(buildingName).toBe('Independent Study');
  });
});
