import sortSemesters from '../sortSemesters';

describe('sortSemesters', () => {
  it('sorts a list of semesters in ascending year and season, given the semesters - test 1', () => {
    const semesters = ['12022', '92021', '02022'];
    const sortedSemesters = sortSemesters(semesters);
    expect(sortedSemesters).toEqual(['92021', '02022', '12022']);
    // Fall 2021, Spring 2022,  Winter 2022
  });

  it('sorts a list of semesters in ascending year and season, given the semesters - test 2', () => {
    const semesters = ['92021', '02022', '12022'];
    const sortedSemesters = sortSemesters(semesters);
    expect(sortedSemesters).toEqual(['92021', '02022', '12022']);
  });

  it('sorts a list of semesters in ascending year and season, semesters have same season', () => {
    const semesters = ['92021', '02022', '02022'];
    const sortedSemesters = sortSemesters(semesters);
    expect(sortedSemesters).toEqual(['92021', '02022', '02022']);
  });
});
