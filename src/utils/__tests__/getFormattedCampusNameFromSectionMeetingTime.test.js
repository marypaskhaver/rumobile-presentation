import getFormattedCampusNameFromSectionMeetingTime from '../getFormattedCampusNameFromSectionMeetingTime';

describe('getFormattedCampusNameFromSectionMeetingTime', () => {
  it('returns the campus name in title case if provided in the meeting time', () => {
    const meetingTime = {
      campusName: 'LIVINGSTON',
    };
    const finalCampusName =
      getFormattedCampusNameFromSectionMeetingTime(meetingTime);

    expect(finalCampusName).toBe('Livingston');
  });

  it('returns the campus name in title case if provided in the meeting time', () => {
    const meetingTime = {
      campusName: 'DOUGLAS/COOK',
    };
    const finalCampusName =
      getFormattedCampusNameFromSectionMeetingTime(meetingTime);

    expect(finalCampusName).toBe('Cook/Douglass');
  });

  it("returns 'Rutgers' if the given section has no designated campus", () => {
    const meetingTime = {
      campusName: null,
    };
    const finalCampusName =
      getFormattedCampusNameFromSectionMeetingTime(meetingTime);

    expect(finalCampusName).toBe('Rutgers');
  });

  it('handles invalid input', () => {
    const meetingTime = {
      campusName: '** INVALID **',
    };
    const finalCampusName =
      getFormattedCampusNameFromSectionMeetingTime(meetingTime);

    expect(finalCampusName).toBe('** INVALID **');
  });

  it('handles off campus classes', () => {
    const meetingTime = {
      campusName: null,
      campusAbbrev: 'OFF',
    };
    const finalCampusName =
      getFormattedCampusNameFromSectionMeetingTime(meetingTime);

    expect(finalCampusName).toBe('OFF CAMPUS');
  });
});
