import {DefaultTheme} from '@react-navigation/native';
import {CustomDarkTheme} from '../../constants/appThemes';
import getThemeIDFromTheme from '../getThemeIDFromTheme';

describe('getThemeIDFromTheme', () => {
  describe('returns the ID of a theme given the theme', () => {
    it('works for light theme', () => {
      const lightTheme = DefaultTheme;
      const ID = getThemeIDFromTheme(lightTheme);
      expect(ID).toEqual(1);
    });

    it('works for dark theme', () => {
      const ID = getThemeIDFromTheme(CustomDarkTheme);
      expect(ID).toEqual(2);
    });
  });
});
