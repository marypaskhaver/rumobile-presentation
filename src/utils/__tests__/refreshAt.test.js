import refreshAt from '../refreshAt';

describe('refreshAt', () => {
  it('refreshes at a specified time', () => {
    setTimeout = jest.fn();

    const hours = 1;
    const minutes = 1;
    const seconds = 1;
    const callback = () => {};

    refreshAtComponent = refreshAt(hours, minutes, seconds, callback);

    expect(setTimeout).toHaveBeenCalledTimes(1);
  });

  it('refreshes at a specified time when now and the time to be refreshed at are separated only by seconds', () => {
    setTimeout = jest.fn();

    const now = new Date();

    const hours = now.getHours();
    const minutes = now.getMinutes();
    const seconds = now.getSeconds();
    const callback = () => {};

    refreshAtComponent = refreshAt(hours, minutes, seconds, callback);

    expect(setTimeout).toHaveBeenCalledTimes(1);
  });
});
