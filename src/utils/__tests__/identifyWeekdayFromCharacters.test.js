import identifyWeekdayFromCharacters from '../identifyWeekdayFromCharacters';

describe('identifyWeekdayFromCharacters', () => {
  it('returns the full weekday that starts with the given character', () => {
    const character = 'M';
    const weekday = identifyWeekdayFromCharacters(character);
    expect(weekday).toBe('Monday');
  });

  it('returns the full weekday that starts with multiple characters', () => {
    const character = 'TH';
    const weekday = identifyWeekdayFromCharacters(character);
    expect(weekday).toBe('Thursday');
  });

  it('identifies asynchronous classes from a U character', () => {
    const character = 'U';
    const weekday = identifyWeekdayFromCharacters(character);
    expect(weekday).toBe('Sunday');
  });

  it('identifies invalid arguments', () => {
    const weekday = identifyWeekdayFromCharacters(null);
    expect(weekday).toBe('Independent Study');
  });
});
