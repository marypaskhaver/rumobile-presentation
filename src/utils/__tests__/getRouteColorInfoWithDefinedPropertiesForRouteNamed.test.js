import getRouteColorInfoWithDefinedPropertiesForRouteNamed from '../getRouteColorInfoWithDefinedPropertiesForRouteNamed';

describe('getRouteColorInfoWithDefinedPropertiesForRouteNamed', () => {
  it("creates a route color info object with a defined tag and gray color given an unknown route's name", () => {
    const result =
      getRouteColorInfoWithDefinedPropertiesForRouteNamed('Cat Bus');
    expect(result).toEqual({
      rtag: 'C Bus',
      rname: 'Cat Bus',
      rcolor: 'rgb(74, 74, 74)',
    });
  });

  it("creates a route color info object with a defined tag and gray color given a known route's name", () => {
    const result =
      getRouteColorInfoWithDefinedPropertiesForRouteNamed('Route A');
    expect(result).toEqual({
      rtag: 'A',
      rname: 'Route A',
      rcolor: 'rgb(124, 31, 206)',
    });
  });
});
