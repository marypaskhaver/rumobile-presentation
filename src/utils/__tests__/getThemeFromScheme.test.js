import {DefaultTheme} from '@react-navigation/native';
import {CustomDarkTheme} from '../../constants/appThemes';
import getThemeFromScheme from '../getThemeFromScheme';

describe('getThemeFromScheme', () => {
  it("returns light theme when given a scheme that indicates the user's phone is in light mode", () => {
    const lightTheme = DefaultTheme;
    const scheme = 'light';
    const themeFromScheme = getThemeFromScheme(scheme);
    expect(themeFromScheme).toEqual(lightTheme);
  });

  it("returns dark theme when given a scheme that indicates the user's phone is in dark mode", () => {
    const scheme = 'dark';
    const themeFromScheme = getThemeFromScheme(scheme);
    expect(themeFromScheme).toEqual(CustomDarkTheme);
  });
});
