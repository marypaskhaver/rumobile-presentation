import {campusColor} from '../../bus_color.json';

export default getFormattedCampusNameFromSectionMeetingTime =
  sectionMeetingTime => {
    if (sectionMeetingTime.campusName === '** INVALID **')
      return sectionMeetingTime.campusName;

    const campus = campusColor.find(
      campusItem => campusItem.campus == sectionMeetingTime.campusName,
    );

    if (!campus) {
      if (sectionMeetingTime.campusAbbrev === 'OFF') return 'OFF CAMPUS';

      return 'Rutgers';
    }

    return campus.cName;
  };
