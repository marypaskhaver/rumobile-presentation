export default sortSemesters = semesters => {
  return semesters.sort((semesterID1, semesterID2) => {
    const semesterYear1 = Number(semesterID1.slice(1));
    const semesterSeason1 = Number(semesterID1.charAt(0));

    const semesterYear2 = Number(semesterID2.slice(1));
    const semesterSeason2 = Number(semesterID2.charAt(0));

    // Compare years
    if (semesterYear1 < semesterYear2) return -1;
    if (semesterYear1 > semesterYear2) return 1;

    // Compare seasons
    if (semesterSeason1 < semesterSeason2) return -1;
    else if (semesterSeason1 > semesterSeason2) return 1;
    else return 0;
  });
};
