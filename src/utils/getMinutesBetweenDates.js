export default getMinutesBetweenDates = (futureDate, currentDate) => {
  const timeDifference = futureDate.getTime() - currentDate.getTime();
  const minuteDifference = timeDifference / 60 / 1000;
  return minuteDifference;
};
