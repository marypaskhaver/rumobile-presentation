export default formatInstructorNames = listOfInstructors => {
  if (!listOfInstructors || listOfInstructors.length === 0)
    return 'NO INSTRUCTOR LISTED';

  let formattedNames = '';
  listOfInstructors.forEach(
    instructor =>
      (formattedNames = formattedNames.concat(`${instructor.name}; `)),
  );

  const formattedNamesWithoutLastColonAndSpace = formattedNames.substring(
    0,
    formattedNames.length - 2,
  );

  return formattedNamesWithoutLastColonAndSpace;
};
