import {routeColor} from '../../bus_color.json';
import {GRAY_6} from '../constants/colors';
import createBusRouteTagFromName from './createBusRouteTagFromName';

export default getRouteColorInfoWithDefinedPropertiesForRouteNamed =
  routeName => {
    const storedRouteColorInfo = routeColor.find(
      route => route.rname === routeName,
    );

    if (!storedRouteColorInfo) {
      return {
        rtag: createBusRouteTagFromName(routeName),
        rname: routeName,
        rcolor: GRAY_6,
      };
    }

    const safeRouteColorInfo = {
      rtag: storedRouteColorInfo.rtag
        ? storedRouteColorInfo.rtag
        : createBusRouteTagFromName(routeName),
      rname: routeName,
      rcolor: storedRouteColorInfo.rcolor
        ? storedRouteColorInfo.rcolor
        : GRAY_6,
    };

    return safeRouteColorInfo;
  };
