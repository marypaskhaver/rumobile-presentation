export default class BusRouteInformation {
  constructor(busRoutes = [], activeVehicles = []) {
    this.busRoutes = busRoutes;
    this.activeVehicles = activeVehicles;
  }

  getActiveRoutes = () => {
    if (!this.hasFunctionalBusData()) return [];

    // Get the IDs of buses which have arrival estimates
    const busesWithArrivalEstimates = []
      .concat(this.activeVehicles)
      .filter(activeVehicle => activeVehicle.arrival_estimates.length > 0);

    const IDsOfBusesWithArrivalEstimates = [
      ...new Set(
        busesWithArrivalEstimates
          .map(activeVehicle => activeVehicle.route_id)
          .flat(),
      ),
    ];

    // Remove any buses from active routes that do not have bus predictions
    const activeRoutes = this.busRoutes.filter(busRoute =>
      IDsOfBusesWithArrivalEstimates.includes(busRoute.route_id),
    );

    return activeRoutes;
  };

  getInactiveRoutes = () => {
    if (!this.hasFunctionalBusData()) return [];

    const activeRoutes = this.getActiveRoutes();

    const IDsOfBusesWithActiveRoutes = activeRoutes.map(
      busRoute => busRoute.route_id,
    );

    return this.busRoutes.filter(
      busRoute => !IDsOfBusesWithActiveRoutes.includes(busRoute.route_id),
    );
  };

  hasFunctionalBusData = () => {
    return this.busRoutes && this.busRoutes.length > 0;
  };
}
