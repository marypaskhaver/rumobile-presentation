export default createBusRouteTagFromName = routeName => {
  const words = routeName.split(' ');
  let tag = '';

  for (let i = 0; i < words.length - 1; i++) {
    const word = words[i];
    tag += word.charAt(0);
  }

  tag += ` ${words[words.length - 1]}`;

  return tag;
};
