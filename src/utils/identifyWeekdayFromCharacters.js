export default identifyWeekdayFromCharacters = letter => {
  if (!letter) return 'Independent Study';

  const letterToWeekday = {
    M: 'Monday',
    T: 'Tuesday',
    W: 'Wednesday',
    TH: 'Thursday',
    F: 'Friday',
    S: 'Saturday',
    U: 'Sunday',
  };

  return letterToWeekday[letter];
};
