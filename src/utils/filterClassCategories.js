export default filterClassCategories = (classCategories, text) => {
  let filteredClassCategories = [];

  classCategories.filter(classCategory => {
    const filtered = classCategory.data.filter(academicClass =>
      academicClass.displayTitle.toUpperCase().includes(text.toUpperCase()),
    );

    if (filtered && filtered.length > 0) {
      const filteredClass = {title: classCategory.title, data: filtered};
      filteredClassCategories = filteredClassCategories.concat(filteredClass);
    }
  });

  return filteredClassCategories;
};
