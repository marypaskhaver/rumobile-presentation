import React from 'react';
import store from '../redux/store';
import {updateChosenOverrideThemeID} from '../redux/actions/themeActions';
import {updateChosenCampusID} from '../redux/actions/campusActions';

import {
  CAMPUS_STORAGE_KEY,
  CLASSES_CATEGORIES_FILTER_CAMPUSES_STORAGE_KEY,
  CLASSES_CATEGORIES_FILTER_EDUCATION_LEVEL_STORAGE_KEY,
  CLASSES_CATEGORIES_FILTER_SEMESTER_STORAGE_KEY,
  FAVORITE_BUS_STOPS_STORAGE_KEY,
  FAVORITE_SECTIONS_STORAGE_KEY,
  OVERRIDE_THEME_CHOICE_STORAGE_KEY,
  PASSED_WELCOME_SCREENS_STORAGE_KEY,
  SORT_FAVORITE_BUSES_STORAGE_KEY,
} from '../constants/storageKeys';

import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  addFavoriteSection,
  setCampuses,
  setLevels,
  updateSemester,
} from '../redux/actions/academicActions';
import {
  loadWelcomeScreen,
  passWelcomeScreen,
} from '../redux/actions/welcomeScreensActions';
import {
  addFavoriteBusStopIDWithRouteID,
  sortFavoriteBuses,
} from '../redux/actions/busesActions';

const loadStore = () => {
  AsyncStorage.getItem(PASSED_WELCOME_SCREENS_STORAGE_KEY, (error, value) => {
    if (!error) {
      store.dispatch(passWelcomeScreen(JSON.parse(!!value)));
      store.dispatch(loadWelcomeScreen(JSON.parse(true)));
    }
  });

  AsyncStorage.getItem(SORT_FAVORITE_BUSES_STORAGE_KEY, (error, value) => {
    if (!error && !!value) {
      store.dispatch(sortFavoriteBuses(JSON.parse(value)));
    }
  });

  AsyncStorage.getItem(FAVORITE_SECTIONS_STORAGE_KEY, (error, value) => {
    if (!error && !!value) {
      const sections = Object.values(JSON.parse(value));

      for (const section of sections) {
        store.dispatch(addFavoriteSection(section));
      }
    }
  });

  AsyncStorage.getItem(FAVORITE_BUS_STOPS_STORAGE_KEY, (error, value) => {
    if (!error && !!value) {
      const busStopIds = JSON.parse(value);

      Object.keys(busStopIds).forEach(stopID => {
        const routeIds = busStopIds[stopID];

        routeIds.forEach(routeID => {
          store.dispatch(
            addFavoriteBusStopIDWithRouteID({
              stopID,
              routeID,
            }),
          );
        });
      });
    }
  });

  AsyncStorage.getItem(CAMPUS_STORAGE_KEY, (error, value) => {
    if (!error) {
      if (value !== null) {
        store.dispatch(updateChosenCampusID(JSON.parse(value)));
      }
    }
  });

  AsyncStorage.getItem(OVERRIDE_THEME_CHOICE_STORAGE_KEY, (error, value) => {
    if (!error) {
      store.dispatch(updateChosenOverrideThemeID(JSON.parse(value)));
    }
  });

  AsyncStorage.getItem(
    CLASSES_CATEGORIES_FILTER_SEMESTER_STORAGE_KEY,
    (error, value) => {
      if (!error) store.dispatch(updateSemester(JSON.parse(value)));
    },
  );

  AsyncStorage.getItem(
    CLASSES_CATEGORIES_FILTER_EDUCATION_LEVEL_STORAGE_KEY,
    (error, value) => {
      if (!error) {
        const levels = JSON.parse(value);
        store.dispatch(setLevels(levels));
      }
    },
  );

  AsyncStorage.getItem(
    CLASSES_CATEGORIES_FILTER_CAMPUSES_STORAGE_KEY,
    (error, value) => {
      if (!error) {
        const campuses = JSON.parse(value);
        store.dispatch(setCampuses(campuses));
      }
    },
  );
};

export default loadStore;
