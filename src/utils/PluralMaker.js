export default class PluralMaker {
  constructor(singularItem) {
    this.singularItem = singularItem;
  }

  sentenceWithQuantityAndSubject(quantity) {
    return `${quantity} ${this.itemFormattedForQuantity(quantity)}`;
  }

  itemFormattedForQuantity(quantity) {
    return quantity === 1 ? this.singularItem : this.itemWithLetterSAdded();
  }

  itemWithLetterSAdded() {
    return `${this.singularItem}s`;
  }
}
