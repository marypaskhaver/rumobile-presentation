import DateFormatter from './DateFormatter';

export default getCurrentFoodTimeOfDay = () => {
  const now = new Date();
  const hoursNow = now.getHours();
  const today = new DateFormatter(now).getWeekday();

  const todayIsEndOfWeekDay =
    today === 'FRIDAY' || today === 'SATURDAY' || today === 'SUNDAY';

  const defaultTimeOfDay =
    hoursNow >= 21 && hoursNow < 23 && !todayIsEndOfWeekDay
      ? 'takeout'
      : hoursNow >= 17 && (hoursNow < 21 || todayIsEndOfWeekDay)
      ? 'dinner'
      : hoursNow >= 11 && hoursNow < 17
      ? 'lunch'
      : 'breakfast';

  return defaultTimeOfDay;
};
