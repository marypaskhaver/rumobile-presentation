export default class TimeFormatter {
  constructor(unformattedTime) {
    this.time = unformattedTime || '';
  }

  addColon() {
    if (!this.time) return this;

    const hours = Number.parseInt(this.time.slice(0, 2));
    const minutes = this.time.slice(2);

    this.time = `${hours}:${minutes}`;

    // Builder Pattern: Allows swappable method chaining
    return this;
  }

  addTimeOfDay(pmCode) {
    if (!this.time) return this;

    this.time += `${pmCode.trim()}M`;
    this.pmCode = pmCode;

    return this;
  }

  createSpan(otherTimeFormatter) {
    if (!this.time || !otherTimeFormatter || !otherTimeFormatter.time)
      return '';

    if (!this.isSpanValidAsAClassLength(otherTimeFormatter)) {
      const otherTimeDesignatedInAfternoon =
        otherTimeFormatter.getOppositeTime();

      return `${this.time}-${otherTimeDesignatedInAfternoon}`;
    }

    return `${this.time}-${otherTimeFormatter.time}`;
  }

  createUnadjustedSpan(otherTimeFormatter) {
    if (!this.time || !otherTimeFormatter || !otherTimeFormatter.time)
      return '';

    return `${this.time}-${otherTimeFormatter.time}`;
  }

  isEndTimeAfterStartTime(span) {
    const startAndEndTime = span.split('-');

    const startTimeMeridian = startAndEndTime[0].slice(-2);
    let startTime = Number(startAndEndTime[0].slice(0, -2).replace(':', ''));
    if (startTimeMeridian === 'PM' && Math.floor(startTime / 100) !== 12) {
      startTime += 1200;
    }

    const endTimeMeridian = startAndEndTime[1].slice(-2);
    let endTime = Number(startAndEndTime[1].slice(0, -2).replace(':', ''));
    if (endTimeMeridian === 'PM' && Math.floor(endTime / 100) !== 12) {
      endTime += 1200;
    }

    if (startTimeMeridian === 'AM' && endTimeMeridian === 'PM') {
      return true;
    }

    if (startTimeMeridian === 'PM' && endTimeMeridian === 'AM') {
      return false;
    }

    if (startTimeMeridian === 'AM' && endTimeMeridian === 'AM') {
      if (
        (startTime >= 1200 && endTime <= 1200) ||
        (startTime < endTime && endTime <= 1200)
      )
        return true;

      return false;
    }

    return startTime < endTime;
  }

  getStartHour(time) {
    if (!time)
      return Number.parseInt(this.time.substring(0, this.time.indexOf(':')));

    return Number.parseInt(time.substring(0, time.indexOf(':')));
  }

  getMinutes(time) {
    if (!time)
      return Number.parseInt(this.time.substring(this.time.indexOf(':') + 1));

    return Number.parseInt(time.substring(time.indexOf(':') + 1));
  }

  replace0thHourWith12thIfNeeded(time) {
    return time.substring(0, time.indexOf(':')).charAt(0) === '0'
      ? time.replace('0', '12')
      : time;
  }

  // In the JSON data, the pmCode is the same for a section's startTime and
  // endTime, even if the startTime is in the morning (AM) endTime is in the
  // afternoon (PM).
  isSpanValidAsAClassLength(otherTimeFormatter) {
    if (!otherTimeFormatter || !otherTimeFormatter.time) return '';

    const otherTimeFormatterClassHour = this.getStartHour(
      otherTimeFormatter.time,
    );

    if (
      this.pmCode === 'A' &&
      otherTimeFormatter.pmCode === 'A' &&
      otherTimeFormatterClassHour <= 12
    ) {
      if (this.getStartHour() === 12) {
        return true;
      }

      if (
        otherTimeFormatterClassHour < this.getStartHour() ||
        otherTimeFormatterClassHour === 12
      )
        return false;
    } else if (
      this.pmCode === 'P' &&
      otherTimeFormatter.pmCode === 'P' &&
      otherTimeFormatterClassHour >= 12
    ) {
      if (this.getStartHour() < 12) {
        return false;
      }
      return true;
    }

    return true;
  }

  getOppositeTime() {
    if (!this.time || !this.time.includes(':') || !this.time.includes('M'))
      return '';

    const timeWithColonButWithoutTimeOfDay = this.time.replace(
      `${this.pmCode}M`,
      '',
    );

    if (this.pmCode === 'A') return `${timeWithColonButWithoutTimeOfDay}PM`;
    else return `${timeWithColonButWithoutTimeOfDay}AM`;
  }

  getOriginalTime(time) {
    const timeToUse = time ?? this.time;
    if (!timeToUse) return '';

    const timeWithoutPmCode = timeToUse.substring(0, timeToUse.length - 2);

    const hours = Number.parseInt(timeWithoutPmCode.slice(0, 2));
    const minutes = timeWithoutPmCode.slice(-2);

    const adjustedHours = hours < 10 ? `0${hours}` : hours;

    return `${adjustedHours}${minutes}`;
  }

  getPmCodeFromFormattedTime(time) {
    if (!time) return '';

    const meridian = time.slice(-2);
    return meridian === 'PM' ? 'P' : 'A';
  }
}
