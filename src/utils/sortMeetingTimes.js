export default sortMeetingTimes = meetingTimes => {
  const sortedDays = ['null', 'U', 'M', 'T', 'W', 'TH', 'F', 'S'];

  const sortMeetingTimesByDayAndStartTime = (meetingTime1, meetingTime2) => {
    const meetingTime1Index = sortedDays.indexOf(`${meetingTime1.meetingDay}`);
    const meetingTime2Index = sortedDays.indexOf(`${meetingTime2.meetingDay}`);

    if (meetingTime1Index !== meetingTime2Index) {
      return meetingTime1Index < meetingTime2Index ? -1 : 1;
    }

    const startTimeMeeting1 = convertToMilitary(
      meetingTime1.startTime,
      meetingTime1.pmCode,
    );
    const startTimeMeeting2 = convertToMilitary(
      meetingTime2.startTime,
      meetingTime2.pmCode,
    );

    return `${startTimeMeeting1}${meetingTime1.pmCode}`.localeCompare(
      `${startTimeMeeting2}${meetingTime2.pmCode}`,
    );
  };

  const convertToMilitary = (time, pmCode) => {
    let militaryTime = Number(time);

    if (Math.floor(time / 100) === 12 && pmCode === 'A') {
      militaryTime -= 1200;
    }

    if (pmCode === 'P' && Math.floor(time / 100) !== 12) {
      militaryTime += 1200;
    }

    return `${militaryTime < 1000 ? 0 : ''}${militaryTime}`;
  };

  return meetingTimes.sort(sortMeetingTimesByDayAndStartTime);
};
