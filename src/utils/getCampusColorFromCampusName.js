import {campusColor} from '../../bus_color.json';
import {DARK_RED, GRAY_5} from '../constants/colors';

export default getCampusColorFromCampusName = campusName => {
  let color;

  if (campusName) {
    const campus = campusColor.find(
      campusItem => campusItem.campus == campusName,
    );

    color = campus ? campus.ccolor : GRAY_5;
  } else {
    // 'Rutgers'
    color = DARK_RED;
  }

  return color;
};
