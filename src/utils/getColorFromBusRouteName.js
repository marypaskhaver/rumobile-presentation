import {routeColor} from '../../bus_color.json';
import {GRAY_4} from '../constants/colors';

export const getColorFromBusRouteName = routeName => {
  const routeDescription = routeColor.find(route => route.rname === routeName);
  return routeDescription ? routeDescription.rcolor : GRAY_4;
};
