import React from 'react';

import {Container} from '../shared-components/Container';
import {BigText} from '../shared-components/BigText';

// For Dark Mode compatibility
import {useTheme} from '@react-navigation/native';

const TemporaryScreen = ({title}) => {
  const {colors} = useTheme();

  return (
    <Container>
      <BigText style={{marginTop: 64, marginLeft: 20, color: colors.text}}>
        {title}
      </BigText>
    </Container>
  );
};

export default TemporaryScreen;
