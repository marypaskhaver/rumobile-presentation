import React from 'react';
import LinksList from './LinksList';
import {BigText} from '../../shared-components/BigText';
import {useTheme} from '@react-navigation/native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {FocusAwareStatusBar} from '../../shared-components';

const LinksScreen = ({title}) => {
  const {colors} = useTheme();

  return (
    <SafeAreaView style={{flex: 1, top: 20}}>
      <FocusAwareStatusBar />
      <BigText style={{marginTop: 17, marginLeft: 20, color: colors.text}}>
        {title}
      </BigText>
      <LinksList />
    </SafeAreaView>
  );
};

export default LinksScreen;
