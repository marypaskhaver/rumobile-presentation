import React from 'react';

// Holds / displays data in table format
import {
  Alert,
  FlatList,
  Linking,
  Platform,
  TouchableOpacity,
} from 'react-native';

// styled-components library, used for styling components
import styled from 'styled-components/native';

// For Dark Mode compatibility
import {useTheme} from '@react-navigation/native';

import {Row} from '../../shared-components/Row';
import {PASTEL_RED} from '../../constants/colors';
import SafariView from 'react-native-safari-view';

// To hold FlatList "table".
const Container = styled.View`
  margin-top: 8px;
`;

// Styling for the text describing a link in each cell.
const LinkText = styled.Text`
  font-size: 17px;
  padding-vertical: 14px;
  letter-spacing: -0.41px;
  width: 375px;
  margin-left: 17px;
  flex: 1;
`;

// Styling for the image each cell will display.
const Icon = styled.Image`
  width: 24px;
  height: 24px;
  margin-left: 17px;
  margin-bottom: 10px;
  margin-top: 10px;
`;

import {Chevron} from '../../shared-components/Chevron';
import {Line} from '../../shared-components/Line';

const openLink = url => {
  if (Platform.OS === 'android') {
    Linking.openURL(url);
    return;
  }

  SafariView.isAvailable()
    .then(
      SafariView.show({
        url,
        tintColor: '#ed4545',
        barTintColor: 'white',
        readerMode: true,
      }),
    )
    .catch(error => {
      Linking.openURL(url)
        .then(() => {
          console.log('Link opened successfully');
        })
        .catch(() => {
          console.log('Link failed to open.');
          Alert.alert('Unable to open link.');
        });
    });
};

// Consists of Container which holds a FlatList with cells containing table data.
const LinksList = () => {
  // Provides colors for particular elements depending on the current theme (dark or light).
  const {colors} = useTheme();

  return (
    <Container style={{height: '100%'}}>
      <FlatList
        // Data to display in the FlatList table. Consists of link to image asset and
        // the text to display to the right of that image.
        data={[
          {
            imageSource: require('../../assets/icons/links_screen/university.png'),
            text: 'myRutgers',
            link: 'https://cas.rutgers.edu/login?service=https://my.rutgers.edu/portal/Login',
          },
          {
            imageSource: require('../../assets/icons/links_screen/classroom.png'),
            text: 'Canvas',
            link: 'https://canvas.rutgers.edu/',
          },
          {
            imageSource: require('../../assets/icons/links_screen/clock.png'),
            text: 'Library Hours',
            link: 'https://www.libraries.rutgers.edu',
          },
          {
            imageSource: require('../../assets/icons/links_screen/news.png'),
            text: 'Targum',
            link: 'http://www.dailytargum.com/',
          },
          {
            imageSource: require('../../assets/icons/links_screen/reddit.png'),
            text: 'Rutgers Reddit',
            link: 'https://m.reddit.com/r/rutgers/',
          },
          {
            imageSource: require('../../assets/icons/links_screen/monkey.png'),
            text: 'The Medium',
            link: 'https://rutgersthemedium.wordpress.com',
          },
          {
            imageSource: require('../../assets/icons/links_screen/map.png'),
            text: 'Student Organizations',
            link: 'https://rutgers.collegiatelink.net',
          },
          {
            imageSource: require('../../assets/icons/links_screen/exam.png'),
            text: 'Grades',
            link: 'https://cas.rutgers.edu/login?service=https://my.rutgers.edu/portal/Login%3fuP_fname=my-grades&uP_args=',
          },
          {
            imageSource: require('../../assets/icons/links_screen/student.png'),
            text: 'eCollege',
            link: 'https://cas.rutgers.edu/login?service=http%3A%2F%2Fonlinelearning.rutgers.edu%2Facademics.php',
          },
          {
            imageSource: require('../../assets/icons/links_screen/bank.png'),
            text: 'Financial Aid',
            link: 'https://finservices.rutgers.edu/otb/chooseSemester.htm?login=cas',
          },
          {
            imageSource: require('../../assets/icons/links_screen/privacy.png'),
            text: 'Privacy Policy',
            link: 'https://rumobile-cbb58.firebaseapp.com',
          },
        ]}
        // Lines / separators between cells in FlatList table.
        ItemSeparatorComponent={() => <Line style={{marginLeft: 60}} />}
        ListFooterComponent={() => <Line style={{opacity: 0, height: 100}} />}
        // For every item in data, render its image in an Icon component, its text
        // in a LinkText element, and a chevron to the right of the text.
        renderItem={({item}) => (
          <TouchableOpacity onPress={() => openLink(item.link)}>
            <Row>
              {/* Chooses which type of Icon to use based on item text. */}

              <Icon style={{tintColor: PASTEL_RED}} source={item.imageSource} />

              {/* Style with built-in React Navigation styling for Dark Mode, i.e.
                text is black on a white background in light mode and white on a 
                black background in Dark Mode. */}
              <LinkText style={{color: colors.text}}>{item.text}</LinkText>

              <Chevron />
            </Row>
          </TouchableOpacity>
        )}
      />
    </Container>
  );
};

export default LinksList;
