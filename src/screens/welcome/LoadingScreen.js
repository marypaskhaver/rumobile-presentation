import React from 'react';
import RedBackground from './RedBackground';
import {Text, View, Image, StatusBar} from 'react-native';
import {SFProDisplayBold, SFProTextSemibold} from '../../constants/fonts';

const LoadingScreen = () => {
  return (
    <RedBackground style={{justifyContent: 'center'}}>
      <StatusBar barStyle={'light-content'} />
      <View
        style={{
          width: '100%',
          display: 'flex',
          alignItems: 'center',
        }}
      >
        <View>
          <Text
            style={{
              fontSize: 100,
              fontFamily: SFProDisplayBold,
              color: 'white',
            }}
          >
            RU
          </Text>
          <Text
            style={{
              fontSize: 60,
              fontFamily: SFProTextSemibold,
              color: 'white',
              marginTop: -20,
            }}
          >
            Mobile
          </Text>
        </View>
      </View>
      <Image
        style={{
          height: 360,
          width: 360,
          alignSelf: 'center',
          position: 'absolute',
          bottom: -100,
        }}
        source={require('../../assets/icons/other/launch_bear.png')}
      />
    </RedBackground>
  );
};

export default LoadingScreen;
