import React from 'react';
import {Image, Text, View} from 'react-native';
import {SFProDisplayBold, SFProTextSemibold} from '../../constants/fonts';
import ButtonWithBear from './ButtonWithBear';
import {useNavigation} from '@react-navigation/native';
import RedBackground from './RedBackground';

const WelcomeScreen1 = () => {
  const navigation = useNavigation();

  return (
    <RedBackground>
      <View
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Text
          style={{
            fontFamily: SFProTextSemibold,
            fontSize: 20,
            letterSpacing: -0.46,
            textAlign: 'center',
            color: 'white',
          }}
        >
          Welcome to
        </Text>
        <Text
          style={{
            marginTop: 12,
            fontFamily: SFProDisplayBold,
            fontSize: 40,
            letterSpacing: -0.46,
            textAlign: 'center',
            color: 'white',
          }}
        >
          RUMobile
        </Text>
        <Image
          source={require('../../assets/images/message_of_the_day/taro_kuma.jpeg')}
          style={{
            width: 120,
            height: 120,
            alignSelf: 'center',
            marginTop: 16,
          }}
        />
      </View>
      <ButtonWithBear
        text="Continue"
        onPressAction={() => navigation.navigate('WelcomeScreen2')}
      />
    </RedBackground>
  );
};

export default WelcomeScreen1;
