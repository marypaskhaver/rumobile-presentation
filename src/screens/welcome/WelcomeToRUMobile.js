import React from 'react';

import {View, Text} from 'react-native';
import {GRAY_1} from '../../constants/colors';
import {SFProDisplayBold, SFProTextSemibold} from '../../constants/fonts';

const WelcomeToRUMobile = () => {
  return (
    <View style={{paddingHorizontal: 20, alignSelf: 'flex-start'}}>
      <Text
        style={{
          alignSelf: 'flex-start',
          marginTop: 16,
          fontFamily: SFProTextSemibold,
          fontSize: 12,
          color: GRAY_1,
        }}
      >
        WELCOME TO
      </Text>
      <Text
        style={{
          alignSelf: 'flex-start',
          marginTop: 4,
          fontFamily: SFProDisplayBold,
          fontSize: 32,
          letterSpacing: -0.46,
          color: 'white',
        }}
      >
        RUMobile
      </Text>
    </View>
  );
};

export default WelcomeToRUMobile;
