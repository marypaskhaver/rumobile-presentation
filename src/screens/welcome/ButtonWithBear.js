import React from 'react';
import {Text, Image, TouchableOpacity} from 'react-native';
import {PASTEL_RED} from '../../constants/colors';
import {SFProDisplayBold} from '../../constants/fonts';
import {Row} from '../../shared-components/Row';

const ButtonWithBear = ({text, onPressAction}) => {
  return (
    <TouchableOpacity
      onPress={onPressAction}
      color="white"
      style={{
        width: 340,
        bottom: 40,
        alignSelf: 'center',
        position: 'absolute',
        backgroundColor: PASTEL_RED,
        borderColor: 'white',
        borderWidth: 2,
        borderRadius: 8,
        justifyContent: 'center',
      }}
    >
      <Row
        style={{
          justifyContent: 'center',
        }}
      >
        <Image
          source={require('../../assets/icons/tab_bar/todayiconselected.png')}
          style={{
            height: 24,
            paddingVertical: 24,
            width: 28,
            resizeMode: 'contain',
            tintColor: 'white',
          }}
        />
        <Text
          style={{
            color: 'white',
            alignSelf: 'center',
            fontSize: 17,
            fontFamily: SFProDisplayBold,
            marginLeft: 16,
          }}
        >
          {text}
        </Text>
      </Row>
    </TouchableOpacity>
  );
};

export default ButtonWithBear;
