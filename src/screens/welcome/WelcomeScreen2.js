import React from 'react';
import {View, Text, ScrollView, Platform} from 'react-native';
import {GRAY_1, GRAY_4} from '../../constants/colors';
import {
  SFProDisplayBold,
  SFProTextRegular,
  SFProTextSemibold,
} from '../../constants/fonts';
import ButtonWithBear from './ButtonWithBear';
import CardWithImage from '../today/CardWithImage';
import {
  BackButton,
  Row,
  SectionLocation,
  SectionTime,
} from '../../shared-components';
import ColorStripe from '../buses/ColorStripe';
import {
  getCampusColorFromCampusName,
  getRouteColorInfoWithDefinedPropertiesForRouteNamed,
} from '../../utils';
import {useNavigation, useTheme} from '@react-navigation/native';
import RouteTag from '../../shared-components/RouteTag';
import {BackVArrow} from '../../shared-components';
import WelcomeToRUMobile from './WelcomeToRUMobile';
import RedBackground from './RedBackground';
import {passWelcomeScreen} from '../../redux/actions/welcomeScreensActions';
import {useDispatch} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {PASSED_WELCOME_SCREENS_STORAGE_KEY} from '../../constants/storageKeys';

const WelcomeScreen2 = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const busArrivalTimes = [5, 10, 13, 25];
  const {dark, colors} = useTheme();
  const exampleSection = {
    meetingTimes: [
      {
        buildingCode: 'BRR',
        campusAbbrev: 'LIV',
        campusName: 'LIVINGSTON',
        endTime: '0950',

        pmCode: 'A',
        roomNumber: '5113',
        startTime: '0830',
      },
    ],
    title: 'INTRO TO BUSINESS',
  };

  const campusColor = getCampusColorFromCampusName(
    exampleSection.meetingTimes[0].campusName,
  );

  const long_name = 'Route A';
  const routeColorInfo =
    getRouteColorInfoWithDefinedPropertiesForRouteNamed(long_name);

  return (
    <RedBackground>
      <BackButton text="Back" isWhite={true} />
      <View
        style={{
          marginTop: 72,
          paddingRight: 20,
        }}
      >
        <Row style={{justifyContent: 'space-between'}}>
          <WelcomeToRUMobile />

          <BackVArrow
            onPressAction={() => {
              navigation.navigate('Tabs');
              dispatch(passWelcomeScreen(true));
              AsyncStorage.setItem(
                PASSED_WELCOME_SCREENS_STORAGE_KEY,
                JSON.stringify(true),
              );
            }}
          />
        </Row>
      </View>

      <ScrollView
        style={{
          marginBottom: 120,
        }}
      >
        <Text
          style={{
            marginTop: 28,
            marginLeft: 20,
            marginRight: 60,
            marginBottom: 20,
            fontSize: 16,
            color: GRAY_1,
            fontFamily: SFProTextSemibold,
            flexWrap: 'wrap',
          }}
        >
          In RUMobile, you can track which classes you have for the day.
        </Text>
        <CardWithImage
          cardText={'My Classes'}
          cardImageSource={require('../../assets/icons/class_categories_filters_screen/calendar.png')}
          topRightButtonText={'+'}
          style={{
            paddingRight: 0,
            marginHorizontal: 12,
            borderColor: dark ? 'lightgray' : 'clear',
            borderWidth: dark ? 1 : 0,
          }}
        >
          <Text
            style={{
              color: colors.text,
              fontFamily: SFProTextRegular,
              fontSize: 16,
              textTransform: 'capitalize',
              paddingTop: 20,
            }}
          >
            {exampleSection.title}
          </Text>
          <Row
            style={{
              justifyContent: 'space-between',
              marginTop: -8,
            }}
          >
            <SectionLocation meetingTime={exampleSection.meetingTimes[0]} />
            <Row>
              <SectionTime meetingTime={exampleSection.meetingTimes[0]} />

              <View style={{paddingLeft: 16}}>
                <ColorStripe backgroundColor={campusColor} />
              </View>
            </Row>
          </Row>
        </CardWithImage>
        <Text
          style={{
            marginTop: 28,
            marginLeft: 20,
            marginRight: 60,
            marginBottom: 20,
            fontSize: 16,
            color: GRAY_1,
            fontFamily: SFProTextSemibold,
            flexWrap: 'wrap',
          }}
        >
          You can also add a bus from a bus route or a bus stop, making it easy
          to track bus predictions right from the home screen!
        </Text>
        <CardWithImage
          cardText={'My Buses'}
          cardImageSource={require('../../assets/icons/tab_bar/busiconselected.png')}
          style={{
            marginHorizontal: 12,
            paddingRight: 0,
            borderColor: dark ? 'lightgray' : 'clear',
            borderWidth: dark ? 1 : 0,
          }}
        >
          <Row style={{paddingVertical: 12}}>
            <View style={{flex: 1}}>
              <Row
                style={{justifyContent: 'space-between', paddingVertical: 8}}
              >
                <Text
                  style={{
                    fontSize: 16,
                    fontFamily: SFProTextSemibold,
                    color: colors.text,
                  }}
                >
                  Scott Hall
                </Text>
                <Text
                  style={{
                    fontFamily: SFProTextRegular,
                    color: GRAY_4,
                    fontSize: 12,
                  }}
                >
                  0.2 mi
                </Text>
              </Row>

              <Row style={{justifyContent: 'space-between'}}>
                <RouteTag routeColorInfo={routeColorInfo} />
                <Text
                  style={{
                    fontFamily: SFProDisplayBold,
                    fontSize: 24,
                    marginTop: -8,
                    marginBottom: 8,
                    textAlign: 'right',
                    color: colors.text,
                  }}
                >
                  {busArrivalTimes[0]}
                </Text>
              </Row>

              <Row style={{justifyContent: 'flex-end', marginTop: -8}}>
                {busArrivalTimes.slice(1).length > 0 && (
                  <Text
                    style={{
                      fontFamily: SFProTextRegular,
                      color: GRAY_4,
                      fontSize: 12,
                      textAlign: 'right',
                    }}
                  >
                    Also in
                  </Text>
                )}
                <Text
                  style={{
                    fontFamily: SFProTextSemibold,
                    color: colors.text,
                    fontSize: 12,
                    textAlign: 'right',
                  }}
                >
                  {' '}
                  {busArrivalTimes.length < 3
                    ? busArrivalTimes.slice(1).join(', ')
                    : busArrivalTimes.slice(1, 4).join(', ')}{' '}
                </Text>
                <Text
                  style={{
                    fontFamily: SFProTextRegular,
                    color: GRAY_4,
                    fontSize: 12,
                    textAlign: 'right',
                  }}
                >
                  {' '}
                  mins
                </Text>
              </Row>
            </View>
            <View style={{paddingLeft: 16, marginTop: 28}}>
              <ColorStripe backgroundColor="rgb(124, 31, 206)" />
            </View>
          </Row>
        </CardWithImage>
      </ScrollView>
      <ButtonWithBear
        text="Continue"
        onPressAction={() => navigation.navigate('WelcomeScreen3')}
      />
    </RedBackground>
  );
};

export default WelcomeScreen2;
