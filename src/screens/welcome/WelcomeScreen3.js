import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import store from '../../redux/store';
import {addLevel, removeLevel} from '../../redux/actions/academicActions';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  CAMPUS_STORAGE_KEY,
  CLASSES_CATEGORIES_FILTER_EDUCATION_LEVEL_STORAGE_KEY,
  PASSED_WELCOME_SCREENS_STORAGE_KEY,
} from '../../constants/storageKeys';
import {View, Text, TouchableOpacity, ScrollView} from 'react-native';
import {GRAY_1} from '../../constants/colors';
import {SFProTextSemibold} from '../../constants/fonts';
import ButtonWithBear from './ButtonWithBear';
import {
  BackButton,
  RowWithCheckmark,
  Line,
  Card,
  BackVArrow,
  Row,
} from '../../shared-components';
import {useNavigation, useTheme} from '@react-navigation/native';
import {CAMPUS_IDs_TO_NAMES} from '../../constants/campuses';
import {selectChosenCampusID} from '../../redux/reducers/campusesReducer';
import {updateChosenCampusID} from '../../redux/actions/campusActions';
import {selectLevels} from '../../redux/reducers/academicsReducer';
import WelcomeToRUMobile from './WelcomeToRUMobile';
import RedBackground from './RedBackground';
import {passWelcomeScreen} from '../../redux/actions/welcomeScreensActions';
import BulletPoint from '../food/BulletPoint';

const CardWithText = ({children}) => {
  const {dark, colors} = useTheme();

  return (
    <Card
      style={{
        backgroundColor: colors.card,
        marginBottom: 20,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
        borderColor: dark ? 'lightgray' : 'clear',
        borderWidth: dark ? 1 : 0,
      }}
    >
      {children}
    </Card>
  );
};

const WelcomeScreen3 = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const currentLevels = useSelector(selectLevels);

  const allLevels = {
    U: 'Undergraduate',
    G: 'Graduate',
  };

  const [selectedLevelsIDs, setSelectedLevelsIDs] = useState(currentLevels);

  const updateSelectedLevelsIDs = levelID => {
    if (selectedLevelsIDs.includes(levelID)) {
      const selectedLevelsIDWithoutGivenLevelID = selectedLevelsIDs.filter(
        selectedLevelID => selectedLevelID !== levelID,
      );

      // Prevent user from deselecting all levels
      // Can show notification bar here
      if (selectedLevelsIDWithoutGivenLevelID.length === 0) return;

      setSelectedLevelsIDs(selectedLevelsIDWithoutGivenLevelID);
      store.dispatch(removeLevel(levelID));

      AsyncStorage.setItem(
        CLASSES_CATEGORIES_FILTER_EDUCATION_LEVEL_STORAGE_KEY,
        JSON.stringify(selectedLevelsIDWithoutGivenLevelID),
      );

      return;
    }

    setSelectedLevelsIDs([...selectedLevelsIDs, levelID]);
    store.dispatch(addLevel(levelID));

    AsyncStorage.setItem(
      CLASSES_CATEGORIES_FILTER_EDUCATION_LEVEL_STORAGE_KEY,
      JSON.stringify([...selectedLevelsIDs, levelID]),
    );
  };

  const initialChosenCampus = useSelector(selectChosenCampusID);

  // Initial selected campus ID is 1-- should be set w/ AsyncStorage when app loads in App.js
  const [selectedCampusID, setSelectedCampusID] = useState(initialChosenCampus);

  const selectAndSaveCampus = campusID => {
    setSelectedCampusID(campusID);
    store.dispatch(updateChosenCampusID(campusID));
    AsyncStorage.setItem(CAMPUS_STORAGE_KEY, JSON.stringify(campusID));
  };

  return (
    <RedBackground>
      <View
        style={{
          marginTop: 72,
          paddingRight: 20,
        }}
      >
        <Row style={{justifyContent: 'space-between'}}>
          <WelcomeToRUMobile />
          <BackVArrow
            onPressAction={() => {
              navigation.navigate('Tabs');
              dispatch(passWelcomeScreen(true));
              AsyncStorage.setItem(
                PASSED_WELCOME_SCREENS_STORAGE_KEY,
                JSON.stringify(true),
              );
            }}
          />
        </Row>
      </View>
      <BackButton text="Back" isWhite={true} />
      <ScrollView
        style={{
          marginBottom: 120,
        }}
      >
        <View style={{paddingHorizontal: 20}}>
          <Text
            style={{
              marginTop: 28,
              marginRight: 60,
              marginBottom: 12,
              fontSize: 16,
              color: GRAY_1,
              fontFamily: SFProTextSemibold,
              flexWrap: 'wrap',
            }}
          >
            Please select your education level.
          </Text>
          <CardWithText>
            <View style={{paddingBottom: 14}}>
              {Object.keys(allLevels).map((levelID, index) => (
                <View key={levelID}>
                  <View style={{paddingRight: 16}}>
                    <TouchableOpacity
                      onPress={() => updateSelectedLevelsIDs(levelID)}
                    >
                      <RowWithCheckmark
                        textToDisplay={allLevels[levelID]}
                        isChecked={selectedLevelsIDs.includes(levelID)}
                        style={{height: 'auto', paddingTop: 14}}
                        customBulletPoint={<BulletPoint />}
                      />
                    </TouchableOpacity>
                  </View>

                  {index < Object.values(allLevels).length - 1 ? (
                    <Line style={{paddingBottom: 14}} />
                  ) : null}
                </View>
              ))}
            </View>
          </CardWithText>
          <Text
            style={{
              marginTop: 28,
              marginRight: 60,
              marginBottom: 12,
              fontSize: 16,
              color: GRAY_1,
              fontFamily: SFProTextSemibold,
              flexWrap: 'wrap',
            }}
          >
            Please select your campus.
          </Text>
          <CardWithText>
            <View style={{paddingBottom: 14}}>
              {Object.values(CAMPUS_IDs_TO_NAMES).map((campus, index) => (
                <View key={campus.id}>
                  <View style={{paddingRight: 16}}>
                    <TouchableOpacity
                      onPress={() => selectAndSaveCampus(campus.id)}
                    >
                      <RowWithCheckmark
                        textToDisplay={campus.name}
                        isChecked={campus.id === selectedCampusID}
                        style={{height: 'auto', paddingTop: 14}}
                        customBulletPoint={<BulletPoint />}
                      />
                    </TouchableOpacity>
                  </View>
                  {index < Object.keys(CAMPUS_IDs_TO_NAMES).length - 1 && (
                    <Line style={{paddingBottom: 14}} />
                  )}
                </View>
              ))}
            </View>
          </CardWithText>
        </View>
      </ScrollView>
      <ButtonWithBear
        text="Get Started"
        onPressAction={() => {
          navigation.navigate('Tabs');
          dispatch(passWelcomeScreen(true));
          AsyncStorage.setItem(
            PASSED_WELCOME_SCREENS_STORAGE_KEY,
            JSON.stringify(true),
          );
        }}
      />
    </RedBackground>
  );
};

export default WelcomeScreen3;
