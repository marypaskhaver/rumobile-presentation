import React from 'react';
import {PASTEL_RED} from '../../constants/colors';
import {StatusBar, View} from 'react-native';

const RedBackground = ({children, style}) => {
  return (
    <View
      style={[
        style,
        {
          backgroundColor: PASTEL_RED,
          flex: 1,
        },
      ]}
    >
      <StatusBar
        barStyle={'light-content'}
        hidden={false}
        translucent={true}
        backgroundColor="transparent"
      />
      {children}
    </View>
  );
};

export default RedBackground;
