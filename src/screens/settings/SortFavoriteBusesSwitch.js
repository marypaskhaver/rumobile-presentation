import React, {useState} from 'react';
import {Switch} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {sortFavoriteBuses} from '../../redux/actions/busesActions';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {selectSortFavoriteBuses} from '../../redux/reducers/busesReducer';
import {SORT_FAVORITE_BUSES_STORAGE_KEY} from '../../constants/storageKeys';

export const SortFavoriteBusesSwitch = () => {
  const initialEnabledness = useSelector(selectSortFavoriteBuses);

  const [isEnabled, setIsEnabled] = useState(initialEnabledness);
  const dispatch = useDispatch();

  const updateSortFavoriteBusesStatus = newStatus => {
    try {
      AsyncStorage.setItem(
        SORT_FAVORITE_BUSES_STORAGE_KEY,
        JSON.stringify(newStatus),
      );
    } catch (error) {
      console.log(`Error saving sort favorite buses update. ${error}`);
    }
  };

  const toggleSwitch = () => {
    setIsEnabled(previousState => !previousState);

    if (!isEnabled) {
      // Switch was switched on; sort buses
      dispatch(sortFavoriteBuses(true));
      updateSortFavoriteBusesStatus(true);
    } else {
      // Switch was turned off; don't sort buses
      dispatch(sortFavoriteBuses(false));
      updateSortFavoriteBusesStatus(false);
    }
  };

  return (
    <Switch
      thumbColor={'white'}
      onValueChange={toggleSwitch}
      value={isEnabled}
      style={{marginLeft: 'auto'}}
    ></Switch>
  );
};
