import React, {useState} from 'react';
import {Switch} from 'react-native';
import {updateChosenOverrideThemeID} from '../../redux/actions/themeActions';
import {useDispatch, useSelector} from 'react-redux';
import {selectChosenOverrideThemeID} from '../../redux/reducers/themesReducer';
import {useTheme} from '@react-navigation/native';
import {getThemeIDFromTheme} from '../../utils';

export const AutomaticDarkModeSwitch = () => {
  const initialEnabledness = useSelector(selectChosenOverrideThemeID) === null;
  const [isEnabled, setIsEnabled] = useState(initialEnabledness);
  const themeFromStoreID = getThemeIDFromTheme(useTheme());
  const dispatch = useDispatch();

  const toggleSwitch = () => {
    setIsEnabled(previousState => !previousState);

    if (!isEnabled) {
      // Switch was switched on; enable automatic dark mode
      dispatch(updateChosenOverrideThemeID(null));
    } else {
      // Switch was turned off; disable automatic dark mode
      dispatch(updateChosenOverrideThemeID(themeFromStoreID));
    }
  };

  return (
    <Switch
      thumbColor={'white'}
      onValueChange={toggleSwitch}
      value={isEnabled}
      style={{marginLeft: 'auto'}}
    ></Switch>
  );
};
