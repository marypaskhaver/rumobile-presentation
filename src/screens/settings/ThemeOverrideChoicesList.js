import React, {useState} from 'react';
import {TouchableOpacity, View} from 'react-native';
import store from '../../redux/store';
import {useSelector} from 'react-redux';
import {RowWithCheckmark} from '../../shared-components/RowWithCheckmark';
import {Line} from '../../shared-components/Line';

// Constants
import {DARK_MODE_OVERRIDE_OPTIONS} from '../../constants/darkModeOverrideOptions';

import {selectChosenOverrideThemeID} from '../../redux/reducers/themesReducer';
import {updateChosenOverrideThemeID} from '../../redux/actions/themeActions';
import {useTheme} from '@react-navigation/native';
import {getThemeIDFromTheme} from '../../utils';

export const ThemeOverrideChoicesList = () => {
  const initialChosenOverrideThemeID = useSelector(selectChosenOverrideThemeID);
  const currentPhoneThemeID = getThemeIDFromTheme(useTheme());

  const [selectedThemeID, setSelectedThemeID] = useState(
    initialChosenOverrideThemeID
      ? initialChosenOverrideThemeID
      : currentPhoneThemeID,
  );

  const selectAndSaveThemeChoice = themeID => {
    setSelectedThemeID(themeID);
    store.dispatch(updateChosenOverrideThemeID(themeID));
  };

  return (
    <View>
      {Object.values(DARK_MODE_OVERRIDE_OPTIONS).map((option, index) => (
        <TouchableOpacity
          key={option.id}
          onPress={() => selectAndSaveThemeChoice(option.id)}
        >
          <RowWithCheckmark
            textToDisplay={option.themeName}
            isChecked={option.id === selectedThemeID}
            style={{paddingVertical: 14, height: 'auto'}}
          />
          {index < Object.values(DARK_MODE_OVERRIDE_OPTIONS).length - 1 && (
            <Line />
          )}
        </TouchableOpacity>
      ))}
    </View>
  );
};
