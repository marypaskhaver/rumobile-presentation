import React, {useState} from 'react';
import {TouchableOpacity, View} from 'react-native';
import {RowWithCheckmark} from '../../shared-components/RowWithCheckmark';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {CAMPUS_STORAGE_KEY} from '../../constants/storageKeys';
import {updateChosenCampusID} from '../../redux/actions/campusActions';
import {useSelector} from 'react-redux';
import {CAMPUS_IDs_TO_NAMES} from '../../constants/campuses';
import {selectChosenCampusID} from '../../redux/reducers/campusesReducer';
import store from '../../redux/store';
import {Line} from '../../shared-components';

export const CampusItems = () => {
  const initialChosenCampus = useSelector(selectChosenCampusID);

  const [selectedCampusID, setSelectedCampusID] = useState(initialChosenCampus);

  const selectAndSaveCampus = campusID => {
    setSelectedCampusID(campusID);
    store.dispatch(updateChosenCampusID(campusID));
    AsyncStorage.setItem(CAMPUS_STORAGE_KEY, JSON.stringify(campusID));
  };

  return (
    <View style={{paddingBottom: 8, paddingTop: 4}}>
      {Object.values(CAMPUS_IDs_TO_NAMES).map((campus, index) => (
        <View key={campus.id}>
          <View style={{paddingRight: 16}}>
            <TouchableOpacity onPress={() => selectAndSaveCampus(campus.id)}>
              <RowWithCheckmark
                textToDisplay={campus.name}
                isChecked={campus.id === selectedCampusID}
                style={{paddingVertical: 14, height: 'auto'}}
              />
            </TouchableOpacity>
          </View>

          {index < Object.keys(CAMPUS_IDs_TO_NAMES).length - 1 && <Line />}
        </View>
      ))}
    </View>
  );
};
