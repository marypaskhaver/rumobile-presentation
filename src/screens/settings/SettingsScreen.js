import React from 'react';
import {Image, ScrollView, Text} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {Card} from '../../shared-components/Card';
import {BigText} from '../../shared-components/BigText';
import {LocationItems} from './LocationItems';
import {CampusItems} from './CampusItems';
import {DarkModeItems} from './DarkModeItems';
import {BackButton} from '../../shared-components/BackButton';
import {GRAY_3, GRAY_5} from '../../constants/colors';
import {View} from 'react-native';
import {FocusAwareStatusBar, Row} from '../../shared-components';
import {SFProTextRegular} from '../../constants/fonts';
import {FavoriteBusesItems} from './FavoriteBusesItems';

const CardWithText = ({title, imageSource, children}) => {
  const {dark, colors} = useTheme();

  return (
    <Card
      style={{
        marginTop: 16,
        marginBottom: 16,
        marginLeft: 24,
        marginRight: 24,
        paddingRight: 0,
        paddingBottom: 0,
        backgroundColor: colors.card,
      }}
    >
      <Row>
        <Image
          style={{
            width: 16,
            height: 16,
            tintColor: dark ? GRAY_3 : GRAY_5,
          }}
          source={imageSource}
        />
        <Text
          style={{
            fontSize: 16,
            color: dark ? GRAY_3 : GRAY_5,
            marginLeft: 8,
            fontFamily: SFProTextRegular,
          }}
        >
          {title}
        </Text>
      </Row>
      {children}
    </Card>
  );
};

const SettingsScreen = () => {
  const campusImage = require('../../assets/icons/class_categories_filters_screen/campus.png');
  const settingsImage = require('../../assets/icons/settings_screen/settingsicon.png');
  const moonImage = require('../../assets/icons/settings_screen/moonicon.png');
  const busImage = require('../../assets/icons/settings_screen/busicon.png');

  // Provides colors for particular elements depending on the current theme (dark or light).
  const {colors} = useTheme();

  return (
    <View style={{flex: 1}}>
      <BackButton text="Back" />
      <FocusAwareStatusBar />
      <View style={{marginTop: 72}}>
        <BigText style={{marginLeft: 20, color: colors.text}}>Settings</BigText>
        <ScrollView>
          <CardWithText imageSource={settingsImage} title="App Settings">
            <LocationItems />
          </CardWithText>
          <CardWithText imageSource={campusImage} title="Campus">
            <CampusItems />
          </CardWithText>
          <CardWithText imageSource={busImage} title="Favorite Buses">
            <FavoriteBusesItems />
          </CardWithText>
          <CardWithText imageSource={moonImage} title="Appearance">
            <DarkModeItems />
          </CardWithText>
          <View style={{height: 100}} />
        </ScrollView>
      </View>
    </View>
  );
};

export default SettingsScreen;
