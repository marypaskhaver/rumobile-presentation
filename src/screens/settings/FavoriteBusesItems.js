import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {GRAY_4} from '../../constants/colors';
import {Row} from '../../shared-components';
import {useTheme} from '@react-navigation/native';
import {SortFavoriteBusesSwitch} from './SortFavoriteBusesSwitch';
import {useSelector} from 'react-redux';
import {selectSortFavoriteBuses} from '../../redux/reducers/busesReducer';

export const FavoriteBusesItems = () => {
  const {colors} = useTheme();

  const sorted = useSelector(selectSortFavoriteBuses);

  return (
    <View style={{paddingBottom: 8, paddingRight: 16}}>
      <Row style={{alignItems: 'center', marginTop: 20}}>
        <Text
          style={{
            fontSize: 17,
            color: sorted ? colors.text : GRAY_4,
          }}
        >
          Sort by Distance
        </Text>
        <SortFavoriteBusesSwitch />
      </Row>
      <View style={{height: 12}} />
    </View>
  );
};
