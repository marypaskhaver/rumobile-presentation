import React, {useEffect, useRef} from 'react';
import {
  Image,
  Text,
  Linking,
  TouchableOpacity,
  Alert,
  AppState,
} from 'react-native';
import {PASTEL_RED, GRAY_4} from '../../constants/colors';
import {useSelector} from 'react-redux';
import {selectUserLocation} from '../../redux/reducers/busesReducer';
import {getLocation} from '../../shared-components/LocationGetter';
import {Row} from '../../shared-components';

const openSettings = () => {
  if (Platform.OS == 'android') {
    Linking.openSettings().catch(() => {
      Alert.alert('Unable to open settings.');
    });
  } else {
    Linking.openURL('app-settings:').catch(() => {
      Alert.alert('Unable to open settings.');
    });
  }
}; // after testing this on iPhone, consider sending viewers straight to review
// on the FeedbackScreen, considering https://stackoverflow.com/questions/3124080/app-store-link-for-rate-review-this-app

export const LocationItems = () => {
  const userLocation = useSelector(selectUserLocation);
  const locationImage = require('../../assets/icons/settings_screen/locationicon.png');

  const appState = useRef(AppState.currentState);

  useEffect(() => {
    const subscription = AppState.addEventListener('change', nextAppState => {
      if (
        appState.current.match(/inactive|background/) &&
        nextAppState === 'active'
      ) {
        getLocation();
      }

      appState.current = nextAppState;
    });

    return () => {
      subscription.remove();
    };
  }, []);

  return (
    <TouchableOpacity
      onPress={openSettings}
      style={{paddingBottom: 24, paddingRight: 16}}
    >
      <Row style={{alignItems: 'center', marginTop: 20}}>
        <Image source={locationImage} style={{width: 29, height: 29}} />
        <Text
          style={{
            fontSize: 17,
            color: PASTEL_RED,
            width: 139,
            marginLeft: 14,
          }}
        >
          Location
        </Text>
        <Text
          style={{
            fontSize: 17,
            color: GRAY_4,
            marginLeft: 'auto',
          }}
        >
          {userLocation ? 'Enabled' : 'Disabled'}
        </Text>
      </Row>
    </TouchableOpacity>
  );
};
