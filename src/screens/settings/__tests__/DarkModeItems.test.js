import React from 'react';
import {DarkModeItems} from '../DarkModeItems';

import {fireEvent, render, screen} from '../../../testUtils/testUtils';

describe('DarkModeItems', () => {
  it('shows custom dark mode options when automatic dark mode is disabled', () => {
    render(<DarkModeItems />);

    const autoDarkModeSwitch = screen.getByRole('switch');
    fireEvent(autoDarkModeSwitch, 'valueChange', false);

    expect(screen.getByText('Light Mode')).toBeTruthy();
    expect(screen.getByText('Dark Mode')).toBeTruthy();
  });
});
