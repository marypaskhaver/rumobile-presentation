import React from 'react';
import {View, Text} from 'react-native';
import {useSelector} from 'react-redux';
import {useTheme} from '@react-navigation/native';

import {selectChosenOverrideThemeID} from '../../redux/reducers/themesReducer';
import {ThemeOverrideChoicesList} from './ThemeOverrideChoicesList';
import {AutomaticDarkModeSwitch} from './AutomaticDarkModeSwitch';
import {GRAY_4} from '../../constants/colors';
import {Row} from '../../shared-components/Row';

export const DarkModeItems = () => {
  const chosenOverrideThemeID = useSelector(selectChosenOverrideThemeID);
  const {colors} = useTheme();

  return (
    <View style={{paddingBottom: 8, paddingRight: 16}}>
      <Row style={{alignItems: 'center', marginTop: 20}}>
        <Text
          style={{
            fontSize: 17,
            color: chosenOverrideThemeID ? GRAY_4 : colors.text,
          }}>
          Automatic
        </Text>
        <AutomaticDarkModeSwitch />
      </Row>
      {chosenOverrideThemeID ? (
        <ThemeOverrideChoicesList />
      ) : (
        <View style={{height: 12}} />
      )}
    </View>
  );
};
