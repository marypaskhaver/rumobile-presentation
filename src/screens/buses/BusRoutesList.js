import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import DropdownMenu from './DropdownMenu';
import {GRAY_4} from '../../constants/colors';
import {useTheme, useNavigation} from '@react-navigation/native';
import {Row} from '../../shared-components/Row';
import {getColorFromBusRouteName} from '../../utils/getColorFromBusRouteName';
import {useSelector} from 'react-redux';
import {
  selectBusRoutes,
  selectVehicles,
} from '../../redux/reducers/busesReducer';
import {BusRouteInformation} from '../../utils';
import {Line} from '../../shared-components/Line';
import {FocusAwareStatusBar} from '../../shared-components';

const BusList = ({routes, routeType}) => {
  if (routes.length === 0) {
    return (
      <Text
        style={{
          fontSize: 18,
          color: GRAY_4,
          width: '100%',
          textAlign: 'center',
          paddingTop: 16,
          paddingBottom: 24,
        }}
      >
        No {routeType} routes now.
      </Text>
    );
  }

  return routes.map((route, index) => (
    <View key={route.route_id}>
      <BusRouteView long_name={route.long_name} route_id={route.route_id} />

      {index < routes.length - 1 ? (
        <Line />
      ) : (
        <View style={{marginBottom: 16}} />
      )}
    </View>
  ));
};

const BusRouteView = ({long_name, route_id}) => {
  const {dark, colors} = useTheme();
  const navigation = useNavigation();

  const showBusRoutePredictionsScreen = () => {
    navigation.navigate('BusRoutePredictionsScreen', {
      route_id,
      long_name,
    });
  };

  return (
    <TouchableOpacity
      key={route_id}
      style={{paddingLeft: 4}}
      onPress={() => showBusRoutePredictionsScreen()}
    >
      <Row style={{paddingVertical: 15}}>
        <View
          style={{
            height: 16,
            width: 16,
            borderRadius: 8,
            borderWidth: 2,
            borderColor: dark ? GRAY_4 : 'white',
            shadowOffset: {width: 0, height: 2},
            shadowColor: GRAY_4,
            shadowOpacity: 0.5,
            marginRight: 15,
            backgroundColor: getColorFromBusRouteName(long_name),
          }}
        />
        <Text
          style={{
            fontSize: 18,
            color: colors.text,
          }}
        >
          {long_name}
        </Text>
      </Row>
    </TouchableOpacity>
  );
};

export default BusRoutesList = () => {
  const busRoutes = useSelector(selectBusRoutes);
  const vehicles = useSelector(selectVehicles);

  // Make into selectors
  const busRouteInformation = new BusRouteInformation(busRoutes, vehicles);
  const activeRoutes = busRouteInformation.getActiveRoutes();

  return (
    <View
      style={{
        marginBottom: 100,
      }}
    >
      <FocusAwareStatusBar />
      <View
        style={{
          marginBottom: 12,
        }}
      >
        <DropdownMenu title="Active Routes">
          <View style={{marginTop: 8}}>
            <BusList routeType="active" routes={activeRoutes} />
          </View>
        </DropdownMenu>
      </View>
      <View>
        <DropdownMenu title="Inactive Routes" open={activeRoutes.length === 0}>
          <View style={{marginTop: 8}}>
            <BusList
              routeType="inactive"
              routes={busRouteInformation.getInactiveRoutes()}
            />
          </View>
        </DropdownMenu>
      </View>
    </View>
  );
};
