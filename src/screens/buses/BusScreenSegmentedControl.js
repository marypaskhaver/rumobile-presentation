import {useTheme} from '@react-navigation/native';
import React from 'react';
import SegmentedControlTab from 'react-native-segmented-control-tab';
import {PASTEL_RED} from '../../constants/colors';
import {SFProTextRegular} from '../../constants/fonts';

export default BusScreenSegmentedControl = ({
  selectedIndex,
  setSelectedIndex,
}) => {
  const {colors} = useTheme();

  return (
    <SegmentedControlTab
      values={['Routes', 'Stops']}
      selectedIndex={selectedIndex}
      tabTextStyle={{
        color: PASTEL_RED,
        fontSize: 16,
        fontFamily: SFProTextRegular,
      }}
      tabStyle={{
        borderColor: PASTEL_RED,
        borderWidth: 1,
        backgroundColor: colors.background,
      }}
      activeTabStyle={{
        backgroundColor: PASTEL_RED,
        borderColor: PASTEL_RED,
        borderWidth: 1,
      }}
      tabsContainerStyle={{
        marginVertical: 12,
      }}
      onTabPress={index => setSelectedIndex(index)}
    />
  );
};
