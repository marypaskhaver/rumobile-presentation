import React, {useState} from 'react';
import {Row} from '../../shared-components/Row';
import DropdownArrow from '../../shared-components/DropdownArrow';
import {Text, TouchableOpacity, View} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {SFProTextSemibold} from '../../constants/fonts';

const TitleText = ({children}) => {
  const {colors} = useTheme();

  return (
    <Text
      style={{
        fontFamily: SFProTextSemibold,
        fontSize: 22,
        color: colors.text,
      }}
    >
      {children}
    </Text>
  );
};

export default DropdownMenu = ({
  title,
  titleStyle,
  titleAfterPress = title,
  rowStyle,
  children,
  open = true,
  arrowStyle,
}) => {
  const [openDropdown, setOpenDropdown] = useState(open);

  return (
    <>
      <TouchableOpacity onPress={() => setOpenDropdown(!openDropdown)}>
        <Row style={rowStyle ? rowStyle : {justifyContent: 'space-between'}}>
          {titleStyle ? (
            <Text style={titleStyle}>
              {openDropdown ? title : titleAfterPress}
            </Text>
          ) : (
            <TitleText>{openDropdown ? title : titleAfterPress}</TitleText>
          )}
          <View style={arrowStyle}>
            <DropdownArrow selected={openDropdown} />
          </View>
        </Row>
      </TouchableOpacity>
      {openDropdown ? children : null}
    </>
  );
};
