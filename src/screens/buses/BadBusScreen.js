import React from 'react';
import {Text, View, Image} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {BackButton} from '../../shared-components/BackButton';
import {useTheme} from '@react-navigation/native';
import {SFProTextRegular} from '../../constants/fonts';
import {GRAY_4} from '../../constants/colors';
import {FocusAwareStatusBar} from '../../shared-components';

export default BadBusInfoScreen = ({backButtonText, busInfoType}) => {
  const {colors, dark} = useTheme();

  return (
    <SafeAreaView>
      <FocusAwareStatusBar />

      <BackButton text={backButtonText} />
      <View
        style={{
          paddingHorizontal: 40,
          marginTop: 40,
          height: '100%',
          justifyContent: 'center',
        }}
      >
        <Text
          style={{
            textAlign: 'center',
            color: colors.text,
            fontSize: 20,
            fontFamily: SFProTextRegular,
          }}
        >
          Something went wrong.
        </Text>
        <Text
          style={{
            fontFamily: SFProTextRegular,
            color: GRAY_4,
            fontSize: 16,
            textAlign: 'center',
            paddingVertical: 20,
          }}
        >
          We couldn't get the data for this {busInfoType}.
        </Text>
        <Image
          source={require('../../assets/images/opinion_screens/bus_kuma.png')}
          style={{
            width: '100%',
            height: undefined,
            aspectRatio: 1,
            tintColor: dark ? 'white' : null,
            resizeMode: 'contain',
          }}
        />
      </View>
    </SafeAreaView>
  );
};
