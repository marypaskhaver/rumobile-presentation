import {useTheme} from '@react-navigation/native';
import React from 'react';
import {useState} from 'react';
import {RefreshControl} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {
  requestBusPredictions,
  requestBusRoutes,
  requestBusStops,
  requestVehicles,
} from '../../redux/actions/busesActions';
import {selectBusRoutes} from '../../redux/reducers/busesReducer';
import {selectChosenCampusID} from '../../redux/reducers/campusesReducer';

export default BusDataRefreshControl = () => {
  const [refreshing, setRefreshing] = useState(false);
  const {dark} = useTheme();

  const chosenCampusID = useSelector(selectChosenCampusID);
  const busRoutes = useSelector(selectBusRoutes);

  const dispatch = useDispatch();

  const refresh = () => {
    setRefreshing(true);

    dispatch(requestBusRoutes(chosenCampusID));
    dispatch(requestVehicles(chosenCampusID));
    dispatch(requestBusStops(chosenCampusID));

    dispatch(
      requestBusPredictions(busRoutes.map(busRoute => busRoute.route_id)),
    );

    setRefreshing(false);
  };

  return (
    <RefreshControl
      tintColor={dark ? 'white' : 'gray'}
      refreshing={refreshing}
      onRefresh={refresh}
    />
  );
};
