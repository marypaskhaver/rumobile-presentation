import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {useSelector} from 'react-redux';
import {
  selectBusRoutes,
  selectBusStops,
  selectUserLocation,
  selectVehicles,
} from '../../redux/reducers/busesReducer';
import DropdownMenu from './DropdownMenu';
import {stopOrders} from '../../../bus_stop_orders.json';
import {
  BusRouteInformation,
  getRouteColorInfoWithDefinedPropertiesForRouteNamed,
} from '../../utils';
import {useTheme, useNavigation} from '@react-navigation/native';
import {Row} from '../../shared-components/Row';
import {SFProTextRegular} from '../../constants/fonts';
import {Line} from '../../shared-components/Line';
import {GRAY_4} from '../../constants/colors';
import {getDistance, convertDistance} from 'geolib';
import {busTagToDisplayNameMap} from '../../constants/buses';

const BusStopNameAndDistance = ({name, distance}) => {
  const {colors} = useTheme();
  const displayDistance = !distance ? '-- mi' : `${distance} mi`;

  return (
    <Row style={{paddingVertical: 8}}>
      <Text
        style={{
          fontFamily: SFProTextRegular,
          color: colors.text,
          fontSize: 17,
          paddingRight: 64,
        }}
      >
        {name}
      </Text>
      <Text
        style={{
          marginLeft: 'auto',
          fontFamily: SFProTextRegular,
          color: GRAY_4,
        }}
      >
        {displayDistance}
      </Text>
    </Row>
  );
};

const RouteTags = ({busStop, busRoutes}) => {
  return busRoutes
    .filter(
      busRoute =>
        stopOrders[busRoute.long_name] &&
        stopOrders[busRoute.long_name].includes(busStop.name),
    )
    .map(busRoute => {
      const routeColorInfo =
        getRouteColorInfoWithDefinedPropertiesForRouteNamed(busRoute.long_name);

      return (
        <View
          key={busRoute.long_name}
          style={{
            backgroundColor: routeColorInfo.rcolor,
            paddingHorizontal: 6,
            paddingVertical: 4,
            borderRadius: 6,
            marginRight: 8,
            marginBottom: 8,
          }}
        >
          <Text
            style={{
              color: 'white',
            }}
          >
            {busTagToDisplayNameMap[routeColorInfo.rtag]
              ? busTagToDisplayNameMap[routeColorInfo.rtag]
              : routeColorInfo.rtag}
          </Text>
        </View>
      );
    });
};

const BusStopInfo = ({busInformation, busStop, showSeparator}) => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      onPress={() => navigation.navigate('StopDetailsScreen', {busStop})}
      key={busStop.stop_id}
    >
      <BusStopNameAndDistance {...busStop} />

      <Row style={{flexWrap: 'wrap'}}>
        {busInformation
          .getActiveRoutes()
          .filter(
            busRoute =>
              stopOrders[busRoute.long_name] &&
              stopOrders[busRoute.long_name].includes(busStop.name),
          ).length === 0 ? (
          <Text
            style={{
              fontFamily: SFProTextRegular,
              fontSize: 16,
              color: GRAY_4,
            }}
          >
            No active routes
          </Text>
        ) : null}

        <RouteTags
          busStop={busStop}
          busRoutes={busInformation.getActiveRoutes()}
        />
      </Row>
      {showSeparator ? <Line style={{paddingTop: 16}} /> : null}
    </TouchableOpacity>
  );
};

export default StopsList = () => {
  const allBusRoutes = useSelector(selectBusRoutes);
  const allActiveVehicles = useSelector(selectVehicles);

  const location = useSelector(selectUserLocation);

  const allBusStops = useSelector(selectBusStops).sort(
    (a, b) => a.name > b.name,
  );

  const busInformation = new BusRouteInformation(
    allBusRoutes,
    allActiveVehicles,
  );

  return (
    <>
      <DropdownMenu title="Nearby">
        <View style={{marginTop: 8}}>
          {location ? (
            allBusStops
              .map(busStop => {
                const busStopLocation = {
                  longitude: busStop.location.lng,
                  latitude: busStop.location.lat,
                };

                const userLocation = {
                  longitude: location?.coords?.longitude ?? 0,
                  latitude: location?.coords?.latitude ?? 0,
                };

                const distanceBetweenUserAndStop = location
                  ? convertDistance(
                      getDistance(userLocation, busStopLocation),
                      'mi',
                    ).toFixed(1)
                  : null;

                busStop.distance = distanceBetweenUserAndStop;

                return busStop;
              })
              .sort((a, b) => a.distance - b.distance)
              .slice(0, 5)
              .map((busStop, index) => {
                return (
                  <BusStopInfo
                    key={busStop.stop_id}
                    busInformation={busInformation}
                    busStop={busStop}
                    showSeparator={index < 4}
                  />
                );
              })
          ) : (
            <Text
              style={{
                fontSize: 16,
                color: GRAY_4,
                width: '100%',
                textAlign: 'center',
                paddingTop: 16,
                paddingBottom: 12,
              }}
            >
              Location services not enabled.
            </Text>
          )}
        </View>
      </DropdownMenu>
      <View style={{paddingTop: 24}}>
        <DropdownMenu title="All">
          <View style={{marginTop: 8}}>
            {allBusStops.map((busStop, index) => {
              return (
                <BusStopInfo
                  key={busStop.stop_id}
                  busInformation={busInformation}
                  busStop={busStop}
                  showSeparator={index < allBusStops.length - 1}
                />
              );
            })}
          </View>
          <View style={{height: 100}} />
        </DropdownMenu>
      </View>
    </>
  );
};
