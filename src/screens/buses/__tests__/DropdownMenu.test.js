import React from 'react';
import {Text} from 'react-native';
import DropdownMenu from '../DropdownMenu';
import {fireEvent, render, screen} from '../../../testUtils/testUtils';

describe('DropdownMenu', () => {
  it('hides children when pressed', () => {
    render(
      <DropdownMenu title={'Methods of Transport'} open={true}>
        <Text>Hippogriff</Text>
      </DropdownMenu>,
    );

    expect(screen.getByText('Hippogriff')).toBeTruthy();

    const dropdownMenuTitle = screen.getByText('Methods of Transport');
    fireEvent.press(dropdownMenuTitle);

    expect(screen.queryByText('Hippogriff')).toBeFalsy();
  });

  it('test that styling is applied conditionally', () => {
    render(
      <DropdownMenu
        title={'Counterespionage Professors'}
        titleAfterPress={"They've Disappeared!"}
        rowStyle={{justifyContent: 'center'}}
        titleStyle={{color: 'red'}}>
        <Text>Maxwell Smart</Text>
      </DropdownMenu>,
    );

    expect(screen.getByText('Maxwell Smart')).toBeTruthy();

    const dropdownMenuTitle = screen.getByText('Counterespionage Professors');
    fireEvent.press(dropdownMenuTitle);

    expect(screen.queryByText('Maxwell Smart')).toBeFalsy();
    expect(screen.queryByText("They've Disappeared!")).toBeTruthy();
  });
});
