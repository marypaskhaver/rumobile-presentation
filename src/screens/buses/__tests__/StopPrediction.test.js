import React from 'react';
import {render, screen} from '@testing-library/react-native';
import {useSelector} from 'react-redux';

import StopPrediction from '../../../shared-components/StopPrediction';

jest.mock('react-redux');

describe('StopPrediction', () => {
  beforeAll(() => {
    jest.useFakeTimers('modern');
    jest.setSystemTime(new Date('2017-01-01'));
  });

  afterAll(() => {
    jest.useRealTimers();
  });

  it('displays "No predictions" if there are no predictions', () => {
    const stop_id = '1';
    const route_id = '2';
    useSelector.mockReturnValue([
      {
        stop_id,
        arrivals: [],
      },
    ]);
    const stopPrediction = {stop_id, route_id};
    render(<StopPrediction stopPrediction={stopPrediction} />);

    expect(screen.getByText('No predictions')).toBeTruthy();
  });
});
