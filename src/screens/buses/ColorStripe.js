import React from 'react';
import {View} from 'react-native';
import {GRAY_5} from '../../constants/colors';

const ColorStripe = ({backgroundColor}) => {
  return (
    <View
      style={{
        width: 8,
        height: 48,
        marginLeft: 'auto',
        borderTopLeftRadius: 4,
        borderBottomLeftRadius: 4,
        backgroundColor: backgroundColor ? backgroundColor : GRAY_5,
      }}
    />
  );
};

export default ColorStripe;
