import React, {useEffect, useState} from 'react';
import {View, ScrollView, Platform} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {selectChosenCampusID} from '../../redux/reducers/campusesReducer';
import {
  requestBusPredictions,
  requestBusRoutes,
  requestBusStops,
  requestVehicles,
} from '../../redux/actions/busesActions';
import {
  selectBusRoutes,
  selectUserLocation,
} from '../../redux/reducers/busesReducer';
import BusRoutePredictionsScreen from './BusRoutePredictionsScreen';
import {createStackNavigator} from '@react-navigation/stack';
import BusRoutesList from './BusRoutesList';
import StopsList from './StopsList';
import StopDetailsScreen from './StopDetailsScreen';
import {
  getLocation,
  LocationGetter,
} from '../../shared-components/LocationGetter';
import BusScreenSegmentedControl from './BusScreenSegmentedControl';
import BusDataRefreshControl from './BusDataRefreshControl';
import TopText from './TopText';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useIsFocused} from '@react-navigation/native';
import {FocusAwareStatusBar} from '../../shared-components';

const RoutesListsView = () => {
  const userLocation = useSelector(selectUserLocation);
  useEffect(() => {
    if (!userLocation) {
      getLocation();
    }
  }, []);

  const chosenCampusID = useSelector(selectChosenCampusID);

  const [selectedIndex, setSelectedIndex] = useState(0);

  const dispatch = useDispatch();
  const busRoutes = useSelector(selectBusRoutes);
  const isFocused = useIsFocused();

  useEffect(() => {
    dispatch(requestBusRoutes(chosenCampusID));
    dispatch(requestVehicles(chosenCampusID));
    dispatch(requestBusStops(chosenCampusID));
  }, [chosenCampusID]);

  useEffect(() => {
    dispatch(
      requestBusPredictions(busRoutes.map(busRoute => busRoute.route_id)),
    );
  }, [busRoutes]);

  return (
    <SafeAreaView style={{flex: 1, top: 20}}>
      <FocusAwareStatusBar />
      <TopText mainTitle={'Buses'} />
      {Platform.OS === 'android' && isFocused && <LocationGetter.AllBusData />}
      <View style={{marginHorizontal: 24}}>
        <BusScreenSegmentedControl
          selectedIndex={selectedIndex}
          setSelectedIndex={setSelectedIndex}
        />
      </View>

      <ScrollView
        style={{paddingHorizontal: 24}}
        refreshControl={
          Platform.OS === 'android' ? null : <BusDataRefreshControl />
        }
      >
        <View
          style={{
            paddingTop: 4,
            paddingBottom: Platform.OS === 'android' ? 100 : 0,
          }}
        >
          <BusDataToDisplay selectedIndex={selectedIndex} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const BusDataToDisplay = ({selectedIndex}) => {
  return selectedIndex === 0 ? <BusRoutesList /> : <StopsList />;
};

const BusScreen = () => {
  const Stack = createStackNavigator();

  return (
    <Stack.Navigator
      initialRouteName="RoutesLists"
      screenOptions={{headerShown: false}}
    >
      <Stack.Screen name="RoutesLists" children={() => <RoutesListsView />} />
      <Stack.Screen
        name="BusRoutePredictionsScreen"
        component={BusRoutePredictionsScreen}
        options={{
          presentation: 'card',
        }}
      />
      <Stack.Screen
        name="StopDetailsScreen"
        component={StopDetailsScreen}
        options={{
          presentation: 'card',
        }}
      />
    </Stack.Navigator>
  );
};

export default BusScreen;
