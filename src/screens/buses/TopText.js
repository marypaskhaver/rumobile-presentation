import React from 'react';
import {Text, View} from 'react-native';
import {BigText} from '../../shared-components/BigText';
import {useSelector} from 'react-redux';
import {
  getCampusNameFromID,
  selectChosenCampusID,
} from '../../redux/reducers/campusesReducer';
import {GRAY_4} from '../../constants/colors';
import {SFProTextSemibold} from '../../constants/fonts';
import {useTheme} from '@react-navigation/native';

const TopText = ({mainTitle}) => {
  const {colors} = useTheme();
  const chosenCampusID = useSelector(selectChosenCampusID);
  const campusName = getCampusNameFromID(chosenCampusID);

  return (
    <View>
      <Text
        style={{
          textTransform: 'uppercase',
          color: GRAY_4,
          marginLeft: 20,
          fontFamily: SFProTextSemibold,
        }}
      >
        {campusName}
      </Text>
      <BigText style={{marginLeft: 20, color: colors.text, marginTop: 2}}>
        {mainTitle}
      </BigText>
    </View>
  );
};

export default TopText;
