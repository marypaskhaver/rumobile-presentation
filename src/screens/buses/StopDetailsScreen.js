import React from 'react';
import {useIsFocused, useTheme} from '@react-navigation/native';
import {ImageBackground, Platform, ScrollView, Text, View} from 'react-native';
import {BackButton} from '../../shared-components/BackButton';
import DropdownMenu from './DropdownMenu';
import {SFProDisplayBold, SFProTextSemibold} from '../../constants/fonts';
import {Row} from '../../shared-components/Row';
import {
  selectBusRoutes,
  selectVehicles,
  selectFavoriteBusStopsIds,
} from '../../redux/reducers/busesReducer';
import {
  BusRouteInformation,
  getRouteColorInfoWithDefinedPropertiesForRouteNamed,
} from '../../utils';
import {useDispatch, useSelector} from 'react-redux';
import HeartButton from '../../shared-components/HeartButton';
import {GRAY_4, PASTEL_RED} from '../../constants/colors';
import ColorStripe from './ColorStripe';
import StopPrediction from '../../shared-components/StopPrediction';
import RouteTag from '../../shared-components/RouteTag';
import {
  addFavoriteBusStopIDWithRouteID,
  removeFavoriteBusStopIDWithRouteID,
} from '../../redux/actions/busesActions';
import BadBusScreen from './BadBusScreen';
import BusDataRefreshControl from './BusDataRefreshControl';
import {selectTabBarBadges} from '../../redux/reducers/tabBarBadgeReducer';
import {modifyBadgeCount} from '../../redux/actions/tabBarBadgeActions';
import {LocationGetter} from '../../shared-components/LocationGetter';
import {FocusAwareStatusBar} from '../../shared-components';

const ActiveRoutesList = ({stop_id}) => {
  const allBusRoutes = useSelector(selectBusRoutes);
  const allVehicles = useSelector(selectVehicles);

  const routesThatVisitThisStop = allBusRoutes.filter(busRoute =>
    busRoute.stops.includes(stop_id),
  );

  const busInformation = new BusRouteInformation(
    routesThatVisitThisStop,
    allVehicles,
  );

  const activeRoutes = busInformation.getActiveRoutes();

  if (activeRoutes.length === 0) return null;

  return (
    <View style={{paddingTop: 16}}>
      {activeRoutes.map(busRoute => (
        <RouteItem
          key={busRoute.route_id}
          busRoute={busRoute}
          stop_id={stop_id}
        />
      ))}
    </View>
  );
};

const InactiveRoutesList = ({stop_id}) => {
  const allBusRoutes = useSelector(selectBusRoutes);
  const allVehicles = useSelector(selectVehicles);

  const routesThatVisitThisStop = allBusRoutes.filter(busRoute =>
    busRoute.stops.includes(stop_id),
  );

  const busInformation = new BusRouteInformation(
    routesThatVisitThisStop,
    allVehicles,
  );

  const inactiveRoutesList = busInformation.getInactiveRoutes();

  if (inactiveRoutesList.length === 0) {
    return (
      <Text
        style={{
          fontSize: 16,
          color: GRAY_4,
          width: '100%',
          textAlign: 'center',
          paddingTop: 16,
        }}
      >
        No inactive routes.
      </Text>
    );
  }

  return inactiveRoutesList.map(busRoute => {
    return (
      <RouteItem
        key={busRoute.route_id}
        busRoute={busRoute}
        stop_id={stop_id}
      />
    );
  });
};

const RouteItem = ({busRoute, stop_id}) => {
  const {route_id} = busRoute;
  const routeColorInfo = getRouteColorInfoWithDefinedPropertiesForRouteNamed(
    busRoute.long_name,
  );
  const favoriteBusStopsIds = useSelector(selectFavoriteBusStopsIds);
  const dispatch = useDispatch();
  const badgeCount = useSelector(selectTabBarBadges);

  return (
    <Row
      key={route_id}
      style={{
        paddingBottom: 16,
      }}
    >
      <HeartButton
        style={{width: 24, height: 24, marginRight: 20}}
        selected={favoriteBusStopsIds[stop_id]?.includes(route_id)}
        onSelectAction={() => {
          dispatch(
            addFavoriteBusStopIDWithRouteID({
              stopID: stop_id,
              routeID: route_id,
            }),
          );
          dispatch(modifyBadgeCount(badgeCount + 1));
        }}
        onDeselectAction={() => {
          dispatch(
            removeFavoriteBusStopIDWithRouteID({
              stopID: stop_id,
              routeID: route_id,
            }),
          );
          dispatch(modifyBadgeCount(badgeCount - 1));
        }}
      />
      <Row style={{flex: 1}}>
        <RouteTag routeColorInfo={routeColorInfo} />
      </Row>
      <Row style={{flex: 1, paddingRight: 16}}>
        <StopPrediction stop_id={stop_id} route_id={route_id} />
      </Row>
      <ColorStripe backgroundColor={routeColorInfo.rcolor} />
    </Row>
  );
};

export default StopDetailsScreen = props => {
  const {colors} = useTheme();
  const {busStop} = props.route.params;
  const {name, distance, stop_id} = busStop;
  const busStopImage = require('../../assets/images/buses/busStopBackground.png');

  const allBusRoutes = useSelector(selectBusRoutes);
  const isFocused = useIsFocused();

  if (!allBusRoutes.find(busRoute => busRoute.stops.includes(stop_id)))
    return <BadBusScreen backButtonText="Stops" busInfoType="stop" />;

  return (
    <View style={{flex: 1, backgroundColor: colors.background}}>
      <FocusAwareStatusBar
        barStyle={'light-content'}
        hidden={false}
        translucent={true}
        backgroundColor="transparent"
      />
      {Platform.OS === 'android' && isFocused && (
        <LocationGetter.BusPredictions />
      )}
      <ImageBackground
        source={busStopImage}
        style={{width: '100%', height: 190, backgroundColor: 'black'}}
      >
        <View style={{paddingTop: 50}}>
          <BackButton text="Stops" isWhite={true} />
          <Row style={{paddingRight: 16}}>
            <Text
              style={{
                color: 'white',
                marginLeft: 'auto',
                fontSize: 16,
                fontFamily: SFProTextSemibold,
              }}
            >
              {distance ?? '--'} mi
            </Text>
          </Row>

          <Text
            style={{
              color: 'white',
              paddingHorizontal: 20,
              paddingTop: 16,
              fontSize: 24,
              fontFamily: SFProDisplayBold,
            }}
          >
            {name}
          </Text>
        </View>
      </ImageBackground>
      <ScrollView
        style={{
          marginTop: -48,
          borderTopLeftRadius: 16,
          borderTopRightRadius: 16,
          paddingTop: 16,
          height: '100%',
          backgroundColor: colors.background,
        }}
        refreshControl={
          Platform.OS === 'android' ? null : <BusDataRefreshControl />
        }
      >
        <RoutesToDisplay stop_id={stop_id} />
      </ScrollView>
    </View>
  );
};

const RoutesToDisplay = ({stop_id}) => {
  const allBusRoutes = useSelector(selectBusRoutes);
  const allVehicles = useSelector(selectVehicles);

  const routesThatVisitThisStop = allBusRoutes.filter(busRoute =>
    busRoute.stops.includes(stop_id),
  );

  const busInformation = new BusRouteInformation(
    routesThatVisitThisStop,
    allVehicles,
  );

  const activeRoutes = busInformation.getActiveRoutes();

  return (
    <View style={{paddingLeft: 20}}>
      <ActiveRoutesList stop_id={stop_id} />

      <DropdownMenu
        title={'Hide Inactive Routes'}
        titleStyle={{
          color: PASTEL_RED,
          fontSize: 16,
        }}
        titleAfterPress={'Show Inactive Routes'}
        rowStyle={{
          justifyContent: 'center',
          paddingTop: 30,
          paddingBottom: 30,
        }}
        open={activeRoutes.length === 0}
        arrowStyle={{display: 'none'}}
      >
        <InactiveRoutesList stop_id={stop_id} />
      </DropdownMenu>
    </View>
  );
};
