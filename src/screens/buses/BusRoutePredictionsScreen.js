import React, {useState} from 'react';
import {Text, ScrollView, View, Platform} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {BackButton} from '../../shared-components/BackButton';
import {getColorFromBusRouteName} from '../../utils/getColorFromBusRouteName';
import {
  addFavoriteBusStopIDWithRouteID,
  removeFavoriteBusStopIDWithRouteID,
} from '../../redux/actions/busesActions';
import {useSelector} from 'react-redux';
import {
  selectBusPredictions,
  selectBusRoutes,
  selectBusStops,
  selectFavoriteBusStopsIds,
  selectUserLocation,
} from '../../redux/reducers/busesReducer';
import {useIsFocused, useTheme} from '@react-navigation/native';
import {SFProTextRegular, SFProTextSemibold} from '../../constants/fonts';
import StopPrediction from '../../shared-components/StopPrediction';
import {Row} from '../../shared-components/Row';
import {Line} from '../../shared-components/Line';
import HeartButton from '../../shared-components/HeartButton';
import {getDistance, convertDistance} from 'geolib';
import {stopOrders} from '../../../bus_stop_orders.json';
import {GRAY_4} from '../../constants/colors';
import {useDispatch} from 'react-redux';
import BadBusScreen from './BadBusScreen';
import BusDataRefreshControl from './BusDataRefreshControl';
import LowToast from '../../shared-components/LowToast';
import {selectTabBarBadges} from '../../redux/reducers/tabBarBadgeReducer';
import {modifyBadgeCount} from '../../redux/actions/tabBarBadgeActions';
import {LocationGetter} from '../../shared-components/LocationGetter';
import {FocusAwareStatusBar} from '../../shared-components';
const {Storyboard} = require('@glassbox/react-native-storyboard');

const BusRoutePredictionsScreen = props => {
  const {route_id, long_name} = props.route.params;

  const allBusRoutes = useSelector(selectBusRoutes);

  // Make into selector (also look into reselect to nest selectors, memoized selectors)
  const currentRoute = allBusRoutes.find(
    busRoute => busRoute.route_id === route_id,
  );

  const routeStops = stopOrders[long_name];

  if (!currentRoute || !routeStops) {
    return <BadBusScreen backButtonText="Buses" busInfoType="route" />;
  }

  return <RoutesAndPredictions route_id={route_id} long_name={long_name} />;
};

const RoutesAndPredictions = ({route_id, long_name}) => {
  const allBusRoutes = useSelector(selectBusRoutes);
  const [showLowToast, setShowLowToast] = useState(false);

  // Make into selector (also look into reselect to nest selectors, memoized selectors)
  const currentRoute = allBusRoutes.find(
    busRoute => busRoute.route_id === route_id,
  );

  const routeStops = stopOrders[long_name];

  const location = useSelector(selectUserLocation);
  const allBusStops = useSelector(selectBusStops);

  const currentStopsIDs = currentRoute.stops;

  const userLocation = {
    longitude: location?.coords?.longitude ?? 0,
    latitude: location?.coords?.latitude ?? 0,
  };

  const stopInfo = [];
  const busesWithDistances = [];

  const getFormattedBusArrivalTimesFromStopID = (stop_id, route_id) => {
    const currentRoutePredictions = useSelector(selectBusPredictions).filter(
      prediction =>
        prediction.arrivals.filter(arrival => arrival.route_id === route_id)
          .length > 0,
    );

    const stopPrediction = currentRoutePredictions.find(
      prediction => prediction.stop_id === stop_id,
    );

    const busArrivalTimes = stopPrediction?.arrivals
      ?.map(arrival =>
        getMinutesBetweenDates(new Date(arrival.arrival_at), new Date()),
      )
      .filter(time => time > 0)
      .sort((time1, time2) => time1 - time2);

    if (busArrivalTimes?.length === 0 && !showLowToast) {
      setShowLowToast(true);
      return null;
    }

    return <StopPrediction stop_id={stop_id} route_id={route_id} />;
  };

  allBusStops
    .filter(busStop => currentStopsIDs.includes(busStop.stop_id))
    .map(busStop => {
      if (routeStops.indexOf(busStop.name) === -1) {
        Storyboard.reportEvent('stopNotRecordedInRoute', {
          message: `${busStop.name} not recorded in ${long_name}.`,
        });
      }
      return busStop;
    })
    .sort((busStop1, busStop2) => {
      const busStop1Index = routeStops.indexOf(busStop1.name);
      const busStop2Index = routeStops.indexOf(busStop2.name);

      return busStop1Index - busStop2Index;
    })
    .map(busStop => {
      const busStopLocation = {
        longitude: busStop.location.lng,
        latitude: busStop.location.lat,
      };

      const distanceBetweenUserAndBus = location
        ? convertDistance(
            getDistance(userLocation, busStopLocation),
            'mi',
          ).toFixed(1)
        : '--';

      const busStopWithDistance = Object.assign(busStop, {
        distance: distanceBetweenUserAndBus,
      });
      busesWithDistances.push(busStopWithDistance);

      stopInfo.push({
        name: busStop.name,
        stop_id: busStop.stop_id,
        distance: `${distanceBetweenUserAndBus} mi`,
      });

      return stopInfo;
    });

  const sortedDistances = busesWithDistances.sort((a, b) => {
    if (a && b) return Number(a.distance) - Number(b.distance);
  });

  const nearestStopID = sortedDistances.length && sortedDistances[0].stop_id;

  const Predictions = ({stopInfo, nearestStopID, route_id}) => {
    const dispatch = useDispatch();
    const badgeCount = useSelector(selectTabBarBadges);
    const favoriteBusStopsIds = useSelector(selectFavoriteBusStopsIds);
    const {dark, colors} = useTheme();
    const location = useSelector(selectUserLocation);
    const isFocused = useIsFocused();

    return stopInfo.map((info, index) => (
      <View
        key={info.stop_id}
        style={{
          backgroundColor:
            info.stop_id === nearestStopID && location
              ? dark
                ? 'rgb(35, 66, 2)'
                : 'rgb(231, 255, 205)'
              : colors.background,
          paddingLeft: 24,
        }}
      >
        {Platform.OS === 'android' && isFocused && (
          <LocationGetter.BusPredictions />
        )}
        <Row style={{alignItems: 'center'}}>
          <HeartButton
            style={{height: 24, width: 24, marginRight: 22}}
            selected={favoriteBusStopsIds[info.stop_id]?.includes(route_id)}
            onSelectAction={() => {
              dispatch(
                addFavoriteBusStopIDWithRouteID({
                  stopID: info.stop_id,
                  routeID: route_id,
                }),
              );

              dispatch(modifyBadgeCount(badgeCount + 1));
            }}
            onDeselectAction={() => {
              dispatch(
                removeFavoriteBusStopIDWithRouteID({
                  stopID: info.stop_id,
                  routeID: route_id,
                }),
              );

              dispatch(modifyBadgeCount(badgeCount - 1));
            }}
          />
          <View style={{flex: 1, flexDirection: 'column'}}>
            <Row style={{paddingVertical: 12, paddingRight: 24}}>
              <View style={{flexDirection: 'column', flex: 1}}>
                <Text
                  key={info.stop_id}
                  style={{
                    fontFamily: SFProTextSemibold,
                    fontSize: 17,
                    flexWrap: 'wrap',
                    color: colors.text,
                  }}
                >
                  {info.name}
                </Text>

                <View style={{flexDirection: 'column'}}>
                  {info.stop_id === nearestStopID && location ? (
                    <Text
                      style={{
                        paddingTop: 4,
                        fontFamily: SFProTextRegular,
                        color: dark ? 'rgb(140,218,55)' : 'rgb(78, 136, 14)',
                      }}
                    >
                      Nearest stop to your location
                    </Text>
                  ) : null}
                  <Text
                    style={{
                      color: GRAY_4,
                      fontFamily: SFProTextRegular,
                      paddingTop: 4,
                    }}
                  >
                    {info.distance}
                  </Text>
                </View>
              </View>
              <View style={{flex: 1}}>
                {getFormattedBusArrivalTimesFromStopID(info.stop_id, route_id)}
              </View>
            </Row>
            <Line style={{opacity: index < stopInfo.length - 1 ? 0.2 : 0}} />
          </View>
        </Row>
      </View>
    ));
  };

  return (
    <SafeAreaView style={{flex: 1}} edges={['right', 'left']}>
      <FocusAwareStatusBar
        barStyle={'light-content'}
        hidden={false}
        translucent={true}
        backgroundColor="transparent"
      />
      {showLowToast && <LowToast message={"Can't get clear data."} />}

      <View
        style={{
          height: 'auto',
          paddingBottom: 12,
          paddingTop: 48,
          backgroundColor: getColorFromBusRouteName(long_name),
        }}
      >
        <BackButton text="Buses" isWhite={true} />
        <Text
          style={{
            fontSize: 22,
            fontFamily: SFProTextSemibold,
            color: 'white',
            marginTop: 32,
            paddingLeft: 26,
            paddingRight: 18,
          }}
        >
          {long_name}
        </Text>
      </View>

      <ScrollView
        refreshControl={
          Platform.OS === 'android' ? null : <BusDataRefreshControl />
        }
      >
        <Predictions
          stopInfo={stopInfo}
          nearestStopID={nearestStopID}
          route_id={route_id}
        />
      </ScrollView>
    </SafeAreaView>
  );
};

export default BusRoutePredictionsScreen;
