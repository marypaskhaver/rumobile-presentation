import {useTheme} from '@react-navigation/native';
import React from 'react';
import {Text, View, TouchableOpacity, Platform} from 'react-native';
import {PASTEL_RED, GRAY_1, GRAY_3, GRAY_4} from '../../constants/colors';
import {SFProTextSemibold} from '../../constants/fonts';
import {
  BackVArrow,
  BigText,
  FocusAwareStatusBar,
  Row,
} from '../../shared-components/';

const BoldText = ({color, children}) => {
  return (
    <Text
      style={{
        textTransform: 'uppercase',
        color,
        fontFamily: SFProTextSemibold,
      }}
    >
      {children}
    </Text>
  );
};

export default RedTopView = ({
  children,
  boldText,
  bigText,
  buttonText,
  showButton = true,
  inverted = false,
  onButtonPressAction,
}) => {
  const {dark, colors} = useTheme();

  let barStyle;

  if (Platform.OS === 'android') {
    if (inverted) {
      barStyle = dark ? 'light-content' : 'dark-content';
    } else {
      barStyle = 'light-content';
    }
  } else {
    barStyle = 'light-content';
  }

  return (
    <View style={{flex: 1}}>
      <FocusAwareStatusBar
        barStyle={barStyle}
        hidden={false}
        translucent={true}
        backgroundColor="transparent"
      />
      <View
        style={{
          backgroundColor: inverted ? colors.background : PASTEL_RED,
          height: Platform.OS === 'android' ? 124 : 108,
          paddingTop: Platform.OS === 'android' ? 16 : 0,
          paddingHorizontal: 16,
        }}
      >
        <Row>
          <View style={{paddingTop: 20}}>
            <BoldText color={inverted ? (dark ? GRAY_3 : GRAY_4) : GRAY_1}>
              {boldText}
            </BoldText>
            <BigText style={{color: inverted ? colors.text : 'white'}}>
              {bigText}
            </BigText>
          </View>
          <BackVArrow color={inverted ? PASTEL_RED : 'white'} />
        </Row>
        {showButton && (
          <TouchableOpacity
            disabled={!!!onButtonPressAction}
            onPress={onButtonPressAction}
          >
            <Text
              style={{
                color: 'white',
                position: 'absolute',
                textTransform: 'capitalize',
                fontFamily: SFProTextSemibold,
                fontSize: 16,
                right: 0,
                top: -12,
              }}
            >
              {buttonText}
            </Text>
          </TouchableOpacity>
        )}
      </View>
      {children}
    </View>
  );
};
