import {useNavigation, useTheme} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {View, Text, TouchableOpacity, Animated, Platform} from 'react-native';
import {useSelector} from 'react-redux';
import {SFProTextRegular, SFProTextSemibold} from '../../../constants/fonts';
import {selectFavoriteSections} from '../../../redux/reducers/academicsReducer';
import {
  DateFormatter,
  getCampusColorFromCampusName,
  identifyWeekdayFromCharacters,
  refreshAt,
} from '../../../utils';
import {Row, SectionLocation, SectionTime} from '../../../shared-components';
import ColorStripe from '../../buses/ColorStripe';
import {Swipeable} from 'react-native-gesture-handler';
import {PASTEL_RED} from '../../../constants/colors';
import store from '../../../redux/store';
import {removeFavoriteSection} from '../../../redux/actions/academicActions';

const noClassesTodayMessages = [
  'No classes, have a beary great day!-🐻',
  'No classes today, time to play.-👾🎮',
  "No classes, yup that's right.-😎",
  'Keep Calm and No Classes Today.-👑',
  'No classes, time for Zzz.-😴',
  'Looks like you have no classes today.-😋',
  'Woohoo, no classes!-😜',
  'Sweet freedom!!-😄🍬',
  'The coast is clear, no classes today.-🐳',
  'Take it easy, no classes today.-🐌',
];

export default FavoriteSectionsDisplay = () => {
  const {colors} = useTheme();
  const favoriteSections = useSelector(selectFavoriteSections);

  const [currentWeekday, setCurrentWeekday] = useState(
    new DateFormatter().getWeekday().toLowerCase(),
  );

  useEffect(() => {
    refreshAt(0, 0, 0, () => {
      setCurrentWeekday(new DateFormatter().getWeekday().toLowerCase());
    });
  }, []);

  const navigation = useNavigation();
  const [sectionToDelete, setSectionToDelete] = useState(null);

  const allMeetingTimes = Object.values(favoriteSections)
    .map(section => {
      const {meetingTimes} = section;
      meetingTimes.map(meetingTime => {
        meetingTime.section = {title: section.title, index: section.index};
      });
      return meetingTimes;
    })
    .flat();

  const meetingTimesToday = allMeetingTimes.filter(meetingTime => {
    const {meetingDay} = meetingTime;
    const weekday = identifyWeekdayFromCharacters(meetingDay).toLowerCase();
    return weekday === currentWeekday;
  });

  const renderRightActions = (progress, dragX) => {
    const translateX = dragX.interpolate({
      inputRange: [-100, 100],
      outputRange: [-20, 100],
    });

    return (
      <TouchableOpacity
        style={{marginTop: Platform.OS === 'ios' ? 12 : 14}}
        onPress={() => {
          store.dispatch(removeFavoriteSection(sectionToDelete));
        }}
      >
        <Animated.View
          style={[
            {
              transform: [{translateX}],
              backgroundColor: PASTEL_RED,
              width: 64,
              height: 48,
              alignSelf: 'center',
              resizeMode: 'contain',
            },
          ]}
        >
          <Text
            style={{
              fontFamily: SFProTextRegular,
              color: 'white',
              paddingVertical: Platform.OS === 'ios' ? 16 : 14,
              height: '100%',
              alignSelf: 'center',
            }}
          >
            Delete
          </Text>
        </Animated.View>
      </TouchableOpacity>
    );
  };

  if (meetingTimesToday.length === 0) {
    const randomIndex = Math.floor(
      Math.random() * noClassesTodayMessages.length,
    );

    const messageInfo = noClassesTodayMessages[randomIndex].split('-');

    return (
      <View style={{alignItems: 'center'}}>
        <Text style={{fontSize: 20, marginTop: -8, paddingBottom: 4}}>
          {messageInfo[1]}
        </Text>
        <Text
          style={{
            color: colors.text,
            fontSize: 14,
            fontFamily: SFProTextRegular,
            alignSelf: 'center',
            paddingBottom: 8,
            paddingRight: 8,
          }}
        >
          {messageInfo[0]}
        </Text>
      </View>
    );
  }

  return sortMeetingTimes(meetingTimesToday).map(
    (meetingTime, meetingTimeIndex) => {
      const {section} = meetingTime;
      const {title, index} = section;

      const campusColor = getCampusColorFromCampusName(meetingTime.campusName);

      return (
        <Swipeable
          key={meetingTimeIndex}
          onSwipeableOpen={() => setSectionToDelete(section)}
          onSwipeableClose={() => setSectionToDelete(null)}
          renderRightActions={renderRightActions}
        >
          <></>
          <TouchableOpacity
            disabled={Platform.OS === 'android'}
            onPress={() =>
              navigation.navigate('EditSectionScreen', {
                sectionIndex: index,
              })
            }
          >
            <View
              style={{
                justifyContent: 'space-between',
              }}
            >
              {/* DOESN'T WRAP - FIX */}
              <Text
                style={{
                  color: colors.text,
                  fontFamily: SFProTextSemibold,
                  fontSize: 16,
                  textTransform: 'capitalize',
                }}
              >
                {title.trim().length > 0 ? title : 'No name provided'}
              </Text>

              <Row
                key={meetingTimeIndex}
                style={{
                  marginTop: -8,
                  justifyContent: 'space-between',
                }}
              >
                <SectionLocation
                  meetingTime={meetingTime}
                  useEditedLocationIfExists={true}
                />

                <Row>
                  <SectionTime meetingTime={meetingTime} />

                  <View style={{paddingLeft: 16}}>
                    <ColorStripe backgroundColor={campusColor} />
                  </View>
                </Row>
              </Row>
            </View>
          </TouchableOpacity>
        </Swipeable>
      );
    },
  );
};
