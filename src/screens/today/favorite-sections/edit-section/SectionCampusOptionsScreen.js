import React, {useState} from 'react';
import {FlatList, TouchableOpacity} from 'react-native';
import {useNavigation, useTheme} from '@react-navigation/native';
import RedTopView from '../../RedTopView';
import {Card, Line, RowWithCheckmark} from '../../../../shared-components';
import {campusColor} from '../../../../../bus_color.json';
import {useDispatch, useSelector} from 'react-redux';
import {selectMeetingTimeAtNthIndex} from '../../../../redux/reducers/editSectionReducer';
import {editMeetingTime} from '../../../../redux/actions/editSectionActions';

const campuses = {
  'COLLEGE AVENUE': 'College Ave',
  BUSCH: 'Busch',
  LIVINGSTON: 'Livingston',
  'DOUGLAS/COOK': 'Cook/Douglass',
  NEWARK: 'Newark',
  CAMDEN: 'Camden',
  'CAMDEN COUNTY COLLEGE': 'Camden CC',
  'DOWNTOWN NB': 'Downtown NB',
  'COUNTY COLLEGE OF MORR': 'CC of Morr',
  'ATLANTIC CITY/MAYS LAN': 'Mays Landing',
  'BROOKDALE COMMUNITY CO': 'Brookdale CC',
  RUTGERS: 'Rutgers',
  'MERCER COUNTY COLLEGE': 'Mercer CC',
  CHINA: 'China',
};

export default SectionCampusOptionsScreen = props => {
  const {selectedMeetingDayIndex} = props.route.params;
  const navigation = useNavigation();
  const {colors} = useTheme();

  const currentMeetingTime = useSelector(
    selectMeetingTimeAtNthIndex(selectedMeetingDayIndex),
  );

  const currentCampusName =
    campuses[currentMeetingTime.campusName] ?? 'Rutgers';

  const [checkedCampus, setCheckedCampus] = useState(currentCampusName);

  const dispatch = useDispatch();

  if (!currentMeetingTime) return null;

  const updateCheckedCampuses = newCampus => {
    setCheckedCampus(newCampus);

    setTimeout(() => {
      const newCampusName =
        newCampus === '** INVALID **' || newCampus === 'Rutgers'
          ? 'RUTGERS'
          : campusColor.find(campus => campus.cName === newCampus).campus;

      dispatch(
        editMeetingTime(selectedMeetingDayIndex, {campusName: newCampusName}),
      );

      navigation.goBack();
    }, 200);
  };

  return (
    <RedTopView boldText={'Schedule'} bigText={'Campuses'}>
      <Card
        style={{
          maxHeight: '80%',
          marginHorizontal: 16,
          paddingTop: 8,
          paddingBottom: 8,
          paddingRight: 0,
          marginTop: 16,
          backgroundColor: colors.card,
        }}
      >
        <FlatList
          data={Object.values(campuses)}
          renderItem={({item}) => (
            <TouchableOpacity onPress={() => updateCheckedCampuses(item)}>
              <RowWithCheckmark
                textToDisplay={item}
                style={{paddingVertical: 14, paddingRight: 16}}
                isChecked={checkedCampus === item}
              />
            </TouchableOpacity>
          )}
          ItemSeparatorComponent={() => <Line />}
        />
      </Card>
    </RedTopView>
  );
};
