import React from 'react';
import {Text} from 'react-native';
import {GRAY_4} from '../../../../constants/colors';
import {SFProTextRegular} from '../../../../constants/fonts';

export default GrayText = ({children}) => {
  return (
    <Text
      style={{
        color: GRAY_4,
        fontFamily: SFProTextRegular,
        fontSize: 16,
      }}
    >
      {children}
    </Text>
  );
};
