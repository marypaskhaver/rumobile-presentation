import React, {useState} from 'react';
import {FlatList, TouchableOpacity} from 'react-native';
import {useNavigation, useTheme} from '@react-navigation/native';
import RedTopView from '../../RedTopView';
import {Card, Line, RowWithCheckmark} from '../../../../shared-components';
import {useDispatch, useSelector} from 'react-redux';
import {selectMeetingTimes} from '../../../../redux/reducers/editSectionReducer';
import {
  addDefaultMeetingTimeForWeekdayAbbreviation,
  deleteMeetingTimesMeetingOnWeekdayAbbreviation,
} from '../../../../redux/actions/editSectionActions';
import {sortMeetingTimes} from '../../../../utils';
import {abbreviationToWeekdayMap} from '../../../../constants/academics';

export default SectionDaysOptionsScreen = () => {
  const currentMeetingTimes = useSelector(selectMeetingTimes);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const {colors} = useTheme();

  const weekdaysToAbbreviationsMap = {
    Monday: 'M',
    Tuesday: 'T',
    Wednesday: 'W',
    Thursday: 'TH',
    Friday: 'F',
    Saturday: 'S',
    Sunday: 'U',
    'Independent Study': null,
  };

  const sortedMeetingTimes = sortMeetingTimes(currentMeetingTimes);

  const abbreviatedWeekdaysThisMeets = sortedMeetingTimes.map(meetingTime => {
    const {meetingDay} = meetingTime;
    return meetingDay;
  });

  const fullWeekdaysThisMeets = abbreviatedWeekdaysThisMeets.map(
    abbreviatedWeekday => abbreviationToWeekdayMap[abbreviatedWeekday],
  );

  const [checkedWeekdays, setCheckedWeekdays] = useState(fullWeekdaysThisMeets);

  const updateCheckedWeekdays = weekday => {
    if (checkedWeekdays.includes(weekday)) {
      const checkedWeekdaysWithoutGivenWeekday = checkedWeekdays.filter(
        day => day !== weekday,
      );

      if (checkedWeekdaysWithoutGivenWeekday.length === 0) return;

      setCheckedWeekdays(checkedWeekdaysWithoutGivenWeekday);
      return;
    }

    setCheckedWeekdays([...checkedWeekdays, weekday]);
  };

  const clearStoredMeetingTimesOfUnselectedDays = () => {
    const uncheckedWeekdays = Object.keys(weekdaysToAbbreviationsMap).filter(
      weekday => !checkedWeekdays.includes(weekday),
    );
    uncheckedWeekdays.map(weekday => {
      const weekdayAbbreviation = weekdaysToAbbreviationsMap[weekday];
      dispatch(
        deleteMeetingTimesMeetingOnWeekdayAbbreviation(weekdayAbbreviation),
      );
    });
  };

  const addDefaultMeetingTimesForNewlySelectedDays = () => {
    checkedWeekdays.map(weekday => {
      const abbreviation = weekdaysToAbbreviationsMap[weekday];
      dispatch(addDefaultMeetingTimeForWeekdayAbbreviation(abbreviation));
    });
  };

  return (
    <RedTopView
      boldText={'Schedule'}
      bigText={'Days'}
      buttonText={'Save'}
      onButtonPressAction={() => {
        navigation.goBack();
        clearStoredMeetingTimesOfUnselectedDays();
        addDefaultMeetingTimesForNewlySelectedDays();
      }}
    >
      <Card
        style={{
          maxHeight: '80%',
          marginHorizontal: 16,
          paddingTop: 8,
          paddingBottom: 8,
          paddingRight: 0,
          marginTop: 16,
          backgroundColor: colors.card,
        }}
      >
        <FlatList
          data={Object.keys(weekdaysToAbbreviationsMap)}
          renderItem={({item}) => (
            <TouchableOpacity onPress={() => updateCheckedWeekdays(item)}>
              <RowWithCheckmark
                textToDisplay={item === 'Independent Study' ? 'All Day' : item}
                style={{paddingVertical: 14, paddingRight: 16}}
                isChecked={checkedWeekdays.includes(item)}
              />
            </TouchableOpacity>
          )}
          ItemSeparatorComponent={() => <Line />}
        />
      </Card>
    </RedTopView>
  );
};
