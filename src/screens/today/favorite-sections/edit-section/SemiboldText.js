import {useTheme} from '@react-navigation/native';
import React from 'react';
import {Text} from 'react-native';
import {SFProTextSemibold} from '../../../../constants/fonts';

export default SemiboldText = ({children}) => {
  const {colors} = useTheme();

  return (
    <Text
      style={{
        fontFamily: SFProTextSemibold,
        fontSize: 16,
        maxWidth: '80%',
        flexWrap: 'wrap',
        color: colors.text,
        textAlign: 'right',
      }}
    >
      {children}
    </Text>
  );
};
