import React, {useEffect, useState} from 'react';
import {useNavigation, useTheme} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {GRAY_4, PASTEL_RED} from '../../../../constants/colors';
import {SFProTextRegular, SFProTextSemibold} from '../../../../constants/fonts';
import {selectFavoriteSections} from '../../../../redux/reducers/academicsReducer';
import {Card, Line, Row} from '../../../../shared-components';
import RedTopView from '../../RedTopView';
import {
  Alert,
  Image,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {sortMeetingTimes, TimeFormatter} from '../../../../utils';
import {titleCase} from 'title-case';
import {
  addMeetingTime,
  deleteMeetingTimesMeetingOnWeekdayAbbreviation,
} from '../../../../redux/actions/editSectionActions';
import {selectMeetingTimes} from '../../../../redux/reducers/editSectionReducer';
import {
  addFavoriteSection,
  editSectionTitleAndMeetingTimes,
  removeFavoriteSection,
} from '../../../../redux/actions/academicActions';
import SectionInfoForm from './SectionInfoForm';
import GrayText from './GrayText';
import SemiboldText from './SemiboldText';
import {selectTabBarBadges} from '../../../../redux/reducers/tabBarBadgeReducer';
import {modifyBadgeCount} from '../../../../redux/actions/tabBarBadgeActions';
import {abbreviationToWeekdayMap} from '../../../../constants/academics';

const weekdayToSemiAbbreviationMap = {
  'Independent Study': 'Any',
  Monday: 'Mon',
  Tuesday: 'Tue',
  Wednesday: 'Wed',
  Thursday: 'Thu',
  Friday: 'Fri',
  Saturday: 'Sat',
  Sunday: 'Sun',
};

export default EditSectionScreen = props => {
  const {colors} = useTheme();
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const badgeCount = useSelector(selectTabBarBadges);

  const customIndex = Math.random();

  const defaultMeetingTime = {
    baClassHours: 'B',
    buildingCode: null,
    campusAbbrev: null,
    campusLocation: null,
    campusName: null,
    endTime: '0130',
    meetingDay: 'M',
    meetingModeCode: '19',
    meetingModeDesc: 'PROJ-IND',
    pmCode: 'P',
    roomNumber: null,
    startTime: '1200',
  };

  const defaultSection = {
    index: customIndex,
    meetingTimes: [defaultMeetingTime],
    title: 'CUSTOM CLASS',
  };

  const sectionIndex = props.route.params
    ? props.route.params.sectionIndex
    : customIndex;
  const favoriteSections = props.route.params
    ? useSelector(selectFavoriteSections)
    : {[customIndex]: defaultSection};
  const sectionToEdit = favoriteSections[sectionIndex];

  const meetingTimesUnderEdit = useSelector(selectMeetingTimes);

  const [newTitle, setNewTitle] = useState(
    sectionToEdit ? sectionToEdit.title : '',
  );

  const [selectedMeetingDayIndex, setSelectedMeetingDayIndex] = useState(0);

  useEffect(() => {
    if (selectedMeetingDayIndex > meetingTimesUnderEdit.length) {
      setSelectedMeetingDayIndex(0);
    }
  }, []);

  const title = sectionToEdit?.title;
  const meetingTimes = sectionToEdit?.meetingTimes;

  useEffect(() => {
    Object.keys(abbreviationToWeekdayMap).forEach(abbreviation => {
      dispatch(deleteMeetingTimesMeetingOnWeekdayAbbreviation(abbreviation));
    });
  }, []);

  useEffect(() => {
    sortMeetingTimes(meetingTimes).forEach(meetingTime =>
      dispatch(addMeetingTime(meetingTime)),
    );
  }, []);

  const [isKeyboardVisible, setKeyboardVisible] = useState(false);

  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      () => {
        setKeyboardVisible(true);
      },
    );
    const keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => {
        setKeyboardVisible(false);
      },
    );

    return () => {
      keyboardDidHideListener.remove();
      keyboardDidShowListener.remove();
    };
  }, []);

  if (!sectionToEdit) return null;

  const sortedMeetingTimes = sortMeetingTimes(meetingTimesUnderEdit);

  const abbreviatedWeekdaysThisMeets = sortedMeetingTimes.map(meetingTime => {
    const {meetingDay} = meetingTime;
    return meetingDay;
  });

  const fullWeekdaysThisMeets = abbreviatedWeekdaysThisMeets.map(
    abbreviatedWeekday => abbreviationToWeekdayMap[abbreviatedWeekday],
  );

  const semiAbbreviatedWeekdaysThisMeets = fullWeekdaysThisMeets.map(
    weekday => weekdayToSemiAbbreviationMap[weekday],
  );

  const showInvalidMeetingTimesAlert = invalidTime => {
    const {meetingDay} = invalidTime;
    const fullWeekday = abbreviationToWeekdayMap[meetingDay];

    Alert.alert(
      `Cannot Save ${fullWeekday} Class`,
      'The start date must be before the end date.',
      [
        {
          text: 'Okay',
          onPress: () => {},
        },
      ],
    );
  };

  const updateFavoriteSections = () => {
    const genericTimeFormatter = new TimeFormatter();

    const invalidTime = meetingTimesUnderEdit.find(meetingTime => {
      const specialSpan = meetingTime.specialSpan;

      const specialStartTime = specialSpan ? specialSpan.split('-')[0] : '';
      const specialEndTime = specialSpan ? specialSpan.split('-')[1] : '';

      const specialStartTimePmCode =
        genericTimeFormatter.getPmCodeFromFormattedTime(specialStartTime);
      const specialEndTimePmCode =
        genericTimeFormatter.getPmCodeFromFormattedTime(specialEndTime);

      const startTimeFormatter = new TimeFormatter(
        specialStartTime
          ? genericTimeFormatter.getOriginalTime(specialStartTime)
          : meetingTime.startTime ?? '1200',
      ).addColon();

      startTimeFormatter.addTimeOfDay(
        specialStartTimePmCode
          ? specialStartTimePmCode
          : meetingTime.pmCode ?? 'P',
      );

      const endTimeFormatter = new TimeFormatter(
        specialEndTime
          ? genericTimeFormatter.getOriginalTime(specialEndTime)
          : meetingTime.endTime ?? '0130',
      )
        .addColon()
        .addTimeOfDay(
          specialEndTimePmCode
            ? specialEndTimePmCode
            : meetingTime.pmCode ?? 'P',
        );

      const span = startTimeFormatter.createUnadjustedSpan(endTimeFormatter);

      return !startTimeFormatter.isEndTimeAfterStartTime(span);
    });

    if (invalidTime) {
      showInvalidMeetingTimesAlert(invalidTime);
      return;
    }

    if (props.route.params) {
      dispatch(
        editSectionTitleAndMeetingTimes({
          sectionIndex,
          newTitle,
          newMeetingTimes: meetingTimesUnderEdit,
        }),
      );
    } else {
      const newSection = {
        index: customIndex,
        meetingTimes: meetingTimesUnderEdit,
        title: newTitle,
      };
      dispatch(addFavoriteSection(newSection));
    }

    dispatch(modifyBadgeCount(badgeCount + 1));
    navigation.goBack();
  };

  const showDeleteSectionAlert = () => {
    Alert.alert('Delete Class', 'Are you sure you want to delete this class?', [
      {
        text: 'Cancel',
        onPress: () => {},
      },
      {
        text: 'Delete',
        style: 'destructive',
        onPress: () => {
          dispatch(removeFavoriteSection(sectionToEdit));
          navigation.goBack();
          navigation.navigate('MyScheduleScreen');
        },
      },
    ]);
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      keyboardVerticalOffset={56}
      style={{flex: 1}}
    >
      <RedTopView
        boldText={'Rutgers'}
        bigText={props.route.params ? 'Class' : 'New Class'}
        buttonText="Save"
        onButtonPressAction={updateFavoriteSections}
      >
        <ScrollView>
          <Card
            style={{
              maxHeight: '80%',
              marginHorizontal: 16,
              marginTop: 16,
              paddingTop: Platform.OS === 'android' ? 0 : 16,
              backgroundColor: colors.card,
            }}
          >
            <Row style={{justifyContent: 'space-between'}}>
              <GrayText>Name</GrayText>
              <TextInput
                onChangeText={setNewTitle}
                style={{
                  fontFamily: SFProTextSemibold,
                  maxWidth: '80%',
                  fontSize: 16,
                  flexWrap: 'wrap',
                  color: colors.text,
                }}
                defaultValue={titleCase(
                  (title.trim().length > 0
                    ? title
                    : 'No name provided'
                  ).toLowerCase(),
                )}
              />
            </Row>
            <Line
              style={{
                marginBottom: 16,
                marginTop: Platform.OS === 'android' ? 0 : 16,
              }}
            />
            <TouchableOpacity
              onPress={() => navigation.navigate('SectionDaysOptionsScreen')}
            >
              <Row style={{justifyContent: 'space-between'}}>
                <GrayText>Days</GrayText>
                <SemiboldText>
                  {semiAbbreviatedWeekdaysThisMeets.join(', ')}
                </SemiboldText>
              </Row>
            </TouchableOpacity>
          </Card>

          <Row
            style={{
              justifyContent: 'flex-start',
              marginTop: 36,
              marginBottom: 10,
            }}
          >
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              style={{marginHorizontal: 28, paddingBottom: 10}}
            >
              {fullWeekdaysThisMeets.map((weekday, index) => (
                <TouchableOpacity
                  key={index}
                  onPress={() => setSelectedMeetingDayIndex(index)}
                >
                  <Text
                    style={{
                      color:
                        index === selectedMeetingDayIndex ? PASTEL_RED : GRAY_4,
                      fontFamily: SFProTextSemibold,
                      fontSize: 12,
                      paddingRight: 10,
                    }}
                  >
                    {weekday === 'Independent Study'
                      ? 'ANY DAY'
                      : weekday.toUpperCase()}
                  </Text>
                </TouchableOpacity>
              ))}
            </ScrollView>
          </Row>

          {meetingTimesUnderEdit[selectedMeetingDayIndex] && (
            <SectionInfoForm
              selectedMeetingDayIndex={selectedMeetingDayIndex}
            />
          )}
        </ScrollView>
      </RedTopView>

      {!isKeyboardVisible && props.route.params && (
        <TouchableOpacity
          style={{
            marginHorizontal: 24,
            marginBottom: 40,
            backgroundColor: PASTEL_RED,
            alignItems: 'center',
            borderRadius: 8,
          }}
          onPress={() => showDeleteSectionAlert()}
        >
          <Row style={{height: 44}}>
            <Image
              style={{resizeMode: 'contain', width: 20}}
              source={require('../../../../assets/icons/today_screen/trash.png')}
            />
            <Text
              style={{
                fontFamily: SFProTextRegular,
                color: 'white',
                fontSize: 18,
                marginLeft: 16,
              }}
            >
              Delete Class
            </Text>
          </Row>
        </TouchableOpacity>
      )}
      {!isKeyboardVisible && !props.route.params && (
        <TouchableOpacity
          style={{
            marginHorizontal: 24,
            marginBottom: 40,
            backgroundColor: PASTEL_RED,
            alignItems: 'center',
            borderRadius: 8,
          }}
          onPress={updateFavoriteSections}
        >
          <Row style={{height: 44}}>
            <Image
              style={{resizeMode: 'contain', width: 20, tintColor: 'white'}}
              source={require('../../../../assets/images/buttons/addclassbutton.png')}
            />
            <Text
              style={{
                fontFamily: SFProTextRegular,
                color: 'white',
                fontSize: 18,
                marginLeft: 16,
              }}
            >
              Add Class
            </Text>
          </Row>
        </TouchableOpacity>
      )}
    </KeyboardAvoidingView>
  );
};
