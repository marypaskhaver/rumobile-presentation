import React, {useEffect, useMemo, useState} from 'react';
import {useNavigation, useTheme} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {GRAY_4} from '../../../../constants/colors';
import {SFProTextSemibold} from '../../../../constants/fonts';
import {Card, Line, Row} from '../../../../shared-components';
import {Alert, Platform, Text, TextInput, TouchableOpacity} from 'react-native';
import {
  DateFormatter,
  getFormattedCampusNameFromSectionMeetingTime,
  TimeFormatter,
} from '../../../../utils';
import {editMeetingTime} from '../../../../redux/actions/editSectionActions';
import {selectMeetingTimes} from '../../../../redux/reducers/editSectionReducer';
import DateTimePicker from '@react-native-community/datetimepicker';
import GrayText from './GrayText';
import SemiboldText from './SemiboldText';
import {titleCase} from 'title-case';

const isPM = date => {
  if (!date) {
    return new Date().getHours() >= 12;
  }

  return date.getHours() >= 12;
};

const getDateWithTime = (time, pmCode) => {
  const dateFormatter = new DateFormatter();
  const today = titleCase(
    `${dateFormatter.getMonth()} ${dateFormatter.getDay()}, ${dateFormatter.getYear()}`.toLowerCase(),
  );

  let fallbackMinutes = new Date().getMinutes();
  fallbackMinutes =
    fallbackMinutes < 10 ? `0${fallbackMinutes}` : fallbackMinutes;

  const fallbackHours = getAdjustedHoursFrom(new Date().getHours());

  const startTimeFormatter = new TimeFormatter(
    time ?? `${fallbackHours}${fallbackMinutes}`,
  ).addColon();

  let formattedTime = `${startTimeFormatter.time} `;
  formattedTime += pmCode ? `${pmCode}M` : isPM() ? 'PM' : 'AM';

  const dateWithTime = new Date(`${today} ${formattedTime}`);

  return dateWithTime;
};

const getAdjustedHoursFrom = originalHours => {
  let adjustedHours = originalHours;

  if (originalHours < 10) return `0${originalHours}`;

  if (originalHours > 12) {
    adjustedHours -= 12;
  }

  if (adjustedHours < 10) return `0${adjustedHours}`;

  return `${adjustedHours}`;
};

const getAdjustedMinutesFrom = originalMinutes => {
  return originalMinutes < 10 ? `0${originalMinutes}` : originalMinutes;
};

const getTimeAndPmCodeFromDate = date => {
  const hours = getAdjustedHoursFrom(date.getHours());
  const minutes = getAdjustedMinutesFrom(date.getMinutes());

  return {
    time: `${hours === '00' ? '12' : hours}${minutes}`,
    pmCode: isPM(date) ? 'P' : 'A',
  };
};

export default SectionInfoForm = ({selectedMeetingDayIndex}) => {
  const {colors} = useTheme();
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const [newMeetingDayLocation, setNewMeetingDayLocation] = useState();
  const [showStartTimeDateTimePicker, setShowStartTimeDateTimePicker] =
    useState(false);
  const [showEndTimeDateTimePicker, setShowEndTimeDateTimePicker] =
    useState(false);

  const currentMeetingTime =
    useSelector(selectMeetingTimes)[selectedMeetingDayIndex];

  const genericTimeFormatter = new TimeFormatter();

  const specialSpan = currentMeetingTime.specialSpan;

  const specialStartTime = specialSpan ? specialSpan.split('-')[0] : '';
  const specialEndTime = specialSpan ? specialSpan.split('-')[1] : '';

  const specialStartTimePmCode =
    genericTimeFormatter.getPmCodeFromFormattedTime(specialStartTime);
  const specialEndTimePmCode =
    genericTimeFormatter.getPmCodeFromFormattedTime(specialEndTime);

  const startTimeFormatter = new TimeFormatter(
    specialStartTime
      ? genericTimeFormatter.getOriginalTime(specialStartTime)
      : currentMeetingTime.startTime ?? '1200',
  ).addColon();

  startTimeFormatter.addTimeOfDay(
    specialStartTimePmCode
      ? specialStartTimePmCode
      : currentMeetingTime.pmCode ?? 'P',
  );

  const endTimeFormatter = new TimeFormatter(
    specialEndTime
      ? genericTimeFormatter.getOriginalTime(specialEndTime)
      : currentMeetingTime.endTime ?? '0130',
  )
    .addColon()
    .addTimeOfDay(
      specialEndTimePmCode
        ? specialEndTimePmCode
        : currentMeetingTime.pmCode ?? 'P',
    );

  const oppositePmCode = pmCode => {
    return pmCode === 'P' ? 'A' : 'P';
  };

  // Auto-formats class times
  if (!startTimeFormatter.isSpanValidAsAClassLength(endTimeFormatter)) {
    const spans = startTimeFormatter.createSpan(endTimeFormatter).split('-');
    endTimeFormatter.time = spans[1];
    endTimeFormatter.pmCode = oppositePmCode(endTimeFormatter.pmCode);
  }

  const sectionStartDate = getDateWithTime(
    currentMeetingTime.startTime,
    startTimeFormatter.pmCode,
  );

  const sectionEndDate = getDateWithTime(
    currentMeetingTime.endTime,
    endTimeFormatter.pmCode,
  );

  const [currentSectionStartDate, setCurrentSectionStartDate] = useState(null);
  const [currentSectionEndDate, setCurrentSectionEndDate] = useState(null);

  const {buildingCode, roomNumber, editedLocation} = currentMeetingTime;

  const buildingCodeAndRoomNumber =
    editedLocation ??
    (buildingCode ?? '') +
      ' ' +
      (roomNumber ? `Room ${roomNumber}` : 'No room info');

  useEffect(() => {
    setNewMeetingDayLocation(buildingCodeAndRoomNumber);
  }, []);

  const updateMeetingTimeWithNewBuildingCodeAndRoomNumber = () => {
    dispatch(
      editMeetingTime(selectedMeetingDayIndex, {
        editedLocation: newMeetingDayLocation,
      }),
    );
  };

  return (
    <Card
      style={{
        marginHorizontal: 16,
        paddingBottom: Platform.OS === 'android' ? 0 : 16,
        backgroundColor: colors.card,
      }}
    >
      <TouchableOpacity
        onPress={() => {
          setShowStartTimeDateTimePicker(!showStartTimeDateTimePicker);
          setShowEndTimeDateTimePicker(false);
        }}
      >
        <Row style={{justifyContent: 'space-between'}}>
          <GrayText>Start Time</GrayText>
          {!currentMeetingTime.startTime && (
            <Text
              style={{
                fontFamily: SFProTextSemibold,
                maxWidth: '80%',
                flexWrap: 'wrap',
                color: GRAY_4,
                textAlign: 'right',
              }}
            >
              Select Time
            </Text>
          )}
          {currentMeetingTime.startTime && (
            <SemiboldText>
              {specialStartTime
                ? specialStartTime
                : startTimeFormatter.replace0thHourWith12thIfNeeded(
                    startTimeFormatter.time,
                  )}
            </SemiboldText>
          )}
        </Row>
      </TouchableOpacity>
      {useMemo(() => {
        return (
          showStartTimeDateTimePicker && (
            <DateTimePicker
              value={currentSectionStartDate ?? sectionStartDate ?? new Date()}
              is24Hour={false}
              mode="time"
              display={Platform.OS === 'ios' ? 'spinner' : 'default'}
              textColor={colors.text}
              onChange={(event, selectedDate) => {
                if (Platform.OS === 'android')
                  setShowStartTimeDateTimePicker(false);

                const {time, pmCode} = getTimeAndPmCodeFromDate(selectedDate);

                let endTimeFormatterForNewSpan = new TimeFormatter(
                  endTimeFormatter.getOriginalTime(),
                )
                  .addColon()
                  .addTimeOfDay(endTimeFormatter.pmCode);

                let potentialSpan = new TimeFormatter(time)
                  .addColon()
                  .addTimeOfDay(pmCode)
                  .createUnadjustedSpan(endTimeFormatterForNewSpan);

                if (
                  !startTimeFormatter.isEndTimeAfterStartTime(potentialSpan)
                ) {
                  const dateOneHourAhead = new Date(selectedDate);
                  dateOneHourAhead.setHours(dateOneHourAhead.getHours() + 1);

                  setCurrentSectionEndDate(dateOneHourAhead);

                  const formatted = getTimeAndPmCodeFromDate(dateOneHourAhead);

                  endTimeFormatterForNewSpan = new TimeFormatter(formatted.time)
                    .addColon()
                    .addTimeOfDay(formatted.pmCode);

                  potentialSpan = new TimeFormatter(time)
                    .addColon()
                    .addTimeOfDay(pmCode)
                    .createUnadjustedSpan(endTimeFormatterForNewSpan);

                  dispatch(
                    editMeetingTime(selectedMeetingDayIndex, {
                      specialSpan: potentialSpan,
                    }),
                  );

                  dispatch(
                    editMeetingTime(selectedMeetingDayIndex, {
                      endTime: formatted.time,
                    }),
                  );
                } else {
                  dispatch(
                    editMeetingTime(selectedMeetingDayIndex, {
                      specialSpan: potentialSpan,
                    }),
                  );
                }

                setCurrentSectionStartDate(selectedDate);
                dispatch(
                  editMeetingTime(selectedMeetingDayIndex, {startTime: time}),
                );
                dispatch(editMeetingTime(selectedMeetingDayIndex, {pmCode}));
              }}
            />
          )
        );
      }, [showStartTimeDateTimePicker, currentSectionStartDate])}
      <Line style={{marginVertical: 16}} />
      <TouchableOpacity
        onPress={() => {
          setShowEndTimeDateTimePicker(!showEndTimeDateTimePicker);
          setShowStartTimeDateTimePicker(false);
        }}
      >
        <Row style={{justifyContent: 'space-between'}}>
          <GrayText>End Time</GrayText>
          {!currentMeetingTime.endTime && (
            <Text
              style={{
                fontFamily: SFProTextSemibold,
                maxWidth: '80%',
                flexWrap: 'wrap',
                color: GRAY_4,
                textAlign: 'right',
              }}
            >
              Select Time
            </Text>
          )}
          {currentMeetingTime.endTime && (
            <SemiboldText>
              {specialEndTime
                ? specialEndTime
                : endTimeFormatter.replace0thHourWith12thIfNeeded(
                    endTimeFormatter.time,
                  )}
            </SemiboldText>
          )}
        </Row>
      </TouchableOpacity>
      {useMemo(() => {
        return (
          showEndTimeDateTimePicker && (
            <DateTimePicker
              value={currentSectionEndDate ?? sectionEndDate ?? new Date()}
              is24Hour={false}
              mode="time"
              display={Platform.OS === 'ios' ? 'spinner' : 'default'}
              textColor={colors.text}
              onChange={(event, selectedDate) => {
                if (Platform.OS === 'android')
                  setShowEndTimeDateTimePicker(false);

                const {time, pmCode} = getTimeAndPmCodeFromDate(selectedDate);

                let endTimeFormatterForNewSpan = new TimeFormatter(time)
                  .addColon()
                  .addTimeOfDay(pmCode);

                const potentialSpan = new TimeFormatter(
                  startTimeFormatter.getOriginalTime(),
                )
                  .addColon()
                  .addTimeOfDay(startTimeFormatter.pmCode)
                  .createUnadjustedSpan(endTimeFormatterForNewSpan);

                if (
                  !startTimeFormatter.isEndTimeAfterStartTime(potentialSpan)
                ) {
                  const endTimeSpan = potentialSpan.split('-')[1];
                  const timeFormatter = new TimeFormatter();
                  timeFormatter.time;
                  timeFormatter.time =
                    timeFormatter.getOriginalTime(endTimeSpan);
                  timeFormatter
                    .addColon()
                    .addTimeOfDay(
                      timeFormatter.getPmCodeFromFormattedTime(endTimeSpan),
                    );

                  const newDate = getDateWithTime(
                    timeFormatter.getOriginalTime(),
                    timeFormatter.pmCode,
                  );

                  setCurrentSectionEndDate(newDate);

                  dispatch(
                    editMeetingTime(selectedMeetingDayIndex, {
                      endTime: timeFormatter.getOriginalTime(),
                    }),
                  );
                } else {
                  setCurrentSectionEndDate(selectedDate);

                  const timeAndPmCode = getTimeAndPmCodeFromDate(selectedDate);

                  dispatch(
                    editMeetingTime(selectedMeetingDayIndex, {
                      endTime: timeAndPmCode.time,
                    }),
                  );
                }

                dispatch(
                  editMeetingTime(selectedMeetingDayIndex, {
                    specialSpan: potentialSpan,
                  }),
                );
              }}
            />
          )
        );
      }, [showEndTimeDateTimePicker, currentSectionEndDate])}
      <Line style={{marginVertical: 16}} />
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('SectionCampusOptionsScreen', {
            selectedMeetingDayIndex,
          })
        }
      >
        <Row style={{justifyContent: 'space-between'}}>
          <GrayText>Campus</GrayText>
          <SemiboldText>
            {getFormattedCampusNameFromSectionMeetingTime(currentMeetingTime)}
          </SemiboldText>
        </Row>
      </TouchableOpacity>
      <Line
        style={{
          marginTop: 16,
          marginBottom: Platform.OS === 'android' ? 0 : 16,
        }}
      />

      <Row style={{justifyContent: 'space-between'}}>
        <GrayText>Classroom</GrayText>
        <TextInput
          style={{
            fontFamily: SFProTextSemibold,
            maxWidth: '80%',
            flexWrap: 'wrap',
            fontSize: 16,
            color: colors.text,
          }}
          defaultValue={
            buildingCodeAndRoomNumber.trim().length > 0
              ? buildingCodeAndRoomNumber.trim()
              : 'No room provided'
          }
          onChangeText={value => setNewMeetingDayLocation(value)}
          onBlur={updateMeetingTimeWithNewBuildingCodeAndRoomNumber}
        />
      </Row>
    </Card>
  );
};
