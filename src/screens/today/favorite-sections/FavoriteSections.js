import {useNavigation} from '@react-navigation/native';
import React, {useEffect} from 'react';
import {Platform, View} from 'react-native';
import {useSelector} from 'react-redux';
import {selectFavoriteSections} from '../../../redux/reducers/academicsReducer';
import CardWithImage from '../CardWithImage';
import FavoriteSectionsDisplay from './FavoriteSectionsDisplay';
import NoFavoriteSectionsDisplay from './NoFavoriteSectionsDisplay';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {FAVORITE_SECTIONS_STORAGE_KEY} from '../../../constants/storageKeys';

export default FavoriteSections = () => {
  const navigation = useNavigation();
  const favoriteSections = useSelector(selectFavoriteSections);

  useEffect(() => {
    AsyncStorage.setItem(
      FAVORITE_SECTIONS_STORAGE_KEY,
      JSON.stringify(favoriteSections),
    );
  }, [favoriteSections]);

  const topRightButtonAction = () => navigation.navigate('EditSectionScreen');
  return (
    <CardWithImage
      cardText={'My Classes'}
      onPressAction={() => navigation.navigate('MyScheduleScreen')}
      cardImageSource={require('../../../assets/icons/class_categories_filters_screen/calendar.png')}
      topRightButtonText={'+'}
      topRightButtonAction={topRightButtonAction}
      style={{paddingRight: 0}}
    >
      <View style={{paddingTop: 16}}>
        {Object.keys(favoriteSections).length === 0 ? (
          <NoFavoriteSectionsDisplay />
        ) : (
          <FavoriteSectionsDisplay />
        )}
      </View>
    </CardWithImage>
  );
};
