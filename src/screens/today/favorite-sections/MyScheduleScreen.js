import React, {useState} from 'react';
import {
  Animated,
  Image,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {PASTEL_RED, GRAY_3, GRAY_4, GRAY_5} from '../../../constants/colors';
import {SFProTextSemibold, SFProTextRegular} from '../../../constants/fonts';
import {
  Card,
  Row,
  SectionLocation,
  SectionTime,
} from '../../../shared-components/';
import {useDispatch, useSelector} from 'react-redux';
import {selectFavoriteSections} from '../../../redux/reducers/academicsReducer';
import {DateFormatter, sortMeetingTimes} from '../../../utils';
import {useNavigation, useTheme} from '@react-navigation/native';
import ColorStripe from '../../buses/ColorStripe';
import RedTopView from '../RedTopView';
import {SwipeRow} from 'react-native-swipe-list-view';
import {removeFavoriteSection} from '../../../redux/actions/academicActions';
import {Swipeable} from 'react-native-gesture-handler';
import store from '../../../redux/store';
import {abbreviationToWeekdayMap} from '../../../constants/academics';

const NoClasses = () => {
  const {colors} = useTheme();

  return (
    <Row style={{justifyContent: 'flex-end'}}>
      <View>
        <Text
          style={{
            fontFamily: SFProTextSemibold,
            textAlign: 'right',
            fontSize: 17,
            color: colors.text,
          }}
        >
          --
        </Text>
        <Text
          style={{
            fontFamily: SFProTextRegular,
            textAlign: 'right',
            fontSize: 12,
            color: GRAY_4,
          }}
        >
          No classes
        </Text>
      </View>
      <View style={{paddingLeft: 16}}>
        <ColorStripe backgroundColor={PASTEL_RED} />
      </View>
    </Row>
  );
};

const BoldText = ({children, color}) => {
  return (
    <Text
      style={{
        textTransform: 'uppercase',
        color: color ?? GRAY_3,
        fontFamily: SFProTextSemibold,
      }}
    >
      {children}
    </Text>
  );
};

export default MyScheduleScreen = () => {
  const dispatch = useDispatch();
  const favoriteSections = useSelector(selectFavoriteSections);
  const currentWeekday = new DateFormatter().getWeekday().toLowerCase();
  const {dark, colors} = useTheme();
  const navigation = useNavigation();
  const [userIsEditing, setUserIsEditing] = useState(false);
  const [sectionToDelete, setSectionToDelete] = useState(null);

  const allMeetingTimes = Object.values(favoriteSections)
    .map(section => {
      const {meetingTimes} = section;
      meetingTimes.map(meetingTime => {
        meetingTime.section = {title: section.title, index: section.index};
      });
      return meetingTimes;
    })
    .flat();

  const whenEditingMoveBy = 60;

  const renderRightActions = (progress, dragX) => {
    if (userIsEditing) return null;

    const translateX = dragX.interpolate({
      inputRange: [-100, 100],
      outputRange: [-20, 100],
    });

    return (
      <TouchableOpacity
        style={{marginTop: Platform.OS === 'ios' ? 12 : 14}}
        onPress={() => {
          store.dispatch(removeFavoriteSection(sectionToDelete));
        }}
      >
        <Animated.View
          style={[
            {
              transform: [{translateX}],
              backgroundColor: PASTEL_RED,
              width: 64,
              height: 48,
              alignSelf: 'center',
              resizeMode: 'contain',
            },
          ]}
        >
          <Text
            style={{
              fontFamily: SFProTextRegular,
              color: 'white',
              paddingVertical: Platform.OS === 'ios' ? 16 : 14,
              height: '100%',
              alignSelf: 'center',
            }}
          >
            Delete
          </Text>
        </Animated.View>
      </TouchableOpacity>
    );
  };

  return (
    <RedTopView
      boldText={'This Week'}
      bigText={'My Schedule'}
      buttonText={userIsEditing ? 'Done' : 'Edit'}
      showButton={Object.keys(favoriteSections).length > 0}
      onButtonPressAction={() => setUserIsEditing(!userIsEditing)}
    >
      <Card
        style={{
          paddingTop: 20,
          maxHeight: '80%',
          paddingRight: 0,
          marginHorizontal: 16,
          marginTop: 16,
          backgroundColor: colors.card,
        }}
      >
        <ScrollView>
          {Object.keys(favoriteSections).length === 0 ? (
            <View
              style={{
                paddingRight: 16,
                paddingBottom: 24,
                alignItems: 'center',
              }}
            >
              <Image
                style={{
                  marginTop: -20,
                  width: 240,
                  height: undefined,
                  aspectRatio: 1,
                  resizeMode: 'contain',
                  tintColor: dark ? 'white' : null,
                }}
                source={require('../../../assets/images/opinion_screens/anxious_kuma.png')}
              />
              <Text
                style={{
                  textAlign: 'center',
                  color: colors.text,
                  fontFamily: SFProTextRegular,
                  color: dark ? GRAY_3 : GRAY_4,
                }}
              >
                Add favorite classes to see them here.
              </Text>
            </View>
          ) : (
            [
              'independent study',
              'sunday',
              'monday',
              'tuesday',
              'wednesday',
              'thursday',
              'friday',
              'saturday',
            ].map((day, indexOfDay) => {
              const meetingTimesToday = allMeetingTimes.filter(meetingTime => {
                const meetsOn =
                  abbreviationToWeekdayMap[
                    meetingTime.meetingDay
                  ].toLowerCase();

                return meetsOn === day;
              });

              if (
                meetingTimesToday.length === 0 &&
                (day === 'independent study' ||
                  day === 'sunday' ||
                  day === 'saturday')
              )
                return null;

              return (
                <View
                  key={indexOfDay}
                  style={{paddingBottom: indexOfDay < 7 ? 20 : 0}}
                >
                  <Row style={{justifyContent: 'space-between'}}>
                    <View style={{paddingBottom: 16}}>
                      <BoldText color={GRAY_5}>{day}</BoldText>
                    </View>
                    {day === currentWeekday && (
                      <Image
                        style={{
                          height: 23,
                          width: 58,
                          position: 'absolute',
                          right: 0,
                          top: 0,
                          resizeMode: 'contain',
                        }}
                        source={require('../../../assets/images/today/today_flag.png')}
                      />
                    )}
                  </Row>
                  {meetingTimesToday.length === 0 ? (
                    <NoClasses />
                  ) : (
                    sortMeetingTimes(meetingTimesToday).map(
                      (meetingTime, meetingTimeIndex) => {
                        const {section} = meetingTime;
                        const {title, index} = section;

                        const campusColor = getCampusColorFromCampusName(
                          meetingTime.campusName,
                        );

                        return (
                          <Swipeable
                            key={meetingTimeIndex}
                            onSwipeableOpen={() => setSectionToDelete(section)}
                            onSwipeableClose={() => setSectionToDelete(null)}
                            renderRightActions={renderRightActions}
                          >
                            <SwipeRow
                              disableRightSwipe
                              disableLeftSwipe
                              style={{
                                paddingLeft: userIsEditing
                                  ? whenEditingMoveBy
                                  : 0,
                              }}
                            >
                              {userIsEditing ? (
                                <View
                                  style={{
                                    width: 48,
                                  }}
                                >
                                  <TouchableOpacity
                                    onPress={() =>
                                      dispatch(removeFavoriteSection(section))
                                    }
                                  >
                                    <Image
                                      source={require('../../../assets/icons/today_screen/trash.png')}
                                      style={{
                                        width: 20,
                                        tintColor: PASTEL_RED,
                                        marginTop: 'auto',
                                        alignSelf: 'center',
                                        resizeMode: 'contain',
                                      }}
                                    />
                                  </TouchableOpacity>
                                </View>
                              ) : (
                                <></>
                              )}

                              <TouchableOpacity
                                onPress={() =>
                                  navigation.navigate('EditSectionScreen', {
                                    sectionIndex: index,
                                  })
                                }
                              >
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                  }}
                                >
                                  {/* DOESN'T WRAP - FIX */}
                                  <Text
                                    style={{
                                      color: colors.text,
                                      fontFamily: SFProTextRegular,
                                      fontSize: 16,
                                      textTransform: 'capitalize',
                                    }}
                                  >
                                    {title.trim().length > 0
                                      ? title
                                      : 'No name provided'}
                                  </Text>

                                  <Row
                                    key={meetingTimeIndex}
                                    style={{
                                      marginTop: -8,
                                      justifyContent: 'space-between',
                                    }}
                                  >
                                    <SectionLocation
                                      meetingTime={meetingTime}
                                      useEditedLocationIfExists={true}
                                    />

                                    <Row
                                      style={{
                                        right: userIsEditing
                                          ? -whenEditingMoveBy
                                          : 0,
                                      }}
                                    >
                                      <SectionTime meetingTime={meetingTime} />

                                      <View style={{paddingLeft: 16}}>
                                        <ColorStripe
                                          backgroundColor={campusColor}
                                        />
                                      </View>
                                    </Row>
                                  </Row>
                                </View>
                              </TouchableOpacity>
                            </SwipeRow>
                          </Swipeable>
                        );
                      },
                    )
                  )}
                </View>
              );
            })
          )}
        </ScrollView>
      </Card>
    </RedTopView>
  );
};
