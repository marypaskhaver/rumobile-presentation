import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {useNavigation, useTheme} from '@react-navigation/native';
import {GRAY_3, GRAY_4} from '../../../constants/colors';
import {SFProTextRegular} from '../../../constants/fonts';

export default NoFavoriteSectionsDisplay = () => {
  const {dark} = useTheme();
  const navigation = useNavigation();

  return (
    <View style={{alignItems: 'center', paddingRight: 16}}>
      <Text
        style={{
          fontFamily: SFProTextRegular,
          color: dark ? GRAY_3 : GRAY_4,
          textAlign: 'center',
        }}
      >
        Quickly access your schedule of classes here.
      </Text>
      <View
        style={{
          marginTop: 12,
          marginBottom: 8,
          borderWidth: 1,
          borderRadius: 4,
          borderColor: dark ? GRAY_3 : GRAY_4,
        }}
      >
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('More', {screen: 'ClassCategoriesScreen'})
          }
        >
          <Text
            style={{
              fontFamily: SFProTextRegular,
              color: dark ? GRAY_3 : GRAY_4,
              textAlign: 'center',
              paddingVertical: 4,
              paddingHorizontal: 10,
            }}
          >
            Add Classes
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
