import React, {useEffect} from 'react';
import {View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {
  selectBusPredictions,
  selectBusRoutes,
  selectFavoriteBusStopsIds,
  selectUserLocation,
} from '../../../redux/reducers/busesReducer';
import FavoriteBusStopsDisplay from './FavoriteBusStopsDisplay';
import NoFavoriteBusStopsDisplay from './NoFavoriteBusStopsDisplay';
import CardWithImage from '../CardWithImage';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {FAVORITE_BUS_STOPS_STORAGE_KEY} from '../../../constants/storageKeys';
import {
  requestBusPredictions,
  requestBusRoutes,
  requestBusStops,
  requestVehicles,
} from '../../../redux/actions/busesActions';
import {getLocation} from '../../../shared-components/LocationGetter';
import {selectChosenCampusID} from '../../../redux/reducers/campusesReducer';

export default FavoriteBusStops = () => {
  const favoriteBusStopsIds = useSelector(selectFavoriteBusStopsIds);
  const navigation = useNavigation();
  const dispatch = useDispatch();

  useEffect(() => {
    AsyncStorage.setItem(
      FAVORITE_BUS_STOPS_STORAGE_KEY,
      JSON.stringify(favoriteBusStopsIds),
    );
  }, [favoriteBusStopsIds]);

  const chosenCampusID = useSelector(selectChosenCampusID);
  const busRoutes = useSelector(selectBusRoutes);
  const busPredictions = useSelector(selectBusPredictions);

  const userLocation = useSelector(selectUserLocation);
  useEffect(() => {
    if (!userLocation) {
      getLocation();
    }
  }, []);

  useEffect(() => {
    if (busRoutes.length === 0 || busPredictions.length === 0) {
      dispatch(requestBusRoutes(chosenCampusID));
      dispatch(requestVehicles(chosenCampusID));
      dispatch(requestBusStops(chosenCampusID));
    }
  }, [chosenCampusID]);

  useEffect(() => {
    dispatch(
      requestBusPredictions(busRoutes.map(busRoute => busRoute.route_id)),
    );
  }, [busRoutes]);

  return (
    <CardWithImage
      cardText={'My Buses'}
      onPressAction={() => navigation.navigate('MyBusStopsScreen')}
      cardImageSource={require('../../../assets/icons/tab_bar/busiconselected.png')}
      style={{marginTop: 24, paddingRight: 0}}
    >
      <View style={{paddingTop: 16}}>
        {Object.keys(favoriteBusStopsIds).length === 0 ? (
          <NoFavoriteBusStopsDisplay />
        ) : (
          <FavoriteBusStopsDisplay />
        )}
      </View>
    </CardWithImage>
  );
};
