import React, {useState} from 'react';
import {Animated, Text, TouchableOpacity, View} from 'react-native';
import {Row} from '../../../shared-components/Row';
import {useIsFocused, useTheme} from '@react-navigation/native';
import {GRAY_4, PASTEL_RED} from '../../../constants/colors';
import {SFProTextRegular, SFProTextSemibold} from '../../../constants/fonts';
import {useSelector} from 'react-redux';
import {
  selectBusRoutes,
  selectBusStops,
  selectFavoriteBusStopsIds,
  selectUserLocation,
  selectVehicles,
} from '../../../redux/reducers/busesReducer';
import {
  BusRouteInformation,
  getRouteColorInfoWithDefinedPropertiesForRouteNamed,
} from '../../../utils';
import RouteTag from '../../../shared-components/RouteTag';
import StopPrediction from '../../../shared-components/StopPrediction';
import {convertDistance, getDistance} from 'geolib';
import ColorStripe from '../../buses/ColorStripe';
import {Swipeable} from 'react-native-gesture-handler';
import store from '../../../redux/store';
import {removeFavoriteBusStopIDWithRouteID} from '../../../redux/actions/busesActions';
import {LocationGetter} from '../../../shared-components/LocationGetter';

export default ActiveRoutesList = ({stopID, index}) => {
  const {colors} = useTheme();
  const location = useSelector(selectUserLocation);
  const isFocused = useIsFocused();

  const allBusRoutes = useSelector(selectBusRoutes);
  const allBusStops = useSelector(selectBusStops);
  const allActiveVehicles = useSelector(selectVehicles);
  const favoriteBusStopsIds = useSelector(selectFavoriteBusStopsIds);

  const [busStopAndRouteToDelete, setBusStopAndRouteToDelete] = useState({
    stopID: null,
    routeID: null,
  });

  const busRouteInformation = new BusRouteInformation(
    allBusRoutes,
    allActiveVehicles,
  );

  const busStop = allBusStops.find(busStop => busStop.stop_id === stopID);
  if (!busStop) return null;

  const busStopLocation = {
    longitude: busStop.location.lng,
    latitude: busStop.location.lat,
  };

  const userLocation = {
    longitude: location?.coords?.longitude ?? 0,
    latitude: location?.coords?.latitude ?? 0,
  };

  const distanceBetweenUserAndStop = location
    ? convertDistance(getDistance(userLocation, busStopLocation), 'mi').toFixed(
        1,
      )
    : null;

  const displayDistance = !distanceBetweenUserAndStop
    ? '-- mi'
    : `${distanceBetweenUserAndStop} mi`;

  const routeIDs = favoriteBusStopsIds[stopID];

  const activeRoutes = busRouteInformation
    .getActiveRoutes()
    .filter(busRoute => routeIDs.includes(busRoute.route_id));

  if (activeRoutes.length === 0) return null;

  const renderRightActions = (progress, dragX) => {
    const translateX = dragX.interpolate({
      inputRange: [-100, 100],
      outputRange: [-20, 100],
    });

    return (
      <TouchableOpacity
        onPress={() => {
          store.dispatch(
            removeFavoriteBusStopIDWithRouteID(busStopAndRouteToDelete),
          );
        }}
      >
        {isFocused && <LocationGetter.BusPredictions />}
        <Animated.View
          style={[
            {
              transform: [{translateX}],
              backgroundColor: PASTEL_RED,
              width: 64,
              height: 48,
              alignSelf: 'center',
              resizeMode: 'contain',
            },
          ]}
        >
          <Text
            style={{
              fontFamily: SFProTextRegular,
              color: 'white',
              paddingVertical: Platform.OS === 'ios' ? 16 : 14,
              height: '100%',
              alignSelf: 'center',
            }}
          >
            Delete
          </Text>
        </Animated.View>
      </TouchableOpacity>
    );
  };

  return (
    <View key={stopID} style={{paddingTop: index > 0 ? 20 : 0}}>
      <Row
        style={{
          justifyContent: 'space-between',
          paddingRight: 16,
          paddingBottom: 8,
        }}
      >
        <Text
          style={{
            fontFamily: SFProTextSemibold,
            fontSize: 16,
            color: colors.text,
            flex: 1,
          }}
        >
          {busStop && busStop.name}
        </Text>
        <Text
          style={{
            fontFamily: SFProTextRegular,
            color: GRAY_4,
            fontSize: 12,
          }}
        >
          {displayDistance}
        </Text>
      </Row>
      {activeRoutes.map((busRoute, index) => {
        const routeColorInfo =
          getRouteColorInfoWithDefinedPropertiesForRouteNamed(
            busRoute.long_name,
          );

        return (
          <Swipeable
            key={stopID + index}
            onSwipeableOpen={() =>
              setBusStopAndRouteToDelete({
                stopID,
                routeID: busRoute.route_id,
              })
            }
            onSwipeableClose={() =>
              setBusStopAndRouteToDelete({stopID: null, routeID: null})
            }
            renderRightActions={renderRightActions}
          >
            <Row
              style={{
                paddingBottom: index < activeRoutes.length - 1 ? 16 : 0,
                justifyContent: 'space-between',
              }}
            >
              <RouteTag routeColorInfo={routeColorInfo} />
              <Row>
                <StopPrediction stop_id={stopID} route_id={busRoute.route_id} />
                <View style={{paddingLeft: 16}}>
                  <ColorStripe backgroundColor={routeColorInfo.rcolor} />
                </View>
              </Row>
            </Row>
          </Swipeable>
        );
      })}
    </View>
  );
};
