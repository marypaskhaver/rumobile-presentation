import React, {useState} from 'react';
import {
  Animated,
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {GRAY_3, GRAY_4, PASTEL_RED} from '../../../constants/colors';
import {SFProTextSemibold, SFProTextRegular} from '../../../constants/fonts';
import {Card, Row, StopPrediction} from '../../../shared-components';
import {useDispatch, useSelector} from 'react-redux';
import {getRouteColorInfoWithDefinedPropertiesForRouteNamed} from '../../../utils';
import {useTheme} from '@react-navigation/native';
import ColorStripe from '../../buses/ColorStripe';
import RedTopView from '../RedTopView';
import {SwipeRow} from 'react-native-swipe-list-view';
import {Swipeable} from 'react-native-gesture-handler';
import store from '../../../redux/store';
import {
  selectBusRoutes,
  selectBusStops,
  selectFavoriteBusStopsIds,
} from '../../../redux/reducers/busesReducer';
import RouteTag from '../../../shared-components/RouteTag';
import {removeFavoriteBusStopIDWithRouteID} from '../../../redux/actions/busesActions';

export default MyBusStopsScreen = () => {
  const dispatch = useDispatch();
  const favoriteBusStopsIds = useSelector(selectFavoriteBusStopsIds);

  const {dark, colors} = useTheme();
  const [userIsEditing, setUserIsEditing] = useState(false);

  const whenEditingMoveBy = 60;

  const RoutesAndStops = () => {
    const allBusRoutes = useSelector(selectBusRoutes);
    const allBusStops = useSelector(selectBusStops);
    const {colors} = useTheme();
    const [busStopAndRouteToDelete, setBusStopAndRouteToDelete] = useState({
      stopID: null,
      routeID: null,
    });

    const renderRightActions = (progress, dragX) => {
      if (userIsEditing) return null;

      const translateX = dragX.interpolate({
        inputRange: [-100, 100],
        outputRange: [-20, 100],
      });

      return (
        <TouchableOpacity
          style={{marginTop: 8}}
          onPress={() => {
            store.dispatch(
              removeFavoriteBusStopIDWithRouteID(busStopAndRouteToDelete),
            );
          }}
        >
          <Animated.View
            style={[
              {
                transform: [{translateX}],
                backgroundColor: PASTEL_RED,
                width: 64,
                height: 48,
                alignSelf: 'center',
                resizeMode: 'contain',
              },
            ]}
          >
            <Text
              style={{
                fontFamily: SFProTextRegular,
                color: 'white',
                paddingVertical: Platform.OS === 'ios' ? 16 : 14,
                height: '100%',
                alignSelf: 'center',
              }}
            >
              Delete
            </Text>
          </Animated.View>
        </TouchableOpacity>
      );
    };

    return Object.keys(favoriteBusStopsIds).map((stopID, index) => {
      const routeIDs = favoriteBusStopsIds[stopID];

      const favoriteRoutesThatVisitThisStop = allBusRoutes.filter(
        busRoute =>
          routeIDs.includes(busRoute.route_id) &&
          busRoute.stops.includes(stopID),
      );

      const currentBusStop = allBusStops.find(
        busStop => busStop.stop_id === stopID,
      );

      return (
        <View
          key={index}
          style={{
            marginBottom:
              index < Object.keys(favoriteBusStopsIds).length - 1 ? 24 : 0,
          }}
        >
          {currentBusStop ? (
            <Text
              style={{
                fontFamily: SFProTextSemibold,
                fontSize: 16,
                paddingRight: 16,
                color: colors.text,
                alignSelf: 'center',
                flex: 1,
              }}
            >
              {currentBusStop.name}
            </Text>
          ) : (
            <Text
              style={{
                fontFamily: SFProTextRegular,
                fontSize: 16,
                paddingRight: 16,
                color: dark ? GRAY_3 : GRAY_4,
                alignSelf: 'center',
                flex: 1,
              }}
            >
              Can't find bus stop for this campus.
            </Text>
          )}

          {favoriteRoutesThatVisitThisStop.map((busRoute, busRouteIndex) => {
            const routeColorInfo =
              getRouteColorInfoWithDefinedPropertiesForRouteNamed(
                busRoute.long_name,
              );

            return (
              <Swipeable
                key={busRouteIndex}
                onSwipeableOpen={() =>
                  setBusStopAndRouteToDelete({
                    stopID,
                    routeID: busRoute.route_id,
                  })
                }
                onSwipeableClose={() =>
                  setBusStopAndRouteToDelete({stopID: null, routeID: null})
                }
                renderRightActions={renderRightActions}
              >
                <SwipeRow
                  disableRightSwipe
                  disableLeftSwipe
                  style={{
                    paddingLeft: userIsEditing ? whenEditingMoveBy : 0,
                  }}
                >
                  {userIsEditing ? (
                    <View
                      style={{
                        width: 48,
                      }}
                    >
                      <TouchableOpacity
                        onPress={() => {
                          dispatch(
                            removeFavoriteBusStopIDWithRouteID({
                              stopID: currentBusStop.stop_id,
                              routeID: busRoute.route_id,
                            }),
                          );
                        }}
                      >
                        <Image
                          source={require('../../../assets/icons/today_screen/trash.png')}
                          style={{
                            width: 20,
                            tintColor: PASTEL_RED,
                            marginTop: 'auto',
                            alignSelf: 'center',
                            resizeMode: 'contain',
                          }}
                        />
                      </TouchableOpacity>
                    </View>
                  ) : (
                    <></>
                  )}

                  <View
                    key={busRouteIndex}
                    style={{
                      marginVertical: 8,
                      marginRight: userIsEditing ? -whenEditingMoveBy : 0,
                    }}
                  >
                    <Row key={index}>
                      <RouteTag routeColorInfo={routeColorInfo} />
                      <Row style={{flex: 1, paddingRight: 16}}>
                        <StopPrediction
                          stop_id={stopID}
                          route_id={busRoute.route_id}
                        />
                      </Row>
                      <ColorStripe backgroundColor={routeColorInfo.rcolor} />
                    </Row>
                  </View>
                </SwipeRow>
              </Swipeable>
            );
          })}
        </View>
      );
    });
  };

  return (
    <RedTopView
      boldText={'Favorites'}
      bigText={'My Buses'}
      buttonText={userIsEditing ? 'Done' : 'Edit'}
      showButton={Object.keys(favoriteBusStopsIds).length > 0}
      onButtonPressAction={() => setUserIsEditing(!userIsEditing)}
    >
      <Card
        style={{
          paddingTop: 20,
          maxHeight: '80%',
          paddingRight: 0,
          marginHorizontal: 16,
          marginTop: 16,
          backgroundColor: colors.card,
        }}
      >
        <ScrollView>
          {Object.keys(favoriteBusStopsIds).length === 0 ? (
            <View
              style={{
                paddingRight: 16,
                paddingBottom: 24,
                alignItems: 'center',
              }}
            >
              <Image
                style={{
                  width: 240,
                  height: undefined,
                  aspectRatio: 1,
                  resizeMode: 'contain',
                  tintColor: dark ? 'white' : null,
                }}
                source={require('../../../assets/images/opinion_screens/bus_kuma.png')}
              />
              <Text
                style={{
                  textAlign: 'center',
                  color: colors.text,
                  fontFamily: SFProTextRegular,
                  color: dark ? GRAY_3 : GRAY_4,
                }}
              >
                Add favorite buses to see them here.
              </Text>
            </View>
          ) : (
            <RoutesAndStops />
          )}
        </ScrollView>
      </Card>
    </RedTopView>
  );
};
