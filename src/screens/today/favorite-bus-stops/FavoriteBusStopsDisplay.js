import React, {useEffect} from 'react';
import {Image, Text, View} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {GRAY_4} from '../../../constants/colors';
import {useSelector} from 'react-redux';
import {
  selectBusRoutes,
  selectBusStops,
  selectFavoriteBusStopsIds,
  selectSortFavoriteBuses,
  selectUserLocation,
  selectVehicles,
} from '../../../redux/reducers/busesReducer';
import {selectChosenCampusID} from '../../../redux/reducers/campusesReducer';
import {BusRouteInformation} from '../../../utils';
import {getLocation} from '../../../shared-components/LocationGetter';
import ActiveRoutesList from './ActiveRoutesList';
import {convertDistance, getDistance} from 'geolib';
import {requestBusStops} from '../../../redux/actions/busesActions';

export default FavoriteBusStopsDisplay = () => {
  const {dark} = useTheme();
  const favoriteBusStopsIds = useSelector(selectFavoriteBusStopsIds);
  const chosenCampusID = useSelector(selectChosenCampusID);
  const sortFavoriteBuses = useSelector(selectSortFavoriteBuses);

  useEffect(() => {
    requestBusStops(chosenCampusID);
  }, []);

  const allBusStops = useSelector(selectBusStops);

  const userLocation = useSelector(selectUserLocation);
  const routesIDs = Object.values(favoriteBusStopsIds).flat();

  useEffect(() => {
    if (!userLocation) {
      getLocation();
    }
  }, [chosenCampusID]);

  const allBusRoutes = useSelector(selectBusRoutes);
  const allVehicles = useSelector(selectVehicles);
  const location = useSelector(selectUserLocation);

  const busRouteInformation = new BusRouteInformation(
    allBusRoutes,
    allVehicles,
  );

  const activeRoutesContainingUsersStops = busRouteInformation
    .getActiveRoutes()
    .filter(busRoute => routesIDs.includes(busRoute.route_id));

  if (activeRoutesContainingUsersStops.length === 0)
    return (
      <View style={{alignItems: 'center'}}>
        <Image
          style={{
            tintColor: dark ? 'white' : null,
            width: 212,
            height: 212,
            marginTop: 12,
            marginBottom: 8,
          }}
          source={require('../../../assets/images/opinion_screens/bus_kuma.png')}
        />
        <Text style={{color: GRAY_4, marginBottom: 8}}>
          None of your favorite bus routes are active.
        </Text>
      </View>
    );

  const sortedBusStopIdsByDistance = Object.keys(favoriteBusStopsIds).sort(
    (stopID1, stopID2) => {
      const busStop1 = allBusStops.find(busStop => busStop.stop_id === stopID1);
      const busStop2 = allBusStops.find(busStop => busStop.stop_id === stopID2);

      if (busStop1?.distance && busStop2?.distance) {
        return Number(busStop1.distance) - Number(busStop2.distance);
      }

      const busStop1Location = {
        longitude: busStop1?.location?.lng,
        latitude: busStop1?.location?.lat,
      };

      const busStop2Location = {
        longitude: busStop2?.location?.lng,
        latitude: busStop2?.location?.lat,
      };

      const userLocation = {
        longitude: location?.coords?.longitude ?? 0,
        latitude: location?.coords?.latitude ?? 0,
      };

      const distanceBetweenUserAndStop1 =
        location && busStop1?.location
          ? convertDistance(
              getDistance(userLocation, busStop1Location),
              'mi',
            ).toFixed(1)
          : null;

      const distanceBetweenUserAndStop2 =
        location && busStop2?.location
          ? convertDistance(
              getDistance(userLocation, busStop2Location),
              'mi',
            ).toFixed(1)
          : null;

      if (distanceBetweenUserAndStop1 && distanceBetweenUserAndStop2) {
        return distanceBetweenUserAndStop1 - distanceBetweenUserAndStop2;
      }

      return 0;
    },
  );

  return (
    sortFavoriteBuses
      ? sortedBusStopIdsByDistance
      : Object.keys(favoriteBusStopsIds)
  ).map((stopID, index) => (
    <ActiveRoutesList key={stopID} stopID={stopID} index={index} />
  ));
};
