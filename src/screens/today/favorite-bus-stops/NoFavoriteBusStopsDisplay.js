import React from 'react';
import {Image, Text, View, TouchableOpacity} from 'react-native';
import {useNavigation, useTheme} from '@react-navigation/native';
import {GRAY_3, GRAY_4} from '../../../constants/colors';
import {SFProTextRegular} from '../../../constants/fonts';

export default NoFavoriteBusStopsDisplay = () => {
  const {dark} = useTheme();
  const navigation = useNavigation();

  return (
    <View style={{alignItems: 'center', paddingRight: 16}}>
      <Image
        style={{
          tintColor: dark ? 'white' : null,
          width: 212,
          height: 212,
          marginTop: 12,
        }}
        source={require('../../../assets/images/opinion_screens/bus_kuma.png')}
      />
      <Text
        style={{
          fontFamily: SFProTextRegular,
          color: dark ? GRAY_3 : GRAY_4,
          textAlign: 'center',
          paddingTop: 16,
        }}
      >
        Quickly access your favorite bus stops here.
      </Text>
      <View
        style={{
          marginTop: 12,
          marginBottom: 8,
          borderWidth: 1,
          borderRadius: 4,
          borderColor: dark ? GRAY_3 : GRAY_4,
        }}
      >
        <TouchableOpacity
          onPress={() => navigation.navigate('Tabs', {screen: 'Bus'})}
        >
          <Text
            style={{
              fontFamily: SFProTextRegular,
              color: dark ? GRAY_3 : GRAY_4,
              paddingVertical: 4,
              paddingHorizontal: 10,
              textAlign: 'center',
            }}
          >
            Add Buses
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
