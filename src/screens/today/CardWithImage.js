import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {Row} from '../../shared-components/Row';
import {useTheme} from '@react-navigation/native';
import {PASTEL_RED} from '../../constants/colors';
import {SFProTextRegular} from '../../constants/fonts';
import {Card} from '../../shared-components/Card';

export default CardWithImage = ({
  cardImageSource,
  cardText,
  onPressAction,
  children,
  topRightButtonText,
  topRightButtonAction,
  style,
}) => {
  const {colors} = useTheme();

  return (
    <Card style={[style, {backgroundColor: colors.card}]}>
      <Row style={{justifyContent: 'space-between'}}>
        <TouchableOpacity
          onPress={onPressAction}
          disabled={!onPressAction}
          style={{width: '66%'}}
        >
          <Row>
            <Image
              style={{
                width: 16,
                height: 16,
                tintColor: PASTEL_RED,
              }}
              source={cardImageSource}
            />
            <Text
              style={{
                paddingLeft: 8,
                fontSize: 14,
                fontWeight: '500',
                fontFamily: SFProTextRegular,
                color: PASTEL_RED,
              }}
            >
              {cardText}
            </Text>
          </Row>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={topRightButtonAction}
          disabled={!topRightButtonAction}
          style={{
            position: 'absolute',
            right: 16,
            paddingLeft: 12,
            paddingBottom: 12,
          }}
        >
          <Text
            style={{
              fontSize: 30,
              fontFamily: SFProTextRegular,
              color: PASTEL_RED,
            }}
          >
            {topRightButtonText}
          </Text>
        </TouchableOpacity>
      </Row>
      <View>{children}</View>
    </Card>
  );
};
