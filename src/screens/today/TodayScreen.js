import React, {useEffect} from 'react';
import {Platform, ScrollView, StatusBar, View} from 'react-native';
import {MessageOfTheDay} from './message-of-the-day/MessageOfTheDay';
import {SafeAreaView} from 'react-native-safe-area-context';
import FavoriteSections from './favorite-sections/FavoriteSections';
import FavoriteBusStops from './favorite-bus-stops/FavoriteBusStops';
import {useIsFocused, useTheme} from '@react-navigation/native';
import {modifyBadgeCount} from '../../redux/actions/tabBarBadgeActions';
import {useDispatch} from 'react-redux';
import {FocusAwareStatusBar} from '../../shared-components';
import BusDataRefreshControl from '../buses/BusDataRefreshControl';

const TodayScreen = () => {
  const {dark} = useTheme();
  const isFocused = useIsFocused();
  const dispatch = useDispatch();

  useEffect(() => {
    if (isFocused) dispatch(modifyBadgeCount(0));
  }, [isFocused]);

  return (
    <SafeAreaView edges={['top', 'right', 'left']} style={{flex: 1}}>
      <FocusAwareStatusBar
        barStyle={dark ? 'light-content' : 'dark-content'}
        hidden={false}
        translucent={true}
        backgroundColor="transparent"
      />
      <MessageOfTheDay />

      <ScrollView
        refreshControl={
          Platform.OS === 'android' ? null : <BusDataRefreshControl />
        }
        style={{
          paddingHorizontal: 16,
          paddingTop: 8,
          height: '100%',
        }}
      >
        <FavoriteSections />
        <FavoriteBusStops />
        <View style={{height: 10}} />
      </ScrollView>
    </SafeAreaView>
  );
};

export default TodayScreen;
