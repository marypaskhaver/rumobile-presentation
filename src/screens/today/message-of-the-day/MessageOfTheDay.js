import React, {useEffect, useRef, useState} from 'react';
import {Image, View, Text, TouchableOpacity, AppState} from 'react-native';
import DateText from '../../../shared-components/DateText';
import {useNavigation} from '@react-navigation/native';
import {PASTEL_RED} from '../../../constants/colors';
import {SFProTextRegular, SFProTextSemibold} from '../../../constants/fonts';
import getMessageOfTheDay from '../../../redux/api/getMessageOfTheDay';

const BannerBody = () => {
  const [title, setTitle] = useState(
    'Enjoy the beautiful weather this weekend!',
  );

  const appState = useRef(AppState.currentState);

  const updateTitle = () => {
    getMessageOfTheDay()
      .then(response => {
        const title = response.data.subtitle;

        if (title) setTitle(title);
      })
      .catch(error => {
        console.log(
          `Can't update message of the day title. ${JSON.stringify(error)}`,
        );
      });
  };

  useEffect(() => {
    updateTitle();

    const subscription = AppState.addEventListener('change', nextAppState => {
      if (
        appState.current.match(/inactive|background/) &&
        nextAppState === 'active'
      ) {
        updateTitle();
      }

      appState.current = nextAppState;
    });

    return () => {
      subscription.remove();
    };
  }, []);

  const cornerImage = require('../../../assets/images/message_of_the_day/justKumaFace.png');

  return (
    <View
      style={{
        marginHorizontal: 16,
        marginBottom: 12,
      }}
    >
      <View
        style={{
          backgroundColor: PASTEL_RED,
          height: 72,
          width: '100%',
          borderRadius: 6,
          textAlignVertical: 'center',
          justifyContent: 'center',
        }}
      >
        <View style={{paddingHorizontal: 24}}>
          <DateText
            style={{
              color: 'white',
              fontSize: 16,
              fontFamily: SFProTextSemibold,
              textTransform: 'capitalize',
            }}
          />
          <Text
            style={{
              fontSize: 13,
              fontFamily: SFProTextRegular,
              paddingTop: 6,
              color: 'white',
            }}
          >
            {title}
          </Text>
        </View>
      </View>
      <Image
        source={cornerImage}
        style={{
          position: 'absolute',
          bottom: 0,
          right: 0,
          width: 36,
          height: 36,
          resizeMode: 'stretch',
          borderBottomRightRadius: 6,
        }}
      />
    </View>
  );
};

export const MessageOfTheDay = () => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      style={{marginTop: 12}}
      onPress={() => navigation.navigate('MessageOfTheDayPopupScreen')}
    >
      <BannerBody />
    </TouchableOpacity>
  );
};
