import React, {useEffect, useRef, useState} from 'react';
import {
  AppState,
  View,
  Text,
  Image,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import DateText from '../../../shared-components/DateText';
import {FocusAwareStatusBar, Row} from '../../../shared-components';
import {useNavigation, useTheme} from '@react-navigation/native';
import {GRAY_3} from '../../../constants/colors';
import {
  SFProTextRegular,
  SFProTextSemibold,
  SFProDisplayBold,
} from '../../../constants/fonts';
import {format} from 'timeago.js';
import getMessageOfTheDay from '../../../redux/api/getMessageOfTheDay';

const backupMessage = {
  title: 'Message of the Day',
  subtitle: 'Welcome to RUMobile 4.0!',
  profileImage: 'https://d3gcr65qy8nmmj.cloudfront.net/taro_kumakabe.jpg',
  author: 'Taro Kumakabe',
  updatedAt: '2022-09-05T17:46:30.162Z',
  content: [
    {
      text: 'Welcome to RUMobile. We’ve been working hard to give you guys a really special version of RUMobile. It has a cleaner UI and lots of helpful new features.',
    },
    {
      title: 'What’s New',
      text: '☯︎ Welcome screens so you can set up the app for your campus and education level from the get-go.\n☯︎ Customizable dark mode settings.\n☯︎ More informative bus predictions. Now, RUMobile will alert you when bus data is inaccurate.\n☯︎ Food screens for Camden and Newark users.\n☯︎ Meet the team screens to highlight everyone who’s contributed to RUMobile.\n☯︎ And more!\n',
    },
    {
      title: 'Contribute',
      text: 'Want to get involved? Email us at rutgersmobileapp@gmail.com. Please include a short description of your work and your resume. We can’t wait to see what you have to offer!',
      image: 'https://d3gcr65qy8nmmj.cloudfront.net/gitlab_help.jpg',
    },
    {
      title: 'Feedback',
      text: 'Have questions, comments, concerns? Let us know by sending us an email. If you love our app, please leave us a review!',
    },
    {
      title: 'Want to Get Featured?',
      text: 'Send your epic quotes, ideas, and content to feature on the Message of the Day screen to our email, rutgersmobileapp@gmail.com.',
    },
  ],
};
const CustomBackButton = () => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      onPress={() => navigation.goBack()}
      style={{
        alignSelf: 'flex-start',
        left: 18,
        zIndex: 50,
      }}
    >
      <View
        style={{flexDirection: 'row', paddingTop: 16, alignItems: 'center'}}
      >
        <Image
          style={{
            tintColor: 'white',
            height: 20,
            resizeMode: 'contain',
            transform: [{scaleX: -1}],
          }}
          source={require('../../../assets/icons/other/forward.png')}
        />
      </View>
    </TouchableOpacity>
  );
};

const PosterProfileImage = ({imageSource}) => {
  const taroKuma = require('../../../assets/images/message_of_the_day/taro_kuma.jpeg');

  return (
    <Image
      style={{
        marginLeft: 21,
        marginRight: 13,
        width: 35,
        height: 35,
        borderRadius: 4,
      }}
      source={
        imageSource
          ? {
              uri: imageSource,
            }
          : taroKuma
      }
    />
  );
};

export default MessageOfTheDayPopupScreen = () => {
  const {dark, colors} = useTheme();
  const messageBackgroundImage = require('../../../assets/images/message_of_the_day/message_bg.jpg');

  const appState = useRef(AppState.currentState);
  const [message, setMessage] = useState(backupMessage);

  const updateMessageOfTheDay = () => {
    getMessageOfTheDay()
      .then(response => {
        const message = response.data;
        if (message) setMessage(message);
      })
      .catch(error => {
        console.log(
          `Can't update message of the day. ${JSON.stringify(error)}`,
        );
      });
  };

  useEffect(() => {
    updateMessageOfTheDay();

    const subscription = AppState.addEventListener('change', nextAppState => {
      if (
        appState.current.match(/inactive|background/) &&
        nextAppState === 'active'
      ) {
        updateMessageOfTheDay();
      }

      appState.current = nextAppState;
    });

    return () => {
      subscription.remove();
    };
  }, []);

  return (
    <SafeAreaView edges={['right', 'left']}>
      <FocusAwareStatusBar
        barStyle={'light-content'}
        hidden={false}
        translucent={true}
        backgroundColor="transparent"
      />
      <ScrollView style={{backgroundColor: colors.background}}>
        <ImageBackground
          style={{width: '100%', height: 340}}
          source={messageBackgroundImage}
        >
          <Row style={{justifyContent: 'space-between', marginTop: 50}}>
            <CustomBackButton />
            <DateText
              style={{
                alignSelf: 'flex-end',
                paddingRight: 18,
                marginLeft: 'auto',
                color: 'white',
              }}
            />
          </Row>

          <Text
            style={{
              color: 'white',
              fontSize: 34,
              marginLeft: 20,
              width: 174,
              paddingTop: 88,
              fontFamily: SFProDisplayBold,
            }}
          >
            {(message && message.title.trim()) || `Message\nof the Day`}
          </Text>

          <View
            style={{
              marginTop: 26,
              height: 60,
              flexDirection: 'row',
              alignItems: 'center',
              backgroundColor: 'rgba(40, 40, 40, 0.8)',
            }}
          >
            <PosterProfileImage imageSource={message.profileImage} />
            <View style={{flexDirection: 'column'}}>
              <Row>
                <Text
                  style={{
                    color: 'white',
                    fontFamily: SFProTextSemibold,
                    fontSize: 16,
                  }}
                >
                  {(message && message.author.trim()) || 'Taro Kumakabe'}
                  <Text
                    style={{
                      color: GRAY_3,
                      fontFamily: SFProTextRegular,
                      fontSize: 12,
                    }}
                  >
                    {`    `}
                    {message && message.updatedAt
                      ? format(new Date(message.updatedAt))
                      : ''}
                  </Text>
                </Text>
              </Row>

              <Text
                style={{
                  color: 'white',
                  fontFamily: SFProTextRegular,
                  fontSize: 15,
                }}
              >
                {(message && message.subtitle.trim()) ||
                  'New RUMobile version is now live!'}
              </Text>
            </View>
          </View>
        </ImageBackground>

        <View style={{paddingHorizontal: 20, paddingTop: 16}}>
          {message.content.map((section, index) => {
            return (
              <View key={index}>
                {section.title && (
                  <Text
                    style={{
                      fontSize: 32,
                      color: colors.text,
                      fontWeight: 'bold',
                      paddingTop: 30,
                      paddingRight: 0,
                      paddingBottom: 10,
                    }}
                  >
                    {section.title.trim()}
                  </Text>
                )}
                {section.text && (
                  <Text
                    style={{
                      fontSize: 20,
                      color: colors.text,
                      paddingBottom: 10,
                      lineHeight: 32,
                    }}
                  >
                    {section.text.trim()}
                  </Text>
                )}
                {section.image && <ResizedImage uri={section.image} />}
              </View>
            );
          })}
        </View>
        <View style={{height: 100}} />
      </ScrollView>
    </SafeAreaView>
  );
};

const ResizedImage = ({uri}) => {
  const [width, setWidth] = useState(250);
  const [height, setHeight] = useState(250);

  const getImageDimensions = () => {
    Image.getSize(
      uri,
      (width, height) => {
        setWidth(width);
        setHeight(height);
      },
      error => {
        console.log(error);
      },
    );
  };

  useEffect(() => {
    getImageDimensions();
  }, []);

  const imageHeight = (Dimensions.get('window').width - 50) * (height / width);

  return (
    <Image
      style={{
        height: imageHeight,
        flex: 1,
        width: null,
      }}
      source={{
        uri,
      }}
    />
  );
};
