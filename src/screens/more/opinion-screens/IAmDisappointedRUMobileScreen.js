import {useNavigation} from '@react-navigation/native';
import React from 'react';
import sendEmail from './EmailMethod';

// For Dark Mode compatibility
import OpinionScreen from './OpinionScreen';

const IAmDisappointedRUMobileScreen = () => {
  const anxiousKuma = require('../../../assets/images/opinion_screens/anxious_kuma.png');
  const emailImage = require('../../../assets/icons/more_screen/star.png');

  //   const navigation = useNavigation();

  return (
    // Pass in children later
    <OpinionScreen
      topPageText={'I AM DISAPPOINTED'}
      title={'OH NO! :('}
      cardImageSource={anxiousKuma}
      cardText={
        'We are sorry to disappoint you! Help us know how we can improve RUMobile by giving us your feedback.'
      }
      buttonImageSource={emailImage}
      buttonText={'Give my feedback'}
      buttonOnPressEvent={() =>
        sendEmail('rutgersmobileapp@gmail.com', 'Feedback', '')
      }
    />
  );
};

export default IAmDisappointedRUMobileScreen;
