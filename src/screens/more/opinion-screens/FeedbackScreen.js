import React from 'react';
import OpinionScreen from './OpinionScreen';
import sendEmail from './EmailMethod';

const FeedbackScreen = () => {
  const anxiousKuma = require('../../../assets/images/opinion_screens/anxious_kuma.png');
  const emailImage = require('../../../assets/icons/more_screen/email.png');

  return (
    // Pass in children later
    <OpinionScreen
      topPageText={'HOW CAN WE IMPROVE?'}
      title={'Feedback'}
      cardImageSource={anxiousKuma}
      cardText={
        'Please tell us how we can improve RUMobile! What things do you like and/or dislike about it?'
      }
      buttonImageSource={emailImage}
      buttonText={'Give my feedback'}
      buttonOnPressEvent={() =>
        sendEmail('rutgersmobileapp@gmail.com', 'Feedback', '')
      }
    />
  );
};

export default FeedbackScreen;
