import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Alert, Linking, Platform} from 'react-native';
import OpinionScreen from './OpinionScreen';
import InAppReview from 'react-native-in-app-review';
const {Storyboard} = require('@glassbox/react-native-storyboard');

const requestReview = () => {
  const canRequestReview = InAppReview.isAvailable();
  Storyboard.reportEvent('customUserEvent', {
    button_pressed: 'Give 5 stars to RUMobile',
  });

  // Go to App / Google Play Store
  if (!canRequestReview) {
    if (Platform.OS === 'ios') {
      Linking.openURL(
        'itms-apps://apps.apple.com/us/app/rutgers-rumobile/id1036544263',
      ).then(
        () => {
          console.log('Link opened successfully');
        },
        () => {
          console.log('Link failed to open');
          Alert.alert('Unable to open link.');
        },
      );
    } else {
      Linking.openURL('market://details?id=com.rumobile&gl=US').then(
        () => {
          console.log('Link opened successfully');
        },
        () => {
          console.log('Link failed to open');
          Alert.alert('Unable to open link.');
        },
      );
    }
  } else {
    InAppReview.RequestInAppReview()
      .then(hasFlowFinishedSuccessfully => {
        // hasFlowFinishedSuccessfully: In android it means the user finished or close review flow
        // in iOS it means review flow lanuched to user.
        // for android:
        // The flow has finished. The API does not indicate whether the user
        // reviewed or not, or even whether the review dialog was shown. Thus, no
        // matter the result, we continue our app flow.
        // for ios
        // the flow lanuched successfully, The API does not indicate whether the user
        // reviewed or not, or he/she closed flow yet as android, Thus, no
        // matter the result, we continue our app flow.
      })
      .catch(error => {
        // Alert.alert('Unable to open link.');

        if (Platform.OS === 'android') {
          Linking.openURL('market://details?id=com.rumobile&gl=US').then(
            () => {
              console.log('Link opened successfully');
            },
            () => {
              console.log('Link failed to open');
              Alert.alert('Unable to open link.');
            },
          );
        } else {
          Linking.openURL(
            'itms-apps://apps.apple.com/us/app/rutgers-rumobile/id1036544263',
          ).then(
            () => {
              console.log('Link opened successfully');
            },
            () => {
              console.log('Link failed to open');
              Alert.alert('Unable to open link.');
            },
          );
        }

        console.log(
          `Couldn't request review from user. ${JSON.stringify(error)}.`,
        );
      });
  }
};

const RateUsScreen = () => {
  const aboutKuma = require('../../../assets/images/opinion_screens/about_kuma.png');
  const heart = require('../../../assets/images/other/likeFilled.png');

  const navigation = useNavigation();
  const caption = `We are proud you love RUMobile. Share your experience by rating us on the ${
    Platform.OS === 'ios' ? 'App Store' : 'Google Play Store'
  }.`;
  return (
    <OpinionScreen
      topPageText={'TELL US WHAT YOU LOVE'}
      title={'Rate Us'}
      cardImageSource={aboutKuma}
      buttonImageSource={heart}
      buttonText={'Give 5 stars to RUMobile'}
      dismissScreenText={'No, thanks'}
      cardText={caption}
      dismissButtonEvent={() => navigation.goBack()}
      buttonOnPressEvent={() => requestReview()}
      marginBetweenTextAndImage={{
        marginTop: 20,
        marginBottom: 20,
      }}
    />
  );
};

export default RateUsScreen;
