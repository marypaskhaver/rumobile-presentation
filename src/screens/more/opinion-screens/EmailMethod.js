import qs from 'qs';
import {Alert, Linking} from 'react-native';

export default sendEmail = (to, subject, body) => {
  let url = `mailto:${to}`;

  // Create email link query
  const query = qs.stringify({
    subject: subject,
    body: body,
  });

  if (query.length) {
    url += `?${query}`;
  }

  return Linking.openURL(url).then(
    () => {
      console.log('Link opened successfully');
    },
    () => {
      console.log('Link failed to open');
      Alert.alert('Unable to open link.');
    },
  );
};
