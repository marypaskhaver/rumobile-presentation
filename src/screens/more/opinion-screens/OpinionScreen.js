import React from 'react';
import {View, Image, Text, TouchableOpacity, ScrollView} from 'react-native';
import {Container} from '../../../shared-components/Container';

// For Dark Mode compatibility
import {useNavigation, useTheme} from '@react-navigation/native';
import {GRAY_6, PASTEL_RED, GRAY_3} from '../../../constants/colors';
import {Card} from '../../../shared-components/Card';
import {SFProTextRegular, SFProTextSemibold} from '../../../constants/fonts';
import {Row} from '../../../shared-components';
import RedTopView from '../../today/RedTopView';

const CardWithImage = ({
  cardImageSource,
  cardText,
  marginBetweenTextAndImage,
}) => {
  const {dark, colors} = useTheme();

  return (
    <Card
      style={{
        marginTop: 16,
        marginLeft: 16,
        marginRight: 16,
        paddingTop: cardText ? 0 : 32,
        paddingBottom: cardText ? 27 : 0,
        backgroundColor: colors.card,
      }}
    >
      <Image
        style={[
          {
            width: '100%',
            height: undefined,
            aspectRatio: 1,
            tintColor: dark ? 'white' : null,
          },
          marginBetweenTextAndImage,
        ]}
        source={cardImageSource}
      />
      <Text
        style={{
          fontSize: 18,
          fontFamily: SFProTextRegular,
          color: dark ? colors.text : GRAY_6,
          textAlign: 'center',
        }}
      >
        {cardText}
      </Text>
    </Card>
  );
};

const ButtonWithImage = ({
  buttonImageSource,
  buttonText,
  buttonOnPressEvent,
}) => {
  const buttonItemsColor = 'white';

  return (
    <TouchableOpacity
      onPress={buttonOnPressEvent}
      style={{alignSelf: 'center'}}
    >
      <View
        style={{
          borderRadius: 8,
          height: 46,
          width: 340,
          marginTop: 48,
          backgroundColor: PASTEL_RED,
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Row>
          <Image
            source={buttonImageSource}
            style={{
              width: 22,
              height: 22,
              marginRight: 12,
              tintColor: buttonItemsColor,
            }}
          />
          <Text
            style={{
              fontSize: 19,
              fontFamily: SFProTextSemibold,
              color: buttonItemsColor,
            }}
          >
            {buttonText}
          </Text>
        </Row>
      </View>
    </TouchableOpacity>
  );
};

const DismissScreenText = ({text, dismissButtonEvent}) => {
  const navigation = useNavigation();

  const dismissEvent = () => {
    if (dismissButtonEvent != null) {
      dismissButtonEvent(); // Potentially goes to other screen
    } else {
      navigation.goBack();
    }
  };

  return (
    <TouchableOpacity
      onPress={() => dismissEvent()}
      style={{alignSelf: 'center'}}
    >
      <Text
        style={{
          fontSize: 17,
          fontFamily: SFProTextSemibold,
          marginTop: 14,
          color: PASTEL_RED,
        }}
      >
        {text ?? 'No, thanks'}
      </Text>
    </TouchableOpacity>
  );
};

const OpinionScreen = ({
  topPageText,
  title,
  cardImageSource,
  cardText,
  buttonImageSource,
  buttonText,
  dismissScreenText,
  dismissButtonEvent,
  buttonOnPressEvent,
  marginBetweenTextAndImage,
}) => {
  return (
    <Container style={{alignItems: 'center'}}>
      <RedTopView boldText={topPageText} bigText={title}>
        <ScrollView>
          <CardWithImage
            cardImageSource={cardImageSource}
            cardText={cardText}
            marginBetweenTextAndImage={marginBetweenTextAndImage}
          />
          <ButtonWithImage
            buttonImageSource={buttonImageSource}
            buttonText={buttonText}
            buttonOnPressEvent={buttonOnPressEvent}
          />
          <DismissScreenText
            text={dismissScreenText}
            dismissButtonEvent={dismissButtonEvent}
          />
          <View style={{marginBottom: 20}} />
        </ScrollView>
      </RedTopView>
    </Container>
  );
};

export default OpinionScreen;
