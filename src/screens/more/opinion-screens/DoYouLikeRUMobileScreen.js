import {Link, useNavigation} from '@react-navigation/native';
import React from 'react';
import {Linking, Platform} from 'react-native';

// For Dark Mode compatibility
import OpinionScreen from './OpinionScreen';

const DoYouLikeRUMobileScreen = () => {
  const anxiousKuma = require('../../../assets/images/opinion_screens/bus_kuma.png');
  const emailImage = require('../../../assets/images/other/likeFilled.png');

  const navigation = useNavigation();

  return (
    // Pass in children later
    <OpinionScreen
      topPageText={'DO YOU LIKE RUMOBILE?'}
      title={'Let Us Know'}
      cardImageSource={anxiousKuma}
      buttonImageSource={emailImage}
      buttonText={'I love it'}
      dismissScreenText={'I am disappointed'}
      dismissButtonEvent={() =>
        navigation.push('IAmDisappointedRUMobileScreen')
      }
      buttonOnPressEvent={() => navigation.push('RateUsScreen')}
    />
  );
};

export default DoYouLikeRUMobileScreen;
