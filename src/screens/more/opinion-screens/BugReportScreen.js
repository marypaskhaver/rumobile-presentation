import React from 'react';
import sendEmail from './EmailMethod';
import OpinionScreen from './OpinionScreen';

const BugReportScreen = () => {
  const anxiousKuma = require('../../../assets/images/opinion_screens/anxious_kuma.png');
  const bugImage = require('../../../assets/icons/more_screen/bug.png');

  return (
    // Pass in children later
    <OpinionScreen
      topPageText={'REPORT A PROBLEM'}
      title={'Bug Report'}
      cardImageSource={anxiousKuma}
      cardText={
        "Tell us what's wrong with RUMobile! Please provide us with as many details as possible."
      }
      buttonImageSource={bugImage}
      buttonText={'Report a bug'}
      buttonOnPressEvent={() =>
        sendEmail('rutgersmobileapp@gmail.com', 'Bug Report', '')
      }
    />
  );
};

export default BugReportScreen;
