import React from 'react';
import {
  FlatList,
  View,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {Row} from '../../../shared-components/Row';
import {useNavigation, useTheme} from '@react-navigation/native';
import {Chevron} from '../../../shared-components/Chevron';
import profiles from '../../../constants/profiles';
import {SFProTextSemibold} from '../../../constants/fonts';
import {GRAY_3, GRAY_4, PASTEL_RED} from '../../../constants/colors';
import {
  BackButton,
  FocusAwareStatusBar,
  Line,
} from '../../../shared-components';
import styled from 'styled-components';

const LinkText = styled.Text`
  font-size: 17px;
  line-height: 22px;
  letter-spacing: -0.41px;
  width: 375px;
  margin-left: 17px;
  flex: 1;
`;

const inactiveProfiles = [
  {
    name: 'Kae',
    role: 'Artist',
    image: require('../../../assets/images/profile-screens/Kae.png'),
  },
  {
    name: 'Ro',
    role: 'Developer',
    image: require('../../../assets/images/profile-screens/Ro.png'),
  },
  {
    name: 'Yaniv',
    role: 'Developer',
    image: require('../../../assets/images/profile-screens/Yaniv.png'),
  },
];

const MeetTheTeamScreen = () => {
  const {colors} = useTheme();
  const navigation = useNavigation();

  return (
    <View style={{flex: 1}}>
      <FocusAwareStatusBar />
      <BackButton color={PASTEL_RED} text="Back" />

      <ScrollView style={{marginTop: 72, paddingLeft: 20}}>
        <Row style={{paddingTop: 20, paddingBottom: 24, paddingRight: 20}}>
          <Image
            source={require('../../../assets/images/profile-screens/rumobile.png')}
            style={{width: 48, height: 48, marginRight: 16, borderRadius: 6}}
          />
          <View style={{flexDirection: 'column'}}>
            <Text
              style={{
                fontFamily: SFProTextSemibold,
                fontSize: 18,
                color: colors.text,
              }}
            >
              RUMobile
            </Text>
            <Text
              style={{
                fontFamily: SFProTextSemibold,
                color: GRAY_3,
                fontSize: 12,
              }}
            >
              Rutgers Student App
            </Text>
          </View>
        </Row>
        <Row style={{flexWrap: 'wrap', paddingRight: 20}}>
          <Text
            style={{
              fontFamily: SFProTextSemibold,
              color: colors.text,
              fontSize: 16,
              letterSpacing: 0.16,
              lineHeight: 24,
            }}
          >
            RUMobile{' '}
            <Text
              style={{
                fontFamily: SFProTextSemibold,
                color: GRAY_4,
                fontSize: 16,
                letterSpacing: 0.16,
                lineHeight: 24,
              }}
            >
              is made by Rutgers Students and Alumni collaborating together to
              give you the best Rutgers experience ever!
            </Text>
          </Text>
        </Row>
        <View style={{paddingRight: 20}}>
          <Text
            style={{
              paddingTop: 36,
              paddingBottom: 12,
              fontFamily: SFProTextSemibold,
              fontSize: 16,
              color: colors.text,
            }}
          >
            Active Members
          </Text>
          <Line />
          {Object.entries(profiles).map((profile, index) => (
            <TouchableOpacity
              key={index}
              onPress={() =>
                navigation.navigate('ProfileScreen', {user_id: profile[0]})
              }
            >
              <Row>
                <Image
                  source={profile[1].image}
                  style={{
                    width: 42,
                    height: 42,
                    borderRadius: 4,
                    marginLeft: 16,
                    marginBottom: 12,
                    marginTop: 12,
                  }}
                />
                <View
                  style={{
                    flexDirection: 'column',
                    marginVertical: 12,
                  }}
                >
                  <LinkText
                    style={{
                      color: colors.text,
                      fontFamily: SFProTextSemibold,
                      fontSize: 16,
                    }}
                  >
                    {profile[1].name}
                  </LinkText>
                  <LinkText
                    style={{
                      color: GRAY_3,
                      fontFamily: SFProTextSemibold,
                      fontSize: 13,
                    }}
                  >
                    {profile[1].role}
                  </LinkText>
                </View>
                <Chevron />
              </Row>
              {index < Object.keys(profiles).length - 1 && (
                <Line style={{marginLeft: 72, width: 'auto'}} />
              )}
            </TouchableOpacity>
          ))}

          <Text
            style={{
              paddingTop: 36,
              paddingBottom: 12,
              fontFamily: SFProTextSemibold,
              fontSize: 16,
              color: colors.text,
            }}
          >
            Inactive Members
          </Text>
          <Line />
          {inactiveProfiles.map((inactiveProfile, index) => (
            <View key={index}>
              <Row>
                <Image
                  source={inactiveProfile.image}
                  style={{
                    width: 42,
                    height: 42,
                    borderRadius: 4,
                    marginLeft: 16,
                    marginBottom: 12,
                    marginTop: 12,
                  }}
                />
                <View
                  style={{
                    flexDirection: 'column',
                    marginVertical: 12,
                  }}
                >
                  <LinkText
                    style={{
                      color: colors.text,
                      fontFamily: SFProTextSemibold,
                      fontSize: 16,
                    }}
                  >
                    {inactiveProfile.name}
                  </LinkText>
                  <LinkText
                    style={{
                      color: GRAY_3,
                      fontFamily: SFProTextSemibold,
                      fontSize: 13,
                    }}
                  >
                    {inactiveProfile.role}
                  </LinkText>
                </View>
              </Row>
              {index < Object.keys(inactiveProfiles).length - 1 && (
                <Line style={{marginLeft: 72, width: 'auto'}} />
              )}
            </View>
          ))}
        </View>
      </ScrollView>
    </View>
  );
};

export default MeetTheTeamScreen;
