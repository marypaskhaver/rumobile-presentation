import React from 'react';
import {Image, Text} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {SFProTextRegular} from '../../../constants/fonts';
import {Row} from '../../../shared-components/Row';

export default Quote = ({userInfo, children}) => {
  const {quote} = userInfo;
  const {colors} = useTheme();

  return (
    <Row
      style={{
        flexWrap: 'wrap',
        alignSelf: 'center',
        paddingBottom: 20,
      }}
    >
      <Image
        style={{
          width: 24,
          height: 20,
          resizeMode: 'contain',
          marginRight: 12,
          alignSelf: 'flex-start',
          tintColor: quote.quotationMarksColor,
        }}
        source={require('../../../assets/images/profile-screens/imageQuoteBegin.png')}
      />
      <Text
        style={{
          fontFamily: SFProTextRegular,
          fontSize: 16,
          maxWidth: '70%',
          alignSelf: 'flex-start',
          alignItems: 'center',
          color: colors.text,
          flexWrap: 'wrap',
        }}
      >
        {children}
      </Text>
    </Row>
  );
};
