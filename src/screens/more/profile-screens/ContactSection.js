import React from 'react';
import {View, Image, Text, Linking, Alert} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {SFProTextRegular, SFProTextSemibold} from '../../../constants/fonts';
import {Row} from '../../../shared-components/Row';
import {TouchableOpacity} from 'react-native';

const Contact = ({image, contactInfo, url}) => {
  const {colors} = useTheme();

  return (
    <View
      style={{
        alignSelf: 'flex-start',
      }}
    >
      <View
        style={{
          paddingLeft: 20,
        }}
      >
        <Row>
          <Image
            source={image}
            style={{
              height: 24,
              width: 24,
              tintColor: colors.text,
            }}
          />
          <TouchableOpacity
            disabled={!url}
            onPress={() =>
              url
                ? Linking.openURL(url).then(
                    () => {
                      console.log('Link opened successfully');
                    },
                    () => {
                      console.log('Link failed to open');
                      Alert.alert('Unable to open link.');
                    },
                  )
                : ''
            }
          >
            <Row>
              <Text
                style={{
                  paddingLeft: 24,
                  paddingVertical: 12,
                  fontFamily: SFProTextRegular,
                  fontSize: 16,
                  color: colors.text,
                }}
              >
                {contactInfo}
              </Text>
            </Row>
          </TouchableOpacity>
        </Row>
      </View>
    </View>
  );
};

export default ContactSection = ({section}) => {
  const {colors} = useTheme();

  return (
    <View style={{paddingBottom: 20}}>
      <Text
        style={{
          paddingBottom: 6,
          fontFamily: SFProTextSemibold,
          fontSize: 20,
          alignSelf: 'flex-start',
          color: colors.text,
        }}
      >
        {section.type}
      </Text>
      {section.contents.map((element, index) => (
        <Contact key={index} {...element}></Contact>
      ))}
    </View>
  );
};
