import React from 'react';
import {View, Image} from 'react-native';
import GRAY_4 from '../../../constants/colors';

export default ProfileImage = ({imageSource}) => {
  return (
    <View
      style={{
        shadowOffset: {width: 1, height: 1},
        shadowColor: GRAY_4,
        shadowOpacity: 0.5,
      }}
    >
      <Image
        style={{
          width: 120,
          height: 120,
          top: -60,
          borderRadius: 12,
          position: 'absolute',
          alignSelf: 'center',
        }}
        source={imageSource}
      />
    </View>
  );
};
