import React from 'react';
import {Text, ImageBackground, ScrollView} from 'react-native';
import {Card} from '../../../shared-components/Card';
import {GRAY_4} from '../../../constants/colors';
import {useTheme} from '@react-navigation/native';
import {BackButton} from '../../../shared-components/BackButton';
import {SFProTextRegular, SFProTextSemibold} from '../../../constants/fonts';
import {SafeAreaView} from 'react-native-safe-area-context';
import profiles from '../../../constants/profiles';
import ProfileImage from './ProfileImage';
import Quote from './Quote';
import BadgeSection from './BadgeSection';
import ContactSection from './ContactSection';
import HobbySection from './HobbySection';
import {FocusAwareStatusBar} from '../../../shared-components';

const ProfileScreen = props => {
  const {user_id} = props.route.params;

  const userInfo = profiles[user_id];

  const {name, role, quote, image, backgroundImage, sections} = userInfo;
  const {colors} = useTheme();

  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: 'rgba(18, 16, 16, 0.86)',
      }}
      edges={['right', 'left']}
    >
      <ImageBackground
        source={backgroundImage}
        style={{
          resizeMode: 'contain',
          paddingBottom: 160,
        }}
      >
        <FocusAwareStatusBar
          barStyle={'light-content'}
          hidden={false}
          translucent={true}
          backgroundColor="transparent"
        />
        <BackButton isWhite={true} text="Back" />
      </ImageBackground>
      <Card
        style={{
          height: '100%',
          borderTopLeftRadius: 24,
          borderTopRightRadius: 24,
          backgroundColor: colors.card,
          alignItems: 'center',
          marginTop: -20,
          paddingLeft: 20,
          paddingRight: 20,
          flex: 1,
          paddingBottom: 0,
          borderBottomLeftRadius: 0,
          borderBottomRightRadius: 0,
        }}
      >
        <ProfileImage imageSource={image} />
        <Text
          style={{
            marginTop: 72,
            fontFamily: SFProTextSemibold,
            fontSize: 24,
            alignSelf: 'center',
            textAlign: 'center',
            color: colors.text,
            flexWrap: 'wrap',
          }}
        >
          {name}
        </Text>
        <Text
          style={{
            paddingTop: 12,
            fontFamily: SFProTextRegular,
            fontSize: 18,
            color: GRAY_4,
            alignSelf: 'center',
            textAlign: 'center',
            flexWrap: 'wrap',
          }}
        >
          {role}
        </Text>
        <ScrollView style={{width: '100%', height: '100%'}}>
          <Text
            style={{
              paddingVertical: 12,
              fontFamily: SFProTextSemibold,
              fontSize: 20,
              alignSelf: 'flex-start',
              color: colors.text,
            }}
          >
            About Me
          </Text>
          <Quote userInfo={userInfo}>{quote.text}</Quote>
          {sections.map((section, index) => {
            if (section.type === 'Badges') {
              return <BadgeSection key={index} section={section} />;
            } else if (section.type == 'Contact Info') {
              return <ContactSection key={index} section={section} />;
            } else if (section.type == 'Hobbies') {
              return <HobbySection key={index} section={section} />;
            }
          })}
        </ScrollView>
      </Card>
    </SafeAreaView>
  );
};

export default ProfileScreen;
