import React from 'react';
import {View, Text} from 'react-native';
import {SFProTextSemibold} from '../../../constants/fonts';
import {GRAY_4} from '../../../constants/colors';

const levelToColor = {
  1: 'rgb(253, 133, 41)',
  2: 'rgb(101, 190, 107)',
  3: 'rgb(141, 48, 146)',
  4: 'rgb(229, 25, 54)',
  5: 'rgb(0, 111, 186)',
  6: 'rgb(254, 201, 56)',
};

export default LevelTag = ({level}) => {
  return (
    <View
      style={{
        borderRadius: 4,
        backgroundColor: levelToColor[level] ?? GRAY_4,
        height: 16,
        alignItems: 'center',
        alignSelf: 'flex-start',
      }}
    >
      <Text
        style={{
          fontFamily: SFProTextSemibold,
          fontSize: 12,
          color: 'white',
        }}
      >
        {level === 6 ? `  Max Lvl  ` : `  Lvl ${level}  `}
      </Text>
    </View>
  );
};
