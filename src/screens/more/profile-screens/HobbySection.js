import React from 'react';
import {View, Image, Text} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {SFProTextRegular, SFProTextSemibold} from '../../../constants/fonts';
import {Row} from '../../../shared-components/Row';

const Hobby = ({hobbyInfo}) => {
  const {colors} = useTheme();

  return (
    <View style={{alignSelf: 'flex-start', paddingLeft: 20}}>
      <Text
        style={{
          paddingLeft: 0,
          paddingVertical: 8,
          fontFamily: SFProTextRegular,
          fontSize: 16,
          color: colors.text,
        }}
      >
        {hobbyInfo}
      </Text>
    </View>
  );
};

export default HobbySection = ({section}) => {
  const {colors} = useTheme();

  return (
    <>
      <Text
        style={{
          fontFamily: SFProTextSemibold,
          fontSize: 20,
          alignSelf: 'flex-start',
          color: colors.text,
        }}
      >
        {section.type}
      </Text>
      {section.contents.map((element, index) => (
        <Hobby key={index} {...element}></Hobby>
      ))}
    </>
  );
};
