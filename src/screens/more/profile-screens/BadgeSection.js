import React from 'react';
import {View, Image, Text} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {SFProTextRegular, SFProTextSemibold} from '../../../constants/fonts';
import {Row} from '../../../shared-components/Row';
import LevelTag from '../profile-screens/LevelTag';

const Badge = ({image, title, subtitle, level, isColored}) => {
  const {colors} = useTheme();

  return (
    <View
      style={{
        alignSelf: 'flex-start',
      }}
    >
      <View
        style={{
          paddingLeft: 20,
        }}
      >
        <Row
          style={{
            paddingBottom: 28,
          }}
        >
          {isColored ? (
            <Image
              source={image}
              style={{
                width: 48,
                height: 48,
                resizeMode: 'contain',
              }}
            />
          ) : (
            <Image
              source={image}
              style={{
                width: 48,
                height: 48,
                resizeMode: 'contain',
                tintColor: colors.text,
              }}
            />
          )}
          <Row
            style={{
              paddingRight: 16,
            }}
          >
            <Text
              style={{
                paddingLeft: 24,
                fontFamily: SFProTextSemibold,
                fontSize: 16,
                color: colors.text,
                flex: 0.85,
                flexWrap: 'wrap',
              }}
            >
              {`${title}: `}
              <Text
                style={{
                  fontFamily: SFProTextRegular,
                  fontSize: 16,
                  color: colors.text,
                }}
              >
                {subtitle}
              </Text>
            </Text>
            <LevelTag level={level} />
          </Row>
        </Row>
      </View>
    </View>
  );
};

export default BadgeSection = ({section}) => {
  const {colors} = useTheme();

  return (
    <View>
      <Text
        style={{
          paddingBottom: 12,
          fontFamily: SFProTextSemibold,
          fontSize: 20,
          alignSelf: 'flex-start',
          color: colors.text,
        }}
      >
        {section.title ?? section.type}
      </Text>
      {section.contents.map((element, index) => (
        <Badge key={index} {...element}></Badge>
      ))}
    </View>
  );
};
