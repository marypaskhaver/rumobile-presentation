import React from 'react';

import {
  View,
  TouchableOpacity,
  Text,
  ImageBackground,
  FlatList,
} from 'react-native';

import {useNavigation, useTheme} from '@react-navigation/native';
import {BackButton} from '../../../shared-components/BackButton';

import {
  TimeFormatter,
  identifyWeekdayFromCharacters,
  invalidStringConverter,
  PluralMaker,
  formatInstructorNames,
  sortMeetingTimes,
} from '../../../utils';
import {Line} from '../../../shared-components/Line';
import {GRAY_5} from '../../../constants/colors';
import {SFProTextRegular, SFProTextSemibold} from '../../../constants/fonts';
import {FocusAwareStatusBar} from '../../../shared-components';

const SectionsScreen = props => {
  const {academicClass} = props.route.params;
  const pluralMaker = new PluralMaker('credit');

  return (
    <View style={{flex: 1}}>
      <FocusAwareStatusBar
        barStyle={'light-content'}
        hidden={false}
        translucent={true}
        backgroundColor="transparent"
      />
      <ImageBackground
        imageStyle={{opacity: 0.4}}
        style={{
          flexDirection: 'column',
          backgroundColor: 'black',
        }}
        source={require('../../../assets/images/classes-screens/sections-screen-top-background.jpg')}>
        <View style={{paddingTop: 68}}>
          <BackButton text={'Classes'} isWhite={true} />
          <Text
            style={{
              fontFamily: SFProTextSemibold,
              color: 'white',
              fontWeight: '600',
              fontSize: 24,
              marginHorizontal: 19,
              marginTop: 12,
              textTransform: 'capitalize',
            }}>
            {academicClass.title}
          </Text>
          <View
            style={{
              marginTop: 32,
              marginRight: 19,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <View
              style={{
                paddingHorizontal: 19,
                paddingVertical: 6,
                backgroundColor: 'rgba(0,0,0,0.3)',
              }}>
              <Text
                style={[
                  {
                    fontFamily: SFProTextRegular,
                    letterSpacing: -0.5,
                    fontSize: 17,
                    paddingBottom: 24,
                  },
                  academicClass.openSections > 0
                    ? {color: 'rgb(96,178,6)'}
                    : {color: 'rgb(255,46,59)'},
                ]}>
                {academicClass.openSections} open section
                {academicClass.openSections === 1 ? '' : 's'} of{' '}
                {academicClass.sections.length}
              </Text>
            </View>
            {academicClass.credits ? (
              <Text
                style={{
                  fontFamily: SFProTextRegular,
                  fontSize: 13,
                  color: 'white',
                  paddingBottom: 11,
                }}>
                <Text
                  style={{
                    fontFamily: SFProTextRegular,
                    fontSize: 35,
                    fontWeight: '500',
                  }}>
                  {academicClass.credits}
                </Text>
                <Text
                  style={{
                    fontFamily: SFProTextRegular,
                  }}>
                  {' '}
                  {/* Extract to common component */}
                  {pluralMaker.itemFormattedForQuantity(academicClass.credits)}
                </Text>
              </Text>
            ) : null}
          </View>
        </View>
      </ImageBackground>
      <FlatList
        data={academicClass.sections}
        renderItem={({item}) => {
          // Add credits and title property to section item
          item.title = academicClass.title;
          item.credits = academicClass.credits;

          return <SectionListItem key={item.number} section={item} />;
        }}
        ListHeaderComponent={() => <View style={{height: 10}} />}
      />
    </View>
  );
};

const SectionListItem = ({section}) => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      onPress={() =>
        navigation.push('SectionDetailsScreen', {
          section,
        })
      }>
      <View
        style={{
          flex: 1,
          paddingTop: 12,
          paddingBottom: 15,
        }}>
        <SectionNumberAndInstructor
          openStatus={section.openStatus}
          number={section.number}
          instructors={section.instructors}
        />

        <SectionMeetingTimes section={section} />

        <Line style={{paddingTop: 24}} />
      </View>
    </TouchableOpacity>
  );
};

const SectionMeetingTimes = ({section}) => {
  const {colors} = useTheme();

  return (
    <View
      style={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginLeft: 16,
        marginRight: 14,
        marginTop: 10,
      }}>
      <View
        style={{
          display: 'flex',
          flexDirection: 'column',
          width: '100%',
        }}>
        {section.meetingTimes &&
          sortMeetingTimes(section.meetingTimes).map(
            (
              {
                startTime,
                endTime,
                pmCode,
                meetingDay,
                campusAbbrev,
                buildingCode,
                roomNumber,
              },
              index,
            ) => {
              const startTimeFormatter = new TimeFormatter(startTime)
                .addColon()
                .addTimeOfDay(pmCode);
              const endTimeFormatter = new TimeFormatter(endTime)
                .addColon()
                .addTimeOfDay(pmCode);
              const finalTime = startTimeFormatter.createSpan(endTimeFormatter);

              const weekday = identifyWeekdayFromCharacters(meetingDay);

              const validCampusAbbrev = invalidStringConverter(
                campusAbbrev === 'OFF' ? '' : campusAbbrev,
              );
              const validRoomNumber = invalidStringConverter(roomNumber);
              const validBuildingCode = invalidStringConverter(buildingCode);

              const location = `${validCampusAbbrev} ${validBuildingCode} ${validRoomNumber}`;

              return (
                <View
                  style={{
                    display: 'flex',
                    width: '100%',
                    flexWrap: 'wrap',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginBottom: 5,
                  }}
                  key={index}>
                  {weekday === 'Independent Study' ? (
                    <Text
                      style={{
                        fontFamily: SFProTextRegular,
                        color: GRAY_5,
                        minWidth: 88,
                        marginLeft: 'auto',
                      }}>
                      {weekday}
                    </Text>
                  ) : (
                    <Text
                      style={{
                        fontFamily: SFProTextRegular,
                        color: colors.text,
                        minWidth: 88,
                      }}>
                      {weekday}
                    </Text>
                  )}

                  <Text
                    style={{
                      fontFamily: SFProTextRegular,
                      color: colors.text,
                    }}>
                    {finalTime}
                  </Text>
                  {weekday !== 'Independent Study' ? (
                    <Text
                      style={{
                        fontFamily: SFProTextRegular,
                        color: colors.text,
                      }}>
                      {location}
                    </Text>
                  ) : (
                    <Text></Text>
                  )}
                </View>
              );
            },
          )}
      </View>
    </View>
  );
};

const SectionNumberAndInstructor = ({openStatus, number, instructors}) => {
  const {colors} = useTheme();

  return (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginLeft: 16,
        marginRight: 14,
      }}>
      <View
        style={[
          {borderRadius: 4},
          openStatus
            ? {backgroundColor: 'rgb(90,175,79)'}
            : {backgroundColor: 'rgb(237,69,69)'},
        ]}>
        <Text
          style={{
            fontFamily: SFProTextRegular,
            color: 'white',
            fontSize: 20,
            paddingHorizontal: 7,
          }}>
          {number}
        </Text>
      </View>
      <Text
        style={{
          fontFamily: SFProTextSemibold,
          letterSpacing: -0.2,
          fontSize: 15,
          fontWeight: '600',
          maxWidth: 300,
          paddingLeft: 2,
          color: colors.text,
        }}>
        {formatInstructorNames(instructors)}
      </Text>
    </View>
  );
};

export default SectionsScreen;
