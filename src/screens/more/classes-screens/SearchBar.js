import {useTheme} from '@react-navigation/native';
import React from 'react';
import {View, TextInput, Image, Platform} from 'react-native';
import {GRAY_4} from '../../../constants/colors';

const SearchBar = ({onChangeTextAction, onFocusAction, value}) => {
  const {dark, colors} = useTheme();
  const searchImage = require('../../../assets/icons/other/search.png');

  return (
    <View
      style={{
        backgroundColor: dark ? 'rgb(23, 23, 23)' : '#E3E3E9',
        flex: 1,
        margin: 16,
        paddingHorizontal: 8,
        borderRadius: 12,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: Platform.OS === 'android' ? 0 : 8,
      }}
    >
      <Image
        source={searchImage}
        style={{
          height: 16,
          width: 16,
          resizeMode: 'contain',
          tintColor: GRAY_4,
        }}
      />
      <TextInput
        spellCheck={false}
        autoCorrect={false}
        clearTextOnFocus={true}
        onChangeText={text => {
          onChangeTextAction(text);
        }}
        value={value}
        onFocus={onFocusAction}
        placeholder="Search"
        clearButtonMode="while-editing"
        placeholderTextColor={GRAY_4}
        style={{fontSize: 16, flex: 1, marginLeft: 8, color: colors.text}}
      />
    </View>
  );
};

export default SearchBar;
