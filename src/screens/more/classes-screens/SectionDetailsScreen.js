import {useTheme} from '@react-navigation/native';
import React from 'react';
import styled from 'styled-components';
import {View, Text, ScrollView} from 'react-native';
import {
  BackButton,
  FocusAwareStatusBar,
  HeartButton,
} from '../../../shared-components/';
import MeetingItem from './MeetingItem';
import {
  formatInstructorNames,
  PluralMaker,
  sortMeetingTimes,
} from '../../../utils/';
import {PASTEL_RED, GRAY_4, GRAY_5} from '../../../constants/colors';
import {
  SFProDisplayBold,
  SFProTextRegular,
  SFProTextSemibold,
} from '../../../constants/fonts';
import {useDispatch, useSelector} from 'react-redux';
import {
  addFavoriteSection,
  removeFavoriteSection,
} from '../../../redux/actions/academicActions';
import {isFavoriteSection} from '../../../redux/reducers/academicsReducer';
import {modifyBadgeCount} from '../../../redux/actions/tabBarBadgeActions';
import {selectTabBarBadges} from '../../../redux/reducers/tabBarBadgeReducer';

const ViewColoredBySectionOpenStatus = styled.View`
  background-color: ${props =>
    props.openStatus ? 'rgb(90,175,79)' : PASTEL_RED};
`;

const SectionDetailsScreen = props => {
  const {section} = props.route.params;
  const {
    openStatus,
    number,
    index,
    title,
    meetingTimes,
    instructors,
    sectionNotes,
    credits,
  } = section;

  const {colors} = useTheme();
  const dispatch = useDispatch();
  const pluralMaker = new PluralMaker('CREDIT');
  const isFavorite = useSelector(isFavoriteSection(section));
  const badgeCount = useSelector(selectTabBarBadges);

  return (
    <ViewColoredBySectionOpenStatus
      openStatus={openStatus}
      style={{
        paddingTop: 12,
        flex: 1,
      }}
    >
      <FocusAwareStatusBar
        barStyle={'light-content'}
        hidden={false}
        translucent={true}
        backgroundColor="transparent"
      />
      <BackButton text={'Sections'} isWhite={true} />
      <Text
        style={{
          fontFamily: SFProTextSemibold,
          color: 'white',
          fontSize: 24,
          marginHorizontal: 20,
          marginTop: 68,
          marginBottom: 20,
          textTransform: 'capitalize',
        }}
      >
        {title}
      </Text>
      <View
        style={{
          marginTop: 16,
          marginRight: 24,
          marginLeft: 28,
          paddingBottom: 12,
          flexDirection: 'row',
          alignItems: 'flex-end',
          justifyContent: 'space-between',
        }}
      >
        <View>
          <Text
            style={{
              fontFamily: SFProTextRegular,
              fontSize: 24,
              fontWeight: '500',
              color: 'white',
            }}
          >
            {number || 'no data'}
          </Text>
          <Text
            style={{
              fontFamily: SFProTextRegular,
              fontSize: 12,
              color: 'white',
            }}
          >
            SECTION
          </Text>
        </View>
        <View>
          <Text
            style={{
              fontFamily: SFProTextRegular,
              fontSize: 24,
              fontWeight: '500',
              color: 'white',
            }}
          >
            {index || 'no data'}
          </Text>
          <Text
            style={{
              fontFamily: SFProTextRegular,
              fontSize: 12,
              color: 'white',
            }}
          >
            INDEX
          </Text>
        </View>
        <View>
          <Text
            style={{
              fontFamily: SFProTextRegular,
              fontSize: 24,
              fontWeight: '500',
              color: 'white',
            }}
          >
            {credits || 'no data'}
          </Text>
          <Text
            style={{
              fontFamily: SFProTextRegular,
              fontSize: 12,
              color: 'white',
            }}
          >
            {pluralMaker.itemFormattedForQuantity(credits).toUpperCase()}
          </Text>
        </View>
      </View>

      <ScrollView
        style={{
          paddingHorizontal: 16,
          paddingTop: 12,
          backgroundColor: colors.background,
        }}
      >
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <Text
            style={{
              fontFamily: SFProDisplayBold,
              fontSize: 24,
              marginTop: 12,
              marginBottom: 4,
              color: colors.text,
            }}
          >
            Schedule
          </Text>
          <HeartButton
            style={{height: 28, width: 28}}
            selected={isFavorite}
            onSelectAction={() => {
              dispatch(addFavoriteSection(section));
              dispatch(modifyBadgeCount(badgeCount + 1));
            }}
            onDeselectAction={() => {
              dispatch(removeFavoriteSection(section));
              dispatch(modifyBadgeCount(badgeCount - 1));
            }}
          />
        </View>

        {sortMeetingTimes(meetingTimes).map((meetingTime, index) => (
          <MeetingItem key={index} meetingTime={meetingTime} />
        ))}

        <View style={{paddingRight: 16}}>
          <Text
            style={{
              fontFamily: SFProDisplayBold,
              fontSize: 24,
              marginTop: 12,
              marginBottom: 4,
              color: colors.text,
            }}
          >
            Instructors
          </Text>
          <SectionText>{formatInstructorNames(instructors)}</SectionText>
          <Text
            style={{
              fontFamily: SFProDisplayBold,
              fontSize: 24,
              marginTop: 12,
              marginBottom: 4,
              color: colors.text,
            }}
          >
            Section Notes
          </Text>
          <SectionText>{sectionNotes || 'No Data'}</SectionText>
        </View>
        <View style={{height: 20}} />
      </ScrollView>
    </ViewColoredBySectionOpenStatus>
  );
};

const SectionText = ({children}) => {
  const {dark} = useTheme();

  return (
    <Text
      style={{
        fontSize: 14,
        color: dark ? GRAY_4 : GRAY_5,
        marginTop: 6,
        marginBottom: 11,
      }}
    >
      {children}
    </Text>
  );
};

export default SectionDetailsScreen;
