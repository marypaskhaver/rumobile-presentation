import {useTheme} from '@react-navigation/native';
import React, {useState} from 'react';
import {Text, ScrollView, View, TouchableOpacity, Image} from 'react-native';
import {GRAY_3, GRAY_5} from '../../../constants/colors';
import {Card} from '../../../shared-components/Card';
import {Line} from '../../../shared-components/Line';
import {SFProTextRegular} from '../../../constants/fonts';
import {RowWithCheckmark} from '../../../shared-components/RowWithCheckmark';
import {Row} from '../../../shared-components/Row';
import {useSelector} from 'react-redux';
import store from '../../../redux/store';
import {
  addCampus,
  addLevel,
  removeCampus,
  removeLevel,
  updateSemester,
} from '../../../redux/actions/academicActions';
import sortSemesters from '../../../utils/sortSemesters';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  CLASSES_CATEGORIES_FILTER_CAMPUSES_STORAGE_KEY,
  CLASSES_CATEGORIES_FILTER_EDUCATION_LEVEL_STORAGE_KEY,
  CLASSES_CATEGORIES_FILTER_SEMESTER_STORAGE_KEY,
} from '../../../constants/storageKeys';
import {ALL_CAMPUSES} from '../../../constants/campuses';

import RedTopView from '../../today/RedTopView';

const CardWithText = ({title, imageSource, children}) => {
  const {dark, colors} = useTheme();

  return (
    <Card
      style={{
        marginBottom: 32,
        marginRight: 24,
        paddingRight: 0,
        backgroundColor: colors.card,
      }}
    >
      <Row>
        <Image
          style={{
            width: 16,
            height: 16,
            tintColor: dark ? GRAY_3 : GRAY_5,
          }}
          source={imageSource}
        ></Image>
        <Text
          style={{
            fontSize: 16,
            color: dark ? GRAY_3 : GRAY_5,
            fontFamily: SFProTextRegular,
            marginLeft: 8,
            flexWrap: 'wrap',
          }}
        >
          {title}
        </Text>
      </Row>
      {children}
    </Card>
  );
};

const ClassCategoriesFiltersScreen = () => {
  const {colors} = useTheme();

  const currentSemester = useSelector(state => state.academics.semester);
  const currentLevels = useSelector(state => state.academics.levels);
  const currentCampuses = useSelector(state => state.academics.campuses);

  const allSemesters = {
    92022: 'Fall 2022',
    '02023': 'Winter 2023',
    12023: 'Spring 2023',
  };

  const allLevels = {
    U: 'Undergraduate',
    G: 'Graduate',
  };

  const [selectedSemesterID, setSelectedSemesterID] = useState(currentSemester);
  const [selectedLevelsIDs, setSelectedLevelsIDs] = useState(currentLevels);
  const [selectedCampusesIDs, setSelectedCampusesIDs] =
    useState(currentCampuses);

  const updateSelectedSemesterID = semesterID => {
    setSelectedSemesterID(semesterID);
    store.dispatch(updateSemester(semesterID));
    AsyncStorage.setItem(
      CLASSES_CATEGORIES_FILTER_SEMESTER_STORAGE_KEY,
      JSON.stringify(semesterID),
    );
  };

  const updateSelectedLevelsIDs = levelID => {
    if (selectedLevelsIDs.includes(levelID)) {
      const selectedLevelsIDWithoutGivenLevelID = selectedLevelsIDs.filter(
        selectedLevelID => selectedLevelID != levelID,
      );

      // Prevent user from deselecting all levels
      // Can show notification bar here
      if (selectedLevelsIDWithoutGivenLevelID.length === 0) return;

      setSelectedLevelsIDs(selectedLevelsIDWithoutGivenLevelID);
      store.dispatch(removeLevel(levelID));

      AsyncStorage.setItem(
        CLASSES_CATEGORIES_FILTER_EDUCATION_LEVEL_STORAGE_KEY,
        JSON.stringify(selectedLevelsIDWithoutGivenLevelID),
      );

      return;
    }

    setSelectedLevelsIDs([...selectedLevelsIDs, levelID]);
    store.dispatch(addLevel(levelID));

    AsyncStorage.setItem(
      CLASSES_CATEGORIES_FILTER_EDUCATION_LEVEL_STORAGE_KEY,
      JSON.stringify([...selectedLevelsIDs, levelID]),
    );
  };

  const updateSelectedCampusesIDs = campusID => {
    if (selectedCampusesIDs.includes(campusID)) {
      const selectedCampusesIDWithoutGivenCampusID = selectedCampusesIDs.filter(
        selectedCampusID => selectedCampusID != campusID,
      );

      // Prevent user from deselecting all campuses
      // Can show notification bar here
      if (selectedCampusesIDWithoutGivenCampusID.length === 0) return;

      setSelectedCampusesIDs(selectedCampusesIDWithoutGivenCampusID);
      store.dispatch(removeCampus(campusID));

      AsyncStorage.setItem(
        CLASSES_CATEGORIES_FILTER_CAMPUSES_STORAGE_KEY,
        JSON.stringify(selectedCampusesIDWithoutGivenCampusID),
      );
      return;
    }

    setSelectedCampusesIDs([...selectedCampusesIDs, campusID]);
    store.dispatch(addCampus(campusID));

    AsyncStorage.setItem(
      CLASSES_CATEGORIES_FILTER_CAMPUSES_STORAGE_KEY,
      JSON.stringify([...selectedCampusesIDs, campusID]),
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.background,
      }}
    >
      <RedTopView boldText={'OPTIONS'} bigText={'Classes'} inverted={true}>
        <ScrollView style={{paddingLeft: 24}}>
          <CardWithText
            title="Semester"
            imageSource={require('../../../assets/icons/class_categories_filters_screen/calendar.png')}
          >
            {sortSemesters(Object.keys(allSemesters)).map(
              (semesterID, index) => (
                <View key={semesterID}>
                  <View style={{paddingRight: 16}}>
                    <TouchableOpacity
                      onPress={() => updateSelectedSemesterID(semesterID)}
                    >
                      <RowWithCheckmark
                        textToDisplay={allSemesters[semesterID]}
                        isChecked={selectedSemesterID === semesterID}
                        style={{paddingVertical: 12, height: 'auto'}}
                      />
                    </TouchableOpacity>
                  </View>
                  {index < Object.values(allSemesters).length - 1 ? (
                    <Line />
                  ) : null}
                </View>
              ),
            )}
          </CardWithText>

          <CardWithText
            title="Level"
            imageSource={require('../../../assets/icons/class_categories_filters_screen/graduate.png')}
          >
            {Object.keys(allLevels).map((levelID, index) => (
              <View key={levelID}>
                <View style={{paddingRight: 16}}>
                  <TouchableOpacity
                    onPress={() => updateSelectedLevelsIDs(levelID)}
                  >
                    <RowWithCheckmark
                      textToDisplay={allLevels[levelID]}
                      isChecked={selectedLevelsIDs.includes(levelID)}
                      style={{paddingVertical: 12, height: 'auto'}}
                    />
                  </TouchableOpacity>
                </View>
                {index < Object.values(allLevels).length - 1 ? <Line /> : null}
              </View>
            ))}
          </CardWithText>

          <CardWithText
            title="Campus"
            imageSource={require('../../../assets/icons/class_categories_filters_screen/campus.png')}
          >
            {Object.keys(ALL_CAMPUSES).map((campusID, index) => (
              <View key={campusID}>
                <View style={{paddingRight: 16}}>
                  <TouchableOpacity
                    onPress={() => updateSelectedCampusesIDs(campusID)}
                  >
                    <RowWithCheckmark
                      textToDisplay={ALL_CAMPUSES[campusID]}
                      isChecked={selectedCampusesIDs.includes(campusID)}
                      style={{paddingVertical: 12, height: 'auto'}}
                    />
                  </TouchableOpacity>
                </View>
                {index < Object.values(ALL_CAMPUSES).length - 1 ? (
                  <Line />
                ) : null}
              </View>
            ))}
          </CardWithText>
        </ScrollView>
      </RedTopView>
    </View>
  );
};

export default ClassCategoriesFiltersScreen;
