import React, {useEffect} from 'react';

import {View, TouchableOpacity, Text, ScrollView} from 'react-native';
import {BigText} from '../../../shared-components/BigText';
import {Line} from '../../../shared-components/Line';
import {GRAY_4} from '../../../constants/colors';

// For Dark Mode compatibility
import {useNavigation, useTheme} from '@react-navigation/native';
import {BackButton} from '../../../shared-components/BackButton';

import {useSelector} from 'react-redux';
import {fetchFilteredClasses} from '../../../redux/actions/classesActions';
import store from '../../../redux/store';
import {Chevron} from '../../../shared-components/Chevron';
import {PluralMaker} from '../../../utils';
import {SFProTextRegular} from '../../../constants/fonts';
import {FocusAwareStatusBar} from '../../../shared-components';
import AddNewClassButton from './AddNewClassButton';

const ClassesScreen = props => {
  const {colors} = useTheme();
  const {displayTitle, code} = props.route.params;

  const semester = useSelector(state => state.academics.semester);
  const campuses = useSelector(state => state.academics.campuses);
  const levels = useSelector(state => state.academics.levels);
  const classes = useSelector(state => state.academics.classes);

  useEffect(() => {
    const classFilters = {
      semester,
      campuses,
      levels,
      code,
    };

    store.dispatch(fetchFilteredClasses(classFilters));
  }, [semester, campuses, levels, code]);

  return (
    <View style={{flex: 1}}>
      <FocusAwareStatusBar />
      <AddNewClassButton />
      <BackButton text="Class Categories" />
      <BigText
        style={{
          marginLeft: 16,
          marginTop: 80,
          fontSize: 24,
          color: colors.text,
          flexWrap: 'wrap',
          height: 'auto',
        }}
      >
        {displayTitle}
      </BigText>
      <ScrollView>
        {classes.map((academicClass, index) => (
          <ClassItem
            showSeparator={index < classes.length - 1}
            key={academicClass.courseNumber + index}
            {...academicClass}
          />
        ))}
      </ScrollView>
    </View>
  );
};

const ClassItem = (academicClass, showSeparator) => {
  const {colors} = useTheme();
  const navigation = useNavigation();
  const pluralMaker = new PluralMaker('credit');

  return (
    <TouchableOpacity
      key={academicClass.courseNumber}
      onPress={() => navigation.push('SectionsScreen', {academicClass})}
    >
      <View
        style={{
          marginLeft: 16,
          paddingVertical: 11,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}
      >
        <View style={{flex: 1}}>
          <Text
            style={{
              fontFamily: SFProTextRegular,
              letterSpacing: -0.5,
              fontSize: 17,
              maxWidth: 300,
              textTransform: 'capitalize',
              color: colors.text,
            }}
          >
            {academicClass.courseNumber}: {academicClass.title}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'flex-end',
              justifyContent: 'space-between',
              fontSize: 13,
              marginTop: 6,
              marginRight: 6,
            }}
          >
            <Text
              style={{
                fontFamily: SFProTextRegular,
                fontSize: 13,
                color:
                  academicClass.openSections > 0
                    ? 'rgb(96,178,6)'
                    : 'rgb(255,46,59)',
              }}
            >
              {academicClass.openSections} open section
              {academicClass.openSections === 1 ? '' : 's'} of{' '}
              {academicClass.sections.length}
            </Text>

            <Text
              style={{
                fontFamily: SFProTextRegular,
                color: GRAY_4,
                fontSize: 13,
                paddingRight: 12,
              }}
            >
              {academicClass.credits
                ? pluralMaker.sentenceWithQuantityAndSubject(
                    academicClass.credits,
                  )
                : '-- credits'}
            </Text>
          </View>
        </View>
        <Chevron />
      </View>
      <Line style={{marginLeft: 16, opacity: showSeparator ? 0.2 : 0}} />
    </TouchableOpacity>
  );
};

export default ClassesScreen;
