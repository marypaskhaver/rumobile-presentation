import React from 'react';
import {
  getSectionLocationFromMeetingTime,
  TimeFormatter,
} from '../../../utils/';
import {View, Text} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {GRAY_1, GRAY_3, GRAY_4} from '../../../constants/colors';
import {SFProTextRegular} from '../../../constants/fonts';
import {SectionLocation} from '../../../shared-components';

const MeetingItem = ({meetingTime}) => {
  const {colors, dark} = useTheme();
  const {meetingDay, startTime, endTime, pmCode} = meetingTime;

  const weekday = identifyWeekdayFromCharacters(meetingDay);

  const startTimeFormatter = new TimeFormatter(startTime)
    .addColon()
    .addTimeOfDay(pmCode);
  const endTimeFormatter = new TimeFormatter(endTime)
    .addColon()
    .addTimeOfDay(pmCode);

  const finalEndTime = !startTimeFormatter.isSpanValidAsAClassLength(
    endTimeFormatter,
  )
    ? endTimeFormatter.getOppositeTime()
    : endTimeFormatter.time;

  return (
    <View>
      {weekday === 'Independent Study' ? null : (
        <Text
          style={{
            fontFamily: SFProTextRegular,
            fontSize: 15,
            fontWeight: '600',
            marginVertical: 11,
            color: colors.text,
          }}
        >
          {weekday.toUpperCase()}
        </Text>
      )}
      <View
        style={{
          fontSize: 15,
          fontWeight: '600',
          marginVertical: 11,
          color: colors.text,
        }}
      >
        <View style={{flexDirection: 'row'}}>
          {startTimeFormatter.time ? (
            <View
              style={{
                paddingRight: 8,
                marginRight: 8,
                borderRightColor: dark ? GRAY_1 : GRAY_3,
                borderRightWidth: 0.5,
                width: 90,
                flexDirection: 'column',
                justifyContent: 'center',
              }}
            >
              <Text
                style={{
                  fontFamily: SFProTextRegular,
                  textAlign: 'right',
                  fontSize: 17,
                  color: colors.text,
                }}
              >
                {startTimeFormatter.time}
              </Text>
              <Text
                style={{
                  fontFamily: SFProTextRegular,
                  textAlign: 'right',
                  fontSize: 12,
                  color: GRAY_4,
                }}
              >
                {finalEndTime}
              </Text>
            </View>
          ) : (
            <View
              style={{
                paddingRight: 8,
                marginRight: 8,
                borderRightColor: dark ? GRAY_1 : GRAY_3,
                borderRightWidth: 0.5,
                width: 90,
                flexDirection: 'column',
                justifyContent: 'center',
              }}
            >
              <Text
                style={{
                  fontFamily: SFProTextRegular,
                  textAlign: 'right',
                  fontSize: 15,
                  color: colors.text,
                }}
              >
                ANY TIME
              </Text>
            </View>
          )}
          <View style={{flex: 2}}>
            <Text
              style={{
                fontFamily: SFProTextRegular,
                fontSize: 14,
                marginBottom: 5,
                flex: 2,
                color: colors.text,
              }}
            >
              {getSectionLocationFromMeetingTime(meetingTime)}
            </Text>

            <SectionLocation meetingTime={meetingTime} />
          </View>
        </View>
      </View>
    </View>
  );
};

export default MeetingItem;
