import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  SectionList,
  TouchableOpacity,
  Keyboard,
  Platform,
} from 'react-native';
import styled from 'styled-components/native';
import {useNavigation, useTheme} from '@react-navigation/native';
import {useSelector} from 'react-redux';

import {BackButton} from '../../../shared-components/BackButton';
import {BigText} from '../../../shared-components/BigText';
import {Chevron} from '../../../shared-components/Chevron';
import {GRAY_1, GRAY_5, PASTEL_RED} from '../../../constants/colors';
import {Line} from '../../../shared-components/Line';
import {reformatClassCategoryData} from '../../../redux/actions/academicActions';
import {requestClassCategories} from '../../../redux/actions/academicActions';
import {
  Row,
  LoadingSign,
  FocusAwareStatusBar,
} from '../../../shared-components/';
import {SFProTextRegular, SFProTextSemibold} from '../../../constants/fonts';
import store from '../../../redux/store';
import SearchBar from './SearchBar';
import filterClassCategories from '../../../utils/filterClassCategories';
import {selectClassCategoriesFetchStatus} from '../../../redux/reducers/academicsReducer';
import AddNewClassButton from './AddNewClassButton';

const HeaderText = styled.Text`
  font-size: 18px;
  padding: 2px 16px;
  font-family: ${`${SFProTextSemibold}`};
  color: ${props => props.color};
`;

const ItemText = styled.Text`
  font-size: 17px;
  padding: 0 32px 0 16px;
  font-family: ${`${SFProTextRegular}`};
  color: ${props => props.color};
`;

const ItemRow = classCategory => {
  const {colors} = useTheme();
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      onPress={() => navigation.push('ClassesScreen', classCategory)}
    >
      <Row
        style={{
          paddingVertical: 12,
        }}
      >
        <ItemText color={colors.text}>{classCategory.displayTitle}</ItemText>
        <Chevron />
      </Row>
    </TouchableOpacity>
  );
};

const Header = ({title}) => {
  const {dark, colors} = useTheme();

  return (
    <View
      backgroundColor={dark ? colors.card : GRAY_1}
      style={{
        justifyContent: 'center',
      }}
    >
      <HeaderText color={colors.text}>{title}</HeaderText>
    </View>
  );
};

const ClassesCategoriesFilters = () => {
  const {semester, campuses, levels} = useSelector(state => state.academics);
  const navigation = useNavigation();

  const semesterDisplayNames = {
    0: 'winter',
    1: 'spring',
    7: 'summer',
    9: 'fall',
  };
  const semesterDisplayName = semesterDisplayNames[semester.charAt(0)];
  const schoolYear = semester.slice(1);

  const sortedCampuses = campuses.sort().toString();

  const formattedCampusesString = sortedCampuses.replace(/,/g, ' ');

  const sortedLevels = levels.sort().reverse().toString();
  const formattedLevelsString = sortedLevels.replace(/,/g, ' ');

  const filtersDisplayString = `${semesterDisplayName} ${schoolYear} ${formattedCampusesString} ${formattedLevelsString}`;

  return (
    <TouchableOpacity
      style={{paddingRight: 16, alignSelf: 'flex-start'}}
      onPress={() => navigation.navigate('ClassCategoriesFiltersScreen')}
    >
      <Text
        style={{
          fontSize: 12,
          fontFamily: SFProTextRegular,
          color: PASTEL_RED,
          paddingTop: 16,
          textTransform: 'uppercase',
        }}
      >
        {filtersDisplayString.length > 18
          ? filtersDisplayString.substring(0, 19) + '...'
          : filtersDisplayString}
      </Text>
    </TouchableOpacity>
  );
};

const CancelButton = ({onPressAction}) => {
  return (
    <TouchableOpacity onPress={onPressAction}>
      <Text
        style={{
          paddingRight: 16,
          fontSize: 17,
          color: 'rgb(237, 69, 69)',
          fontFamily: SFProTextRegular,
        }}
      >
        Cancel
      </Text>
    </TouchableOpacity>
  );
};

const ClassCategoriesScreen = () => {
  const {colors} = useTheme();

  const semester = useSelector(state => state.academics.semester);
  const campuses = useSelector(state => state.academics.campuses);
  const levels = useSelector(state => state.academics.levels);

  const classCategories = useSelector(state => state.academics.classCategories);
  const reformattedClassCategories = reformatClassCategoryData(classCategories);
  const [displayedCategories, setDisplayedCategories] = useState(
    reformattedClassCategories,
  );
  const [isSearching, setIsSearching] = useState(false);
  const [searchValue, setSearchValue] = useState('');

  // Re-runs if semester, campus, or level changes
  useEffect(() => {
    const classCategoryFilters = {
      semester,
      campuses,
      levels,
    };

    store.dispatch(requestClassCategories(classCategoryFilters));
  }, [semester, campuses, levels]);

  const updateSearch = text => {
    const filteredClassCategories = filterClassCategories(
      reformattedClassCategories,
      text,
    );

    setSearchValue(text);
    setDisplayedCategories(filteredClassCategories);
  };

  const classesFetchStatus = useSelector(selectClassCategoriesFetchStatus);

  return (
    <View style={{flex: 1}}>
      <FocusAwareStatusBar />
      <AddNewClassButton />
      <BackButton text="Back" screenToGoTo="MoreItemsList" />
      <View style={{flexDirection: 'row', marginTop: 72, alignItems: 'center'}}>
        {isSearching ? null : (
          <BigText
            style={{
              marginLeft: 20,
              fontSize: 24,
              flex: 1,
              paddingTop: 8,
              color: colors.text,
            }}
          >
            Class Categories
          </BigText>
        )}
        {!isSearching && <ClassesCategoriesFilters />}
      </View>

      {classesFetchStatus !== 'success' && <LoadingSign />}
      {classesFetchStatus === 'success' &&
        (reformattedClassCategories.length === 0 ? (
          <Text
            style={{
              alignSelf: 'center',
              paddingTop: 40,
              color: GRAY_5,
              fontSize: 16,
              fontFamily: SFProTextRegular,
            }}
          >
            No class categories found.
          </Text>
        ) : (
          <View style={{flex: 1}}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <SearchBar
                onChangeTextAction={updateSearch}
                onFocusAction={() => setIsSearching(true)}
                value={searchValue}
              />
              {isSearching && (
                <CancelButton
                  onPressAction={() => {
                    setIsSearching(false);
                    Keyboard.dismiss();
                    setSearchValue('');
                    updateSearch('');
                  }}
                />
              )}
            </View>

            <SectionList
              sections={
                (displayedCategories.length === 0 && !isSearching) ||
                !searchValue
                  ? reformattedClassCategories
                  : displayedCategories
              }
              keyExtractor={item => item.code}
              renderItem={({item}) => ItemRow(item)}
              renderSectionHeader={({section}) => (
                <Header title={section.title} />
              )}
              ItemSeparatorComponent={() => <Line style={{marginLeft: 16}} />}
              style={{marginTop: 8}}
            />
          </View>
        ))}
    </View>
  );
};

export default ClassCategoriesScreen;
