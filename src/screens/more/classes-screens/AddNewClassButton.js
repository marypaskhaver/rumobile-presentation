import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {TouchableOpacity, Image} from 'react-native';

export default AddNewClassButton = () => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      style={{
        alignSelf: 'flex-end',
        position: 'absolute',
        top: 48,
        right: -16,
        zIndex: 50,
      }}
      onPress={() => {
        navigation.navigate('EditSectionScreen');
      }}
    >
      <Image
        style={{height: 24, resizeMode: 'contain'}}
        source={require('../../../assets/images/buttons/addclassbutton.png')}
      />
    </TouchableOpacity>
  );
};
