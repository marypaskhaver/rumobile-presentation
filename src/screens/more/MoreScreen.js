import React from 'react';

// Holds / displays data in table format
import {FlatList, View, TouchableOpacity} from 'react-native';

// styled-components library, used for styling components
import styled from 'styled-components/native';

import {Row} from '../../shared-components/Row';

// For Dark Mode compatibility
import {useNavigation, useTheme} from '@react-navigation/native';

import {createStackNavigator} from '@react-navigation/stack';

import SettingsScreen from '../settings/SettingsScreen';
import ClassCategoriesScreen from './classes-screens/ClassCategoriesScreen';
import SectionsScreen from './classes-screens/SectionsScreen';
import SectionDetailsScreen from './classes-screens/SectionDetailsScreen';
import ClassesScreen from './classes-screens/ClassesScreen';
import ProfileScreen from './profile-screens/ProfileScreen';

const LinkText = styled.Text`
  font-size: 17px;
  padding-vertical: 14px;
  letter-spacing: -0.41px;
  width: 375px;
  margin-left: 17px;
  flex: 1;
`;

import {BigText} from '../../shared-components/BigText';

// Styling for the image each cell will display.
const Icon = styled.Image`
  width: 24px;
  height: 24px;
  margin-left: 17px;
  margin-bottom: 10px;
  margin-top: 10px;
`;

// Styling for > symbol at the end of each cell.
import {Chevron} from '../../shared-components/Chevron';
import {Line} from '../../shared-components/Line';
import MeetTheTeamScreen from './profile-screens/MeetTheTeamScreen';
import {SafeAreaView} from 'react-native-safe-area-context';
import {FocusAwareStatusBar} from '../../shared-components';

// Consists of Container which holds a FlatList with cells containing table data.
const List = () => {
  const {colors} = useTheme();
  const navigation = useNavigation();

  return (
    <View style={{height: '100%'}}>
      <FlatList
        data={[
          {
            imageSource: require('../../assets/icons/more_screen/coursesicon.png'),
            text: 'Class Categories',
            screenToGoTo: 'ClassCategoriesScreen',
          },
          {
            imageSource: require('../../assets/icons/more_screen/todayicon.png'),
            text: 'Meet the Team',
            screenToGoTo: 'MeetTheTeamScreen',
          },
          {
            imageSource: require('../../assets/icons/more_screen/star.png'),
            text: 'Do you like RUMobile?',
            screenToGoTo: 'DoYouLikeRUMobileScreen',
          },
          {
            imageSource: require('../../assets/icons/more_screen/email.png'),
            text: 'Feedback',
            screenToGoTo: 'FeedbackScreen',
          },
          {
            imageSource: require('../../assets/icons/more_screen/bug.png'),
            text: 'Bug Report',
            screenToGoTo: 'BugReportScreen',
          },
          {
            imageSource: require('../../assets/icons/more_screen/cogicon.png'),
            text: 'Settings',
            screenToGoTo: 'Settings',
          },
        ]}
        ItemSeparatorComponent={() => <Line style={{marginLeft: 60}} />}
        renderItem={({item}) => (
          <TouchableOpacity
            onPress={() => navigation.navigate(item.screenToGoTo)}>
            <Row>
              <Icon
                style={{tintColor: colors.text}}
                source={item.imageSource}
              />

              <LinkText style={{color: colors.text}}>{item.text}</LinkText>

              <Chevron />
            </Row>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

const MoreItemsList = ({title}) => {
  const {colors} = useTheme();

  return (
    <SafeAreaView style={{flex: 1, top: 20}}>
      <FocusAwareStatusBar />
      <BigText
        style={{
          marginTop: 17,
          marginLeft: 20,
          marginBottom: 9,
          color: colors.text,
        }}>
        {title}
      </BigText>
      <List />
    </SafeAreaView>
  );
};

const MoreScreen = ({title}) => {
  const Stack = createStackNavigator();

  return (
    <Stack.Navigator
      initialRouteName="MoreItemsList"
      screenOptions={{headerShown: false}}>
      <Stack.Screen
        name="MoreItemsList"
        children={() => <MoreItemsList title={title} />}
      />
      <Stack.Screen
        name="ProfileScreen"
        component={ProfileScreen}
        options={{
          presentation: 'card',
        }}
      />
      <Stack.Screen
        name="MeetTheTeamScreen"
        component={MeetTheTeamScreen}
        options={{
          presentation: 'card',
        }}
      />
      <Stack.Screen
        name="ClassCategoriesScreen"
        component={ClassCategoriesScreen}
        options={{
          presentation: 'card',
        }}
      />
      <Stack.Screen
        name="ClassesScreen"
        component={ClassesScreen}
        options={{
          presentation: 'card',
        }}
      />
      <Stack.Screen
        name="SectionsScreen"
        component={SectionsScreen}
        options={{
          presentation: 'card',
        }}
      />
      <Stack.Screen
        name="SectionDetailsScreen"
        component={SectionDetailsScreen}
        options={{
          presentation: 'card',
        }}
      />
      <Stack.Screen
        name="Settings"
        component={SettingsScreen}
        options={{presentation: 'card'}}
      />
    </Stack.Navigator>
  );
};

export default MoreScreen;
