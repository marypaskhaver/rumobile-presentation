import {useTheme} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {Image, View, Text, ScrollView} from 'react-native';

import {useSelector} from 'react-redux';
import SegmentedControlTab from 'react-native-segmented-control-tab';
import {PASTEL_RED, GRAY_4} from '../../constants/colors';
import {SFProTextRegular} from '../../constants/fonts';

import {BackButton} from '../../shared-components/BackButton';
import {BigText} from '../../shared-components/BigText';

import FoodSection from './FoodSection';

import {menuItemsForDiningHallNameAtTimeOfDay} from '../../redux/reducers/foodReducer';
import {diningHallNamesToFormattedNames} from '../../constants/diningHalls';
import {FocusAwareStatusBar} from '../../shared-components';
import {DateFormatter, getCurrentFoodTimeOfDay, refreshAt} from '../../utils';

const DiningHallPopupScreen = props => {
  const {colors} = useTheme();
  const {diningHallName} = props.route.params;
  const defaultTimeOfDay = getCurrentFoodTimeOfDay();
  const [timeOfDay, setTimeOfDay] = useState(defaultTimeOfDay);
  const today = new DateFormatter().getWeekday();

  useEffect(() => {
    refreshAt(0, 0, 0, () => {
      setTimeOfDay(getCurrentFoodTimeOfDay());
    });

    refreshAt(7, 0, 0, () => {
      setTimeOfDay(getCurrentFoodTimeOfDay());
    });

    refreshAt(11, 0, 0, () => {
      setTimeOfDay(getCurrentFoodTimeOfDay());
    });

    refreshAt(17, 0, 0, () => {
      setTimeOfDay(getCurrentFoodTimeOfDay());
    });

    refreshAt(21, 0, 0, () => {
      setTimeOfDay(getCurrentFoodTimeOfDay());
    });

    if (
      today === 'MONDAY' ||
      today === 'TUESDAY' ||
      today === 'WEDNESDAY' ||
      today === 'THURSDAY'
    ) {
      refreshAt(23, 0, 0, () => {
        setTimeOfDay(getCurrentFoodTimeOfDay());
      });
    }

    if (today === 'SATURDAY' || today === 'SUNDAY') {
      refreshAt(9, 30, 0, () => {
        setTimeOfDay(getCurrentFoodTimeOfDay());
      });

      refreshAt(20, 0, 0, () => {
        setTimeOfDay(getCurrentFoodTimeOfDay());
      });
    }
  }, []);

  const segments = [
    {
      displayName: 'Breakfast',
      filterName: 'breakfast',
    },

    {
      displayName: 'Lunch',
      filterName: 'lunch',
    },

    {
      displayName: 'Dinner',
      filterName: 'dinner',
    },

    {
      displayName: 'Takeout',
      filterName: 'takeout',
    },
  ];

  const menuItemsForDiningHallAtSelectedTime = useSelector(
    menuItemsForDiningHallNameAtTimeOfDay(diningHallName, timeOfDay),
  );

  const genres = Object.keys(menuItemsForDiningHallAtSelectedTime);

  const itemList = genres.map(genreName => (
    <FoodSection
      key={genreName}
      genreName={genreName}
      genreFoods={menuItemsForDiningHallAtSelectedTime[genreName]}
    />
  ));

  const displayNameForTimeOfDay = segments.find(
    segment => segment.filterName === timeOfDay,
  ).displayName;

  const NoMealsForTimeOfDay = (
    <View key="No Food" style={{alignItems: 'center', marginBottom: 20}}>
      <Image
        source={require('../../assets/images/dining_halls/food/blankstate_nofood.png')}
        style={{marginTop: 32, width: 400, height: 400}}
      />
      <Text
        style={{
          color: GRAY_4,
          marginTop: 24,
          fontFamily: SFProTextRegular,
          fontSize: 16,
        }}
      >
        There is currently no {displayNameForTimeOfDay.toLowerCase()} at this
        time.
      </Text>
    </View>
  );

  const formattedName = diningHallNamesToFormattedNames[diningHallName];

  return (
    <View style={{flex: 1}}>
      <FocusAwareStatusBar />
      <BackButton text={'Food'} />
      <View style={{marginLeft: 16, marginRight: 16, marginTop: 76}}>
        <BigText
          style={{
            color: colors.text,
            fontSize: 24,
            width: '100%',
          }}
        >
          {formattedName}
        </BigText>

        <SegmentedControlTab
          values={segments.map(segment => segment.displayName)}
          onTabPress={index => setTimeOfDay(segments[index].filterName)}
          selectedIndex={segments.findIndex(
            segment => segment.filterName === timeOfDay,
          )}
          tabTextStyle={{
            color: PASTEL_RED,
            fontSize: 16,
            fontFamily: SFProTextRegular,
          }}
          tabStyle={{
            borderColor: PASTEL_RED,
            borderWidth: 1,
            backgroundColor: colors.background,
          }}
          activeTabStyle={{
            backgroundColor: PASTEL_RED,
            borderColor: PASTEL_RED,
            borderWidth: 1,
          }}
          tabsContainerStyle={{
            marginVertical: 12,
          }}
        />
      </View>

      <ScrollView style={{marginTop: 16}}>
        {Object.entries(menuItemsForDiningHallAtSelectedTime).length > 0
          ? itemList
          : NoMealsForTimeOfDay}
      </ScrollView>
    </View>
  );
};

export default DiningHallPopupScreen;
