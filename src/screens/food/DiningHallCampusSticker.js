import React from 'react';

import {Container} from '../../shared-components/Container';
import {Sticker} from './Sticker';

import {PURPLE, YELLOW, BLUE, GREEN} from '../../constants/colors';
import {diningHallNameToCampusName} from '../../constants/diningHalls';

export const DiningHallCampusSticker = ({diningHallName}) => {
  const colors = {
    buschDiningHall: PURPLE,
    browerCommons: YELLOW,
    livingstonDiningCommons: BLUE,
    neilsonDiningHall: GREEN,
  };

  const campusColor = colors[diningHallName];
  const displayName = diningHallNameToCampusName[diningHallName];

  return (
    <Container style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
      <Sticker color={campusColor} title={displayName} />
    </Container>
  );
};
