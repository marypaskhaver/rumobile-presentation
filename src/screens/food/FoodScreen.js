import React, {useEffect} from 'react';
import {
  Container,
  FocusAwareStatusBar,
  LoadingSign,
} from '../../shared-components';
import {CardWithImage} from './DiningHallItems';
import styled from 'styled-components/native';
import {useDispatch, useSelector} from 'react-redux';
import {requestMenuItems} from '../../redux/actions/foodActions';
import {
  selectMenuItems,
  selectMenuItemsFetchStatus,
} from '../../redux/reducers/foodReducer';
import {createStackNavigator} from '@react-navigation/stack';
import DiningHallPopupScreen from './DiningHallPopupScreen';
import {selectChosenCampusID} from '../../redux/reducers/campusesReducer';
import CamdenFoodScreen from './CamdenFoodScreen';
import NewarkFoodScreen from './NewarkFoodScreen';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Platform, ScrollView} from 'react-native';
import TopText from '../buses/TopText';
import {GRAY_4} from '../../constants/colors';
import {SFProTextSemibold} from '../../constants/fonts';
import {Text} from 'react-native';
import {DateFormatter, refreshAt} from '../../utils';
import FoodDetailsScreen from './FoodDetailsScreen';
import MenuItemsRefreshControl from './MenuItemsRefreshControl';

const DiningInfoContainer = styled(Container)`
  margin-top: 18px;
  padding: 0 12px;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-around;
`;

const DiningHallCards = () => {
  const dispatch = useDispatch();

  const menuItemsFetchStatus = useSelector(selectMenuItemsFetchStatus);
  const today = new DateFormatter().getWeekday();

  useEffect(() => {
    dispatch(requestMenuItems());

    refreshAt(0, 0, 0, () => {
      dispatch(requestMenuItems());
    });

    refreshAt(7, 0, 0, () => {
      dispatch(requestMenuItems());
    });

    refreshAt(21, 0, 0, () => {
      dispatch(requestMenuItems());
    });

    if (
      today === 'MONDAY' ||
      today === 'TUESDAY' ||
      today === 'WEDNESDAY' ||
      today === 'THURSDAY'
    ) {
      refreshAt(23, 0, 0, () => {
        dispatch(requestMenuItems());
      });
    }

    if (today === 'SATURDAY' || today === 'SUNDAY') {
      refreshAt(9, 30, 0, () => {
        dispatch(requestMenuItems());
      });

      refreshAt(20, 0, 0, () => {
        dispatch(requestMenuItems());
      });
    }
  }, []);

  const menuItems = useSelector(selectMenuItems);

  return (
    <DiningInfoContainer>
      {menuItemsFetchStatus !== 'success' ? (
        <LoadingSign />
      ) : (
        Object.values(menuItems).map(menuItem => (
          <CardWithImage key={menuItem.diningHallName} menuItem={menuItem} />
        ))
      )}
    </DiningInfoContainer>
  );
};

const DiningHallsDisplay = ({title}) => {
  return (
    <SafeAreaView style={{flex: 1, top: 20}}>
      <TopText mainTitle={title} />
      <FocusAwareStatusBar />
      <Text
        style={{
          marginTop: 11,
          marginRight: 0,
          marginBottom: 6,
          marginLeft: 21,
          textTransform: 'uppercase',
          fontSize: 16,
          color: GRAY_4,
          fontFamily: SFProTextSemibold,
        }}
      >
        Dining Halls
      </Text>
      <ScrollView
        refreshControl={
          Platform.OS === 'ios' ? <MenuItemsRefreshControl /> : null
        }
      >
        <DiningHallCards />
      </ScrollView>
    </SafeAreaView>
  );
};

const FoodScreen = ({title}) => {
  const Stack = createStackNavigator();
  const chosenCampusID = useSelector(selectChosenCampusID);

  if (chosenCampusID === 2) {
    return <NewarkFoodScreen />;
  } else if (chosenCampusID === 3) {
    return <CamdenFoodScreen />;
  }

  // New Brunswick
  return (
    <Stack.Navigator
      initialRouteName="DiningHallsDisplay"
      screenOptions={{headerShown: false}}
    >
      <Stack.Screen
        name="DiningHallsDisplay"
        children={() => <DiningHallsDisplay title={title} />}
      />
      <Stack.Screen
        name="DiningHallPopupScreen"
        component={DiningHallPopupScreen}
        options={{
          presentation: 'card',
        }}
      />
      <Stack.Screen
        name="FoodDetailsScreen"
        component={FoodDetailsScreen}
        options={{
          presentation: 'card',
        }}
      />
    </Stack.Navigator>
  );
};

export default FoodScreen;
