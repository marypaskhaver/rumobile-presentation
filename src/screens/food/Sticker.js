import React from 'react';

// styled-components library, used for styling components
import styled from 'styled-components/native';

const ColoredView = styled.View`
  height: 20px;
  border-bottom-left-radius: 3px;
  border-top-left-radius: 3px;
  background-color: ${props => props.color};
  padding: 0 7px;

  align-self: center;
  justify-content: center;
`;

const StickerText = styled.Text`
  color: white;
  font-size: 12px;
`;

export const Sticker = ({color, title}) => {
  return (
    <ColoredView color={color}>
      <StickerText>{title}</StickerText>
    </ColoredView>
  );
};
