import React from 'react';
import {useTheme} from '@react-navigation/native';
import {View} from 'react-native';
import {PASTEL_RED, GRAY_4} from '../../constants/colors';

const BulletPoint = () => {
  const {dark} = useTheme();

  return (
    <View
      style={{
        height: 16,
        width: 16,
        borderRadius: 8,
        borderWidth: 2,
        borderColor: dark ? GRAY_4 : 'white',
        shadowOffset: {width: 0, height: 2},
        shadowColor: GRAY_4,
        shadowOpacity: 0.5,
        marginRight: 16,
        backgroundColor: PASTEL_RED,
      }}
    />
  );
};

export default BulletPoint;
