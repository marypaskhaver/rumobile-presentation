import React from 'react';
import {DiningHallCampusSticker} from '../DiningHallCampusSticker';
import {render, screen} from '../../../testUtils/testUtils';

const useRender = diningHallName => {
  render(<DiningHallCampusSticker diningHallName={diningHallName} />);
};

describe('DiningHallCampusSticker', () => {
  it('displays dining hall text', () => {
    useRender('buschDiningHall');
    expect(screen.getByText('Busch')).toBeTruthy();
  });
});
