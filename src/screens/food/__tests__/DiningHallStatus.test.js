import React from 'react';
import {DiningHallStatus} from '../DiningHallItems';

import {render, screen} from '../../../testUtils/testUtils';

const useRender = hallIsOpen => {
  render(<DiningHallStatus hallIsOpen={hallIsOpen} />);
};

describe('DiningHallStatus', () => {
  it('displays if dining hall is open', () => {
    useRender(true);
    expect(screen.getByText('OPEN')).toBeTruthy();
  });

  it('displays if dining hall is closed', () => {
    useRender(false);
    expect(screen.getByText('CLOSED')).toBeTruthy();
  });
});
