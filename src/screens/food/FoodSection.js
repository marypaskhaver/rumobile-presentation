import React from 'react';

import styled from 'styled-components';
import {ImageBackground, TouchableOpacity, View} from 'react-native';
import {Line, Row} from '../../shared-components/';
import {useNavigation, useTheme} from '@react-navigation/native';
import {SFProTextRegular} from '../../constants/fonts';
import {
  GRAY_3,
  GRAY_5,
  PASTEL_RED,
  GRAY_6,
  GRAY_2,
} from '../../constants/colors';
import {Text} from 'react-native';
import {titleCase} from 'title-case';

const FoodText = styled.Text`
  font-size: 17px;
  margin-top: 16px;
  font-family: ${`${SFProTextRegular}`};
`;

const FoodSectionHeaderText = styled.Text`
  text-align: right;
  color: white;
  font-size: 20px;
  font-weight: 300;
  padding-right: 12px;
  padding-vertical: 4px;
  background-color: rgba(0, 0, 0, 0.42);
`;

const FoodList = ({foodDetailsImageSource, genreFoods}) => {
  const {dark, colors} = useTheme();
  const navigation = useNavigation();

  return genreFoods.map(food => {
    const {name, calories, portionSize, nutritionPath} = food;

    return (
      <TouchableOpacity
        key={name}
        onPress={() => {
          navigation.navigate('FoodDetailsScreen', {
            foodDetailsImageSource,
            calories,
            portionSize,
            name,
            nutritionPath,
          });
        }}
      >
        <View style={{paddingHorizontal: 30}}>
          <FoodText style={{color: colors.text}}>{name}</FoodText>
          <Row
            style={{
              justifyContent: 'space-between',
              marginTop: 8,
              marginBottom: 12,
            }}
          >
            <Text
              style={{
                fontFamily: SFProTextRegular,
                fontSize: 15,
                color: food.calories
                  ? food.calories < 250
                    ? 'rgb(90,175,79)'
                    : PASTEL_RED
                  : dark
                  ? GRAY_3
                  : GRAY_5,
              }}
            >
              {calories ? calories : '--'} calories
            </Text>
            <Text
              style={{
                fontFamily: SFProTextRegular,
                color: dark ? GRAY_3 : GRAY_5,
                fontSize: 15,
              }}
            >
              {portionSize}
            </Text>
          </Row>

          <Line style={{borderBottomColor: dark ? GRAY_6 : GRAY_2}} />
        </View>
      </TouchableOpacity>
    );
  });
};

const genreNameToBackgroundImage = genreName => {
  const backgroundImageMap = {
    Accompaniments: require('../../assets/images/dining_halls/food/Accompaniments.jpg'),
    'Bakery Misc': require('../../assets/images/dining_halls/food/BakeryMisc.jpg'),
    'Breakfast Bakery': require('../../assets/images/dining_halls/food/BreakfastBakery.jpg'),
    'Breakfast Entrees': require('../../assets/images/dining_halls/food/BreakfastEntrees.jpg'),
    'Breakfast Meats': require('../../assets/images/dining_halls/food/BreakfastMeats.jpg'),
    'Breakfast Misc': require('../../assets/images/dining_halls/food/BreakfastMisc.jpg'),
    Cantina: require('../../assets/images/dining_halls/food/Cantina.jpg'),
    'Cook to Order Bar': require('../../assets/images/dining_halls/food/CooktoOrderBar.jpg'),
    'Cook To Order Bar': require('../../assets/images/dining_halls/food/CooktoOrderBar.jpg'),
    'Deli Bar Entree': require('../../assets/images/dining_halls/food/DeliBarEntre.jpg'),
    'Knight Room': require('../../assets/images/dining_halls/food/KnightRoom.jpg'),
    'Lunch to Go': require('../../assets/images/dining_halls/food/LunchtoGo.jpg'),
    'Pizza/ Pasta': require('../../assets/images/dining_halls/food/PizzaPasta.jpg'),
    Pub: require('../../assets/images/dining_halls/food/Pub.jpg'),
    'Nightly Promo': require('../../assets/images/dining_halls/food/NightlyPromo.jpg'),
    'Salad Bar': require('../../assets/images/dining_halls/food/SaladBar.jpg'),
    'Sauces & Gravies': require('../../assets/images/dining_halls/food/SaucesGravies.jpg'),
    'Starch & Potatoes': require('../../assets/images/dining_halls/food/StarchPotatoes.jpg'),
    'Starch &  Potatoes': require('../../assets/images/dining_halls/food/StarchPotatoes.jpg'),
    'Wok/ Mongolian Grill': require('../../assets/images/dining_halls/food/WokMongolianGrill.jpg'),
    Desserts: require('../../assets/images/dining_halls/food/Desserts.jpg'),
    Entrees: require('../../assets/images/dining_halls/food/Entrees.jpg'),
    Salads: require('../../assets/images/dining_halls/food/SaladBar.jpg'),
    Soups: require('../../assets/images/dining_halls/food/Soups.jpg'),
    Vegetarian: require('../../assets/images/dining_halls/food/Vegetarian.jpg'),
    Veggies: require('../../assets/images/dining_halls/food/Veggies.jpg'),
  };

  const defaultBackgroundImage = require('../../assets/images/dining_halls/food/noimage.jpg');

  return backgroundImageMap[genreName] || defaultBackgroundImage;
};

const FoodSection = ({genreName, genreFoods}) => {
  const formattedGenreName = titleCase(genreName.toLowerCase());

  let backgroundImageSource = genreNameToBackgroundImage(formattedGenreName);

  return (
    <View>
      <ImageBackground
        style={{height: 65, flex: 1, justifyContent: 'flex-end'}}
        source={backgroundImageSource}
      >
        <FoodSectionHeaderText>{formattedGenreName}</FoodSectionHeaderText>
      </ImageBackground>
      <FoodList
        foodDetailsImageSource={backgroundImageSource}
        genreFoods={genreFoods}
      />
    </View>
  );
};

export default FoodSection;
