import {useTheme} from '@react-navigation/native';
import React from 'react';
import {useState} from 'react';
import {RefreshControl} from 'react-native';
import {useDispatch} from 'react-redux';
import {requestMenuItems} from '../../redux/actions/foodActions';

export default MenuItemsRefreshControl = () => {
  const [refreshing, setRefreshing] = useState(false);
  const {dark} = useTheme();

  const dispatch = useDispatch();

  const refresh = () => {
    setRefreshing(true);
    dispatch(requestMenuItems());
    setRefreshing(false);
  };

  return (
    <RefreshControl
      tintColor={dark ? 'white' : 'gray'}
      refreshing={refreshing}
      onRefresh={refresh}
    />
  );
};
