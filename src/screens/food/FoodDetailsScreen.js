import React, {useEffect} from 'react';
import styled from 'styled-components';
import {Image, Platform, ScrollView} from 'react-native';
import {
  BackButton,
  Card,
  FocusAwareStatusBar,
  LoadingSign,
  Row,
} from '../../shared-components/';
import {useTheme} from '@react-navigation/native';
import {
  SFProDisplayBold,
  SFProTextRegular,
  SFProTextSemibold,
} from '../../constants/fonts';
import {PASTEL_RED, GRAY_3, GRAY_4, GRAY_5} from '../../constants/colors';
import {Text} from 'react-native';
import {titleCase} from 'title-case';
import {SafeAreaView} from 'react-native-safe-area-context';
import {requestNutritionInfo} from '../../redux/actions/foodActions';
import {useDispatch, useSelector} from 'react-redux';
import {
  selectNutritionItems,
  selectNutritionItemsFetchStatus,
} from '../../redux/reducers/foodReducer';
import NutritionItemsRefreshControl from './NutritionItemsRefreshControl';
import NutritionLabel from './NutritionLabel';
import Macronutrient from './Macronutrient';

const DarkenedImageBackground = styled.ImageBackground`
  resizemode: 'contain';
  padding-bottom: 160px;
`;

const FoodDetailsScreen = props => {
  const {dark, colors} = useTheme();
  const {calories, portionSize, name, foodDetailsImageSource, nutritionPath} =
    props.route.params;
  const caloriesImage = require('../../assets/icons/dining_halls/calories.png');
  const carbsImage = require('../../assets/icons/dining_halls/carbs.png');
  const fatImage = require('../../assets/icons/dining_halls/fat.png');
  const proteinImage = require('../../assets/icons/dining_halls/protein.png');
  const ingredientsImage = require('../../assets/icons/dining_halls/ingredients.png');
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(requestNutritionInfo());
  }, []);

  const nutritionItemsFetchStatus = useSelector(
    selectNutritionItemsFetchStatus,
  );
  const foodDetails = useSelector(selectNutritionItems)[nutritionPath];

  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: 'rgba(18, 16, 16, 0.86)',
      }}
      edges={['right', 'left']}
    >
      <DarkenedImageBackground
        source={foodDetailsImageSource}
        imageStyle={{opacity: 0.5}}
      >
        <FocusAwareStatusBar
          barStyle={'light-content'}
          hidden={false}
          translucent={true}
          backgroundColor="transparent"
        />
        <BackButton isWhite={true} text="Back" />
        <Text
          style={{
            fontFamily: SFProTextSemibold,
            fontSize: 24,
            position: 'absolute',
            marginTop: 40,
            right: 16,
            color: 'white',
          }}
        >
          {portionSize}
        </Text>
        <Text
          style={{
            fontFamily: SFProDisplayBold,
            fontSize: 24,
            position: 'absolute',
            marginTop: 80,
            marginLeft: 20,
            color: 'white',
          }}
        >
          {name}
        </Text>
      </DarkenedImageBackground>

      <Card
        style={{
          height: '100%',
          borderTopLeftRadius: 24,
          borderTopRightRadius: 24,
          backgroundColor: colors.card,
          alignItems: 'center',
          marginTop: -20,
          paddingLeft: 20,
          paddingRight: 20,
          flex: 1,
          paddingBottom: 12,
          borderBottomLeftRadius: 0,
          borderBottomRightRadius: 0,
        }}
      >
        <ScrollView
          refreshControl={
            Platform.OS === 'ios' ? <NutritionItemsRefreshControl /> : null
          }
          style={{width: '100%', height: '100%'}}
        >
          <Row
            style={{
              justifyContent: 'space-between',
              flex: 1,
              paddingTop: 12,
            }}
          >
            <Row>
              <Image
                source={caloriesImage}
                style={{
                  width: 20,
                  height: 24,
                  marginRight: 4,
                  tintColor: colors.text,
                }}
              />
              <Text
                style={{
                  fontFamily: SFProDisplayBold,
                  fontSize: 20,
                  color: colors.text,
                }}
              >
                Calories
              </Text>
            </Row>

            <Text
              style={{
                fontFamily: SFProTextSemibold,
                fontSize: 20,
                color: calories
                  ? calories < 250
                    ? 'rgb(90,175,79)'
                    : PASTEL_RED
                  : dark
                  ? GRAY_3
                  : GRAY_5,
              }}
            >
              <Text
                style={{
                  fontFamily: SFProDisplayBold,
                  fontSize: 24,
                }}
              >
                {calories ? calories : '--'}{' '}
              </Text>
              kcal
            </Text>
          </Row>

          {nutritionItemsFetchStatus !== 'success' ? (
            <LoadingSign />
          ) : (
            <>
              <Row
                style={{
                  justifyContent: 'space-between',
                  flex: 1,
                  paddingHorizontal: 20,
                }}
              >
                <Macronutrient
                  macronutrientImageSource={fatImage}
                  macronutrientType={'Fat'}
                  macronutrientGrams={
                    foodDetails.quantitytable.totalFat
                      ? foodDetails.quantitytable.totalFat.quantity
                      : '-- g'
                  }
                />
                <Macronutrient
                  macronutrientImageSource={carbsImage}
                  macronutrientType={'Carbs'}
                  macronutrientGrams={
                    foodDetails.quantitytable.totCarb
                      ? foodDetails.quantitytable.totCarb.quantity
                      : '-- g'
                  }
                />
                <Macronutrient
                  macronutrientImageSource={proteinImage}
                  macronutrientType={'Protein'}
                  macronutrientGrams={
                    foodDetails.quantitytable.protein
                      ? foodDetails.quantitytable.protein.quantity
                      : '-- g'
                  }
                />
              </Row>

              <NutritionLabel foodDetails={foodDetails} />

              <Text style={{color: GRAY_4, paddingTop: 16}}>
                {foodDetails.notice}
              </Text>

              <Row
                style={{
                  paddingTop: 24,
                }}
              >
                <Image
                  source={ingredientsImage}
                  style={{
                    width: 24,
                    height: 20,
                    marginRight: 4,
                    tintColor: colors.text,
                  }}
                />
                <Text
                  style={{
                    fontFamily: SFProDisplayBold,
                    fontSize: 20,
                    color: colors.text,
                  }}
                >
                  Ingredients
                </Text>
              </Row>
              <Text
                style={{
                  paddingTop: 8,
                  color: colors.text,
                  fontFamily: SFProTextRegular,
                  fontSize: 16,
                }}
              >
                {foodDetails.ingredients
                  ? titleCase(foodDetails.ingredients)
                  : '--'}
              </Text>
            </>
          )}
        </ScrollView>
      </Card>
    </SafeAreaView>
  );
};

export default FoodDetailsScreen;
