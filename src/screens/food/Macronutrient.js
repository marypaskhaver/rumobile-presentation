import React from 'react';

import {Image, View} from 'react-native';
import {Row} from '../../shared-components/';
import {useTheme} from '@react-navigation/native';
import {SFProDisplayBold, SFProTextSemibold} from '../../constants/fonts';
import {GRAY_4} from '../../constants/colors';
import {Text} from 'react-native';

const Macronutrient = ({
  macronutrientImageSource,
  macronutrientType,
  macronutrientGrams,
}) => {
  const {colors} = useTheme();

  return (
    <View
      style={{
        paddingVertical: 28,
      }}
    >
      <Row
        style={{
          justifyContent: 'space-between',
          flex: 1,
        }}
      >
        <Image
          source={macronutrientImageSource}
          style={{
            width: 20,
            height: 20,
            marginRight: 4,
            tintColor: colors.text,
          }}
        />
        <Text
          style={{
            fontFamily: SFProDisplayBold,
            fontSize: 20,
            color: colors.text,
          }}
        >
          {macronutrientType}
        </Text>
      </Row>
      <Text
        style={{
          fontFamily: SFProTextSemibold,
          fontSize: 20,
          color: GRAY_4,
          alignSelf: 'center',
        }}
      >
        {macronutrientGrams}
      </Text>
    </View>
  );
};

export default Macronutrient;
