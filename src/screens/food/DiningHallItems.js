import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import styled from 'styled-components/native';

import {DiningHallCampusSticker} from './DiningHallCampusSticker';
import {LIGHT_GREEN, DARK_RED} from '../../constants/colors';
import {diningHallNamesToDisplayNames} from '../../constants/diningHalls';
import {SFProTextSemibold} from '../../constants/fonts';
import {Card} from '../../shared-components/Card';
import {Row} from '../../shared-components/Row';

export const DiningHallRow = styled(Row)`
  margin-bottom: 32px;
  justify-content: space-around;
`;

const DiningHallCard = styled(Card)`
  flex-direction: column;
  padding: 0;
  border-radius: 10px;
  margin-bottom: 24px;
  width: 170px;
  height: 198px;
`;

const DiningHallName = styled.Text`
  font-family: ${`${SFProTextSemibold}`};
  font-size: 20px;
  color: white;
  padding: 0 8px 16px 0;
  position: absolute;
  right: 0;
  bottom: 0;
`;

const DiningHallStatusText = styled.Text`
  font-size: 15px;
  font-family: ${`${SFProTextSemibold}`};
  margin-bottom: 15px;
  width: 137px;
  height: 24px;
  color: ${props => props.color};
`;

export const DiningHallStatus = ({hallIsOpen}) => {
  const status = hallIsOpen ? 'OPEN' : 'CLOSED';
  const color = hallIsOpen ? LIGHT_GREEN : DARK_RED;

  return <DiningHallStatusText color={color}>{status}</DiningHallStatusText>;
};

const TextContainer = styled.View`
  padding: 12px 0 0 12px;
`;

const DiningHallImage = styled.Image`
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
  width: 100%;
  height: 155px;
  align-self: center;
`;

export const CardWithImage = ({menuItem}) => {
  const navigation = useNavigation();

  const {diningHallName, hallIsOpen} = menuItem;

  const diningHallNamesToImageNames = {
    browerCommons: require('../../assets/images/dining_halls/brower_commons.jpg'),
    buschDiningHall: require('../../assets/images/dining_halls/busch.jpg'),
    livingstonDiningCommons: require('../../assets/images/dining_halls/livingston.jpg'),
    neilsonDiningHall: require('../../assets/images/dining_halls/neilson.jpg'),
  };

  const displayName = diningHallNamesToDisplayNames[diningHallName];

  const imagePath = diningHallNamesToImageNames[diningHallName];

  return (
    <TouchableOpacity
      onPress={() =>
        navigation.navigate('DiningHallPopupScreen', {
          diningHallName,
        })
      }>
      <DiningHallCard>
        <View>
          <DiningHallImage source={imagePath} />
          <DiningHallName>{displayName}</DiningHallName>
        </View>
        <DiningHallCampusSticker diningHallName={diningHallName} />

        <TextContainer style={{flexDirection: 'column'}}>
          <DiningHallStatus hallIsOpen={hallIsOpen} />
        </TextContainer>
      </DiningHallCard>
    </TouchableOpacity>
  );
};
