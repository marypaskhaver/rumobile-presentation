import React from 'react';
import styled from 'styled-components/native';
import {useTheme} from '@react-navigation/native';
import {Row, Line, FocusAwareStatusBar} from '../../shared-components';
import {Alert, Linking, Platform, TouchableOpacity} from 'react-native';
import {Chevron} from '../../shared-components/Chevron';
import {ScrollView} from 'react-native';
import TopText from '../buses/TopText';
import BulletPoint from './BulletPoint';
import {SafeAreaView} from 'react-native-safe-area-context';
import SafariView from 'react-native-safari-view';

// To hold FlatList "table".
const Container = styled.View`
  margin-top: 8px;
  margin-left: 20px;
`;

// Styling for the text describing a link in each cell.
const LinkText = styled.Text`
  font-size: 17px;
  padding-vertical: 14px;
  letter-spacing: -0.41px;
  width: 375px;
  flex: 1;
`;

const diningLocations = [
  {
    text: 'Robeson Food Court',
    link: 'https://runewarkdining.com/robeson-food-court/',
  },
  {
    text: '1 Park Bistro',
    link: 'https://runewarkdining.com/1-park-bistro/',
  },
  {
    text: 'Law & Justice Cafe',
    link: 'https://runewarkdining.com/law-justice-cafe/',
  },
  {
    text: 'On the RU-N',
    link: 'https://runewarkdining.com/market/',
  },
  {
    text: 'Starbucks',
    link: 'https://runewarkdining.com/starbucks/',
  },
  {
    text: 'Stonsby Commons',
    link: 'https://runewarkdining.com/stonsby-commons/',
  },
  {
    text: 'JBJ Soul Kitchen',
    link: 'https://runewarkdining.com/jbjsoulkitchen/',
  },
];

const openLink = url => {
  if (Platform.OS === 'android') {
    Linking.openURL(url);
    return;
  }

  SafariView.isAvailable()
    .then(
      SafariView.show({
        url,
        tintColor: '#ed4545',
        barTintColor: 'white',
        readerMode: true,
      }),
    )
    .catch(error => {
      Linking.openURL(url)
        .then(() => {
          console.log('Link opened successfully');
        })
        .catch(() => {
          console.log('Link failed to open.');
          Alert.alert('Unable to open link.');
        });
    });
};

const NewarkFoodScreen = () => {
  const {colors} = useTheme();

  return (
    <SafeAreaView style={{flex: 1, top: 20}}>
      <FocusAwareStatusBar />
      <TopText mainTitle={'Food'} />
      <ScrollView>
        <Container>
          {diningLocations.map((diningLocation, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => openLink(diningLocation.link)}
            >
              <Row>
                <BulletPoint />
                <LinkText style={{color: colors.text}}>
                  {diningLocation.text}
                </LinkText>
                <Chevron />
              </Row>
              {index < diningLocations.length - 1 && (
                <Line style={{marginLeft: 32}} />
              )}
            </TouchableOpacity>
          ))}
        </Container>
      </ScrollView>
    </SafeAreaView>
  );
};

export default NewarkFoodScreen;
