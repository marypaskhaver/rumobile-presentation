import React from 'react';

import {View} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {
  SFProDisplayBold,
  SFProTextRegular,
  SFProTextSemibold,
} from '../../constants/fonts';
import {GRAY_4} from '../../constants/colors';
import {Text} from 'react-native';
import {Line, Row} from '../../shared-components';

const NutritionLabelSubsection = ({
  nutrientName,
  nutrientGrams,
  nutrientPercentDailyValue,
}) => {
  const {colors} = useTheme();
  return (
    <Row
      style={{
        justifyContent: 'space-between',
        flex: 1,
        paddingHorizontal: 16,
        paddingLeft: 28,
      }}
    >
      <Text style={{fontFamily: SFProTextRegular, color: colors.text}}>
        {nutrientName} {nutrientGrams}
      </Text>

      <Text style={{fontFamily: SFProTextSemibold, color: colors.text}}>
        {nutrientPercentDailyValue}
      </Text>
    </Row>
  );
};

const NutritionLabelSection = ({
  nutrientName,
  nutrientGrams,
  nutrientPercentDailyValue,
}) => {
  const {colors} = useTheme();

  return (
    <Row
      style={{
        justifyContent: 'space-between',
        flex: 1,
        paddingHorizontal: 16,
        paddingLeft: 16,
        paddingTop: 8,
      }}
    >
      <Text style={{fontFamily: SFProTextRegular, color: colors.text}}>
        <Text style={{fontFamily: SFProTextSemibold, color: colors.text}}>
          {nutrientName}
        </Text>{' '}
        {nutrientGrams}
      </Text>
      <Text style={{fontFamily: SFProTextSemibold, color: colors.text}}>
        {nutrientPercentDailyValue}
      </Text>
    </Row>
  );
};

const NutritionLabel = ({foodDetails}) => {
  const nutrientAbbreviationToSubtypes = {
    totalFat: ['satFat', 'transFat'],
    cholesterol: [],
    sodium: [],
    totCarb: ['dietaryFiber', 'sugars'],
    protein: [],
  };

  const nutrientAbbreviationToNameToDisplay = {
    totalFat: 'Total Fat',
    satFat: 'Saturated Fat',
    transFat: 'Trans Fat',
    cholesterol: 'Cholesterol',
    sodium: 'Sodium',
    totCarb: 'Total Carbohydrate',
    dietaryFiber: 'Dietary Fiber',
    sugars: 'Sugars',
    protein: 'Protein',
  };

  const {colors} = useTheme();

  return (
    <View
      style={{
        borderColor: colors.text,
        borderWidth: 1,
        borderRadius: 8,
        paddingBottom: 12,
      }}
    >
      <Text
        style={{
          fontFamily: SFProDisplayBold,
          fontSize: 24,
          paddingLeft: 16,
          paddingVertical: 12,
          color: colors.text,
        }}
      >
        Nutrition Facts
      </Text>
      <Line
        style={{
          borderColor: colors.text,
          borderWidth: 4,
          opacity: 1,
        }}
      />
      <Row
        style={{
          justifyContent: 'space-between',
          flex: 1,
          paddingHorizontal: 16,
          paddingVertical: 12,
        }}
      >
        <Text
          style={{
            color: GRAY_4,
            fontFamily: SFProTextRegular,
            fontSize: 16,
          }}
        >
          Amount per serving
        </Text>
        <Text
          style={{
            color: GRAY_4,
            fontFamily: SFProTextRegular,
            fontSize: 16,
          }}
        >
          % Daily Value *
        </Text>
      </Row>
      <Line
        style={{
          borderColor: colors.text,
          borderWidth: 1,
          opacity: 1,
          marginBottom: 4,
        }}
      />
      {Object.keys(nutrientAbbreviationToSubtypes).map(nutrientAbbreviation => {
        return (
          <View key={nutrientAbbreviation}>
            <NutritionLabelSection
              nutrientName={
                nutrientAbbreviationToNameToDisplay[nutrientAbbreviation]
              }
              nutrientGrams={
                foodDetails['quantitytable'][nutrientAbbreviation]
                  ? foodDetails['quantitytable'][nutrientAbbreviation][
                      'quantity'
                    ]
                  : '--'
              }
              nutrientPercentDailyValue={
                foodDetails['quantitytable'][nutrientAbbreviation]
                  ? foodDetails['quantitytable'][nutrientAbbreviation][
                      'dailyValue'
                    ]
                  : '--'
              }
            />
            {nutrientAbbreviationToSubtypes[nutrientAbbreviation].map(
              nutrientAbbreviationOfSubtype => {
                return (
                  <NutritionLabelSubsection
                    key={nutrientAbbreviationOfSubtype}
                    nutrientName={
                      nutrientAbbreviationToNameToDisplay[
                        nutrientAbbreviationOfSubtype
                      ]
                    }
                    nutrientGrams={
                      foodDetails['quantitytable'][
                        nutrientAbbreviationOfSubtype
                      ]
                        ? foodDetails['quantitytable'][
                            nutrientAbbreviationOfSubtype
                          ]['quantity']
                        : '--'
                    }
                    nutrientPercentDailyValue={
                      foodDetails['quantitytable'][
                        nutrientAbbreviationOfSubtype
                      ]
                        ? foodDetails['quantitytable'][
                            nutrientAbbreviationOfSubtype
                          ]['dailyValue']
                        : '--'
                    }
                  />
                );
              },
            )}
          </View>
        );
      })}
    </View>
  );
};

export default NutritionLabel;
