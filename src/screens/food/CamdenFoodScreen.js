import {useTheme} from '@react-navigation/native';
import React from 'react';
import {Image, View, Text, Linking, Alert, Platform} from 'react-native';
import {GRAY_4, GRAY_3} from '../../constants/colors';
import {SFProTextRegular} from '../../constants/fonts';
import {ScrollView, TouchableOpacity} from 'react-native';
import TopText from '../buses/TopText';
import {SafeAreaView} from 'react-native-safe-area-context';
import {FocusAwareStatusBar} from '../../shared-components';
import SafariView from 'react-native-safari-view';

const openLink = () => {
  const url = 'https://dineoncampus.com/rutgerscamden';
  if (Platform.OS === 'android') {
    Linking.openURL(url);
    return;
  }

  SafariView.isAvailable()
    .then(
      SafariView.show({
        url,
        tintColor: '#ed4545',
        barTintColor: 'white',
        readerMode: true,
      }),
    )
    .catch(error => {
      Linking.openURL(url)
        .then(() => {
          console.log('Link opened successfully');
        })
        .catch(() => {
          console.log('Link failed to open.');
          Alert.alert('Unable to open link.');
        });
    });
};

const CamdenFoodScreen = () => {
  const {dark} = useTheme();

  return (
    <SafeAreaView style={{flex: 1, top: 20}}>
      <FocusAwareStatusBar />
      <TopText mainTitle={'Food'} />
      <ScrollView>
        <View style={{marginLeft: 16, marginRight: 16, alignItems: 'center'}}>
          <Image
            source={require('../../assets/images/dining_halls/food/blankstate_nofood.png')}
            style={{marginTop: 32, width: 400, height: 350}}
          />
          <Text
            style={{
              fontFamily: SFProTextRegular,
              color: GRAY_4,
              fontSize: 16,
              marginTop: 40,
              marginBottom: 4,
              flexWrap: 'wrap',
            }}
          >
            Rutgers Dining Services Official Website
          </Text>

          <TouchableOpacity
            style={{
              marginTop: 20,
              marginBottom: 8,
              borderWidth: 1,
              borderRadius: 4,
              borderColor: dark ? GRAY_3 : GRAY_4,
            }}
            onPress={() => openLink()}
          >
            <Text
              style={{
                fontFamily: SFProTextRegular,
                color: dark ? GRAY_3 : GRAY_4,
                paddingVertical: 4,
                paddingHorizontal: 10,
                textAlign: 'center',
                fontSize: 18,
              }}
            >
              Visit Site
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default CamdenFoodScreen;
