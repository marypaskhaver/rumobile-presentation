import {useTheme} from '@react-navigation/native';
import React from 'react';
import {useState} from 'react';
import {RefreshControl} from 'react-native';
import {useDispatch} from 'react-redux';
import {requestNutritionInfo} from '../../redux/actions/foodActions';

export default NutritionItemsRefreshControl = () => {
  const [refreshing, setRefreshing] = useState(false);
  const {dark} = useTheme();

  const dispatch = useDispatch();

  const refresh = () => {
    setRefreshing(true);
    dispatch(requestNutritionInfo());
    setRefreshing(false);
  };

  return (
    <RefreshControl
      tintColor={dark ? 'white' : 'gray'}
      refreshing={refreshing}
      onRefresh={refresh}
    />
  );
};
