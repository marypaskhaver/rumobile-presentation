import welcomeScreensReducer, {
  initialState,
  selectPassed,
  selectLoaded,
} from '../welcomeScreensReducer';

describe('welcomeScreensReducer', () => {
  describe('initial state', () => {
    it('starts off not passed', () => {
      const action = {};
      const reducer = welcomeScreensReducer(initialState, action);

      expect(reducer.passed).toEqual(false);
    });

    it('starts off not loaded', () => {
      const action = {};
      const reducer = welcomeScreensReducer(initialState, action);

      expect(reducer.loaded).toEqual(false);
    });
  });

  describe('modified state', () => {
    it('passes welcome screen', () => {
      const action = {type: 'PASSED_WELCOME_SCREEN', payload: true};
      const reducer = welcomeScreensReducer(initialState, action);

      expect(reducer.passed).toEqual(true);
    });

    it('loads welcome screen', () => {
      const action = {type: 'LOADED', payload: true};
      const reducer = welcomeScreensReducer(initialState, action);

      expect(reducer.loaded).toEqual(true);
    });
  });

  describe('select if passed or not', () => {
    it('returns boolean representing whether the welcome screens were passed or not', () => {
      const state = {
        welcomeScreens: {passed: true},
      };

      expect(selectPassed(state)).toEqual(true);
    });

    it('returns boolean representing whether the welcome screens were loaded or not', () => {
      const state = {
        welcomeScreens: {loaded: true},
      };

      expect(selectLoaded(state)).toEqual(true);
    });
  });
});
