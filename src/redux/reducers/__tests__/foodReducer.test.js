import foodReducer, {
  INITIAL_STATE,
  menuItemsForDiningHallNameAtTimeOfDay,
  selectMenuItemByDiningHallName,
  selectMenuItems,
  selectMenuItemsFetchStatus,
  selectNutritionItems,
  selectNutritionItemsFetchStatus,
  isDiningHallOpen,
} from '../foodReducer';

describe('foodReducer', () => {
  describe('initial state', () => {
    it('stores no menu items', () => {
      const action = {};
      const reducer = foodReducer(INITIAL_STATE, action);

      expect(reducer.menuItems).toEqual({});
    });

    it('holds status of fetch request for menu items', () => {
      const action = {};
      const reducer = foodReducer(INITIAL_STATE, action);

      expect(reducer.menuItemsFetchStatus).toEqual('initializing');
    });

    it('holds status of fetch request for nutrition items', () => {
      const action = {};
      const reducer = foodReducer(INITIAL_STATE, action);

      expect(reducer.nutritionItemsFetchStatus).toEqual('initializing');
    });
  });

  describe('fetch status', () => {
    describe('when initializing request for menu items', () => {
      it('reflects pending fetch status', () => {
        const action = {type: 'START_MENU_ITEMS_FETCH'};
        const reducer = foodReducer(INITIAL_STATE, action);

        expect(reducer).toEqual({
          menuItemsFetchStatus: 'pending',
          menuItems: {},
          hallIsOpen: false,
          nutritionItems: {},
          nutritionItemsFetchStatus: 'initializing',
        });
      });

      describe('when initializing request for nutrition items', () => {
        it('reflects pending fetch status', () => {
          const action = {type: 'START_NUTRITION_ITEMS_FETCH'};
          const reducer = foodReducer(INITIAL_STATE, action);

          expect(reducer).toEqual({
            menuItemsFetchStatus: 'initializing',
            menuItems: {},
            hallIsOpen: false,
            nutritionItems: {},
            nutritionItemsFetchStatus: 'pending',
          });
        });
      });
    });

    describe('when request for menu items successfully completes', () => {
      it('reflects completed fetch status', () => {
        const action = {type: 'COMPLETE_MENU_ITEMS_FETCH'};
        const reducer = foodReducer(INITIAL_STATE, action);

        expect(reducer).toEqual({
          menuItemsFetchStatus: 'success',
          menuItems: {},
          hallIsOpen: false,
          nutritionItems: {},
          nutritionItemsFetchStatus: 'initializing',
        });
      });
    });

    describe('when request for nutrition items successfully completes', () => {
      it('reflects completed fetch status', () => {
        const action = {type: 'COMPLETE_NUTRITION_ITEMS_FETCH'};
        const reducer = foodReducer(INITIAL_STATE, action);

        expect(reducer).toEqual({
          menuItemsFetchStatus: 'initializing',
          menuItems: {},
          hallIsOpen: false,
          nutritionItems: {},
          nutritionItemsFetchStatus: 'success',
        });
      });
    });

    describe('when request for menu items fails', () => {
      it('reflects failed fetch status', () => {
        const action = {type: 'FAIL_MENU_ITEMS_FETCH'};
        const reducer = foodReducer(INITIAL_STATE, action);

        expect(reducer).toEqual({
          menuItemsFetchStatus: 'failure',
          menuItems: {},
          hallIsOpen: false,
          nutritionItems: {},
          nutritionItemsFetchStatus: 'initializing',
        });
      });
    });

    describe('when request for nutrition items fails', () => {
      it('reflects failed fetch status', () => {
        const action = {type: 'FAIL_NUTRITION_ITEMS_FETCH'};
        const reducer = foodReducer(INITIAL_STATE, action);

        expect(reducer).toEqual({
          menuItemsFetchStatus: 'initializing',
          menuItems: {},
          hallIsOpen: false,
          nutritionItems: {},
          nutritionItemsFetchStatus: 'failure',
        });
      });
    });
  });

  describe('menu items', () => {
    it('receives menu items and adds a property to reflect dining hall open status', () => {
      const action = {
        type: 'RECEIVE_MENU_ITEMS',
        payload: {
          browerCommons: {
            diningHallName: 'browerCommons',
            breakfast: {},
            lunch: {},
            dinner: {},
            takeout: {},
            diningDate: '8/29/2022',
            dataFetchDate: '2022-08-30T13:00:50.711Z',
          },
        },
      };

      const reducer = foodReducer(INITIAL_STATE, action);

      expect(reducer.menuItems).toEqual({
        browerCommons: {
          diningHallName: 'browerCommons',
          breakfast: {},
          lunch: {},
          dinner: {},
          takeout: {},
          hallIsOpen: expect.any(Boolean),
          diningDate: '8/29/2022',
          dataFetchDate: '2022-08-30T13:00:50.711Z',
        },
      });
    });
  });

  describe('nutrition items', () => {
    it('receives nutrition items', () => {
      const action = {
        type: 'RECEIVE_NUTRITION_ITEMS',
        payload: {
          'label.asp?RecNumAndPort=019093%2A3': {
            calories: '161',
            ingredients:
              'frz beef hormel sliced brisket, sauce kansas city bbq (dressing ketchup (tomato concentrate from red ripe tomatoes, distilled vinegar, high fructose corn syrup, corn syrup, salt, spice, onion powder, natural flavoring), water fresh, molasses, dark brown sugar, oil wesson canola 4/1gal, gluten free tamari soy sauce, sauce worcestershire (distilled white vinegar, molasses, sugar, water, salt, onions, anchovies, garlic, cloves, tamarind extract, natural flavorings, chili pepper extract.), prd garlic peeled fresh gal, kosher salt, chili powder (chili pepper, spices, salt, silicon dioxide (to make free flowing), and garlic.), spanish paprika, ground mustard powder, black pepper, crushed red pepper flakes, bay leaf, ground cloves, allspice)',
            name: 'Beef Brisket',
            quantitytable: {},
            servingSize: '3 oz',
          },
        },
      };

      const reducer = foodReducer(INITIAL_STATE, action);

      expect(reducer.nutritionItems).toEqual({
        'label.asp?RecNumAndPort=019093%2A3': {
          calories: '161',
          ingredients:
            'frz beef hormel sliced brisket, sauce kansas city bbq (dressing ketchup (tomato concentrate from red ripe tomatoes, distilled vinegar, high fructose corn syrup, corn syrup, salt, spice, onion powder, natural flavoring), water fresh, molasses, dark brown sugar, oil wesson canola 4/1gal, gluten free tamari soy sauce, sauce worcestershire (distilled white vinegar, molasses, sugar, water, salt, onions, anchovies, garlic, cloves, tamarind extract, natural flavorings, chili pepper extract.), prd garlic peeled fresh gal, kosher salt, chili powder (chili pepper, spices, salt, silicon dioxide (to make free flowing), and garlic.), spanish paprika, ground mustard powder, black pepper, crushed red pepper flakes, bay leaf, ground cloves, allspice)',
          name: 'Beef Brisket',
          quantitytable: {},
          servingSize: '3 oz',
        },
      });
    });
  });

  describe('selectors', () => {
    describe('selectMenuItems', () => {
      it('returns menu items stored in state', () => {
        const appState = {
          food: {
            menuItems: {
              browerCommons: {
                diningHallName: 'browerCommons',
                breakfast: {},
                lunch: {},
                dinner: {},
                takeout: {},
                hallIsOpen: false,
                diningDate: '8/29/2022',
                dataFetchDate: '2022-08-30T13:00:50.711Z',
              },
            },
          },
        };

        const result = selectMenuItems(appState);

        expect(result).toEqual({
          browerCommons: {
            diningHallName: 'browerCommons',
            breakfast: {},
            lunch: {},
            dinner: {},
            takeout: {},
            hallIsOpen: false,
            diningDate: '8/29/2022',
            dataFetchDate: '2022-08-30T13:00:50.711Z',
          },
        });
      });
    });

    describe('selectNutritionItems', () => {
      it('returns nutrition items stored in state', () => {
        const appState = {
          food: {
            nutritionItems: {
              'label.asp?RecNumAndPort=019093%2A3': {
                calories: '161',
                ingredients:
                  'frz beef hormel sliced brisket, sauce kansas city bbq (dressing ketchup (tomato concentrate from red ripe tomatoes, distilled vinegar, high fructose corn syrup, corn syrup, salt, spice, onion powder, natural flavoring), water fresh, molasses, dark brown sugar, oil wesson canola 4/1gal, gluten free tamari soy sauce, sauce worcestershire (distilled white vinegar, molasses, sugar, water, salt, onions, anchovies, garlic, cloves, tamarind extract, natural flavorings, chili pepper extract.), prd garlic peeled fresh gal, kosher salt, chili powder (chili pepper, spices, salt, silicon dioxide (to make free flowing), and garlic.), spanish paprika, ground mustard powder, black pepper, crushed red pepper flakes, bay leaf, ground cloves, allspice)',
                name: 'Beef Brisket',
                quantitytable: {},
                servingSize: '3 oz',
              },
            },
          },
        };

        const result = selectNutritionItems(appState);

        expect(result).toEqual({
          'label.asp?RecNumAndPort=019093%2A3': {
            calories: '161',
            ingredients:
              'frz beef hormel sliced brisket, sauce kansas city bbq (dressing ketchup (tomato concentrate from red ripe tomatoes, distilled vinegar, high fructose corn syrup, corn syrup, salt, spice, onion powder, natural flavoring), water fresh, molasses, dark brown sugar, oil wesson canola 4/1gal, gluten free tamari soy sauce, sauce worcestershire (distilled white vinegar, molasses, sugar, water, salt, onions, anchovies, garlic, cloves, tamarind extract, natural flavorings, chili pepper extract.), prd garlic peeled fresh gal, kosher salt, chili powder (chili pepper, spices, salt, silicon dioxide (to make free flowing), and garlic.), spanish paprika, ground mustard powder, black pepper, crushed red pepper flakes, bay leaf, ground cloves, allspice)',
            name: 'Beef Brisket',
            quantitytable: {},
            servingSize: '3 oz',
          },
        });
      });
    });

    describe('selectNutritionItemsFetchStatus', () => {
      it('returns status of selectNutritionItems fetch request', () => {
        const appState = {
          food: {
            nutritionItemsFetchStatus: 'success',
          },
        };

        const result = selectNutritionItemsFetchStatus(appState);

        expect(result).toBe('success');
      });
    });

    describe('selectMenuItemByDiningHallName', () => {
      it('returns menu items in the given dining hall', () => {
        const appState = {
          food: {
            menuItems: {
              browerCommons: {
                diningHallName: 'browerCommons',
                breakfast: {},
                lunch: {},
                dinner: {},
                takeout: {},
                hallIsOpen: false,
                diningDate: '8/29/2022',
                dataFetchDate: '2022-08-30T13:00:50.711Z',
              },
            },
          },
        };

        const result =
          selectMenuItemByDiningHallName('browerCommons')(appState);

        expect(result).toEqual({
          diningHallName: 'browerCommons',
          breakfast: {},
          lunch: {},
          dinner: {},
          takeout: {},
          hallIsOpen: false,
          diningDate: '8/29/2022',
          dataFetchDate: '2022-08-30T13:00:50.711Z',
        });
      });

      describe('selectMenuItemsFetchStatus', () => {
        it('returns status of selectMenuItems fetch request', () => {
          const appState = {
            food: {
              menuItemsFetchStatus: 'success',
            },
          };

          const result = selectMenuItemsFetchStatus(appState);

          expect(result).toBe('success');
        });
      });
    });

    describe('menuItemsForDiningHallNameAtTimeOfDay', () => {
      it('returns menu items in the given dining hall at the given time of day', () => {
        const appState = {
          food: {
            menuItems: {
              browerCommons: {
                diningHallName: 'browerCommons',
                breakfast: {
                  'BREAKFAST MEATS': [
                    {
                      name: 'Grilled Turkey Sausage Links',
                      portionSize: '2 each',
                      calories: '111',
                      nutritionPath: 'label.asp?RecNumAndPort=113000%2A2',
                      nutritionId: '113000',
                      nutritionPortionMultiplier: '%2A2',
                    },
                  ],
                },
                lunch: {},
                dinner: {},
                takeout: {},
                hallIsOpen: true,
                diningDate: '8/29/2022',
                dataFetchDate: '2022-08-30T13:00:50.711Z',
              },
            },
          },
        };

        const result = menuItemsForDiningHallNameAtTimeOfDay(
          'browerCommons',
          'breakfast',
        )(appState);

        expect(result).toEqual({
          'BREAKFAST MEATS': [
            {
              name: 'Grilled Turkey Sausage Links',
              portionSize: '2 each',
              calories: '111',
              nutritionPath: 'label.asp?RecNumAndPort=113000%2A2',
              nutritionId: '113000',
              nutritionPortionMultiplier: '%2A2',
            },
          ],
        });
      });

      it('returns nothing if dining hall is closed', () => {
        const appState = {
          food: {
            menuItems: {
              browerCommons: {
                diningHallName: 'browerCommons',
                breakfast: {
                  'BREAKFAST MEATS': [
                    {
                      name: 'Grilled Turkey Sausage Links',
                      portionSize: '2 each',
                      calories: '111',
                      nutritionPath: 'label.asp?RecNumAndPort=113000%2A2',
                      nutritionId: '113000',
                      nutritionPortionMultiplier: '%2A2',
                    },
                  ],
                },
                lunch: {},
                dinner: {},
                takeout: {},
                hallIsOpen: false,
                diningDate: '8/29/2022',
                dataFetchDate: '2022-08-30T13:00:50.711Z',
              },
            },
          },
        };

        const result = menuItemsForDiningHallNameAtTimeOfDay(
          'browerCommons',
          'breakfast',
        )(appState);

        expect(result).toEqual([]);
      });
    });
  });

  it('uses initial state when not provided with one', () => {
    const action = {};
    const reducer = foodReducer(undefined, action);

    expect(reducer).toEqual(INITIAL_STATE);
  });
});

describe('isDiningHallOpen', () => {
  afterEach(() => {
    jest.useRealTimers();
  });

  describe('when it is a weekday between Monday and Friday', () => {
    describe('when it is between 7AM and 11PM', () => {
      it('confirms dining hall is open', () => {
        const monday = new Date('December 12, 2022 13:25:00');
        jest.useFakeTimers().setSystemTime(monday);
        expect(isDiningHallOpen()).toBe(true);
      });
    });
  });

  describe('when it is a Friday', () => {
    describe('when it is between 7AM and 9PM', () => {
      it('confirms dining hall is open', () => {
        const friday = new Date('December 16, 2022 13:25:00');
        jest.useFakeTimers().setSystemTime(friday);
        expect(isDiningHallOpen()).toBe(true);
      });
    });
  });

  describe('when it is a weekend day', () => {
    describe('when it is between 9:30AM and 10AM', () => {
      it('confirms dining hall is open', () => {
        const sunday = new Date('December 18, 2022 09:38:00');
        jest.useFakeTimers().setSystemTime(sunday);
        expect(isDiningHallOpen()).toBe(true);
      });
    });
  });

  describe('when it is winter break', () => {
    it('confirms dining hall is closed', () => {
      const saturday = new Date('December 31, 2022 12:00:00');
      jest.useFakeTimers().setSystemTime(saturday);
      expect(isDiningHallOpen()).toBe(false);
    });
  });
});
