import themesReducer, {
  INITIAL_STATE,
  selectChosenOverrideThemeID,
  selectCurrentAppTheme,
  getThemeFromId,
} from '../themesReducer';
import {DefaultTheme} from '@react-navigation/native';
import {CustomDarkTheme} from '../../../constants/appThemes';

describe('themesReducer', () => {
  describe('initial state', () => {
    it('has no user override theme selected', () => {
      const action = {};
      const reducer = themesReducer(INITIAL_STATE, action);

      expect(reducer.chosenOverrideThemeID).toBeNull();
    });

    it('has a light theme', () => {
      const action = {};
      const reducer = themesReducer(INITIAL_STATE, action);
      const lightTheme = DefaultTheme;
      expect(reducer.currentAppTheme).toEqual(lightTheme);
    });
  });

  describe('handles toggle of automatic dark mode', () => {
    it("saves the user's selected theme", () => {
      const action = {
        type: 'UPDATE_CHOSEN_OVERRIDE_THEME_ID',
        payload: 2,
      };
      const reducer = themesReducer(INITIAL_STATE, action);
      expect(reducer.chosenOverrideThemeID).toEqual(2);
    });

    it('updates the currentAppTheme if user selects theme', () => {
      const action = {
        type: 'UPDATE_CHOSEN_OVERRIDE_THEME_ID',
        payload: 2,
      };
      const reducer = themesReducer(INITIAL_STATE, action);
      expect(reducer.currentAppTheme).toEqual(CustomDarkTheme);
    });

    describe("updates the currentAppTheme to phone's theme if user enables automatic dark mode", () => {
      it('switches to light mode by default', () => {
        const action = {type: 'UPDATE_CHOSEN_OVERRIDE_THEME_ID', payload: null};
        const lightTheme = DefaultTheme;
        const reducer = themesReducer(INITIAL_STATE, action);

        expect(reducer.currentAppTheme).toEqual(lightTheme);
      });
    });
  });

  describe('handles update of phone theme', () => {
    const action = {
      type: 'UPDATE_CURRENT_PHONE_THEME',
      payload: 'Dark',
    };
    const reducer = themesReducer(INITIAL_STATE, action);
    expect(reducer.currentPhoneTheme).toBe('Dark');
  });

  describe('getThemeFromId', () => {
    it('returns light theme given its id', () => {
      const lightTheme = DefaultTheme;
      const theme = getThemeFromId(1)(INITIAL_STATE);

      expect(theme).toEqual(lightTheme);
    });

    it('returns dark theme given its id', () => {
      const theme = getThemeFromId(2)(INITIAL_STATE);
      expect(theme).toEqual(CustomDarkTheme);
    });

    it("handles if user has automatic dark mode enabled (hasn't chosen an override theme)", () => {
      const lightTheme = DefaultTheme;
      const theme = getThemeFromId(null)(INITIAL_STATE);
      expect(theme).toEqual(lightTheme);
    });

    it('handles invalid theme ids', () => {
      const lightTheme = DefaultTheme;
      const theme = getThemeFromId(999)(INITIAL_STATE);
      expect(theme).toEqual(lightTheme);
    });
  });

  describe('selectors', () => {
    describe('selectChosenOverrideThemeID', () => {
      it("returns the user's chosen app theme", () => {
        const appState = {
          themes: {
            chosenOverrideThemeID: 1,
          },
        };

        const result = selectChosenOverrideThemeID(appState);
        expect(result).toEqual(1);
      });
    });

    describe('selectChosenAppTheme', () => {
      it("returns the user's current app theme", () => {
        const appState = {
          themes: {
            currentAppTheme: 0,
          },
        };

        const result = selectCurrentAppTheme(appState);
        expect(result).toEqual(0);
      });
    });
  });
});
