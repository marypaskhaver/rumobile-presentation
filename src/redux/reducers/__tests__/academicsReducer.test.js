import academicsReducer, {
  initialState,
  isFavoriteSection,
  selectClassCategoriesFetchStatus,
  selectFavoriteSections,
  selectLevels,
} from '../academicsReducer';

describe('academicsReducer', () => {
  describe('initial state', () => {
    it('starts off with no class categories', () => {
      const action = {};
      const reducer = academicsReducer(initialState, action);

      expect(reducer.classCategories).toEqual([]);
    });

    it('starts off with no classes', () => {
      const action = {};
      const reducer = academicsReducer(initialState, action);

      expect(reducer.classes).toEqual([]);
    });

    it('assumes starting campus of New Brunswick', () => {
      const action = {};
      const reducer = academicsReducer(initialState, action);

      expect(reducer.campuses).toEqual(['NB']);
    });

    it('assumes starting semester of Fall 2022', () => {
      const action = {};
      const reducer = academicsReducer(initialState, action);

      expect(reducer.semester).toEqual('92022');
    });

    it('assumes starting education level that includes undergraduate and graduate courses', () => {
      const action = {};
      const reducer = academicsReducer(initialState, action);

      expect(reducer.levels).toEqual(['U', 'G']);
    });
  });

  describe('actions', () => {
    describe('ADD_FAVORITE_SECTION', () => {
      it("updates and stores user's newly-added section", () => {
        const action = {
          type: 'ADD_FAVORITE_SECTION',
          payload: {name: 'Calc II', index: '01234'},
        };
        const reducer = academicsReducer(initialState, action);

        expect(reducer.favoriteSections).toEqual({
          '01234': {name: 'Calc II', index: '01234'},
        });
      });
    });

    describe('REMOVE_FAVORITE_SECTION', () => {
      it('removes the given section from favorites', () => {
        const appState = {
          academics: {
            favoriteSections: {'01234': {name: 'Calc II', index: '01234'}},
          },
        };

        const action = {
          type: 'REMOVE_FAVORITE_SECTION',
          payload: {name: 'Calc II', index: '01234'},
        };

        const reducer = academicsReducer(appState, action);
        expect(reducer.favoriteSections).toEqual({});
      });
    });

    describe('EDIT_SECTION', () => {
      it('updates the section at the given index with the new given title and meeting times', () => {
        const reducerState = {
          favoriteSections: {
            '01234': {title: 'Calc II', index: '01234', meetingTimes: []},
          },
        };

        const action = {
          type: 'EDIT_SECTION',
          payload: {
            sectionIndex: '01234',
            newTitle: 'Calc 152',
            newMeetingTimes: [{campusName: 'LIVINGSTON'}],
          },
        };

        const reducer = academicsReducer(reducerState, action);
        expect(reducer.favoriteSections).toEqual({
          '01234': {
            title: 'Calc 152',
            index: '01234',
            meetingTimes: [{campusName: 'LIVINGSTON'}],
          },
        });
      });
    });

    describe('START_CLASS_CATEGORIES_FETCH', () => {
      it('starts the fetch request for class categories', () => {
        const action = {
          type: 'START_CLASS_CATEGORIES_FETCH',
        };
        const reducer = academicsReducer(initialState, action);

        expect(reducer.fetchStatus).toEqual('pending');
      });
    });

    describe('COMPLETE_CLASS_CATEGORIES_FETCH', () => {
      it('successfully completes the fetch request for class categories', () => {
        const action = {
          type: 'COMPLETE_CLASS_CATEGORIES_FETCH',
        };
        const reducer = academicsReducer(initialState, action);

        expect(reducer.fetchStatus).toEqual('success');
      });
    });

    describe('FAIL_CLASS_CATEGORIES_FETCH', () => {
      it('fails the fetch request for class categories', () => {
        const action = {
          type: 'FAIL_CLASS_CATEGORIES_FETCH',
        };
        const reducer = academicsReducer(initialState, action);

        expect(reducer.fetchStatus).toEqual('failure');
      });
    });

    describe('RECEIVE_CLASS_CATEGORIES', () => {
      it('receives and stores class categories', () => {
        const classCategories = [
          {
            name: 'Accounting',
            code: '010',
          },

          {
            name: 'Administrative Studies',
            code: '011',
          },
        ];

        const action = {
          type: 'RECEIVE_CLASS_CATEGORIES',
          payload: classCategories,
        };
        const reducer = academicsReducer(initialState, action);

        expect(reducer.classCategories).toEqual(classCategories);
      });
    });

    describe('ADD_CAMPUS', () => {
      it("updates and stores user's newly-added campus", () => {
        const action = {type: 'ADD_CAMPUS', payload: 'CM'};
        const reducer = academicsReducer(initialState, action);

        expect(reducer.campuses).toEqual(['NB', 'CM']);
      });
    });

    describe('REMOVE_CAMPUS', () => {
      it("removes user's campus", () => {
        const action = {type: 'REMOVE_CAMPUS', payload: 'CM'};
        const testState = {
          ...initialState,
          campuses: ['NB', 'CM'],
        };
        const reducer = academicsReducer(testState, action);

        expect(reducer.campuses).toEqual(['NB']);
      });
    });

    describe('ADD_LEVEL', () => {
      it("updates and stores user's newly-added academic level", () => {
        const action = {type: 'ADD_LEVEL', payload: 'PhD'};
        const reducer = academicsReducer(initialState, action);

        expect(reducer.levels).toEqual(['U', 'G', 'PhD']);
      });
    });

    describe('REMOVE_LEVEL', () => {
      it("removes user's academic level", () => {
        const action = {type: 'REMOVE_LEVEL', payload: 'G'};
        const reducer = academicsReducer(initialState, action);

        expect(reducer.levels).toEqual(['U']);
      });
    });

    describe('SET_LEVELS', () => {
      it('updates education levels in store to given levels', () => {
        const action = {
          type: 'SET_LEVELS',
          payload: ['page', 'squire', 'knight'],
        };
        const reducer = academicsReducer(initialState, action);
        expect(reducer.levels).toEqual(['page', 'squire', 'knight']);
      });
    });

    describe('SET_CAMPUSES', () => {
      it('updates campuses in store to given campuses', () => {
        const action = {
          type: 'SET_CAMPUSES',
          payload: ['Hogwarts', 'Durmstrang', 'Beauxbatons'],
        };
        const reducer = academicsReducer(initialState, action);
        expect(reducer.campuses).toEqual([
          'Hogwarts',
          'Durmstrang',
          'Beauxbatons',
        ]);
      });
    });

    describe('UPDATE_SEMESTER', () => {
      it("stores the user's newly-selected semester", () => {
        const action = {type: 'UPDATE_SEMESTER', payload: '72022'};
        const reducer = academicsReducer(initialState, action);
        expect(reducer.semester).toEqual('72022');
      });
    });

    describe('RECEIVE_CLASSES', () => {
      it('receives classes with section information', () => {
        const academicClass = {
          courseNumber: '272',
          subject: '010',
          campusCode: 'NB',
          openSections: 3,
        };

        const action = {type: 'RECEIVE_CLASSES', payload: [academicClass]};
        const reducer = academicsReducer(initialState, action);

        expect(reducer.classes).toEqual([academicClass]);
      });
    });

    describe('invalid action', () => {
      it('preserves state', () => {
        const action = {type: 'Expelliarmus!'};

        const state = {
          classCategories: [],
          semester: '12022',
          campuses: ['NB'],
          levels: ['U', 'G'],
          classes: [],
          favoriteSections: {},
        };

        const reducer = academicsReducer(state, action);

        expect(reducer).toEqual(state);
      });
    });
  });

  describe('selectors', () => {
    describe('selectFavoriteSections', () => {
      it('returns favorite sections stored in state', () => {
        const appState = {
          academics: {
            favoriteSections: {'01234': {name: 'Calc II', index: '01234'}},
          },
        };

        expect(selectFavoriteSections(appState)).toEqual({
          '01234': {name: 'Calc II', index: '01234'},
        });
      });
    });

    describe('isFavoriteSection', () => {
      it('says if given section is a favorite section in state', () => {
        const appState = {
          academics: {
            favoriteSections: {'01234': {name: 'Calc II', index: '01234'}},
          },
        };

        expect(
          isFavoriteSection({name: 'Calc II', index: '01234'})(appState),
        ).toEqual(true);
      });

      it('says if given section is not a favorite section in state', () => {
        const appState = {
          academics: {
            favoriteSections: {43210: {name: 'Calc II', index: '43210'}},
          },
        };

        expect(
          isFavoriteSection({name: 'Calc II', index: '01234'})(appState),
        ).toEqual(false);
      });
    });

    describe('selectLevels', () => {
      it('returns the levels in store', () => {
        const appState = {
          academics: {
            levels: ['U'],
          },
        };

        const result = selectLevels(appState);
        expect(result).toEqual(['U']);
      });
    });

    describe('selectClassCategoriesFetchStatus', () => {
      it('returns the status of the fetch to select class categories', () => {
        const appState = {
          academics: {
            fetchStatus: 'pending',
          },
        };

        const result = selectClassCategoriesFetchStatus(appState);
        expect(result).toEqual('pending');
      });
    });
  });
});
