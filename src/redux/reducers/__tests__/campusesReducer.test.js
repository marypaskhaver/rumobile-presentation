import campusesReducer, {
  getCampusNameFromID,
  INITIAL_STATE,
  selectChosenCampusID,
} from '../campusesReducer';

describe('campusesReducer', () => {
  describe('initial state', () => {
    it('stores a campus id of 1', () => {
      const action = {};
      const reducer = campusesReducer(INITIAL_STATE, action);

      expect(reducer.chosenCampusID).toEqual(1);
    });
  });

  describe('chosenCampusID', () => {
    describe('when is changed by user', () => {
      it('updates to chosen user value in store', () => {
        const action = {type: 'CHANGE_CAMPUS', payload: 2};
        const reducer = campusesReducer(INITIAL_STATE, action);

        expect(reducer.chosenCampusID).toEqual(2);
      });
    });
  });

  describe('selectors', () => {
    describe('selectChosenCampusID', () => {
      it('returns the id of the campus in store', () => {
        const appState = {
          campuses: {
            chosenCampusID: 2,
          },
        };

        const result = selectChosenCampusID(appState);
        expect(result).toEqual(2);
      });
    });
  });

  describe('getCampusNameFromID', () => {
    it("returns the New Brunswick campus's full name given its ID", () => {
      const campusID = 1;
      const campusName = getCampusNameFromID(campusID);
      expect(campusName).toEqual('New Brunswick');
    });

    it("returns the Newark campus's full name given its ID", () => {
      const campusID = 2;
      const campusName = getCampusNameFromID(campusID);
      expect(campusName).toEqual('Newark');
    });

    it("returns the Camden campus's full name given its ID", () => {
      const campusID = 3;
      const campusName = getCampusNameFromID(campusID);
      expect(campusName).toEqual('Camden');
    });
  });
});
