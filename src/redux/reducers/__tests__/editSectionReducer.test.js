import editSectionReducer, {
  areMeetingTimesMeetingOnWeekdayAbbreviation,
  INITIAL_STATE,
  selectMeetingTimeAtNthIndex,
  selectMeetingTimes,
} from '../editSectionReducer';

describe('editSectionReducer', () => {
  describe('initial state', () => {
    it('stores no meetingTimes', () => {
      const action = {};
      const reducer = editSectionReducer(INITIAL_STATE, action);

      expect(reducer.meetingTimes).toEqual([]);
    });
  });

  describe('meetingTimes', () => {
    describe('when are added to', () => {
      it('updates in store', () => {
        const action = {type: 'ADD_MEETING_TIME', payload: 1};
        const reducer = editSectionReducer(INITIAL_STATE, action);

        expect(reducer.meetingTimes).toEqual([1]);
      });
    });

    describe('when a meeting time is edited', () => {
      it('updates in store', () => {
        const reducerState = {
          meetingTimes: [{campusName: 'Busch'}, {campusName: 'Livingston'}],
        };

        const action = {
          type: 'EDIT_MEETING_TIME',
          payload: {index: 1, edit: {campusName: 'Cook/Douglass'}},
        };
        const reducer = editSectionReducer(reducerState, action);

        expect(reducer.meetingTimes).toEqual([
          {campusName: 'Busch'},
          {campusName: 'Cook/Douglass'},
        ]);
      });
    });

    describe('when meeting times held on a certain day are deleted', () => {
      it('updates in store', () => {
        const reducerState = {
          meetingTimes: [
            {campusName: 'Busch', meetingDay: 'M'},
            {campusName: 'Livingston', meetingDay: 'M'},
            {campusName: 'Cook/Douglass', meetingDay: 'W'},
          ],
        };

        const action = {
          type: 'DELETE_MEETING_TIMES_MEETING_ON_DAY',
          payload: 'M',
        };
        const reducer = editSectionReducer(reducerState, action);

        expect(reducer.meetingTimes).toEqual([
          {campusName: 'Cook/Douglass', meetingDay: 'W'},
        ]);
      });

      it('filters out null options', () => {
        const reducerState = {
          meetingTimes: [
            {campusName: 'Livingston', meetingDay: null},
            {campusName: 'Cook/Douglass', meetingDay: 'W'},
          ],
        };

        const action = {
          type: 'DELETE_MEETING_TIMES_MEETING_ON_DAY',
          payload: 'null',
        };
        const reducer = editSectionReducer(reducerState, action);

        expect(reducer.meetingTimes).toEqual([
          {campusName: 'Cook/Douglass', meetingDay: 'W'},
        ]);
      });
    });
  });

  describe('when meeting times held on a certain day are added', () => {
    it('updates in store', () => {
      const reducerState = {
        meetingTimes: [{campusName: 'Busch', meetingDay: 'M'}],
      };

      const action = {
        type: 'ADD_DEFAULT_MEETING_TIME',
        payload: 'W',
      };
      const reducer = editSectionReducer(reducerState, action);

      expect(reducer.meetingTimes).toEqual([
        {campusName: 'Busch', meetingDay: 'M'},
        {
          baClassHours: 'B',
          buildingCode: null,
          campusAbbrev: null,
          campusLocation: null,
          campusName: null,
          endTime: '0130',
          meetingDay: 'W',
          meetingModeCode: '19',
          meetingModeDesc: 'PROJ-IND',
          pmCode: 'P',
          roomNumber: null,
          startTime: '1200',
        },
      ]);
    });

    it('does not add when there is a matching meeting day', () => {
      const reducerState = {
        meetingTimes: [{campusName: 'Busch', meetingDay: 'M'}],
      };

      const action = {
        type: 'ADD_DEFAULT_MEETING_TIME',
        payload: 'M',
      };
      const reducer = editSectionReducer(reducerState, action);

      expect(reducer.meetingTimes).toEqual([
        {campusName: 'Busch', meetingDay: 'M'},
      ]);
    });
  });

  describe('selectors', () => {
    describe('selectMeetingTimes', () => {
      it('returns the meeting times in store', () => {
        const appState = {
          editSection: {
            meetingTimes: [{campusName: 'Busch'}, {campusName: 'Livingston'}],
          },
        };

        const result = selectMeetingTimes(appState);
        expect(result).toEqual([
          {campusName: 'Busch'},
          {campusName: 'Livingston'},
        ]);
      });
    });

    describe('selectMeetingTimeAtNthIndex', () => {
      it('returns the nth meeting time in store (n starts at 0)', () => {
        const appState = {
          editSection: {
            meetingTimes: [
              {campusName: 'Busch'},
              {campusName: 'Livingston'},
              {campusName: 'College Ave'},
            ],
          },
        };

        const result = selectMeetingTimeAtNthIndex(2)(appState);
        expect(result).toEqual({campusName: 'College Ave'});
      });
    });
  });

  describe('helper methods', () => {
    describe('areMeetingTimesMeetingOnWeekdayAbbreviation', () => {
      it('says if reducer state contains any meeting times taking place on the given weekday abbreviation', () => {
        const reducerState = {
          meetingTimes: [
            {campusName: 'Busch', meetingDay: 'M'},
            {campusName: 'Livingston', meetingDay: 'M'},
            {campusName: 'Cook/Douglass', meetingDay: 'W'},
          ],
        };

        const result =
          areMeetingTimesMeetingOnWeekdayAbbreviation('M')(reducerState);
        expect(result).toBe(true);
      });
    });
  });
});
