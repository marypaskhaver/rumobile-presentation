import tabBarBadgeReducer, {
  initialState,
  selectTabBarBadges,
} from '../tabBarBadgeReducer';

describe('tabBarBadgeReducer', () => {
  describe('initial state', () => {
    it('starts off with 0 badges', () => {
      const action = {};
      const reducer = tabBarBadgeReducer(initialState, action);

      expect(reducer.badgeCount).toEqual(0);
    });
  });

  describe('modified state', () => {
    it('changes state according to arguments passed in', () => {
      const action = {type: 'FAVORITED_ITEM', payload: 5};
      const reducer = tabBarBadgeReducer(initialState, action);

      expect(reducer.badgeCount).toEqual(5);
    });

    it('does nothing if badge count is negative', () => {
      const action = {type: 'FAVORITED_ITEM', payload: -5};
      const reducer = tabBarBadgeReducer(initialState, action);

      expect(reducer.badgeCount).toEqual(0);
    });
  });

  describe('select badge count', () => {
    it('returns amount of tab bar badges', () => {
      const state = {
        tabBarBadges: {badgeCount: 42},
      };

      expect(selectTabBarBadges(state)).toEqual(42);
    });
  });
});
