import busesReducer, {
  selectBusRoutes,
  selectBusStops,
  INITIAL_STATE,
  selectBusPredictions,
  selectUserLocation,
  selectVehicles,
  selectFavoriteBusStopsIds,
} from '../busesReducer';

describe('busesReducer', () => {
  it('receives bus routes', () => {
    const busRoutes = [
      {
        is_active: true,
        long_name: 'Example Route 1',
      },
      {
        is_active: false,
        long_name: 'Example Route 2',
      },
    ];

    const action = {type: 'RECEIVE_BUS_ROUTES', payload: busRoutes};
    const reducer = busesReducer(INITIAL_STATE, action);
    expect(reducer.busRoutes).toEqual(busRoutes);
  });

  it('receives bus stops', () => {
    const busStops = [
      {
        long_name: 'Example Stop 1',
      },
      {
        long_name: 'Example Stop 2',
      },
    ];

    const action = {type: 'RECEIVE_BUS_STOPS', payload: busStops};
    const reducer = busesReducer(INITIAL_STATE, action);
    expect(reducer.busStops).toEqual(busStops);
  });

  it('receives vehicles', () => {
    const vehicles = [
      {
        long_name: 'Cat Bus',
      },
      {
        long_name: 'Hogwarts Express',
      },
    ];

    const action = {type: 'RECEIVE_VEHICLES', payload: vehicles};
    const reducer = busesReducer(INITIAL_STATE, action);
    expect(reducer.vehicles).toEqual(vehicles);
  });

  it('receives bus predictions', () => {
    const busPredictions = [
      {
        route_id: '123',
      },
    ];

    const action = {type: 'RECEIVE_BUS_PREDICTIONS', payload: busPredictions};
    const reducer = busesReducer(INITIAL_STATE, action);
    expect(reducer.busPredictions).toEqual(busPredictions);
  });

  it('receives user location', () => {
    const action = {type: 'RECEIVE_USER_LOCATION', payload: 'Narnia'};
    const reducer = busesReducer(INITIAL_STATE, action);
    expect(reducer.userLocation).toBe('Narnia');
  });

  describe('when user has not favorited any stops', () => {
    it("stores user's favorite stop and the route it's in", () => {
      const action = {
        type: 'ADD_FAVORITE_BUS_STOP_ID_WITH_ROUTE_ID',
        payload: {stopID: 123, routeID: '456'},
      };
      const reducer = busesReducer(INITIAL_STATE, action);
      expect(reducer.favoriteBusStopsIds).toEqual({123: ['456']});
    });
  });

  describe('when user removes stop from favorited stops', () => {
    describe('when favorited stop has only one associated route', () => {
      it('removes the stop and route from favorites', () => {
        const state = {
          ...INITIAL_STATE,
          favoriteBusStopsIds: {123: ['456']},
        };

        const action = {
          type: 'REMOVE_FAVORITE_BUS_STOP_ID_WITH_ROUTE_ID',
          payload: {stopID: '123', routeID: '456'},
        };
        const reducer = busesReducer(state, action);
        expect(reducer.favoriteBusStopsIds).toEqual({});
      });
    });

    describe('when favorited stop has only one associated route', () => {
      it("removes route from stop's associated ids", () => {
        const state = {
          ...INITIAL_STATE,
          favoriteBusStopsIds: {123: ['456', '678']},
        };

        const action = {
          type: 'REMOVE_FAVORITE_BUS_STOP_ID_WITH_ROUTE_ID',
          payload: {stopID: '123', routeID: '456'},
        };
        const reducer = busesReducer(state, action);
        expect(reducer.favoriteBusStopsIds).toEqual({
          123: ['678'],
        });
      });
    });

    describe('when multiple stops share a route', () => {
      it("removes route only from given stop's associated ids", () => {
        const state = {
          ...INITIAL_STATE,
          favoriteBusStopsIds: {
            4229620: ['4014680', '4014682'],
            4259014: ['4014680'],
            4259722: ['4014682'],
          },
        };

        const action = {
          type: 'REMOVE_FAVORITE_BUS_STOP_ID_WITH_ROUTE_ID',
          payload: {stopID: '4259722', routeID: '4014682'},
        };
        const reducer = busesReducer(state, action);
        expect(reducer.favoriteBusStopsIds).toEqual({
          4229620: ['4014680', '4014682'],
          4259014: ['4014680'],
        });
      });
    });

    describe('when multiple stops share the same routes', () => {
      it("removes route only from given stop's associated ids", () => {
        const state = {
          ...INITIAL_STATE,
          favoriteBusStopsIds: {
            4229620: ['4014680', '4014682'],
            4259014: ['4014680', '4014682'],
          },
        };

        const action = {
          type: 'REMOVE_FAVORITE_BUS_STOP_ID_WITH_ROUTE_ID',
          payload: {stopID: '4229620', routeID: '4014680'},
        };
        const reducer = busesReducer(state, action);
        expect(reducer.favoriteBusStopsIds).toEqual({
          4229620: ['4014682'],
          4259014: ['4014680', '4014682'],
        });
      });
    });
  });

  describe('selectors', () => {
    describe('selectBusRoutes', () => {
      it('returns the bus routes stored in state', () => {
        const appState = {
          buses: {
            busRoutes: [
              {
                is_active: true,
                long_name: 'Route Weekend 1',
              },
              {
                is_active: false,
                long_name: 'Route Weekend 2',
              },
            ],
          },
        };

        expect(selectBusRoutes(appState)).toEqual([
          {
            is_active: true,
            long_name: 'Route Weekend 1',
          },
          {
            is_active: false,
            long_name: 'Route Weekend 2',
          },
        ]);
      });
    });

    describe('selectBusStops', () => {
      it('returns the bus stops stored in state', () => {
        const appState = {
          buses: {
            busStops: [
              {
                long_name: 'Example Stop 1',
              },
              {
                long_name: 'Example Stop 2',
              },
            ],
          },
        };

        expect(selectBusStops(appState)).toEqual([
          {
            long_name: 'Example Stop 1',
          },
          {
            long_name: 'Example Stop 2',
          },
        ]);
      });
    });

    describe('selectBusPreductions', () => {
      it('returns the bus predictions stored in state', () => {
        const appState = {
          buses: {
            busPredictions: [
              {
                long_name: 'Example Prediction 1',
              },
              {
                long_name: 'Example Prediction 2',
              },
            ],
          },
        };

        expect(selectBusPredictions(appState)).toEqual([
          {
            long_name: 'Example Prediction 1',
          },
          {
            long_name: 'Example Prediction 2',
          },
        ]);
      });
    });

    describe('selectVehicles', () => {
      it('returns the vehicles stored in state', () => {
        const appState = {
          buses: {
            vehicles: [
              {
                long_name: 'Example Vehicle',
              },
            ],
          },
        };

        expect(selectVehicles(appState)).toEqual([
          {
            long_name: 'Example Vehicle',
          },
        ]);
      });
    });

    describe('selectFavoriteBusStopsIds', () => {
      it('returns the favorite bus stop ids stored in state', () => {
        const appState = {
          buses: {
            favoriteBusStopsIds: {123: '456', 789: '012'},
          },
        };

        expect(selectFavoriteBusStopsIds(appState)).toEqual({
          123: '456',
          789: '012',
        });
      });
    });

    describe('selectUserLocation', () => {
      it('returns the user location stored in state', () => {
        const appState = {
          buses: {
            userLocation: 'CONTROL Secret Headquarters',
          },
        };

        expect(selectUserLocation(appState)).toEqual(
          'CONTROL Secret Headquarters',
        );
      });
    });
  });
});
