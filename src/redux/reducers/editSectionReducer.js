import {
  ADD_MEETING_TIME,
  EDIT_MEETING_TIME,
  DELETE_MEETING_TIMES_MEETING_ON_DAY,
  ADD_DEFAULT_MEETING_TIME,
} from '../actionTypes';

const initialState = {
  meetingTimes: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case ADD_MEETING_TIME: {
      // Clone deep
      const newMeetingTime = JSON.parse(JSON.stringify(action.payload));

      return {
        ...state,
        meetingTimes: [...state.meetingTimes, newMeetingTime],
      };
    }

    case EDIT_MEETING_TIME: {
      const {edit, index} = action.payload;

      const editKey = Object.keys(edit)[0];
      const editValue = edit[editKey];

      const meetingTimeToEdit = state.meetingTimes[index];
      const editedMeetingTime = {...meetingTimeToEdit, [editKey]: editValue};

      return {
        ...state,
        meetingTimes: state.meetingTimes.map((meetingTime, ind) =>
          ind === index ? editedMeetingTime : meetingTime,
        ),
      };
    }

    case DELETE_MEETING_TIMES_MEETING_ON_DAY: {
      const abbreviation = action.payload;

      const filterByAbbreviation = meetingTime =>
        meetingTime.meetingDay !== abbreviation;
      const filterByNotNull = meetingTime => !!meetingTime.meetingDay;

      return {
        ...state,
        meetingTimes: state.meetingTimes.filter(
          abbreviation !== 'null' ? filterByAbbreviation : filterByNotNull,
        ),
      };
    }

    case ADD_DEFAULT_MEETING_TIME: {
      const abbreviation = action.payload;

      const defaultMeetingTime = {
        baClassHours: 'B',
        buildingCode: null,
        campusAbbrev: null,
        campusLocation: null,
        campusName: null,
        endTime: '0130',
        meetingDay: abbreviation,
        meetingModeCode: '19',
        meetingModeDesc: 'PROJ-IND',
        pmCode: 'P',
        roomNumber: null,
        startTime: '1200',
      };

      if (!areMeetingTimesMeetingOnWeekdayAbbreviation(abbreviation)(state)) {
        return {
          ...state,
          meetingTimes: [...state.meetingTimes, defaultMeetingTime],
        };
      }

      return state;
    }

    default:
      return state;
  }
}

export const selectMeetingTimes = state => state.editSection.meetingTimes;
export const selectMeetingTimeAtNthIndex = index => state =>
  state.editSection.meetingTimes[index];
export const areMeetingTimesMeetingOnWeekdayAbbreviation =
  abbreviation => state =>
    state.meetingTimes.some(
      meetingTime => meetingTime.meetingDay === abbreviation,
    );
