import {
  RECEIVE_MENU_ITEMS,
  START_MENU_ITEMS_FETCH,
  COMPLETE_MENU_ITEMS_FETCH,
  FAIL_MENU_ITEMS_FETCH,
  RECEIVE_NUTRITION_ITEMS,
  START_NUTRITION_ITEMS_FETCH,
  COMPLETE_NUTRITION_ITEMS_FETCH,
  FAIL_NUTRITION_ITEMS_FETCH,
} from '../../constants/diningHalls';
import DateFormatter from '../../utils/DateFormatter';

export const INITIAL_STATE = {
  menuItemsFetchStatus: 'initializing',
  nutritionItemsFetchStatus: 'initializing',
  menuItems: {},
  nutritionItems: {},
  hallIsOpen: false,
};

export const isDiningHallOpen = () => {
  const now = new Date();

  const winterBreakStartDate = new Date('12/24/2022');
  const winterBreakEndDate = new Date('01/14/2023');

  if (now >= winterBreakStartDate && now <= winterBreakEndDate) {
    return false;
  }

  const hoursNow = now.getHours();
  const minutesNow = now.getMinutes();
  const today = new DateFormatter(now).getWeekday();

  const weekdays = ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY'];
  const weekendDays = ['SATURDAY', 'SUNDAY'];

  if (weekdays.includes(today)) {
    const after7AM = hoursNow >= 7;
    const before11PM = hoursNow < 23;
    return after7AM && before11PM;
  }

  if (weekendDays.includes(today)) {
    const between930and10AM = hoursNow === 9 && minutesNow >= 30;
    const after10AM = hoursNow >= 10;
    const before8PM = hoursNow < 20;
    return (between930and10AM || after10AM) && before8PM;
  }

  // Friday
  const after7AM = hoursNow >= 7;
  const before9PM = hoursNow < 21;
  return after7AM && before9PM;
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case START_MENU_ITEMS_FETCH:
      return {...state, menuItemsFetchStatus: 'pending'};
    case COMPLETE_MENU_ITEMS_FETCH:
      return {...state, menuItemsFetchStatus: 'success'};
    case FAIL_MENU_ITEMS_FETCH:
      return {...state, menuItemsFetchStatus: 'failure'};
    case RECEIVE_MENU_ITEMS:
      const menuItems = action.payload;

      // Maybe make these selectors-- avoid mutation
      Object.entries(menuItems).forEach(diningHall => {
        const menuItem = diningHall[1];
        const {diningHallName} = menuItem;
        const adjustedMenuItem = menuItems[diningHallName];
        adjustedMenuItem.hallIsOpen = isDiningHallOpen();
      });

      return {...state, menuItems};

    case RECEIVE_NUTRITION_ITEMS:
      const nutritionItems = action.payload;
      return {...state, nutritionItems};
    case START_NUTRITION_ITEMS_FETCH:
      return {...state, nutritionItemsFetchStatus: 'pending'};
    case COMPLETE_NUTRITION_ITEMS_FETCH:
      return {...state, nutritionItemsFetchStatus: 'success'};
    case FAIL_NUTRITION_ITEMS_FETCH:
      return {...state, nutritionItemsFetchStatus: 'failure'};

    default:
      return state;
  }
};

// Selectors
export const selectMenuItems = state => state.food.menuItems;
export const selectMenuItemsFetchStatus = state =>
  state.food.menuItemsFetchStatus;
export const selectNutritionItems = state => state.food.nutritionItems;
export const selectNutritionItemsFetchStatus = state =>
  state.food.nutritionItemsFetchStatus;
// Function that accepts diningHallName. Returns a function that accepts a single
// parameter, state, and returns the result of invoking that second func.
export const selectMenuItemByDiningHallName = diningHallName => state => {
  return state.food.menuItems[diningHallName];
};

export const menuItemsForDiningHallNameAtTimeOfDay =
  (diningHallName, timeOfDay) => state => {
    const diningHallMenuItem =
      selectMenuItemByDiningHallName(diningHallName)(state);

    const mealForTimeOfDay = diningHallMenuItem[timeOfDay];

    if (!diningHallMenuItem.hallIsOpen) return [];

    return mealForTimeOfDay;
  };
