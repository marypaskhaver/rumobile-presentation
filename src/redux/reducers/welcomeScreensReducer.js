import {PASSED_WELCOME_SCREEN} from '../../constants/welcomeScreens';

export const INITIAL_STATE = {
  passed: false,
  loaded: false,
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case PASSED_WELCOME_SCREEN: {
      return {
        ...state,
        passed: action.payload,
      };
    }
    case 'LOADED': {
      return {
        ...state,
        loaded: action.payload,
      };
    }

    default:
      return state;
  }
}

export const selectPassed = state => state.welcomeScreens.passed;
export const selectLoaded = state => state.welcomeScreens.loaded;
