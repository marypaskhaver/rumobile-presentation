import {
  RECEIVE_BUS_PREDICTIONS,
  RECEIVE_BUS_ROUTES,
  RECEIVE_BUS_STOPS,
  RECEIVE_VEHICLES,
  RECEIVE_USER_LOCATION,
  ADD_FAVORITE_BUS_STOP_ID_WITH_ROUTE_ID,
  REMOVE_FAVORITE_BUS_STOP_ID_WITH_ROUTE_ID,
  SORT_FAVORITE_BUSES,
} from '../../constants/buses';

export const INITIAL_STATE = {
  busRoutes: [],
  busStops: [],
  busPredictions: [],
  vehicles: [],
  userLocation: null,
  favoriteBusStopsIds: {},
  sortFavoriteBuses: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case RECEIVE_BUS_ROUTES:
      const newBusRoutes = action.payload;
      return {...state, busRoutes: newBusRoutes};

    case RECEIVE_BUS_STOPS:
      const newBusStops = action.payload;
      return {...state, busStops: newBusStops};

    case RECEIVE_BUS_PREDICTIONS:
      const newBusPredictions = action.payload;
      return {...state, busPredictions: newBusPredictions};

    case RECEIVE_VEHICLES:
      const newVehicles = action.payload;
      return {...state, vehicles: newVehicles};

    case RECEIVE_USER_LOCATION:
      const location = action.payload;
      return {...state, userLocation: location};

    case ADD_FAVORITE_BUS_STOP_ID_WITH_ROUTE_ID:
      const {favoriteBusStopsIds} = state;
      const routesForStopID = selectRoutesForFavoriteStop(
        action.payload.stopID,
      )(state);

      return {
        ...state,
        favoriteBusStopsIds: {
          ...favoriteBusStopsIds,
          [action.payload.stopID]: [...routesForStopID, action.payload.routeID],
        },
      };

    case REMOVE_FAVORITE_BUS_STOP_ID_WITH_ROUTE_ID:
      const {stopID, routeID} = action.payload;

      const filteredRoutesForStopID = selectFilteredRoutesForFavoriteStop(
        stopID,
        routeID,
      )(state);

      if (filteredRoutesForStopID.length === 0) {
        const copy = {...state.favoriteBusStopsIds};
        delete copy[stopID];

        return {
          ...state,
          favoriteBusStopsIds: copy,
        };
      }

      return {
        ...state,
        favoriteBusStopsIds: {
          ...state.favoriteBusStopsIds,
          [stopID]: filteredRoutesForStopID,
        },
      };

    case SORT_FAVORITE_BUSES:
      const sortOrNot = action.payload;
      return {...state, sortFavoriteBuses: sortOrNot};

    default:
      return state;
  }
};

const selectRoutesForFavoriteStop = stopID => state => {
  return state.favoriteBusStopsIds[stopID] ?? [];
};

const selectFilteredRoutesForFavoriteStop = (stopID, routeID) => state => {
  const routesForStopID = selectRoutesForFavoriteStop(stopID)(state);

  return routesForStopID.filter(
    favoritedRouteID => favoritedRouteID !== routeID,
  );
};

export const selectBusRoutes = state => state.buses.busRoutes;
export const selectSortFavoriteBuses = state => state.buses.sortFavoriteBuses;
export const selectBusStops = state => state.buses.busStops;
export const selectBusPredictions = state => state.buses.busPredictions;
export const selectVehicles = state => state.buses.vehicles;
export const selectUserLocation = state => state.buses.userLocation;
export const selectFavoriteBusStopsIds = state =>
  state.buses.favoriteBusStopsIds;
