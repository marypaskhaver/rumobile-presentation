import {FAVORITED_ITEM} from '../../constants/tabBarBadges';

export const INITIAL_STATE = {
  badgeCount: 0,
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case FAVORITED_ITEM: {
      if (action.payload < 0) return state;

      return {
        ...state,
        badgeCount: action.payload,
      };
    }

    default:
      return state;
  }
}

export const selectTabBarBadges = state => state.tabBarBadges.badgeCount;
