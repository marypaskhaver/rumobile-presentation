import {combineReducers} from 'redux';
import academics from './academicsReducer';
import buses from './busesReducer';
import campuses from './campusesReducer';
import editSection from './editSectionReducer';
import food from './foodReducer';
import themes from './themesReducer';
import welcomeScreens from './welcomeScreensReducer';
import tabBarBadges from './tabBarBadgeReducer';

export default combineReducers({
  academics,
  buses,
  campuses,
  editSection,
  food,
  themes,
  welcomeScreens,
  tabBarBadges,
});
