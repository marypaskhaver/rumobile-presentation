import {
  ADD_CAMPUS,
  REMOVE_CAMPUS,
  RECEIVE_CLASS_CATEGORIES,
  ADD_LEVEL,
  REMOVE_LEVEL,
  UPDATE_SEMESTER,
  SET_LEVELS,
  SET_CAMPUSES,
  ADD_FAVORITE_SECTION,
  REMOVE_FAVORITE_SECTION,
  EDIT_SECTION,
  START_CLASS_CATEGORIES_FETCH,
  COMPLETE_CLASS_CATEGORIES_FETCH,
  FAIL_CLASS_CATEGORIES_FETCH,
} from '../../constants/academics';
import {RECEIVE_CLASSES} from '../../constants/classes';

export const initialState = {
  classCategories: [],
  semester: '92022',
  campuses: ['NB'],
  levels: ['U', 'G'],
  classes: [],
  fetchStatus: 'initializing',
  favoriteSections: {},
};

// Used in index.js in combineReducers
export default function (state = initialState, action) {
  switch (action.type) {
    case RECEIVE_CLASS_CATEGORIES:
      const classCategories = action.payload;
      return {
        ...state,
        classCategories,
      };

    case RECEIVE_CLASSES:
      const classes = action.payload;

      return {
        ...state,
        classes,
      };

    case UPDATE_SEMESTER:
      const newSemester = action.payload;

      return {
        ...state,
        semester: newSemester || state.semester,
      };

    case ADD_CAMPUS:
      const newCampus = action.payload;

      return {
        ...state,
        campuses: [...state.campuses, newCampus],
      };

    case REMOVE_CAMPUS:
      const campusToRemove = action.payload;

      return {
        ...state,
        campuses: state.campuses.filter(campus => campus !== campusToRemove),
      };

    case ADD_LEVEL:
      const newLevel = action.payload;

      return {
        ...state,
        levels: [...state.levels, newLevel],
      };

    case REMOVE_LEVEL:
      const levelToRemove = action.payload;

      return {
        ...state,
        levels: state.levels.filter(level => level !== levelToRemove),
      };

    case SET_LEVELS:
      const newLevels = action.payload;

      return {
        ...state,
        levels: newLevels || state.levels,
      };

    case SET_CAMPUSES:
      const newCampuses = action.payload;

      return {
        ...state,
        campuses: newCampuses || state.campuses,
      };

    case ADD_FAVORITE_SECTION:
      // Clone
      const section = JSON.parse(JSON.stringify(action.payload));
      const {index} = section;

      return {
        ...state,
        favoriteSections: {...state.favoriteSections, [index]: section},
      };

    case REMOVE_FAVORITE_SECTION:
      const sectionToRemove = action.payload;

      const copy = {...state.favoriteSections};

      if (sectionToRemove && sectionToRemove.index)
        delete copy[sectionToRemove.index];

      return {
        ...state,
        favoriteSections: copy,
      };

    case EDIT_SECTION:
      const {sectionIndex, newTitle, newMeetingTimes} = action.payload;

      const favoriteSections = {...state.favoriteSections};
      const sectionToEdit = state.favoriteSections[sectionIndex];
      sectionToEdit.title = newTitle;
      sectionToEdit.meetingTimes = newMeetingTimes;

      favoriteSections[sectionIndex] = sectionToEdit;

      return {
        ...state,
        favoriteSections,
      };

    case START_CLASS_CATEGORIES_FETCH:
      return {...state, fetchStatus: 'pending'};
    case COMPLETE_CLASS_CATEGORIES_FETCH:
      return {...state, fetchStatus: 'success'};
    case FAIL_CLASS_CATEGORIES_FETCH:
      return {...state, fetchStatus: 'failure'};

    default:
      return state;
  }
}

export const selectClassCategoriesFetchStatus = state =>
  state.academics.fetchStatus;

export const selectFavoriteSections = state => state.academics.favoriteSections;
export const isFavoriteSection = targetSection => state => {
  // Interesting syntax. Casts to a boolean.
  return !!state.academics.favoriteSections[targetSection.index];
};
export const selectLevels = state => state.academics.levels;
