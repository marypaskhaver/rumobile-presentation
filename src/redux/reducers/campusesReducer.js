import {CAMPUS_IDs_TO_NAMES} from '../../constants/campuses';
import {CHANGE_CAMPUS} from '../actionTypes';

const initialState = {
  chosenCampusID: 1,
};

// Used in index.js in combineReducers
export default function (state = initialState, action) {
  switch (action.type) {
    case CHANGE_CAMPUS: {
      return {chosenCampusID: action.payload || state.chosenCampusID};
    }

    default:
      return state;
  }
}

export const selectChosenCampusID = state => state.campuses.chosenCampusID;

export const getCampusNameFromID = id => {
  return CAMPUS_IDs_TO_NAMES[id].name;
};
