import axios from 'axios';
import {BUSES_API_KEY} from '../../../env.json';
import {CAMPUS_IDs_TO_AGENCY_IDs_MAP} from '../../constants/buses';

const getBusPredictions = (routeIDs, stopIDs = []) => {
  const allRoutes = routeIDs.join('%2C');
  const allStops = stopIDs.join('%2C');

  const allPossibleAgencies = Object.values(CAMPUS_IDs_TO_AGENCY_IDs_MAP);
  const uniqueAgenciesSet = new Set(allPossibleAgencies);
  const uniqueAgenciesArray = new Array(...uniqueAgenciesSet);

  const busPredictionsURL = `https://transloc-api-1-2.p.rapidapi.com/arrival-estimates.json?routes=${allRoutes}${
    stopIDs.length > 0 ? `&stops=${allStops}` : ''
  }&agencies=${uniqueAgenciesArray}`;

  return axios.get(busPredictionsURL, {
    headers: {
      Accept: 'application/json',
      'X-Mashape-Key': BUSES_API_KEY,
      'x-rapidapi-host': 'transloc-api-1-2.p.rapidapi.com',
    },
  });
};

export default getBusPredictions;
