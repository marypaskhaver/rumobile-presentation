import axios from 'axios';
import getBusRoutes from '../getBusRoutes';
import {BUSES_API_KEY} from '../../../../env.json';

jest.mock('axios');

describe('getBusRoutes', () => {
  it("generates URL based on user's chosen campus ID (New Brunswick)", () => {
    const chosenCampusID = 1;

    getBusRoutes(chosenCampusID);

    expect(axios.get).toHaveBeenCalledWith(
      'https://transloc-api-1-2.p.rapidapi.com/routes.json?geo_area=40.382690%2C-74.595626%7C40.625639%2C-74.280317&agencies=1323',
      {
        headers: {
          Accept: 'application/json',
          'X-Mashape-Key': BUSES_API_KEY,
          'x-rapidapi-host': 'transloc-api-1-2.p.rapidapi.com',
        },
      },
    );
  });

  it("generates URL based on user's chosen campus ID (Newark)", () => {
    const chosenCampusID = 2;

    getBusRoutes(chosenCampusID);

    expect(axios.get).toHaveBeenCalledWith(
      'https://transloc-api-1-2.p.rapidapi.com/routes.json?geo_area=40.841884%2C-74.011088%7C40.660668%2C-74.277053&agencies=1323',
      {
        headers: {
          Accept: 'application/json',
          'X-Mashape-Key': BUSES_API_KEY,
          'x-rapidapi-host': 'transloc-api-1-2.p.rapidapi.com',
        },
      },
    );
  });

  it("generates URL based on user's chosen campus ID (Camden)", () => {
    const chosenCampusID = 3;

    getBusRoutes(chosenCampusID);

    expect(axios.get).toHaveBeenCalledWith(
      'https://transloc-api-1-2.p.rapidapi.com/routes.json?&agencies=1233',
      {
        headers: {
          Accept: 'application/json',
          'X-Mashape-Key': BUSES_API_KEY,
          'x-rapidapi-host': 'transloc-api-1-2.p.rapidapi.com',
        },
      },
    );
  });
});
