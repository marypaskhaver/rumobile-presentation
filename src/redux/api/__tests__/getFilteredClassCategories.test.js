import axios from 'axios';
import getFilteredClassCategories from '../getFilteredClassCategories';

// Same obj, but jest replaces all of its methods w/ mock implementations
// By default, every method we mock will return undefined
jest.mock('axios');

describe('getFilteredClassCategories', () => {
  it('generates URL based on semester, campus, and education level', () => {
    const classesFilterMetadata = {
      semester: '12022',
      campuses: ['NB'],
      levels: ['U', 'G'],
    };

    getFilteredClassCategories(classesFilterMetadata);

    // Make sure mock has been invoked
    expect(axios.get).toHaveBeenCalledWith(
      'https://sis.rutgers.edu/oldsoc/subjects.json?semester=12022&campus=NB&level=U,G',
    );
  });
});
