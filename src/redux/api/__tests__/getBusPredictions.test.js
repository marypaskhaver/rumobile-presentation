import axios from 'axios';
import getBusPredictions from '../getBusPredictions';
import {BUSES_API_KEY} from '../../../../env.json';

jest.mock('axios');

describe('getBusPredictions', () => {
  it('generates URL based on given bus route IDs and bus stop IDs', () => {
    const routeIDs = [1, 2, 3];
    const stopIDs = [4, 5];

    getBusPredictions(routeIDs, stopIDs);

    expect(axios.get).toHaveBeenCalledWith(
      `https://transloc-api-1-2.p.rapidapi.com/arrival-estimates.json?routes=1%2C2%2C3&stops=4%2C5&agencies=1323,1233`,
      {
        headers: {
          Accept: 'application/json',
          'X-Mashape-Key': BUSES_API_KEY,
          'x-rapidapi-host': 'transloc-api-1-2.p.rapidapi.com',
        },
      },
    );
  });

  it('works with a single route and several stops', () => {
    const routeIDs = [1];
    const stopIDs = [4, 5];

    getBusPredictions(routeIDs, stopIDs);

    expect(axios.get).toHaveBeenCalledWith(
      `https://transloc-api-1-2.p.rapidapi.com/arrival-estimates.json?routes=1&stops=4%2C5&agencies=1323,1233`,
      {
        headers: {
          Accept: 'application/json',
          'X-Mashape-Key': BUSES_API_KEY,
          'x-rapidapi-host': 'transloc-api-1-2.p.rapidapi.com',
        },
      },
    );
  });

  it('works with a single route and a single stop', () => {
    const routeIDs = [1];
    const stopIDs = [4];

    getBusPredictions(routeIDs, stopIDs);

    expect(axios.get).toHaveBeenCalledWith(
      `https://transloc-api-1-2.p.rapidapi.com/arrival-estimates.json?routes=1&stops=4&agencies=1323,1233`,
      {
        headers: {
          Accept: 'application/json',
          'X-Mashape-Key': BUSES_API_KEY,
          'x-rapidapi-host': 'transloc-api-1-2.p.rapidapi.com',
        },
      },
    );
  });

  it('works with routes but no given stops', () => {
    const routeIDs = [1, 2];

    getBusPredictions(routeIDs);

    expect(axios.get).toHaveBeenCalledWith(
      `https://transloc-api-1-2.p.rapidapi.com/arrival-estimates.json?routes=1%2C2&agencies=1323,1233`,
      {
        headers: {
          Accept: 'application/json',
          'X-Mashape-Key': BUSES_API_KEY,
          'x-rapidapi-host': 'transloc-api-1-2.p.rapidapi.com',
        },
      },
    );
  });
});
