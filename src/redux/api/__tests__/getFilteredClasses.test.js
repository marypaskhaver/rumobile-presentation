import axios from 'axios';
import getFilteredClasses from '../getFilteredClasses';

// Same obj, but jest replaces all of its methods w/ mock implementations
// By default, every method we mock will return undefined
jest.mock('axios');

describe('getFilteredClasses', () => {
  it('generates URL based on subject, semester, campus, and education level', () => {
    const classesFilterMetadata = {
      semester: '12022',
      campuses: ['NB'],
      levels: ['U', 'G'],
      code: '010',
    };

    getFilteredClasses(classesFilterMetadata);

    // Make sure mock has been invoked
    expect(axios.get).toHaveBeenCalledWith(
      'https://sis.rutgers.edu/oldsoc/courses.json?subject=010&semester=12022&campus=NB&level=U,G',
    );
  });
});
