import axios from 'axios';

const getDiningHalls = () => {
  return axios.get('https://d1y2okekw6su6w.cloudfront.net/rutgers-dining.json');
};

export default getDiningHalls;
