import axios from 'axios';

const getMessageOfTheDay = () => {
  return axios.get('https://d3gcr65qy8nmmj.cloudfront.net/message.json');
};

export default getMessageOfTheDay;
