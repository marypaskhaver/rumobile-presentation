import axios from 'axios';
import {
  CAMPUS_IDs_TO_GEOGRAPHIC_AREAS_MAP,
  CAMPUS_IDs_TO_AGENCY_IDs_MAP,
} from '../../constants/buses';
import {BUSES_API_KEY} from '../../../env.json';

const getBusRoutes = chosenCampusID => {
  const baseURL = 'https://transloc-api-1-2.p.rapidapi.com/routes.json?';
  const campusGeographicArea =
    CAMPUS_IDs_TO_GEOGRAPHIC_AREAS_MAP[chosenCampusID];
  const campusAgencyID = CAMPUS_IDs_TO_AGENCY_IDs_MAP[chosenCampusID];

  const filteredBusRoutesURL = `${baseURL}${
    campusGeographicArea ? `geo_area=${campusGeographicArea}` : ''
  }&agencies=${campusAgencyID}`;

  return axios.get(filteredBusRoutesURL, {
    headers: {
      Accept: 'application/json',
      'X-Mashape-Key': BUSES_API_KEY,
      'x-rapidapi-host': 'transloc-api-1-2.p.rapidapi.com',
    },
  });
};

export default getBusRoutes;
