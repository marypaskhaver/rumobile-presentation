import axios from 'axios';

const getNutritionItems = () => {
  return axios.get(
    'https://d1y2okekw6su6w.cloudfront.net/nutrition-items.json',
  );
};

export default getNutritionItems;
