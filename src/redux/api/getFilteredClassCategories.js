import axios from 'axios';

const getFilteredClassCategories = ({semester, campuses, levels}) => {
  const baseURL = 'https://sis.rutgers.edu/oldsoc/subjects.json';
  const filteredClassesURL = `${baseURL}?semester=${semester}&campus=${campuses}&level=${levels}`;
  return axios.get(filteredClassesURL);
};

export default getFilteredClassCategories;
