import axios from 'axios';

const getFilteredClasses = ({semester, campuses, levels, code}) => {
  const baseURL = 'https://sis.rutgers.edu/oldsoc/courses.json';
  const filteredSectionsURL = `${baseURL}?subject=${code}&semester=${semester}&campus=${campuses}&level=${levels}`;
  return axios.get(filteredSectionsURL);
};

export default getFilteredClasses;
