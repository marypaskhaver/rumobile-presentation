import {
  ADD_DEFAULT_MEETING_TIME,
  ADD_MEETING_TIME,
  DELETE_MEETING_TIMES_MEETING_ON_DAY,
  EDIT_MEETING_TIME,
} from '../actionTypes';

export const addMeetingTime = meetingTime => ({
  type: ADD_MEETING_TIME,
  payload: meetingTime,
});

export const editMeetingTime = (index, edit) => ({
  type: EDIT_MEETING_TIME,
  payload: {index, edit},
});

export const deleteMeetingTimesMeetingOnWeekdayAbbreviation = abbreviation => ({
  type: DELETE_MEETING_TIMES_MEETING_ON_DAY,
  payload: abbreviation,
});

export const addDefaultMeetingTimeForWeekdayAbbreviation = abbreviation => ({
  type: ADD_DEFAULT_MEETING_TIME,
  payload: abbreviation,
});
