import {CHANGE_CAMPUS} from '../actionTypes';

export const updateChosenCampusID = chosenCampusID => ({
  type: CHANGE_CAMPUS,
  payload: chosenCampusID,
});
