import {RECEIVE_CLASSES} from '../../constants/classes';

import getFilteredClasses from '../api/getFilteredClasses';

const receiveClasses = payload => ({
  type: RECEIVE_CLASSES,
  payload,
});

export const fetchFilteredClasses = classFilters => {
  return dispatch => {
    getFilteredClasses(classFilters)
      .then(response => {
        const classes = response.data;
        dispatch(receiveClasses(classes));
      })
      .catch(error => {});
  };
};
