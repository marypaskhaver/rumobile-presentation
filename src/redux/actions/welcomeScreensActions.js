import {PASSED_WELCOME_SCREEN} from '../../constants/welcomeScreens';

export const passWelcomeScreen = passed => ({
  type: PASSED_WELCOME_SCREEN,
  payload: passed,
});

export const loadWelcomeScreen = loaded => ({
  type: 'LOADED',
  payload: loaded,
});
