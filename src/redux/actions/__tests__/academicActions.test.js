import {
  addCampus,
  removeCampus,
  reformatClassCategoryData,
  addLevel,
  removeLevel,
  setLevels,
  setCampuses,
  updateSemester,
  addFavoriteSection,
  removeFavoriteSection,
  editSectionTitleAndMeetingTimes,
  requestClassCategories,
  receiveClassCategories,
} from '../academicActions';

describe('academicActions', () => {
  describe('receiveClassCategories', () => {
    it('creates action to remove given education level from store', () => {
      // receiveClassCategoriesAction = jest.fn();
      const receiveClassCategoriesAction = receiveClassCategories('Accounting');
      expect(receiveClassCategoriesAction).toEqual({
        type: 'RECEIVE_CLASS_CATEGORIES',
        payload: 'Accounting',
      });
    });
  });

  describe('reformatClassCategoryData', () => {
    it('organizes class categories alphabetically by first letter for user display', () => {
      const classCategories = [
        {
          description: 'AFRICAN, M. EAST. & S. ASIAN LANG & LIT',
          code: '001',
        },
        {
          description: 'CHaRmS',
          code: '003',
        },
        {
          description: 'divinatioN',
          code: '004',
        },

        {
          description: 'AFRO-AMERICAN STUDIES',
          code: '002',
        },
      ];

      const result = reformatClassCategoryData(classCategories);

      const sections = [
        {
          title: 'A',
          data: [
            {
              description: 'AFRICAN, M. EAST. & S. ASIAN LANG & LIT',
              code: '001',
              displayTitle: 'African, M. East. & S. Asian Lang & Lit (001)',
            },
            {
              description: 'AFRO-AMERICAN STUDIES',
              code: '002',
              displayTitle: 'Afro-American Studies (002)',
            },
          ],
        },
        {
          title: 'C',
          data: [
            {
              description: 'CHaRmS',
              code: '003',
              displayTitle: 'Charms (003)',
            },
          ],
        },
        {
          title: 'D',
          data: [
            {
              description: 'divinatioN',
              code: '004',
              displayTitle: 'Divination (004)',
            },
          ],
        },
      ];

      expect(result).toEqual(sections);
    });
  });

  describe('addCampus', () => {
    it("creates action to update and store user's newly-added campus", () => {
      const addCampusAction = addCampus('Hogwarts');
      expect(addCampusAction).toEqual({
        type: 'ADD_CAMPUS',
        payload: 'Hogwarts',
      });
    });
  });

  describe('removeCampus', () => {
    it('creates action to remove given campus from store', () => {
      const removeCampusAction = removeCampus('Durmstrang');
      expect(removeCampusAction).toEqual({
        type: 'REMOVE_CAMPUS',
        payload: 'Durmstrang',
      });
    });
  });

  describe('setCampuses', () => {
    it('creates action to set stored campuses to given campuses', () => {
      const setCampusesAction = setCampuses(['Hogwarts', 'Durmstrang']);
      expect(setCampusesAction).toEqual({
        type: 'SET_CAMPUSES',
        payload: ['Hogwarts', 'Durmstrang'],
      });
    });
  });

  describe('addLevel', () => {
    it("creates action to update and store user's newly-added education level", () => {
      const addLevelAction = addLevel('sorcerer');
      expect(addLevelAction).toEqual({
        type: 'ADD_LEVEL',
        payload: 'sorcerer',
      });
    });
  });

  describe('removeLevel', () => {
    it('creates action to remove given education level from store', () => {
      const removeLevelAction = removeLevel('mage');
      expect(removeLevelAction).toEqual({
        type: 'REMOVE_LEVEL',
        payload: 'mage',
      });
    });
  });

  describe('setLevels', () => {
    it('creates action to set stored education levels to given levels', () => {
      const setLevelsAction = setLevels(['mage', 'sorcerer']);
      expect(setLevelsAction).toEqual({
        type: 'SET_LEVELS',
        payload: ['mage', 'sorcerer'],
      });
    });
  });

  describe('updateSemester', () => {
    it('creates action to update stored semester to given semester', () => {
      const updateSemesterAction = updateSemester('92024');
      expect(updateSemesterAction).toEqual({
        type: 'UPDATE_SEMESTER',
        payload: '92024',
      });
    });
  });

  describe('addFavoriteSection', () => {
    it('creates action to add favorite section to store', () => {
      const favoriteSection = {
        campusCode: 'NB',
        credits: 3,
        index: '03470',
        title: 'INTRO FINANCIAL ACCT',
      };
      const addFavoriteSectionAction = addFavoriteSection(favoriteSection);
      expect(addFavoriteSectionAction).toEqual({
        type: 'ADD_FAVORITE_SECTION',
        payload: {
          campusCode: 'NB',
          credits: 3,
          index: '03470',
          title: 'INTRO FINANCIAL ACCT',
        },
      });
    });

    describe('removeFavoriteSection', () => {
      it('creates action to remove favorite section to store', () => {
        const favoriteSection = {
          campusCode: 'NB',
          credits: 3,
          index: '03470',
          title: 'INTRO FINANCIAL ACCT',
        };
        const removeFavoriteSectionAction =
          removeFavoriteSection(favoriteSection);
        expect(removeFavoriteSectionAction).toEqual({
          type: 'REMOVE_FAVORITE_SECTION',
          payload: {
            campusCode: 'NB',
            credits: 3,
            index: '03470',
            title: 'INTRO FINANCIAL ACCT',
          },
        });
      });
    });

    describe('editSectionTitleAndMeetingTimes', () => {
      it("creates action to edit a section's title and the time at which it meets", () => {
        const sectionToEdit = {
          sectionIndex: '123',
          newTitle: 'Transfiguration',
          newMeetingTimes: ['12:00AM'],
        };

        const editSectionTitleAndMeetingTimesAction =
          editSectionTitleAndMeetingTimes(sectionToEdit);

        expect(editSectionTitleAndMeetingTimesAction).toEqual({
          type: 'EDIT_SECTION',
          payload: {
            sectionIndex: '123',
            newTitle: 'Transfiguration',
            newMeetingTimes: ['12:00AM'],
          },
        });
      });
    });
  });

  describe('requestClassCategories', () => {
    it("creates action to request class categories, filtering those categories according to user's input", () => {
      const classCategoryFilters = {
        sectionIndex: '123',
        newTitle: 'Transfiguration',
        newMeetingTimes: ['12:00AM'],
      };

      const mockDispatch = jest.fn();
      jest.mock('react-redux', () => ({
        useDispatch: () => mockDispatch,
      }));

      requestClassCategories(classCategoryFilters)(mockDispatch);

      expect(mockDispatch).toHaveBeenCalledTimes(1);
    });
  });
});
