import {passWelcomeScreen, loadWelcomeScreen} from '../welcomeScreensActions';

describe('welcomeScreenActions', () => {
  describe('passWelcomeScreen', () => {
    it('creates action to mark user as having passed the welcome screen', () => {
      const passedWelcomeScreen = passWelcomeScreen(true);
      expect(passedWelcomeScreen).toEqual({
        type: 'PASSED_WELCOME_SCREEN',
        payload: true,
      });
    });
  });
  describe('loadWelcomeScreen', () => {
    it('creates action to show welcome screen is loaded', () => {
      const loadedWelcomeScreen = loadWelcomeScreen(true);
      expect(loadedWelcomeScreen).toEqual({
        type: 'LOADED',
        payload: true,
      });
    });
  });
});
