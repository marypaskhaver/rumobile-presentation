import {modifyBadgeCount} from '../tabBarBadgeActions';

describe('tabBarBadgeActions', () => {
  describe('modifyBadgeCount', () => {
    it('creates action to change the badge count', () => {
      const passedWelcomeScreen = modifyBadgeCount(1);
      expect(passedWelcomeScreen).toEqual({
        type: 'FAVORITED_ITEM',
        payload: 1,
      });
    });
  });
});
