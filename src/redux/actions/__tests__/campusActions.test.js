import {updateChosenCampusID} from '../campusActions';

describe('campusActions', () => {
  describe('updateChosenCampusID', () => {
    it('creates action to update the id of the campus in store to the newly-selected campus', () => {
      const receiveBusesAction = updateChosenCampusID(1);
      expect(receiveBusesAction).toEqual({
        type: 'CHANGE_CAMPUS',
        payload: 1,
      });
    });
  });
});
