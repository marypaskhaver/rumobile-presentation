import {
  reformatBusRouteNamesIfExamRoutes,
  receiveBusRoutes,
  receiveBusStops,
  addFavoriteBusStopIDWithRouteID,
  removeFavoriteBusStopIDWithRouteID,
  receiveBusPredictions,
  receiveVehicles,
  receiveUserLocation,
  sortFavoriteBuses,
} from '../busesActions';

describe('busesActions', () => {
  describe('receiveBusRoutes', () => {
    it('creates action to send bus routes to store', () => {
      const busRoutes = [
        {
          long_name: 'Route Weekend 1',
        },
        {
          long_name: 'Route Weekend 2',
        },
      ];

      const receiveBusesAction = receiveBusRoutes(busRoutes);
      expect(receiveBusesAction).toEqual({
        type: 'RECEIVE_BUS_ROUTES',
        payload: busRoutes,
      });
    });
  });

  describe('addFavoriteBusStopIDWithRouteID', () => {
    it('creates action to add favorite bus stop to store', () => {
      const busStop = [
        {
          stopID: '1',
          routeID: '2',
        },
      ];

      const addFavoriteBusStopAction = addFavoriteBusStopIDWithRouteID(busStop);

      expect(addFavoriteBusStopAction).toEqual({
        type: 'ADD_FAVORITE_BUS_STOP_ID_WITH_ROUTE_ID',
        payload: busStop,
      });
    });
  });

  describe('removeFavoriteBusStopIDWithRouteID', () => {
    it('creates action to remove favorite bus stop from store', () => {
      const busStop = [
        {
          stopID: '1',
          routeID: '2',
        },
      ];

      const removeFavoriteBusStopAction =
        removeFavoriteBusStopIDWithRouteID(busStop);

      expect(removeFavoriteBusStopAction).toEqual({
        type: 'REMOVE_FAVORITE_BUS_STOP_ID_WITH_ROUTE_ID',
        payload: busStop,
      });
    });
  });

  describe('receiveBusPredictions', () => {
    it('creates action to receive bus predictions from store', () => {
      const busPredictions = [
        {
          agency_id: '1323',
          stop_id: '4229596',
        },
        {
          agency_id: '1323',
          stop_id: '4259048',
        },
      ];

      const receiveBusPredictionsAction = receiveBusPredictions(busPredictions);

      expect(receiveBusPredictionsAction).toEqual({
        type: 'RECEIVE_BUS_PREDICTIONS',
        payload: busPredictions,
      });
    });
  });

  describe('receiveVehicles', () => {
    it('creates action to receive vehicles from store', () => {
      const vehicles = [
        {
          passenger_load: 0.08955223880597014,
          route_id: '4014494',
          vehicle_id: '4017065',
        },
        {
          arrival_estimates: [],
          tracking_status: 'up',
          vehicle_id: '4017067',
        },
      ];

      const receiveVehiclesAction = receiveVehicles(vehicles);

      expect(receiveVehiclesAction).toEqual({
        type: 'RECEIVE_VEHICLES',
        payload: vehicles,
      });
    });
  });

  describe('receiveUserLocation', () => {
    it('creates action to receive user location from store', () => {
      const userLocation = null;

      const receiveUserLocationAction = receiveUserLocation(userLocation);

      expect(receiveUserLocationAction).toEqual({
        type: 'RECEIVE_USER_LOCATION',
        payload: userLocation,
      });
    });
  });

  describe('sortFavoriteBuses', () => {
    it('creates action to sort the favorite buses', () => {
      const sortOrNot = true;
      const sortFavoriteBusesAction = sortFavoriteBuses(sortOrNot);

      expect(sortFavoriteBusesAction).toEqual({
        type: 'SORT_FAVORITE_BUSES',
        payload: sortOrNot,
      });
    });
  });

  describe('receiveBusStops', () => {
    it('creates action to send bus stops to store', () => {
      const busStops = [
        {
          name: 'Stop 1',
        },
        {
          name: 'Stop 2',
        },
      ];

      const receiveBusesAction = receiveBusStops(busStops);
      expect(receiveBusesAction).toEqual({
        type: 'RECEIVE_BUS_STOPS',
        payload: busStops,
      });
    });
  });

  describe('reformatBusRouteNamesIfExamRoutes', () => {
    it('changes the bus data so every bus that has a long_name of Exam has it changed to Exam - Route [the route letter]', () => {
      const busRoutes = [
        {
          short_name: 'A',
          long_name: 'Exam',
        },
        {
          long_name: 'Route Weekend 2',
        },
      ];

      const reformattedBusRoutes = reformatBusRouteNamesIfExamRoutes(busRoutes);
      expect(reformattedBusRoutes).toEqual([
        {
          short_name: 'A',
          long_name: 'Route A - Exam',
        },
        {
          long_name: 'Route Weekend 2',
        },
      ]);
    });
  });
});
