import {
  addMeetingTime,
  editMeetingTime,
  deleteMeetingTimesMeetingOnWeekdayAbbreviation,
  addDefaultMeetingTimeForWeekdayAbbreviation,
} from '../editSectionActions';

describe('editSectionActions', () => {
  describe('addMeetingTime', () => {
    it('creates action to add the time a class meets', () => {
      const addMeetingTimeAction = addMeetingTime('12:00PM-1:30PM');
      expect(addMeetingTimeAction).toEqual({
        type: 'ADD_MEETING_TIME',
        payload: '12:00PM-1:30PM',
      });
    });
  });

  describe('editMeetingTime', () => {
    it('creates action to edit the time a class meets', () => {
      const index = 1;
      const edit = '12:00PM-1:30PM';

      const editMeetingTimeAction = editMeetingTime(index, edit);

      expect(editMeetingTimeAction).toEqual({
        type: 'EDIT_MEETING_TIME',
        payload: {index: 1, edit: '12:00PM-1:30PM'},
      });
    });
  });

  describe('deleteMeetingTimesMeetingOnWeekdayAbbreviation', () => {
    it('creates action to delete the time a class meets given the weekday abbreviation of the class', () => {
      const deleteMeetingTimesMeetingOnWeekdayAbbreviationAction =
        deleteMeetingTimesMeetingOnWeekdayAbbreviation('M');

      expect(deleteMeetingTimesMeetingOnWeekdayAbbreviationAction).toEqual({
        type: 'DELETE_MEETING_TIMES_MEETING_ON_DAY',
        payload: 'M',
      });
    });
  });

  describe('addDefaultMeetingTimeForWeekdayAbbreviation', () => {
    it('adds a default time for the class to meet given a weekday abbreviation', () => {
      const addDefaultMeetingTimeForWeekdayAbbreviationAction =
        addDefaultMeetingTimeForWeekdayAbbreviation('T');

      expect(addDefaultMeetingTimeForWeekdayAbbreviationAction).toEqual({
        type: 'ADD_DEFAULT_MEETING_TIME',
        payload: 'T',
      });
    });
  });
});
