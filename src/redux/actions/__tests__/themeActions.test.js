import {
  updateChosenOverrideThemeID,
  updateCurrentPhoneTheme,
} from '../themeActions';

describe('themeActions', () => {
  describe('updateChosenOverrideThemeID', () => {
    it('creates action to update the chosen theme, override', () => {
      const updateChosenOverrideThemeIDAction = updateChosenOverrideThemeID(2);
      expect(updateChosenOverrideThemeIDAction).toEqual({
        type: 'UPDATE_CHOSEN_OVERRIDE_THEME_ID',
        payload: 2,
      });
    });
  });

  describe('updateCurrentPhoneTheme', () => {
    it('creates action to update the current phone theme', () => {
      const updateCurrentPhoneThemeAction = updateCurrentPhoneTheme(2);
      expect(updateCurrentPhoneThemeAction).toEqual({
        type: 'UPDATE_CURRENT_PHONE_THEME',
        payload: 2,
      });
    });
  });
});
