import {
  RECEIVE_MENU_ITEMS,
  START_MENU_ITEMS_FETCH,
  COMPLETE_MENU_ITEMS_FETCH,
  FAIL_MENU_ITEMS_FETCH,
  RECEIVE_NUTRITION_ITEMS,
  START_NUTRITION_ITEMS_FETCH,
  COMPLETE_NUTRITION_ITEMS_FETCH,
  FAIL_NUTRITION_ITEMS_FETCH,
} from '../../constants/diningHalls';

import getDiningHalls from '../api/getDiningHalls';
import getNutritionItems from '../api/getNutritionItems';

const receiveMenuItems = payload => ({
  type: RECEIVE_MENU_ITEMS,
  payload,
});

const receiveNutritionItems = nutritionItems => ({
  type: RECEIVE_NUTRITION_ITEMS,
  payload: nutritionItems,
});

export const requestMenuItems = () => {
  return dispatch => {
    dispatch({type: START_MENU_ITEMS_FETCH});

    getDiningHalls()
      .then(response => {
        const foodData = response.data;

        const currentDate = new Date();
        const formattedDate =
          currentDate.getMonth() +
          1 +
          '/' +
          currentDate.getDate() +
          '/' +
          currentDate.getFullYear();

        dispatch(receiveMenuItems(foodData[formattedDate]));
        dispatch({type: COMPLETE_MENU_ITEMS_FETCH});
      })
      .catch(error => {
        console.log(`Error fetching food data. ${JSON.stringify(error)}`);
        dispatch({type: FAIL_MENU_ITEMS_FETCH});
      });
  };
};

export const requestNutritionInfo = () => {
  return dispatch => {
    dispatch({type: START_NUTRITION_ITEMS_FETCH});

    getNutritionItems()
      .then(response => {
        const nutritionData = response.data;
        dispatch(receiveNutritionItems(nutritionData));
        dispatch({type: COMPLETE_NUTRITION_ITEMS_FETCH});
      })
      .catch(error => {
        console.log(`Error fetching nutrition data. ${JSON.stringify(error)}`);
        dispatch({type: FAIL_NUTRITION_ITEMS_FETCH});
      });
  };
};
