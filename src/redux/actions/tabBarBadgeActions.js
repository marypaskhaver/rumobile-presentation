import {FAVORITED_ITEM} from '../../constants/tabBarBadges';

export const modifyBadgeCount = badgeCount => ({
  type: FAVORITED_ITEM,
  payload: badgeCount,
});
