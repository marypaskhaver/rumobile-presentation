import {titleCase} from 'title-case';

import {
  ADD_CAMPUS,
  RECEIVE_CLASS_CATEGORIES,
  REMOVE_CAMPUS,
  ADD_LEVEL,
  REMOVE_LEVEL,
  UPDATE_SEMESTER,
  SET_LEVELS,
  SET_CAMPUSES,
  ADD_FAVORITE_SECTION,
  REMOVE_FAVORITE_SECTION,
  EDIT_SECTION,
  START_CLASS_CATEGORIES_FETCH,
  FAIL_CLASS_CATEGORIES_FETCH,
  COMPLETE_CLASS_CATEGORIES_FETCH,
} from '../../constants/academics';

import getFilteredClassCategories from '../api/getFilteredClassCategories';

export const receiveClassCategories = payload => ({
  type: RECEIVE_CLASS_CATEGORIES,
  payload,
});

export const reformatClassCategoryData = classCategories => {
  const classCategoriesGroupedByFirstLetter = classCategories.reduce(
    (classCategoriesByFirstLetter, category) => {
      const titledClassName = titleCase(category.description.toLowerCase());
      const classNameWithCode = `${titledClassName} (${category.code})`;
      const firstLetter = classNameWithCode.charAt(0);

      // Check if classCategoriesByFirstLetter[firstLetter] is a valid array. If not, initialize empty one.
      classCategoriesByFirstLetter[firstLetter] =
        classCategoriesByFirstLetter[firstLetter] || [];

      classCategoriesByFirstLetter[firstLetter].push({
        displayTitle: classNameWithCode,
        ...category,
      });

      return classCategoriesByFirstLetter;
    },
    {},
  );

  const sortedCategoriesByFirstLetter = Object.entries(
    classCategoriesGroupedByFirstLetter,
  ).sort((array1, array2) => {
    const [currentLetter] = array1;
    const [nextLetter] = array2;
    return currentLetter > nextLetter;
  });

  return sortedCategoriesByFirstLetter.map(category => {
    const [firstLetter, categoriesForLetter] = category;
    return {title: firstLetter, data: categoriesForLetter};
  });
};

export const addCampus = payload => ({
  type: ADD_CAMPUS,
  payload,
});

export const removeCampus = payload => ({
  type: REMOVE_CAMPUS,
  payload,
});

export const addLevel = payload => ({
  type: ADD_LEVEL,
  payload,
});

export const removeLevel = payload => ({
  type: REMOVE_LEVEL,
  payload,
});

export const setLevels = payload => ({
  type: SET_LEVELS,
  payload,
});

export const setCampuses = payload => ({
  type: SET_CAMPUSES,
  payload,
});

export const updateSemester = payload => ({
  type: UPDATE_SEMESTER,
  payload,
});

export const addFavoriteSection = payload => ({
  type: ADD_FAVORITE_SECTION,
  payload,
});

export const removeFavoriteSection = payload => ({
  type: REMOVE_FAVORITE_SECTION,
  payload,
});

export const editSectionTitleAndMeetingTimes = payload => ({
  type: EDIT_SECTION,
  payload,
});

// Mock out getFilteredClassCategories. Then in test, invoke requestClassCategories. -> Will return a function that takes a dispatch.
// Invoke that function and pass in a mock dispatch. Then test the mock obj has received an obj w/ a .data property.
export const requestClassCategories = classCategoryFilters => {
  return dispatch => {
    dispatch({type: START_CLASS_CATEGORIES_FETCH});

    getFilteredClassCategories(classCategoryFilters)
      .then(response => {
        dispatch(receiveClassCategories(response.data));
        dispatch({type: COMPLETE_CLASS_CATEGORIES_FETCH});
      })
      .catch(error => {
        dispatch({type: FAIL_CLASS_CATEGORIES_FETCH});
        dispatch(receiveClassCategories([]));
      });
  };
};
