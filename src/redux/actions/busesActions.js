import {
  CAMPUS_IDs_TO_AGENCY_IDs_MAP,
  RECEIVE_BUS_ROUTES,
  RECEIVE_BUS_STOPS,
  RECEIVE_BUS_PREDICTIONS,
  RECEIVE_VEHICLES,
  RECEIVE_USER_LOCATION,
  ADD_FAVORITE_BUS_STOP_ID_WITH_ROUTE_ID,
  REMOVE_FAVORITE_BUS_STOP_ID_WITH_ROUTE_ID,
  SORT_FAVORITE_BUSES,
} from '../../constants/buses';
import getBusPredictions from '../api/getBusPredictions';
import getBusRoutes from '../api/getBusRoutes';
import getBusStops from '../api/getBusStops';
import getVehicles from '../api/getVehicles';

export const receiveBusRoutes = payload => ({
  type: RECEIVE_BUS_ROUTES,
  payload,
});

export const receiveBusStops = payload => ({
  type: RECEIVE_BUS_STOPS,
  payload,
});

export const addFavoriteBusStopIDWithRouteID = payload => ({
  type: ADD_FAVORITE_BUS_STOP_ID_WITH_ROUTE_ID,
  payload,
});

export const removeFavoriteBusStopIDWithRouteID = payload => ({
  type: REMOVE_FAVORITE_BUS_STOP_ID_WITH_ROUTE_ID,
  payload,
});

export const receiveBusPredictions = payload => ({
  type: RECEIVE_BUS_PREDICTIONS,
  payload,
});

export const receiveVehicles = payload => ({
  type: RECEIVE_VEHICLES,
  payload,
});

export const receiveUserLocation = payload => ({
  type: RECEIVE_USER_LOCATION,
  payload,
});

export const sortFavoriteBuses = sortOrNot => ({
  type: SORT_FAVORITE_BUSES,
  payload: sortOrNot,
});

export const reformatBusRouteNamesIfExamRoutes = busRoutes => {
  busRoutes.forEach(busRoute => {
    if (busRoute.long_name === 'Exam') {
      busRoute.long_name = `Route ${busRoute.short_name} - Exam`;
      return busRoute;
    }
  });

  return busRoutes;
};

export const requestBusRoutes = chosenCampusID => {
  return dispatch => {
    getBusRoutes(chosenCampusID)
      .then(response => {
        const agencyID = CAMPUS_IDs_TO_AGENCY_IDs_MAP[chosenCampusID];
        const busRoutes = response.data.data[agencyID];
        const reformattedBusRoutes =
          reformatBusRouteNamesIfExamRoutes(busRoutes);
        dispatch(receiveBusRoutes(reformattedBusRoutes));
      })
      .catch(error => {
        console.log(`Error fetching bus routes data. ${JSON.stringify(error)}`);
      });
  };
};

export const requestBusStops = chosenCampusID => {
  return dispatch => {
    getBusStops(chosenCampusID)
      .then(response => {
        const busStops = response.data.data;
        dispatch(receiveBusStops(busStops));
      })
      .catch(error => {
        console.log(`Error fetching bus stops data. ${error}`);
      });
  };
};

export const requestBusPredictions = (routeIDs, stopIDs = []) => {
  return dispatch => {
    getBusPredictions(routeIDs, stopIDs)
      .then(response => {
        const busPredictions = response.data.data;
        dispatch(receiveBusPredictions(busPredictions));
      })
      .catch(error => {
        console.log(`Error fetching bus predictions data. ${error}`);
      });
  };
};

export const requestVehicles = chosenCampusID => {
  const agencyID = CAMPUS_IDs_TO_AGENCY_IDs_MAP[chosenCampusID];

  return dispatch => {
    getVehicles(chosenCampusID)
      .then(response => {
        const vehicles = response.data.data[agencyID];
        dispatch(receiveVehicles(vehicles));
      })
      .catch(error => {
        console.log(`Error fetching vehicles data. ${error}`);
      });
  };
};
