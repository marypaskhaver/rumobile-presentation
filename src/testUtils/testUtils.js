import React from 'react';
import {render} from '@testing-library/react-native';
import {Provider} from 'react-redux';

import store from '../redux/store';

const customRender = ui => {
  render(<Provider store={store}>{ui}</Provider>);
};

export * from '@testing-library/react-native';
export {customRender as render};
