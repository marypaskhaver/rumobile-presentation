import React from 'react';
import {Provider} from 'react-redux';

import store from './src/redux/store';
import loadStore from './src/utils/loadStore';
import {MainNavigatorAppScreens} from './src/shared-components/MainNavigatorAppScreens';
import {GLASSBOX_APP_ID, GLASSBOX_REPORT_SITE} from './env.json';

const {Storyboard} = require('@glassbox/react-native-storyboard');

Storyboard.start(GLASSBOX_REPORT_SITE, GLASSBOX_APP_ID);

loadStore();

function App() {
  return (
    <Provider store={store}>
      <MainNavigatorAppScreens />
    </Provider>
  );
}

export default App;
